<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$mysql = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
$s3mysql = get_db_con("simplewiki_p", "simplewiki.analytics.db.svc.wikimedia.cloud");

if ($mysql && $s3mysql) {
	
  $now = time();

  print_header("Script Status", "NOH1");

  print "<table width=\"100%\">\n";

  print "<tr><td><font size=\"-1\"><b>";
  print "<p>The current time is ".str_replace( '_', ' ', date('F j, Y, G:i e', $now)).".</p>";
  print "<p>S1 replication lag is ".convert_time(get_replag($mysql)).".</p>";
  print "<p>S3 replication lag is ".convert_time(get_simplewiki_replag($s3mysql)).".</p>";
  print "</b></font></td></tr>\n";

  mysqli_close($mysql);
  mysqli_close($s3mysql);
}
else {
  log_error(date("F j G:i", time()), "script_status.php", "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print "</table>\n\n";
$mgr = [];    // holds results of in-progress scripts
$start = [];  // holds start time of each in-progress script

if (file_exists("$HOME_DIR/status/ch_results.php")) {
  include("$HOME_DIR/status/ch_results.php");
  $mgr[] = array("title" => "DAB CHALLENGE UPDATE", "prefix" => "ch_", "start" => $ch_begin_run_wiki);
}
if (file_exists("$HOME_DIR/status/mo_results.php")) {
  include("$HOME_DIR/status/mo_results.php");
  $mgr[] = array("title" => "MONTHLY DAB CHALLENGE UPDATE", "prefix" => "mo_", "start" => $mo_begin_run_wiki);
}
if (file_exists("$HOME_DIR/status/uu_results.php")) {
  include("$HOME_DIR/status/uu_results.php");
  $mgr[] = array("title" => "UNCAT MICRO UPDATE", "prefix" => "uu_", "start" => $uu_begin_run_wiki);
}
if (file_exists("$HOME_DIR/status/ru_results.php")) {
  include("$HOME_DIR/status/ru_results.php");
  $mgr[] = array("title" => "RECENT DAB UPDATE", "prefix" => "ru_", "start" => $ru_begin_run_wiki);
}
if (file_exists("$HOME_DIR/status/dab_results.php")) {
  include("$HOME_DIR/status/dab_results.php");
  $mgr[] = array("title" => "DISAMBIG UPDATE", "prefix" => "dab_", "start" => $dab_begin_run_wiki);
}
if (file_exists("$HOME_DIR/status/i_results.php")) {
  include("$HOME_DIR/status/i_results.php");
  $mgr[] = array("title" => "ORPHANED IMAGES UPDATE", "prefix" => "i_", "start" => $i_begin_run_wiki);
}
if (file_exists("$HOME_DIR/status/uc_results.php")) {
  include("$HOME_DIR/status/uc_results.php");
  $mgr[] = array("title" => "UNCAT ARTICLES UPDATE", "prefix" => "uc_", "start" => $uc_begin_run_wiki);
}
if (file_exists("$HOME_DIR/status/n_results.php")) {
  include("$HOME_DIR/status/n_results.php");
  $mgr[] = array("title" => "NAMED DISAMBIG UPDATE", "prefix" => "n_", "start" => $n_begin_run_wiki);
}
if (file_exists("$HOME_DIR/status/mv_results.php")) {
  include("$HOME_DIR/status/mv_results.php");
  $mgr[] = array("title" => "MOVE WATCH UPDATE", "prefix" => "mv_", "start" => $mv_begin_run_wiki);
}
if (file_exists("$HOME_DIR/status/sdab_results.php")) {
  include("$HOME_DIR/status/sdab_results.php");
  $mgr[] = array("title" => "SIMPLE DISAMBIG UPDATE", "prefix" => "sdab_", "start" => $sdab_begin_run_wiki);
}

foreach ($mgr as $key => $row)
  $start[$key] = $row['start'];

array_multisort($start, SORT_DESC, $mgr);

foreach ($mgr as $row)
  printInProgress($row['title'], $row['prefix']);

$lgr = [];    // holds results of last good run of each script
$finish = []; // holds finish time of each last good run

if (file_exists("$HOME_DIR/status/ch_last_good_run.php")) {
  include("$HOME_DIR/status/ch_last_good_run.php");
  $lgr[] = array("title" => "DAB CHALLENGE UPDATE", "prefix" => "ch_", "finish" => $ch_finish_run_wiki);
}
if (file_exists("$HOME_DIR/status/mo_last_good_run.php")) {
  include("$HOME_DIR/status/mo_last_good_run.php");
  $lgr[] = array("title" => "MONTHLY DAB CHALLENGE UPDATE", "prefix" => "mo_", "finish" => $mo_finish_run_wiki);
}
if (file_exists("$HOME_DIR/status/uu_last_good_run.php")) {
  include("$HOME_DIR/status/uu_last_good_run.php");
  $lgr[] = array("title" => "UNCAT MICRO UPDATE", "prefix" => "uu_", "finish" => $uu_finish_run_wiki);
}
if (file_exists("$HOME_DIR/status/ru_last_good_run.php")) {
  include("$HOME_DIR/status/ru_last_good_run.php");
  $lgr[] = array("title" => "RECENT DAB UPDATE", "prefix" => "ru_", "finish" => $ru_finish_run_wiki);
}
if (file_exists("$HOME_DIR/status/dab_last_good_run.php")) {
  include("$HOME_DIR/status/dab_last_good_run.php");
  $lgr[] = array("title" => "DISAMBIG UPDATE", "prefix" => "dab_", "finish" => $dab_finish_run_wiki);
}
if (file_exists("$HOME_DIR/status/i_last_good_run.php")) {
  include("$HOME_DIR/status/i_last_good_run.php");
  $lgr[] = array("title" => "ORPHANED IMAGES UPDATE", "prefix" => "i_", "finish" => $i_finish_run_wiki);
}
if (file_exists("$HOME_DIR/status/uc_last_good_run.php")) {
  include("$HOME_DIR/status/uc_last_good_run.php");
  $lgr[] = array("title" => "UNCAT ARTICLES UPDATE", "prefix" => "uc_", "finish" => $uc_finish_run_wiki);
}
if (file_exists("$HOME_DIR/status/n_last_good_run.php")) {
  include("$HOME_DIR/status/n_last_good_run.php");
  $lgr[] = array("title" => "NAMED DISAMBIG UPDATE", "prefix" => "n_", "finish" => $n_finish_run_wiki);
}
if (file_exists("$HOME_DIR/status/mv_last_good_run.php")) {
  include("$HOME_DIR/status/mv_last_good_run.php");
  $lgr[] = array("title" => "MOVE WATCH UPDATE", "prefix" => "mv_", "finish" => $mv_finish_run_wiki);
}
if (file_exists("$HOME_DIR/status/sdab_last_good_run.php")) {
  include("$HOME_DIR/status/sdab_last_good_run.php");
  $lgr[] = array("title" => "SIMPLE DISAMBIG UPDATE", "prefix" => "sdab_", "finish" => $sdab_finish_run_wiki);
}

foreach ($lgr as $key => $row)
  $finish[$key] = $row['finish'];

array_multisort($finish, SORT_DESC, $lgr);

foreach ($lgr as $row)
  printLastGoodRun($row['title'], $row['prefix']);

print "<hr><br/>\n";
print "</body>\n";
print "</html>";


function printInProgress($title, $prefix = "") {
  global $HOME_DIR;
  global $now;

  include("$HOME_DIR/status/".$prefix."results.php");
  print "<H2>$title IN PROGRESS</H2>\n";
  if (isset(${$prefix."begin_run_str"})) {
    print "<b><p>Start: ".${$prefix."begin_run_str"}." (".convert_time($now - ${$prefix."begin_run"})." ago)</p></b>\n";
    array_shift(${$prefix."proc_results"});
    print "<table>\n";
    foreach (${$prefix."proc_results"} as $sql => $time) {
      print "<tr><td width=\"5%\"/><td><font size=\"-1\"><b>$sql;</b></font></td><td><font size=\"-1\">&nbsp;</font></td><td align=\"right\"><font size=\"-1\"><b>$time</b></font></td></tr>\n";
    }
    print "</table>\n";
  }
  if (isset(${$prefix."error"}))
    print "<b><p><font color=\"red\">ERROR: ".${$prefix."error"}."</font></p></b>\n";
}


function printLastGoodRun($title, $prefix = "") {
  global $HOME_DIR;
  global $now;
  
  include("$HOME_DIR/status/".$prefix."last_good_run.php");
  $last_finish = convert_time($now - ${$prefix."finish_run"});
  print "<H2>$title</H2>\n";
  print "<b><p>Start: ".${$prefix."begin_run_str"}." (".convert_time($now - ${$prefix."begin_run"})." ago)</p>\n";
  array_shift(${$prefix."proc_results"});
  print "<table>\n";
  foreach (${$prefix."proc_results"} as $sql => $time) {
    print "<tr><td width=\"5%\"/><td><font size=\"-1\"><b>$sql;</b></font></td><td><font size=\"-1\">&nbsp;</font></td><td align=\"right\"><font size=\"-1\"><b>$time</b></font></td></tr>\n";
  }
  print "</table>\n";
  print "<p>Finish: ".${$prefix."finish_run_str"}." ($last_finish ago)</p>\n";
  print "<p>Total time: ".${$prefix."total_time_str"}."</p></b>\n";
}


?>
