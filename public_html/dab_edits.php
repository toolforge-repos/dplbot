<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

$ip_only = $registered_only = false;

$userFilter = $_GET['userFilter'] ?? "";

if ($userFilter == "ip")
  $ip_only = true;
else if ($userFilter == "reg")
  $registered_only = true;

include_once("$HOME_DIR/status/ru_last_good_run.php");
include_once("$HOME_DIR/status/dab_last_good_run.php");

$page_name = "dab_edits.php";
$page_title = "New Article Dablinks Arranged by Editor";

$sql = "
             SELECT user,
                    count(*) AS count
               FROM recent_dabs
              WHERE type = 5
                AND is_fix = 0";

if ($ip_only)
  $sql .= "
                AND is_reg = 0";
else if ($registered_only)
  $sql .= "
                AND is_reg = 1";

$sql .= "
              GROUP BY user
              ORDER BY count DESC, user
        ";

$sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if ($mysql) {

  print_header($page_title);

  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>.</p>\n\n";

  print "<p>This report lists all recently created article dablinks - that is, dablinks that were created by direct article editing, such as adding \"The closest planet to the Sun is [[Mercury]].\" to an article. Article dablinks that are introduced by other methods are <b>excluded</b> from this list; for example:</p>\n\n";
  
  print "<li>a link to a redirect that has been changed to point to a disambig is excluded\n";
  print "<li>a link to a page that has been converted into a disambig (usually by adding the {{disambig}} template) is excluded\n";
  print "<li>a link to a page that has become a disambig due to a page move (e.g., moving [[X (disambiguation)]] to [[X]]) is excluded\n";
  print "<li>a link via a transcluded template that has been edited to now have dablinks is excluded\n\n";
  
  print "<p>This report focuses solely on dablinks introduced by direct article editing because there is no simple way for an editor to realize they've introduced dablinks to an article - unless they click each and every wikilink they create. This is easy to overlook. In the other methods of dablink creation listed above, the editor should be well aware that they are creating dablinks (with the exception of the template edits; they may be added to this report at a later date).</p>\n\n";

  # The User status form
  print "<p><form action=\"$page_name\" method=\"get\"><input type=\"hidden\" name=\"limit\" value=\"$limit\"/>";

  print "User status: <input type=\"radio\" name=\"userFilter\" value=\"ip\" ".($ip_only ? "checked": "")."/> Show IPs only";
  print "<input type=\"radio\" name=\"userFilter\" value=\"reg\" ".($registered_only ? "checked": "")."/> Show registered users only";
  print "<input type=\"radio\" name=\"userFilter\" value=\"all\" ".((!$ip_only && !$registered_only) ? "checked": "")."/> Show all users";

  print "&nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"Apply\"/></form></p>\n\n";
  # End User status form
  
  print "<p>This report was last generated ".convert_time(time() - $dab_begin_run)." ago. You have the ability to update the report - that is, kick off a process that removes all fixed dablinks from the report (minus replag). If you run the update, please be patient - it may take several seconds.</p>\n\n";

  # Update button
  print "<p><form action=\"data/trigger_recent_update.php\" method=\"get\">";
  
  if (file_exists("$HOME_DIR/status/ru_last_good_run.php"))
    print "This page was last updated ".convert_time(time() - $ru_begin_run)." ago.\n";
  
  print "<input type=\"hidden\" name=\"limit\" value=\"$limit\"/>";
  print "<input type=\"hidden\" name=\"userFilter\" value=\"$userFilter\"/>";
  if ((is_numeric($offset) && $offset != 0)) print "<input type=\"hidden\" name=\"offset\" value=\"$offset\"/>";
  
  print "&nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"Update\"/></form></p>\n\n";
  # End update button

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNavLocal($limit, $num, $offset, $page_name, $ip_only, $registered_only);
      printResultsLocal($mysql, $limit, $res, $num, $offset);
      printNavLocal($limit, $num, $offset, $page_name, $ip_only, $registered_only);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printNavLocal($limit, $num, $offset, $page_name, $ip_only, $registered_only) {

  if ($ip_only)
    $mo = "&userFilter=ip";
  else if ($registered_only)
    $mo = "&userFilter=reg";
  else
    $mo = "";

  if ($offset > 0) {
    $po = $offset - $limit;
    if ($po < 0) $po = 0;
    print "<p>View (<a href=\"$page_name?limit=$limit&offset=$po".$mo."\">previous ".$limit."</a>) ";
  }
  else
    print "<p>View (previous ".$limit.") ";

  if ($num == $limit + 1) {
    $no = $offset + $limit;
    print "(<a href=\"$page_name?limit=$limit&offset=$no".$mo."\">next ".$limit."</a>) ";
  }
  else
    print "(next ".$limit.") ";

  print "(<a href=\"$page_name?limit=20&offset=".$offset.$mo."\">20</a> ";
  print "| <a href=\"$page_name?limit=50&offset=".$offset.$mo."\">50</a> ";
  print "| <a href=\"$page_name?limit=100&offset=".$offset.$mo."\">100</a> ";
  print "| <a href=\"$page_name?limit=250&offset=".$offset.$mo."\">250</a> ";
  print "| <a href=\"$page_name?limit=500&offset=".$offset.$mo."\">500</a>)</p>";
}


function printResultsLocal($mysql, $limit, $res, $num, $offset) {

  if ($num == $limit + 1) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      printRowLocal($mysql, $row);
    }
    print "</ol>\n\n";
  }
  else if ($num > 0) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
      printRowLocal($mysql, $row);
    }
    print "</ol>\n\n";
  }
}

function printRowLocal($mysql, $row) {

  $user = $row['user'];
  $display_user = str_replace( '_', ' ', $user );
  $count = $row['count'];
  
  print "<li>";
  print "<a href=\"//en.wikipedia.org/wiki/User:".encodeTitle($user)."\">User:$display_user</a> ($count dablink".($count == 1 ? "" : "s").")<br/>\n";

  $escuser = mysqli_real_escape_string($mysql, $user);
  $sql = "
          SELECT article_id,
                 article_title,
                 count(*) AS article_count
            FROM recent_dabs
           WHERE user = '$escuser'
             AND type = 5
             AND is_fix = 0
           GROUP BY article_id
           ORDER BY article_count DESC, article_title
         ";
  
  $dab_res = mysqli_query($mysql, $sql);
  
  if ($dab_res) {
  
    print "<ul>\n";
    
    while ($dab_row = mysqli_fetch_assoc($dab_res)) {
      
      $article_id = $dab_row['article_id'];
      $article_title = $dab_row['article_title'];
      $disp_article_title = str_replace( '_', ' ', $article_title);
      $article_count = $dab_row['article_count'];
      
      print "<li>";
      print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($article_title)."\">$disp_article_title</a><br/>\n";
    
      $sql = "
              SELECT redirect_title
                FROM recent_dabs
               WHERE user = '$escuser'
                 AND article_id = $article_id
                 AND type = 5
                 AND is_fix = 0
               ORDER BY redirect_title
             ";
        
      $red_res = mysqli_query($mysql, $sql);
        
      if ($red_res) {
      	
      	$is_first = true;
        print "<ul>\n <li>";
        
        while ($red_row = mysqli_fetch_assoc($red_res)) {
            
          $redirect_title = $red_row['redirect_title'];
          $disp_redirect_title = str_replace( '_', ' ', $redirect_title);
          
          if (!$is_first) print ", ";
          else print "Added ";
          
          print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($redirect_title)."\">$disp_redirect_title</a>";
          
          $is_first = false;
        }
          
        print "\n</ul>\n";
      }
    }
    
    print "</ul>\n";
  }
}

?>

