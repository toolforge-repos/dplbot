<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

/* To create a new page this is all you have to change */

$page_name = "untagged_excessive_dablinks.php";
$page_title = "Articles with Excessive Dablinks - Untagged";

$sql = "
             SELECT lc_id,
                    lc_title AS title,
                    lc_amnt AS count
               FROM excessive_dab_links
              ORDER BY count DESC, title ASC
        ";

/* Finish customization section */

$sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

$tooldb = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$wikidb = get_db_con("enwiki_p", "enwiki.web.db.svc.wikimedia.cloud");

if ($tooldb && $wikidb) {

  print_header($page_title, "", "../css/main.css");

  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>. Replication lag is ".convert_time(get_replag($wikidb)).".<br/>\n\n";

  if (file_exists("$HOME_DIR/status/dab_last_good_run.php")) {
    include_once("$HOME_DIR/status/dab_last_good_run.php");
    print "<p>This page normally is updated at least daily; the last update occurred ".convert_time(time() - $dab_begin_run)." ago.</p>\n\n";
  }
  else
    print "</p>This page normally is updated at least daily.</p>\n\n";

  $res = mysqli_query($tooldb, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNav($limit, $num, $offset, $page_name);
      printResultsLocal($limit, $res, $num, $offset);
      printNav($limit, $num, $offset, $page_name);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($tooldb));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($tooldb)."</p>\n\n";
  }

  mysqli_close($tooldb);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printResultsLocal($limit, $res, $num, $offset) {
  global $wikidb, $page_name;

  if ($num == $limit + 1) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      $lc_id = (int) $row['lc_id'];
      $wres = mysqli_query($wikidb, "
        SELECT 1
          FROM templatelinks, linktarget
         WHERE lt_namespace = 10
           AND lt_title = 'Dablinks'
           AND lt_id = tl_target_id
           AND tl_from = $lc_id
      ");
      if ($wres && mysqli_num_rows($wres) == 0) {
        printRowLocal($row, $number);
        $number++;
      }
    }
    print "</table>\n\n";
  }
  else if ($num > 0) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
      $lc_id = (int) $row['lc_id'];
      $wres = mysqli_query($wikidb, "
        SELECT 1
          FROM templatelinks, linktarget
         WHERE lt_namespace = 10
           AND lt_title = 'Dablinks'
           AND lt_id = tl_target_id
           AND tl_from = $lc_id
      ");
      if ($wres && mysqli_num_rows($wres) == 0) {
        printRowLocal($row, $number);
        $number++;
      }
    }
    print "</table>\n\n";
  }
}

function printRowLocal($row, $number) {

  $title = $row['title'];
  $count = $row['count'];
  $display_title = str_replace( '_', ' ', $title );
  print "<tr><td>&nbsp;&nbsp;&nbsp;".$number.". <a href=\"//en.wikipedia.org/wiki/".encodeTitle($title)."\">$display_title</a>";
  print "        (<a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($title)."&action=edit\">edit</a>)</td>\n";
  print "    <td align=\"right\">&nbsp;&nbsp;&nbsp;&nbsp;".$count." dablinks\n";
  print "        (<a href=\"//dplbot.toolforge.org/disambigs_in_an_article.php?title=".encodeTitle($title)."\" target=\"_blank\">check</a>)</td>\n";
}

?>

