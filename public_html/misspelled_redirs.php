<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

/* To create a new page this is all you have to change */

$page_name = "misspelled_redirs.php";
$page_title = "Misspelled Disambiguation Redirects";

$sql = "
             SELECT page_title AS title
               FROM page
              WHERE page_is_redirect = 1
                AND page_namespace = 0
                AND page_title LIKE '%(dis%'
                AND page_title NOT LIKE '%_(disambiguation)'
                AND page_title NOT LIKE '%_(discography)'
                AND page_title NOT LIKE '%_(district%'
                AND page_title NOT LIKE '%_(disease)'
                AND page_title NOT LIKE '%_(discworld)'
              ORDER BY title ASC
        ";

/* Finish customization section */

$sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

$mysql = get_db_con("enwiki_p", "enwiki.web.db.svc.wikimedia.cloud");

if ($mysql) {

  print_header($page_title);

  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e')).
        "</b>. Replication lag is ".convert_time(get_replag($mysql)).".<br/>\n\n";

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    print "<code>\n\n".nl2br(str_replace(" ", "&nbsp;", $sql))."\n\n</code>\n\n";

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNav($limit, $num, $offset, $page_name);
      printResultsLocal($limit, $res, $num, $offset);
      printNav($limit, $num, $offset, $page_name);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printResultsLocal($limit, $res, $num, $offset) {
  global $page_name;

  if ($num == $limit + 1) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
    	printRowLocal($row, $number);
    	$number++;
    }
    print "</table>\n\n";
  }
  else if ($num > 0) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
    	printRowLocal($row, $number);
    	$number++;
    }
    print "</table>\n\n";
  }
}

function printRowLocal($row, $number) {
	$title = $row['title'];
	$display_title = str_replace( '_', ' ', $title );
	print "<tr><td>&nbsp;&nbsp;&nbsp;".$number++.". <a href=\"//en.wikipedia.org/wiki/".encodeTitle($title)."?redirect=no\">$display_title</a></td></tr>\n";
}

?>

