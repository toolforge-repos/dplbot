<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

if (file_exists("$HOME_DIR/status/sdab_last_good_run.php"))
  include_once("$HOME_DIR/status/sdab_last_good_run.php");

list( $limit, $offset ) = check_limits();

$page_name = "templates_with_dab_links.php";
$page_title = "Templates with disambiguation links";

  if (file_exists("$HOME_DIR/status/sdab_last_good_run.php"))
    $base_time = $sdab_begin_run_wiki;
  else
    $base_time = "99999999999999";

$sql = "
         SELECT tc_id,
                tc_title AS title,
                tc_amnt AS count
           FROM sdab_template_tc_count
          ORDER BY count DESC, title ASC
        ";

$sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if ($mysql) {

  print_header($page_title, "", "../css/main.css");
  $replag = convert_time(get_simplewiki_replag(get_db_con("simplewiki_p", "simplewiki.analytics.db.svc.wikimedia.cloud")));
  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>. Replication lag is $replag.<br/>\n\n";

  print "<p>This is a list of templates with <a href=\"//simple.wikipedia.org/wiki/Wikipedia:Disambiguation_pages_with_links\">links to disambiguation pages</a>, ordered by number of inclusions. This page offers a quick way to identify and fix large numbers of disambiguation links with a single edit. ";

  if (file_exists("$HOME_DIR/status/sdab_last_good_run.php")) {
    include_once("$HOME_DIR/status/sdab_last_good_run.php");
    print "This page normally is updated hourly; the last update occurred ".convert_time(time() - $sdab_begin_run)." ago.</p>\n\n";
  }
  else
    print "This page normally is updated hourly.</p>\n\n";

  print "<p>Once you fix a disambiguation link in a template, it will take some time (hours or even days) for the effects to propagate, as a page that transcludes a template is not updated immediately after that template is changed. If you don't want to wait you can force a template-transcluding page to refresh its links with a <a href=\"//en.wikipedia.org/wiki/Help:Dummy_edit#Null_edit\">null edit</a>.</p>";

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNav($limit, $num, $offset, $page_name);
      printResultsLocal($mysql, $limit, $res, $num, $offset, $base_time);
      printNav($limit, $num, $offset, $page_name);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printResultsLocal($mysql, $limit, $res, $num, $offset, $base_time) {

  if ($num == $limit + 1) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
    	$row = mysqli_fetch_assoc($res);
    	printRowLocal($mysql, $row, $base_time);
    }
    print "</ol>\n\n";
  }
  else if ($num > 0) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
    	printRowLocal($mysql, $row, $base_time);
    }
    print "</ol>\n\n";
  }
}

function printRowLocal($mysql, $row, $base_time) {

	$template_id = $row['tc_id'];
	$title = $row['title'];
	$count = $row['count'];
#	$page_touched = $row['page_touched'];
	$disp_title = str_replace( '_', ' ', $title );
	print "<li>";
#	if (strcmp($base_time, $page_touched) < 0) print "<s>";
	print "<a href=\"//simple.wikipedia.org/wiki/Template:".encodeTitle($title)."\">$disp_title</a>";
#	if (strcmp($base_time, $page_touched) < 0) print "</s>";
	print " ($count transclusions)<br/>\n";
	
	$sql = "
	        SELECT dab_title,
	               r_title
	          FROM s_template_dab_links
	         WHERE t_id = $template_id
	         ORDER BY dab_title, r_title
	       ";
	
	$dab_res = mysqli_query($mysql, $sql);
	
	if ($dab_res) {
	
	  print "<ul>\n";
	
	  while ($dab_row = mysqli_fetch_assoc($dab_res)) {
	
	    $dab_title = $dab_row['dab_title'];
	    $disp_dab_title = str_replace( '_', ' ', $dab_title );
	
	    $r_title = $dab_row['r_title'];
	    $disp_r_title = str_replace( '_', ' ', $r_title );
	
	    if ($dab_title != $r_title) {
	      print "<li><a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($r_title)."&redirect=no\">$disp_r_title</a> (redirect)<br/>\n";
	      print "<ul><li><a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($dab_title)."\">$disp_dab_title</a></ul>\n";
	    }
	    else
	      print "<li><a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($dab_title)."\">$disp_dab_title</a><br/>\n";
	  }
	
	  print "</ul>\n";
	}
}

?>

