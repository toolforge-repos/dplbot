<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

include_once("$HOME_DIR/status/sdab_last_good_run.php");
$base_time = $sdab_begin_run_wiki;

$page_name = "low_link_dabs.php";
$page_title = "Low Link Disambigs";

$ct = isset($_GET['ct']) ? intval($_GET['ct']) : 0;

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$wikidb = get_db_con("simplewiki_p", "simplewiki.web.db.svc.wikimedia.cloud");

if ($mysql) {

	print_header($page_title, $page_title, "../css/main.css");

	print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e')).
	"</b>. Replication lag is ".convert_time(get_simplewiki_replag($wikidb)).".</p>\n\n";

	print "<p>This is a list of low-link disambiguation pages. The page normally is updated hourly; the last update completed ".
	convert_time(time() - $sdab_finish_run)." ago. When the list is updated, any dablink that has been fixed
	will be removed from this list. If an article has been edited since the most recent update, its title
	will be <s>struck through</s>. That doesn't necessarily mean the dablink has been fixed; if it hasn't the
	strikethrough will be removed with the next update.</p>\n\n";

	$count = 1;

	# The Config form
	print "<form action=\"$page_name\" method=\"get\">\n\n";

	print "<p><input type=\"hidden\" name=\"limit\" value=\"$limit\"/>";

	print "Link count filter: ";

	while ($count <= 5) {
		print " <input type=\"radio\" name=\"ct\" value=\"".$count."\" ".($ct == $count ? "checked": "")."/> $count &nbsp;&nbsp;";
		$count++;
	}

	print "<input type=\"radio\" name=\"ct\" value=\"all\" ".($ct == 0 ? "checked": "")."/> Show all";

	print " &nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"Apply\"/></p>\n";

	print "</form>\n\n";
	# End Config form

	$sql = "
									SELECT lc_id AS dab_id,
												 lc_title AS dab_title,
												 lc_amnt AS count
										FROM sdab_link_count";

	if ($ct > 0)
		$sql .= "
									 WHERE lc_amnt = $ct";
	else
		$sql .= "
									 WHERE lc_amnt <= 5";

	$sql .= "
									 ORDER BY count DESC, dab_title
				";

	$sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

	$res = mysqli_query($mysql, $sql);

	if ($res) {

		$num = mysqli_num_rows($res);

		if ($num > 0) {
			print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
			printNavLocal($limit, $num, $offset, $page_name);
			printResultsLocal($mysql, $limit, $res, $num, $offset, $base_time);
			printNavLocal($limit, $num, $offset, $page_name);
		}
		else print "<p>There are no results in this query.</p>\n\n";

	}
	else {
		log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
		print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
	}

	mysqli_close($mysql);
}
else {
	log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
	print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printNavLocal($limit, $num, $offset, $page_name) {

	global $ct;

	if ($ct > 0)
		$mo = "&ct=".$ct;
	else
		$mo = "";

	if ($offset > 0) {
		$po = $offset - $limit;
		if ($po < 0) $po = 0;
		print "<p>View (<a href=\"$page_name?limit=$limit".$mo."&offset=$po\">previous ".$limit."</a>) ";
	}
	else
		print "<p>View (previous ".$limit.") ";

	if ($num == $limit + 1) {
		$no = $offset + $limit;
		print "(<a href=\"$page_name?limit=$limit".$mo."&offset=$no\">next ".$limit."</a>) ";
	}
	else
		print "(next ".$limit.") ";

	print "(<a href=\"$page_name?limit=20".$mo."&offset=".$offset."\">20</a> ";
	print "| <a href=\"$page_name?limit=50".$mo."&offset=".$offset."\">50</a> ";
	print "| <a href=\"$page_name?limit=100".$mo."&offset=".$offset."\">100</a> ";
	print "| <a href=\"$page_name?limit=250".$mo."&offset=".$offset."\">250</a> ";
	print "| <a href=\"$page_name?limit=500".$mo."&offset=".$offset."\">500</a>)</p>";
}


function printResultsLocal($mysql, $limit, $res, $num, $offset, $base_time) {

	if ($num == $limit + 1) {
		print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
		for ($counter = 1; $counter < $limit + 1; $counter++) {
			$row = mysqli_fetch_assoc($res);
			printRowLocal($mysql, $row, $base_time);
		}
		print "</ol>\n\n";
	}
	else if ($num > 0) {
		print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
		while ($row = mysqli_fetch_assoc($res)) {
			printRowLocal($mysql, $row, $base_time);
		}
		print "</ol>\n\n";
	}
}

function printRowLocal($mysql, $row, $base_time) {
	global $wikidb;

	$dab_id = $row['dab_id'];
	$dab_title = $row['dab_title'];
	$count = $row['count'];
	$display_dab_title = str_replace( '_', ' ', $dab_title );
	print "<li>";
	print "<a href=\"//simple.wikipedia.org/wiki/".encodeTitle($dab_title)."\">$display_dab_title</a>";
	print " (<a href=\"//simple.wikipedia.org/w/index.php?title=Special:WhatLinksHere&target=".encodeTitle($dab_title)."&namespace=0\">".number_format($count)." link".($count > 1 ? "s" : "")."</a>)<br/>\n";

	$sql = "
					SELECT article_id,
								 article_title,
								 redirect_id,
								 redirect_title,
								 template_id,
								 template_title
						FROM s_all_dab_links
					 WHERE dab_id = $dab_id
					 ORDER BY redirect_title, template_title, article_title
				 ";

	$dab_res = mysqli_query($mysql, $sql);

	if ($dab_res) {

		$prev_redirect_title = "";
		$prev_template_title = "";
		$redirect_started = false;
		$template_started = false;

		print "<ul>\n";

		# Replace with iterate array
		while ($dab_row = mysqli_fetch_assoc($dab_res)) {

			$article_id = $dab_row['article_id'];
			$article_title = $dab_row['article_title'];
			$disp_article_title = str_replace( '_', ' ', $article_title );
			$touch_sql = "SELECT page_touched FROM page WHERE page_id = $article_id";
			$touch_res = mysqli_query($wikidb, $touch_sql);
			if (!$touch_res)
				continue;
			$touch_row = mysqli_fetch_assoc($touch_res);
			$article_touched = $touch_row['page_touched'];

			$redirect_title = $dab_row['redirect_title'];
			$disp_redirect_title = str_replace( '_', ' ', $redirect_title );
			$redirect_id = $dab_row['redirect_id'];

			$template_title = $dab_row['template_title'];
			$disp_template_title = str_replace( '_', ' ', $template_title );
			$template_id = $dab_row['template_id'];

			if ($template_started && $template_title != $prev_template_title) {
				print "</ul>\n";
				$template_started = false;
			}

			if ($redirect_started && $redirect_title != $prev_redirect_title) {
				print "</ul>\n";
				$redirect_started = false;
			}

			if ($dab_title != $redirect_title && $redirect_title != $prev_redirect_title && !$redirect_started) {
				$rtitle = mysqli_real_escape_string($wikidb, $redirect_title);
				$sql = "
								SELECT page_touched AS redirect_touched
									FROM page
								 WHERE page_title = '$rtitle'
									 AND page_namespace = 0
							 ";

				$r_res = mysqli_query($wikidb, $sql);
				$r_row = mysqli_fetch_assoc($r_res);
				$redirect_touched = $r_row['redirect_touched'];

				if (strcmp($base_time, $redirect_touched) < 0) print "<s>";
				print "<li><a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($redirect_title)."&redirect=no\">$disp_redirect_title</a>";
				if (strcmp($base_time, $redirect_touched) < 0) print "</s>";
				print " (redirect)\n<ul>\n";
				$redirect_started = true;
			}

			if ($template_title != "" && $template_title != $prev_template_title && !$template_started) {

				$sql = "
								 SELECT page_touched AS template_touched
									 FROM page
									WHERE page_id = $template_id
							 ";

				$t_res = mysqli_query($wikidb, $sql);
				$t_row = mysqli_fetch_assoc($t_res);
				$template_touched = $t_row['template_touched'];

				if (strcmp($base_time, $template_touched) < 0) print "<s>";
				print "<li><a href=\"//simple.wikipedia.org/w/index.php?title=Template:".encodeTitle($template_title)."\">Template:$disp_template_title</a>";
				if (strcmp($base_time, $template_touched) < 0) print "</s>";
				print "\n<ul>\n";
				$template_started = true;
			}

			if ($template_started) {
				print "<li>(transcluded by) <a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($article_title)."\">$disp_article_title</a>\n";
			} else {
				if (strcmp($base_time, $article_touched) < 0) print "<s>";
				print "<li><a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($article_title)."\">$disp_article_title</a>\n";
				if (strcmp($base_time, $article_touched) < 0) print "</s>";
				print "\n";
			}

			$prev_redirect_title = $redirect_title;
			$prev_template_title = $template_title;
		}

		if ($template_started)  {
			print "</ul>\n";
		}

		if ($redirect_started)  {
			print "</ul>\n";
		}

		print "</ul>\n";
	}
}

?>