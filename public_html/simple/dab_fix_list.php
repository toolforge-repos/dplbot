<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

include_once("$HOME_DIR/status/sdab_last_good_run.php");
$base_time = $sdab_begin_run_wiki;

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$wikidb = get_db_con("simplewiki_p", "simplewiki.web.db.svc.wikimedia.cloud");

$dab_title = isset($_GET['title']) ? trim($_GET['title']) : "";

if ($dab_title != "") {
	$display_title = str_replace("_", " ", $dab_title);
	$dab_title = str_replace(" ", "_", $dab_title);

	$page_title = "Disambig fix list for $display_title";
	$h1_title = "Disambig fix list for <i>$display_title</i>";
}
else {
	$display_title = "";
	$dab_title = "";

	$page_title = $h1_title = "Articles linked by a disambig";
}

print_header($page_title, $h1_title, "../css/main.css");

print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>. Replication lag is ".convert_time(get_simplewiki_replag($wikidb)).".</p>\n\n";

print "<p>This is a list of links to the disambiguation page <a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($dab_title)."\">$display_title</a>.</p>\n\n";

print "This page normally is updated hourly; the last update completed ".convert_time(time() - $sdab_finish_run)." ago. When the list is updated, any dablink that has been fixed will be removed from this list. If an article title is <s>struck through</s>, that means it's been edited since the last update. That way, with a page refresh, you can see what articles you've already worked on. A struck through title hasn't necessarily been fixed yet.</p>\n\n";

# Submit button
print "<form action=\"dab_fix_list.php\" method=\"get\">\n\n<p>\n\n";

print "<input type=\"text\" name=\"title\" size=\"50\"/>\n\n";

print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"Submit\"/>\n\n";

print "</p><br/>\n\n</form>\n\n";
# End submit button

$dtitle = mysqli_real_escape_string($mysql, $dab_title);
$links = [];

$sql = "
				SELECT article_id,
							 article_title,
							 redirect_title,
							 template_id,
							 template_title,
							 dab_title
					FROM s_all_dab_links
				 WHERE dab_title = '$dtitle'
			ORDER BY template_title, article_title
			 ";

$result1 = mysqli_query($mysql, $sql);

if ($result1) {
	while ($row1 = mysqli_fetch_assoc($result1)) {
		# make sure linking article still exists
		$sql2 = "SELECT page_touched FROM page WHERE page_id = {$row1['article_id']}";
		$res2 = mysqli_query($wikidb, $sql2);
		if ($res2 && mysqli_num_rows($res2) > 0) {
			$row2 = mysqli_fetch_assoc($res2);
			$links[] = array (
					"article_title" => $row1['article_title'],
					"article_touched" => $row2['page_touched'],
					"redirect_title" => $row1['redirect_title'],
					"template_id" => $row1['template_id'],
					"template_title" => $row1['template_title'],
					"dab_title" => $row1['dab_title']
			);
		}
		mysqli_free_result($res2);
	}
	mysqli_free_result($result1);
}

if (count($links) > 0) {

	$prev_redirect_title = "";
	$prev_template_title = "";

	$redirect_started = false;
	$template_started = false;

	print "<ul>\n";

	foreach ($links as $link) {

		$article_title = $link['article_title'];
		$disp_article_title = str_replace( '_', ' ', $article_title );
		$article_touched = $link['article_touched'];

		$redirect_title = $link['redirect_title'];
		$disp_redirect_title = str_replace( '_', ' ', $redirect_title );

		$template_title = $link['template_title'];
		$disp_template_title = str_replace( '_', ' ', $template_title );
		$template_id = $link['template_id'];

		$dab_title = $link['dab_title'];

		if ($template_started && $template_title != $prev_template_title) {
			print "</ul>\n";
			$template_started = false;
		}

		if ($redirect_started && $redirect_title != $prev_redirect_title) {
			print "</ul>\n";
			$redirect_started = false;
		}

		if ($dab_title != $redirect_title && $redirect_title != $prev_redirect_title && !$redirect_started) {
			$rtitle = mysqli_real_escape_string($wikidb, $redirect_title);
			$sql = "
							 SELECT page_touched AS redirect_touched
								 FROM page
								WHERE page_title = '$rtitle'
									AND page_namespace = 0
						 ";

			$r_res = mysqli_query($wikidb, $sql);
			$r_row = mysqli_fetch_assoc($r_res);
			$redirect_touched = $r_row['redirect_touched'];

			if (strcmp($base_time, $redirect_touched) < 0) print "<s>";
			print "<li><a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($redirect_title)."&redirect=no\">$disp_redirect_title</a>";
			if (strcmp($base_time, $redirect_touched) < 0) print "</s>";
			print " (redirect)\n<ul>\n";
			$redirect_started = true;
		}

		if ($template_title != "" && $template_title != $prev_template_title && !$template_started) {

			$sql = "
							 SELECT page_touched AS template_touched
								 FROM page
								WHERE page_id = $template_id
						 ";

			$t_res = mysqli_query($wikidb, $sql);
			$t_row = mysqli_fetch_assoc($t_res);
			$template_touched = $t_row['template_touched'];

			if (strcmp($base_time, $template_touched) < 0) print "<s>";
			print "<li><a href=\"//simple.wikipedia.org/w/index.php?title=Template:".encodeTitle($template_title)."\">Template:$disp_template_title</a>";
			if (strcmp($base_time, $template_touched) < 0) print "</s>";
			print "\n<ul>\n";
			$template_started = true;
		}

		if ($template_started) {
			print "<li>(transcluded by) <a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($article_title)."\">$disp_article_title</a>\n";
		} else {
			if (strcmp($base_time, $article_touched) < 0) print "<s>";
			print "<li><a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($article_title)."\">$disp_article_title</a>\n";
			if (strcmp($base_time, $article_touched) < 0) print "</s>";
			print "\n";
		}

		$prev_redirect_title = $redirect_title;
		$prev_template_title = $template_title;
	}

	if ($template_started)  {
		print "</ul>\n";
	}

	if ($redirect_started)  {
		print "</ul>\n";
	}

	print "</ul>\n\n";
} else {
	print "<p>There are no results to display.</p>\n\n";
}

print_footer();

?>