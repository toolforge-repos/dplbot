<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$title = $_GET['title'] ?? "";
$referrer = $_SERVER['HTTP_REFERER'] ?? "";

if (trim($title) != "") {
    $display_title = str_replace("_", " ", trim($title));
    $title = str_replace(" ", "_", trim($title));

    $page_title = "Disambiguation pages linked by $display_title";
    $h1_title = "Disambiguation pages linked by <i>$display_title</i>";
}
else if ($referrer != "" && strstr($referrer, 'http://simple.wikipedia.org/wiki/')) {
    
    $parsed_title = urldecode(substr($referrer, strlen('http://simple.wikipedia.org/wiki/')));
    
    if (strpos($parsed_title, "?") !== false) {
        $parsed_title = strstr($parsed_title, '?', true);
    }
    if (strpos($parsed_title, "#") !== false) {
        $parsed_title = strstr($parsed_title, '#', true);
    }
    
    $display_title = str_replace("_", " ", trim($parsed_title));
    $title = str_replace(" ", "_", trim($parsed_title));

    $page_title = "Disambiguation pages linked by $display_title";
    $h1_title = "Disambiguation pages linked by <i>$display_title</i>";
} else {
    $display_title = "";
    $title = "";

    $page_title = $h1_title = "List of disambiguation pages linked by an article";
}

$mysql = get_db_con("simplewiki_p", "simplewiki.web.db.svc.wikimedia.cloud");

if ($mysql) {

    print_header($page_title, $h1_title, "../css/main.css");

    print "<p>The current time is ".str_replace( '_', ' ', date('F j, Y, G:i e')).". Replication lag is ".convert_time(get_simplewiki_replag($mysql)).".</p>\n\n";

    print "<p>Use this page to find out all disambiguation pages linked from a certain article.  This page only works for mainspace non-redirect pages and is case sensitive.</p>";

    print "<p>Results are based on the current Toolforge copy of Simple English Wikpedia, so if there is a significant replication lag, there will be a delay before changes in Wikipedia are reflected here. Otherwise, results are real-time.</p><br/>";

    # Submit button
    print "<form action=\"disambigs_in_an_article.php\" method=\"get\">\n\n<p>\n\n";
    print "<input type=\"text\" name=\"title\" size=\"50\"/>\n\n";
    print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"Submit\"/>\n\n";
    print "</p><br/>\n\n</form>\n\n";
    # End submit button

    if ($title != "") {

        $sql = "
                        SELECT page_id AS id
                            FROM page
                         WHERE page_namespace = 0
                             AND page_is_redirect = 0
                             AND page_title = '".str_replace("'", "\'", $title)."'
                        ";

        $res = mysqli_query($mysql, $sql);

        if ($res) {

            if (mysqli_num_rows($res) == 0)
                print "<p>There is no mainspace non-redirect article with the name \"$display_title\".  If you believe this is an error, check the <a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($title)."&redirect=no\">$display_title</a> page.</p><br/>";
            else {

                $row = mysqli_fetch_assoc($res);
                $id = $row['id'];

                $sql = "
                            SELECT lt_title AS pl_title,
                                   lt_title AS t_title,
                                          0 AS is_redirect
                              FROM pagelinks, page, linktarget
                             WHERE lt_namespace = 0
                               AND pl_from = $id
                               AND pl_target_id = lt_id
                               AND lt_title NOT LIKE '%_(disambiguation)'
                               AND lt_title NOT LIKE '%_(number)'
                               AND lt_title = page_title
                               AND page_namespace = 0
                               AND page_is_redirect = 0
                               AND EXISTS (
                                           SELECT 1
                                             FROM categorylinks
                                            WHERE page_id = cl_from
                                              AND cl_to = 'All_disambiguation_pages'
                                          )
                        UNION
                            SELECT lt_title AS pl_title,
                                   rd_title AS t_title,
                                          1 AS is_redirect
                              FROM pagelinks, linktarget, page a, redirect
                             WHERE lt_namespace = 0
                               AND pl_from = $id
                               AND pl_target_id = lt_id
                               AND lt_title NOT LIKE '%_(disambiguation)'
                               AND lt_title NOT LIKE '%_(number)'
                               AND lt_title = a.page_title
                               AND a.page_namespace = 0
                               AND a.page_is_redirect = 1
                               AND a.page_id = rd_from
                               AND rd_namespace = 0
                               AND EXISTS (
                                           SELECT 1
                                             FROM categorylinks, page b
                                            WHERE rd_title = b.page_title
                                              AND b.page_namespace = 0
                                              AND b.page_is_redirect = 0
                                              AND b.page_id = cl_from
                                              AND cl_to = 'All_disambiguation_pages'
                                          )
                     ORDER BY t_title, is_redirect";

                $redirect_res = mysqli_query($mysql, $sql);

                if ($redirect_res) {

                    $count_array = array();

                    while ($row = mysqli_fetch_assoc($redirect_res)) {
                        $count_array[] = $row['t_title'];
                    }

                    $num_unique_dabs = count(array_unique($count_array));

                    if ( $num_unique_dabs == 0 )
                        print "<p>The <a href=\"//simple.wikipedia.org/wiki/".encodeTitle($title)."\">$display_title</a> article doesn't link to any disambiguation pages.</p>\n\n";
                    else if ( $num_unique_dabs == 1 )
                        print "<p>The <a href=\"//simple.wikipedia.org/wiki/".encodeTitle($title)."\">$display_title</a> article links to one disambiguation page.</p>\n\n";
                    else
                        print "<p>The <a href=\"//simple.wikipedia.org/wiki/".encodeTitle($title)."\">$display_title</a> article links to $num_unique_dabs different disambiguation pages.</p>\n\n";

                    # Reset result set
                    mysqli_data_seek($redirect_res, 0);

                    print "<ul>\n\n";

                    while ($row = mysqli_fetch_assoc($redirect_res)) {

                        $pl_title = $row['pl_title'];
                        $t_title = $row['t_title'];
                        $is_redirect = $row['is_redirect'];

                        if ($is_redirect) {
                            print "<li><a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($pl_title)."&redirect=no\">".str_replace("_", " ", $pl_title)."</a> (redirect page)\n";
                            print "    <ul><li><a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($t_title)."\">".str_replace("_", " ", $t_title)."</a></ul>\n";
                        }
                        else
                            print "<li><a href=\"//simple.wikipedia.org/w/index.php?title=".encodeTitle($pl_title)."\">".str_replace("_", " ", $pl_title)."</a>\n";
                    }

                    print "</ul><br/>\n\n";
                }
            }
        }
    }

    mysqli_close($mysql);
}

print_footer();

?>