<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$title = $_GET['title'] ?? "";

if (trim($title) != "") {
  $display_title = str_replace("_", " ", trim($title));
  $title = str_replace(" ", "_", trim($title));

  $page_title = "Link count for $display_title";
  $h1_title = "Link count for <i>$display_title</i>";
}
else {
  $display_title = "";
  $title = "";

  $page_title = $h1_title = "Disambiguation link count";
}

$mysql = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

if ($mysql) {

  print_header($page_title, $h1_title);

  print "<p>The current time is ".str_replace( '_', ' ', date('F j, Y, G:i e')).". Replication lag is ".convert_time(get_replag($mysql)).".</p>\n\n";

  print "<p>Use this page to find out the number of mainspace article links to any disambiguation page. This includes links from other disambig pages. Links through a redirect are also counted, but the link from the redirect itself is not.  This page only works for disambiguation pages and is case sensitive.</p>";

  print "<p>Results are based on the current toolserver's copy of English Wikpedia, so if there is a significant replication lag, there will be a delay before changes in Wikipedia are reflected here. Otherwise, results are real-time.</p><br/>";

  # Submit button
  print "<form action=\"disambig_link_count.php\" method=\"get\">\n\n<p>\n\n";

  print "<input type=\"text\" name=\"title\" size=\"50\"/>\n\n";

  print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"Submit\"/>\n\n";

  print "</p><br/>\n\n</form>\n\n";
  # End submit button

  if ($title != "") {
    $ttitle = mysqli_real_escape_string($mysql, $title);

    $sql = "
            SELECT 1
              FROM page
             WHERE page_namespace = 0
               AND page_is_redirect = 0
               AND page_title = '$ttitle'
               AND EXISTS
                   (
                    SELECT 1
                      FROM page_props
                     WHERE page_id = pp_page
                       AND pp_propname = 'disambiguation'
                   )
            ";

    $res = mysqli_query($mysql, $sql);

    if ($res) {

      if (mysqli_num_rows($res) == 0)
        print "<p>There is no disambiguation page with the name \"$display_title\".  If you believe this is an error, check the <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($title)."&redirect=no\">$display_title</a> page and make sure it is a properly tagged disambiguation page. (And remember, a redirect should not also be tagged as a disambig.)</p><br/>";
      else {
        $isNamedDab = false;
        
        if (strstr($title, "(disambiguation)")) {
            $isNamedDab = true;
        }
        
        if ($isNamedDab) {
            $link_candidates = "(";
        } else {
            $link_candidates = "('".str_replace("'", "\'", $title)."'";
        }

        $sql = "
                SELECT page_title as r_title
                  FROM redirect, page
                 WHERE rd_title = '$ttitle'
                   AND rd_namespace = 0
                   AND rd_from != 0
                   AND page_id = rd_from
                   AND page_namespace = 0
                   AND page_title != ''
                   AND page_is_redirect = 1
                   AND page_title NOT LIKE '%_(disambiguation)'
                 GROUP BY r_title";

        $redirect_res = mysqli_query($mysql, $sql);
        
        $count_redirect = 0;

        if ($redirect_res) {
            $count_redirect = mysqli_num_rows($redirect_res);
            $counter = 0;
          while ($row = mysqli_fetch_assoc($redirect_res)) {
            $r_title = mysqli_real_escape_string($mysql, $row['r_title']);
            if ($isNamedDab && $counter == 0) {
                $link_candidates .= "'$r_title'";
            } else {
                $link_candidates .= ",'$r_title'";
            }
            $counter++;
          }
        }

        $link_candidates .= ")";

        $sql = "
                SELECT pl_from
                  FROM pagelinks, linktarget, page
                 WHERE lt_title IN $link_candidates
                   AND lt_namespace = 0
                   AND lt_id = pl_target_id
                   AND pl_from = page_id
                   AND page_namespace = 0
                   AND page_title NOT LIKE '%_(disambiguation)'
                 GROUP BY pl_from";

        $count_res = mysqli_query($mysql, $sql);
        
        $article_links = mysqli_num_rows($count_res) - $count_redirect;
        
        if ($article_links < 0) {
            $article_links = 0;
        }

        if ($count_res)
          if ($article_links == 1)
            print "<p>There is <a href=\"//en.wikipedia.org/w/index.php?title=Special:WhatLinksHere&target=".encodeTitle($title)."&namespace=0\">1 link</a> to <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($title)."\">$display_title</a>.</p><br/>";
          else
            print "<p>There are <a href=\"//en.wikipedia.org/w/index.php?title=Special:WhatLinksHere&target=".encodeTitle($title)."&namespace=0\">".$article_links." links</a> to <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($title)."\">$display_title</a>.</p><br/>";

      }
    }
  }

  mysqli_close($mysql);
}

print_footer();

?>

