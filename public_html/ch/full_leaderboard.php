<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$page_name = "full_leaderboard.php";
$page_title = "The Monthly DAB Challenge";

$sql = "
             SELECT user,
                    count(*) AS count
               FROM ch_results
              GROUP BY user
             HAVING count >= 10
              ORDER BY count DESC, user
        ";

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if ($mysql) {

  print_header($page_title, "", "../css/main.css");

  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>.<br/>\n\n";

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      printResultsLocal($res);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printResultsLocal($res) {

  $number = 1;
  print "\n\n<br/><table style=\"font-size:95%\">\n";
  while ($row = mysqli_fetch_assoc($res)) {
    $user = str_replace( ' ', '_', $row['user']);
    $count = $row['count'];
    $display_user = str_replace( '_', ' ', $user );
    print "<tr><td>&nbsp;&nbsp;".($number < 10 ? "&nbsp;" : "").$number++.". <a href=\"//en.wikipedia.org/wiki/User:".encodeTitle($user)."\">$display_user</a></td><td>&nbsp;&nbsp;</td><td align=\"right\"><a href=\"user_results.php?user=".encodeTitle($display_user)."\">$count fixed</a></td></tr>\n";
  }
  print "</table>\n\n";
}

?>
