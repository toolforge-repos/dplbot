<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

include_once("$HOME_DIR/status/ch_last_good_run.php");
$base_time = $ch_begin_run_wiki;

list( $limit, $offset ) = check_limits();

$page_name = "most_contest_links.php";
$page_title = "Most DAB Challenge Links";

$sql = "
             SELECT article_id,
                    article_title AS title,
                    count(*) AS count
               FROM monthly_list_full
              GROUP BY article_title
              ORDER BY count DESC, title ASC
        ";

/* Finish customization section */

$sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$wikidb = get_db_con("enwiki_p", "enwiki.web.db.svc.wikimedia.cloud");

if ($mysql && $wikidb) {

  print_header($page_title, $page_title, "../css/main.css");

  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>.
  Replication lag is ".convert_time(get_replag($wikidb)).".</p>\n\n";

  print "<p>This is a list of articles that link to this month's <a href=\"monthly_list.php\">DAB
  Challenge list</a>, by descending number of links. The list is updated hourly;
  the last update completed ".convert_time(time() - $ch_finish_run)." ago.</p>\n\n";

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNav($limit, $num, $offset, $page_name);
      printResultsLocal($limit, $res, $num, $offset, $base_time);
      printNav($limit, $num, $offset, $page_name);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
  }

  mysqli_close($mysql);
  mysqli_close($wikidb);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printResultsLocal($limit, $res, $num, $offset, $base_time) {
  global $page_name;

  if ($num == $limit + 1) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
	    $row = mysqli_fetch_assoc($res);
    	printRowLocal($row, $number, $base_time);
    	$number++;
    }
    print "</table>\n\n";
  }
  else if ($num > 0) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
    	printRowLocal($row, $number, $base_time);
    	$number++;
    }
    print "</table>\n\n";
  }
}

function printRowLocal($row, $number, $base_time) {
  global $wikidb;
  
  $page_id = (int) $row['article_id'];
	$title = $row['title'];
	$count = $row['count'];
  $touch_res = mysqli_query($wikidb, "
      SELECT page_touched
        FROM page
       WHERE page_id = $page_id
  ");
  if ($touch_res && mysqli_num_rows($touch_res) > 0)
    $page_touched = mysqli_fetch_assoc($touch_res)['page_touched'];
  else
    $page_touched = "19700101000000";
	$display_title = str_replace( '_', ' ', $title );
	print "<tr><td>&nbsp;&nbsp;&nbsp;".$number.". ";
	if (strcmp($base_time, $page_touched) < 0) print "<s>";
	print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($title)."\">$display_title</a>";
	if (strcmp($base_time, $page_touched) < 0) print "</s>";
	print "</td>\n";
	print "    <td align=\"right\">&nbsp;&nbsp;&nbsp;&nbsp;".$count." links</td>\n";
	print "</tr>\n";
}

?>
