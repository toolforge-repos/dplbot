<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$user = $_GET['user'] ?? "";

if (trim($user) != "") {
	$page_title = "DAB fixes by $user";
	$h1_title = "Disambiguation link fixes by $user";
}
else {
	$user = "";
	$page_title = $h1_title = "Disambiguation link fixes";
}

list( $limit, $offset ) = check_limits();

include_once("$HOME_DIR/status/ch_last_good_run.php");
$base_time = $ch_begin_run_wiki;

$page_name = "user_results.php";

$sql = "
       SELECT rev_id,
              article,
              edited_article,
              dab,
              type
         FROM ch_results
        WHERE user = '".str_replace("'", "\'", $user)."'
        ORDER BY rev_id DESC, article
";

$sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if ($mysql) {

	print_header($page_title, $h1_title, "/css/main.css");
	print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>.</p>\n\n";
	print "<p>This page normally is updated hourly; the last update completed ".convert_time(time() - $ch_finish_run)." ago.</p>\n\n";

	$res = mysqli_query($mysql, $sql);

	if (trim($user) == "") { # Adding this -- Dispenser April 2017
		print "<form method='get'><label>User:<input name='user'/></label><input type='submit' value='Go'/></form>";
	} else if ($res) {
		$num = mysqli_num_rows($res);
		if ($num > 0) {
			print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
			printNavLocal($limit, $num, $offset, $page_name, $user);
			printResultsLocal($limit, $res, $num, $offset);
			printNavLocal($limit, $num, $offset, $page_name, $user);
		}
		else print "<p>There are no results in this query.</p>\n\n";
	} else {
		log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
		print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
	}

	mysqli_close($mysql);
}
else {
	log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
	print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printResultsLocal($limit, $res, $num, $offset) {
	global $page_name;

	if ($num == $limit + 1) {
		$number = $offset + 1;
		print "\n\n<table style=\"font-size:95%\">\n";
		for ($counter = 1; $counter < $limit + 1; $counter++) {
			$row = mysqli_fetch_assoc($res);
			printRowLocal($row, $number);
			$number++;
		}
		print "</table>\n\n";
	}
	else if ($num > 0) {
		$number = $offset + 1;
		print "\n\n<table style=\"font-size:95%\">\n";
		while ($row = mysqli_fetch_assoc($res)) {
			printRowLocal($row, $number);
			$number++;
		}
		print "</table>\n\n";
	}
}


function printRowLocal($row, $number) {
	$rev_id = $row['rev_id'];
	$article = $row['article'];
	$edited_article = $row['edited_article'];
	$dab_title = $row['dab'];
	$type = $row['type'];
	$disp_article = str_replace( '_', ' ', $article );
	$disp_ed_article = str_replace( '_', ' ', $edited_article );
	$disp_dab_title = str_replace( '_', ' ', $dab_title );
	if ($type == "article")
	  print "<tr><td>&nbsp;&nbsp;&nbsp;".$number.". <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($article)."\">$disp_article</a> (<a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($article)."&diff=".$rev_id."\">diff</a>)</td><td>&nbsp;</td>\n";
	else if ($type == "template")
	  print "<tr><td>&nbsp;&nbsp;&nbsp;".$number.". <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($article)."\">$disp_article</a></td><td>via <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($edited_article)."\">$disp_ed_article</a> (<a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($edited_article)."&diff=".$rev_id."\">diff</a>)</td>\n";
	else
	  print "<tr><td>&nbsp;&nbsp;&nbsp;".$number.". <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($article)."\">$disp_article</a></td><td>via redirect <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($edited_article)."\">$disp_ed_article</a> (<a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($edited_article)."&diff=".$rev_id."\">diff</a>)</td>\n";
	print "    <td align=\"left\">&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"//en.wikipedia.org/wiki/".encodeTitle($dab_title)."\">$disp_dab_title</a></td></tr>\n";
}


function printNavLocal($limit, $num, $offset, $page_name, $user) {

	if ($offset > 0) {
		$po = $offset - $limit;
		if ($po < 0) $po = 0;
		print "<p>View (<a href=\"$page_name?user=$user&limit=$limit&offset=$po\">previous ".$limit."</a>) ";
	}
	else
		print "<p>View (previous ".$limit.") ";

	if ($num == $limit + 1) {
		$no = $offset + $limit;
		print "(<a href=\"$page_name?user=$user&limit=$limit&offset=$no\">next ".$limit."</a>) ";
	}
	else
		print "(next ".$limit.") ";

	print "(<a href=\"$page_name?user=$user&limit=20&offset=".$offset."\">20</a> ";
	print "| <a href=\"$page_name?user=$user&limit=50&offset=".$offset."\">50</a> ";
	print "| <a href=\"$page_name?user=$user&limit=100&offset=".$offset."\">100</a> ";
	print "| <a href=\"$page_name?user=$user&limit=250&offset=".$offset."\">250</a> ";
	print "| <a href=\"$page_name?user=$user&limit=500&offset=".$offset."\">500</a>)</p>";
}

?>
