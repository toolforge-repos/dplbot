<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

include_once("$HOME_DIR/status/ch_last_good_run.php");
$base_time = $ch_begin_run_wiki;

$page_name = "monthly_list.php";

$ct = isset($_GET['ct']) ? intval($_GET['ct']) : 0;
$use_ct = false;

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$wikidb = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

if ($mysql) {

  $sql = "
               SELECT month_no
                 FROM dab_hof
                ORDER BY year DESC, month_no DESC
                LIMIT 1
          ";

  $res = mysqli_query($mysql, $sql);

  if ($res && mysqli_num_rows($res) == 1) {

    $row = mysqli_fetch_assoc($res);
    $prev_month_no = $row['month_no'];

    if ($prev_month_no == 1)
      $month = "February";
    else if ($prev_month_no == 2)
      $month = "March";
    else if ($prev_month_no == 3)
      $month = "April";
    else if ($prev_month_no == 4)
      $month = "May";
    else if ($prev_month_no == 5)
      $month = "June";
    else if ($prev_month_no == 6)
      $month = "July";
    else if ($prev_month_no == 7)
      $month = "August";
    else if ($prev_month_no == 8)
      $month = "September";
    else if ($prev_month_no == 9)
      $month = "October";
    else if ($prev_month_no == 10)
      $month = "November";
    else if ($prev_month_no == 11)
      $month = "December";
    else if ($prev_month_no == 12)
      $month = "January";
  }
  else
    $month = date('F', time() - (24 * 60 * 60));

  print_js_header("DAB Challenge - $month", "", "../css/main.css");

  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>.
  Replication lag is ".convert_time(get_replag($wikidb)).".</p>\n\n";

  print "<p>These are the <a href=\"//dplbot.toolforge.org/disambig_links.php\">disambiguation
  pages with links</a>, as of the beginning of $month (excluding any pages that have been deleted
  or moved since then, or are no longer disambiguation pages).  You can earn points in the
  <a href=\"dab_challenge.php\">Monthly DAB Challenge</a> by fixing links to the disambigs listed below.</p>\n\n";

  print "This page normally is updated hourly; the last update completed ".convert_time(time() - $ch_finish_run)." ago.
  When the list is updated, any dablink that has been fixed will be removed from this list.
  If an article has been edited since the most recent update, its title will be <s>struck through</s>.
  That doesn't necessarily mean the dablink has been fixed; if it hasn't the strikethrough
  will be removed with the next update.</p>\n\n";

  print "Only links that count for challenge points are in the dropdown lists.
  Dablinks created since the beginning of $month will not be shown.
  If you collapse a list and open it again, it will be updated.</p>\n\n";
  
  $sql = "
             SELECT DISTINCT lc_amnt 
                        FROM monthly_list_count
                       ORDER BY lc_amnt DESC
        ";
  
  $form_res = mysqli_query($mysql, $sql);
  
  if ($form_res && mysqli_num_rows($form_res) > 1) {
        
    $use_ct = true;
    
    # The Config form
    print "<form action=\"$page_name\" method=\"get\">\n\n";
  
    print "<p><input type=\"hidden\" name=\"limit\" value=\"$limit\"/>";
    
    print "Link count filter: ";
    $lc_amnt = 5;
    print " <input type=\"radio\" name=\"ct\" value=\"".$lc_amnt."\" ".($ct == $lc_amnt ? "checked": "")."/> 5&nbsp;or&nbsp;more&nbsp;&nbsp;";
    
    while ($row = mysqli_fetch_assoc($form_res)) {
      $lc_amnt = $row['lc_amnt'];
      if ($lc_amnt < 5) {
        print " <input type=\"radio\" name=\"ct\" value=\"".$lc_amnt."\" ".($ct == $lc_amnt ? "checked": "")."/> $lc_amnt &nbsp;&nbsp;";
      }
    }
    
    print "<input type=\"radio\" name=\"ct\" value=\"all\" ".($ct == 0 ? "checked": "")."/> Show all";
  
    print " &nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"Apply\"/></p>\n";
    
    print "</form>\n\n";
    # End Config form
  }

  $sql = "
                  SELECT lc_id AS dab_id,
                         lc_title AS dab_title,
                         lc_amnt AS count
                    FROM monthly_list_count";
  
  if ($ct > 0 && $use_ct) {
    if ($ct == 5) {
      $sql .= "
                   WHERE lc_amnt >= $ct";
    }
    else {
      $sql .= "
                   WHERE lc_amnt = $ct";
    }
  }
  else {
    $sql .= "
                   WHERE lc_amnt > 0";
  }  
  $sql .= "
                   ORDER BY count DESC, dab_title
        ";
  
  $sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);
  
  $res = mysqli_query($mysql, $sql);
  
  if ($res) {
    
    $num = mysqli_num_rows($res);
    
    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNavLocal($limit, $num, $offset, $page_name);
      printResultsLocal($mysql, $limit, $res, $num, $offset, $base_time);
      printNavLocal($limit, $num, $offset, $page_name);
    }
    else print "<p>There are no results in this query.</p>\n\n";
    
  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
  }
  
  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printNavLocal($limit, $num, $offset, $page_name) {
  
  global $ct, $use_ct;

  if ($ct > 0 && $use_ct)
    $mo = "&ct=".$ct;
  else
    $mo = "";

  if ($offset > 0) {
    $po = $offset - $limit;
    if ($po < 0) $po = 0;
    print "<p>View (<a href=\"$page_name?limit=$limit".$mo."&offset=$po\">previous ".$limit."</a>) ";
  }
  else
    print "<p>View (previous ".$limit.") ";

  if ($num == $limit + 1) {
    $no = $offset + $limit;
    print "(<a href=\"$page_name?limit=$limit".$mo."&offset=$no\">next ".$limit."</a>) ";
  }
  else
    print "(next ".$limit.") ";

  print "(<a href=\"$page_name?limit=20".$mo."&offset=".$offset."\">20</a> ";
  print "| <a href=\"$page_name?limit=50".$mo."&offset=".$offset."\">50</a> ";
  print "| <a href=\"$page_name?limit=100".$mo."&offset=".$offset."\">100</a> ";
  print "| <a href=\"$page_name?limit=250".$mo."&offset=".$offset."\">250</a> ";
  print "| <a href=\"$page_name?limit=500".$mo."&offset=".$offset."\">500</a>)</p>";
}


function printResultsLocal($mysql, $limit, $res, $num, $offset, $base_time) {
  global $wikidb;
  if ($num == $limit + 1) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      printRowLocal($mysql, $row, $base_time);
    }
    print "</ol>\n\n";
  }
  else if ($num > 0) {  
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
      printRowLocal($mysql, $row, $base_time);
    }
    print "</ol>\n\n";
  }
}


function printRowLocal($mysql, $row, $base_time) {
  global $wikidb;

  $dab_id = (int) $row['dab_id'];
  $dab_title = $row['dab_title'];
  $e_dab_title = mysqli_real_escape_string($wikidb, $dab_title);
  // make sure page still exists and is still a dab
  $check = mysqli_query($wikidb, 
     "SELECT page_id 
        FROM page
       WHERE page_id = $dab_id
         AND page_namespace = 0
         AND page_title = '$e_dab_title'
         AND EXISTS (SELECT *
                       FROM page_props
                      WHERE pp_page = page_id
                        AND pp_propname = 'disambiguation'
                    )
     "
  );
  if (!$check || mysqli_num_rows($check) == 0)
    return;
  $count = (int) $row['count'];
  $display_dab_title = str_replace( '_', ' ', $dab_title );
  print "<li>";
  print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($dab_title)."\">$display_dab_title</a>";
  if ($count > 0) {
    print " (<a href=\"dablink_list.php?dabid=$dab_id\">".number_format($count)." link".($count == 1 ? "" : "s")."</a>) ";
    print "<a id=\"switch$dab_id\" href=\"javascript:expand('$dab_id');\">+</a> \n";
    print "<div id=\"listing$dab_id\" style=\"display: none;\">";
    $sql = "
            SELECT article_id,
                   article_title,
                   redirect_id,
                   redirect_title,
                   template_id,
                   template_title
              FROM monthly_list_full
             WHERE dab_id = $dab_id
             ORDER BY redirect_title, template_title, article_title
           ";
  
    $dab_res = mysqli_query($mysql, $sql);
  
    if ($dab_res) {
    
      $prev_redirect_title = "";
      $prev_template_title = "";
    
      $redirect_started = false;
      $template_started = false;
  
      print "<ul>\n";
    
      # Replace with iterate array
      while ($dab_row = mysqli_fetch_assoc($dab_res)) {
      
        $article_title = $dab_row['article_title'];
        $disp_article_title = str_replace( '_', ' ', $article_title );
        $touchresult = mysqli_query($wikidb, "
            SELECT page_touched 
              FROM page 
             WHERE page_id = {$dab_row['article_id']}"
        );
        if (! $touchresult)
          continue;
        $touchrow = mysqli_fetch_assoc($touchresult);
        if ($touchrow) {
          $article_touched = $touchrow['page_touched'];
        } else {
          $article_touched = '99999999999999';
          error_log("Failed to retrieve page_touched for id {$dab_row['article_id']}.");
        }
        $redirect_title = $dab_row['redirect_title'];
        $disp_redirect_title = str_replace( '_', ' ', $redirect_title );
        $redirect_id = $dab_row['redirect_id'];
      
        $template_title = $dab_row['template_title'];
        $disp_template_title = str_replace( '_', ' ', $template_title );
        $template_id = $dab_row['template_id'];
      
        if ($template_started && $template_title != $prev_template_title) {
          print "</ul>\n";
          $template_started = false;
        }
      
        if ($redirect_started && $redirect_title != $prev_redirect_title) {
          print "</ul>\n";
          $redirect_started = false;
        }
      
        if ($dab_title != $redirect_title && $redirect_title != $prev_redirect_title && !$redirect_started) {
          $rtitle = mysqli_real_escape_string($wikidb, $redirect_title);
          $sql = "
                 SELECT page_touched AS redirect_touched
                   FROM page
                  WHERE page_title = '$rtitle'
                    AND page_namespace = 0
                 ";
        
          $r_res = mysqli_query($wikidb, $sql);
          $r_row = mysqli_fetch_assoc($r_res);
          $redirect_touched = $r_row['redirect_touched'];
        
          if (strcmp($base_time, $redirect_touched) < 0) print "<s>";
          print "<li><a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($redirect_title)."&redirect=no\">$disp_redirect_title</a>";
          if (strcmp($base_time, $redirect_touched) < 0) print "</s>";
          print " (redirect)\n<ul>\n";
          $redirect_started = true;
        }
      
        if ($template_title != "" && $template_title != $prev_template_title && !$template_started) {
      
          $sql = "
                 SELECT page_touched AS template_touched
                   FROM page
                  WHERE page_id = $template_id
                 ";
        
          $t_res = mysqli_query($wikidb, $sql);
          $t_row = mysqli_fetch_assoc($t_res);
          $template_touched = $t_row ? $t_row['template_touched'] : '00000000000000';
        
          if (strcmp($base_time, $template_touched) < 0) print "<s>";
          print "<li><a href=\"//en.wikipedia.org/w/index.php?title=Template:".encodeTitle($template_title)."\">Template:$disp_template_title</a>";
          if (strcmp($base_time, $template_touched) < 0) print "</s>";
          print "\n<ul>\n";
          $template_started = true;
        }
      
        if ($template_started) {
          print "<li>(transcluded by) <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($article_title)."\">$disp_article_title</a>\n";
        } else {
          if (strcmp($base_time, $article_touched) < 0) print "<s>";
          print "<li><a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($article_title)."\">$disp_article_title</a>\n";
          if (strcmp($base_time, $article_touched) < 0) print "</s>";
          print "\n";
        }
      
        $prev_redirect_title = $redirect_title;
        $prev_template_title = $template_title;
      }
    
      if ($template_started)  {
        print "</ul>\n";
      }
    
      if ($redirect_started)  {
        print "</ul>\n";
      }
  
      print "</ul>\n";
    }
    print "</div>\n";
  } else {
    print " (0 links) ";
  } 
}

function header_js () {
  print "\n<script language=\"javascript\">\n";
  print "  function expand (fieldID) {\n";
  print "    dataObj = document.getElementById('listing'+fieldID);\n";
  print "    switchObj = document.getElementById('switch'+fieldID);\n";
  print "    if (dataObj.style.display != 'none') {\n";
  print "      dataObj.style.display = 'none';\n";
  print "      switchObj.innerHTML = '+';\n";
  print "      dataObj.innerHTML = '';\n";
  print "    } else {\n";
  print "      dataObj.style.display = 'block';\n";
  print "      switchObj.innerHTML = '-';\n";
  print "      url = 'test.php?dabid='+fieldID;\n";
  print "      dataObj.innerHTML = '<iframe frameborder=\"no\" width=\"100%\" height=\"0\" scrolling=\"no\" src=\"dablink_list.php?fragment=true&dabid='+fieldID+'\"  onload=\"this.height=this.contentWindow.document.body.scrollHeight\"></iframe>';\n";
  print "    }\n";
  print "  }\n";
  print "</script>\n\n";
}

?>


