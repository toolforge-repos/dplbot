<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$page_name = "dab_hall_of_fame.php";
$page_title = "Disambiguator Hall of Fame";

$sql = "
             SELECT *
               FROM dab_hof
              ORDER BY year DESC, month_no DESC
        ";

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if ($mysql) {

  print_header($page_title, "", "../css/main.css");

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      printResultsLocal($res);
    }
    else print "There are no results in this query.\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "Database error: ".mysqli_error($mysql)."</p>\n\n";
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "Database connection error: ".mysqli_connect_error()."\n\n";
}


function printResultsLocal($res) {

  print "<p>The <a href=\"dab_challenge.php\">Monthly DAB Challenge</a> began in April 2009 to find the greatest, most dedicated fixers of <a href=\"//en.wikipedia.org/wiki/Wikipedia:Disambiguation_pages_with_links\">links to disambiguation pages</a>. Listed here are the winners of each month's competition, hallowed be their usernames.</p><br/>\n\n";

  print "\n\n<table style=\"font-size:95%\" width=\"85%\" align=\"center\">\n";
  print "\n<tr align=\"left\"><th>Contest</th><th colspan=\"2\">First place</th><th colspan=\"2\">Second place</th><th colspan=\"2\">Third place</th><th colspan=\"2\">Fourth place</th><th colspan=\"2\">Bonus list champion</th></tr>\n";
  while ($row = mysqli_fetch_assoc($res)) {
    $month_name = $row['month_name'];
    $year = $row['year'];
    $user1 = $row['user1'];
    $count1 = $row['count1'];
    $user2 = $row['user2'];
    $count2 = $row['count2'];
    $user3 = $row['user3'];
    $count3 = $row['count3'];
    $user4 = $row['user4'];
    $count4 = $row['count4'];
    $bonus = $row['bonus'];
    $count5 = $row['count5'];
    print "<tr><td>$month_name $year</td>";
    print "<td><a href=\"//en.wikipedia.org/wiki/User:$user1\">$user1</a></td><td>$count1 fixed</td>";
    print "<td><a href=\"//en.wikipedia.org/wiki/User:$user2\">$user2</a></td><td>$count2 fixed</td>";
    print "<td><a href=\"//en.wikipedia.org/wiki/User:$user3\">$user3</a></td><td>$count3 fixed</td>";
    print "<td><a href=\"//en.wikipedia.org/wiki/User:$user4\">$user4</a></td><td>$count4 fixed</td>";
    if ($count5 > 0) {
      print "<td><a href=\"//en.wikipedia.org/wiki/User:$bonus\">$bonus</a></td><td>$count5 fixed</td></tr>";
    } else {
      print "<td/><td/></tr>";
    }
  }
  print "</table>\n\n";
}

?>
