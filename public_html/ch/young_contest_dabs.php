<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

/* To create a new page this is all you have to change */

include_once("$HOME_DIR/status/ch_last_good_run.php");
$base_time = $ch_begin_run_wiki;

$page_name = "young_contest_dabs.php";
$page_title = "Dablinks for the Monthly Dab Challenge Created Last Month";

$curr_month = date('n', time());
$curr_year = date('Y', time());

if ($curr_month == 1) {
  $last_month = 12;
	$year = $curr_year - 1;
} else {
	$last_month = $curr_month - 1;
	$year = $curr_year;
}

if ($last_month < 10) $last_month = "0".$last_month;

$last_month_string = $year.$last_month;

$sql = "
             SELECT a.dab_id AS dab_id,
                    a.dab_title AS dab_title,
                    count(*) AS count,
                    lc_amnt AS all_count
               FROM all_dab_links_basic a, monthly_list_count
              WHERE a.dab_id = lc_id
                AND EXISTS
                      (
                       SELECT 1
                         FROM monthly_list_full b
                        WHERE a.dab_id = b.dab_id
                          AND a.article_id = b.article_id
                      )
                AND EXISTS
                      (
                       SELECT 1
                         FROM all_dab_links c
                        WHERE a.dab_id = c.dab_id
                          AND a.article_id = c.article_id
                          AND dab_date LIKE '".$last_month_string."%'
                      )
              GROUP BY a.dab_id
              ORDER BY count DESC, dab_title
        ";

/* Finish customization section */

$sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if ($mysql) {

  print_header($page_title, $page_title, "../css/main.css");

  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>.<br/>\n\n";

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    print "<code>\n\n".nl2br(str_replace(" ", "&nbsp;", $sql))."\n\n</code>\n\n";

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNav($limit, $num, $offset, $page_name);
      printResultsLocal($limit, $res, $num, $offset);
      printNav($limit, $num, $offset, $page_name);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printResultsLocal($limit, $res, $num, $offset) {
  global $page_name;

  if ($num == $limit + 1) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
	    $row = mysqli_fetch_assoc($res);
    	printRowLocal($row, $number);
    	$number++;
    }
    print "</table>\n\n";
  }
  else if ($num > 0) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
    	printRowLocal($row, $number);
    	$number++;
    }
    print "</table>\n\n";
  }
}

function printRowLocal($row, $number) {
	$dab_id = $row['dab_id'];
	$dab_title = $row['dab_title'];
	$count = $row['count'];
	$all_count = $row['all_count'];
	$display_dab_title = str_replace( '_', ' ', $dab_title );
	print "<tr><td>&nbsp;&nbsp;&nbsp;".$number.". <a href=\"//en.wikipedia.org/wiki/".encodeTitle($dab_title)."\">$display_dab_title</a>\n";
	if ($count > 0) {
	  print " (<a href=\"dablink_list.php?dabid=$dab_id\">".number_format($count)." new link".($count == 1 ? "" : "s");
	  print " of ".number_format($all_count).")</a>";
  } else {
	  print " (0 new links) ";
  }
	print "</td></tr>\n";
}

?>
