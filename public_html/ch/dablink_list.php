<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

include_once("$HOME_DIR/status/ch_last_good_run.php");
$base_time = $ch_begin_run_wiki;

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$enwiki = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

$dab_id = isset($_GET['dabid']) ? trim($_GET['dabid']) : -1;

if (isset($_GET['fragment'])) {
	print "<html>\n";
	print "<head>\n";
	print "<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/main.css\" />\n";
	print "</head>\n\n";
  print "<body marginwidth=\"0\" marginheight=\"0\" leftmargin=\"0\" topmargin=\"0\">\n\n";
} else {
	
	$sql = "SELECT page_title AS dab_title
	          FROM page
	         WHERE page_id = $dab_id
	       ";
	
	$title_res = mysqli_query($enwiki, $sql);
	
	if ($title_res && mysqli_num_rows($title_res) > 0) {
	  $title_row = mysqli_fetch_assoc($title_res);
    $dab_title = $title_row['dab_title'];
	  $display_dab_title = str_replace( '_', ' ', $dab_title );
	  $page_title = "Dablink list for $display_dab_title";
	
    print_header($page_title, $page_title, "../css/main.css");

	  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>. Replication lag is ".convert_time(get_replag($enwiki)).".</p>\n\n";
	
	  print "<p>This is a list of links to the disambiguation page <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($dab_title)."\">$display_dab_title</a> that are part of Monthly DAB Challenge. Every link in this list counts for points in the contest. Dablinks created since the beginning of this month's contest aren't shown here.</p>\n\n";
	
	  print "This page normally is updated hourly; the last update completed ".convert_time(time() - $ch_finish_run)." ago. When the list is updated, any dablink that has been fixed will be removed from this list. If an article has been edited since the most recent update, its title will be <s>struck through</s>. That doesn't necessarily mean the dablink has been fixed; if it hasn't the strikethrough will be removed with the next update.</p>\n\n";

    print "<p><small>Return to the <a href=\"monthly_list.php\">monthly list</a>.</small></p>\n\n";
	} else {
    print_header("Dablink list", "", "../css/main.css");
	}
}

$sql = "SELECT article_id, 
               article_title,
               /* page_touched AS article_touched, */
               redirect_id,
               redirect_title,
               template_id,
               template_title,
               dab_title
          FROM monthly_list_full /*, enwiki_p.page */
         WHERE dab_id = $dab_id
        /* AND article_id = page_id */
         ORDER BY redirect_title, template_title, article_title
       ";

$dab_res = mysqli_query($mysql, $sql);

if ($dab_res) {
	
	$prev_redirect_title = "";
	$prev_template_title = "";
	
	$redirect_started = false;
	$template_started = false;

  print "<ul>\n";

  # Replace with iterate array
  while ($dab_row = mysqli_fetch_assoc($dab_res)) {

    $article_id = (int) $dab_row['article_id'];
    $article_title = $dab_row['article_title'];
    $disp_article_title = str_replace( '_', ' ', $article_title );
    
    $touch_sql = "SELECT page_touched FROM page WHERE page_id = $article_id";
    $touch_res = mysqli_query($enwiki, $touch_sql);
    if (!$touch_res)
      continue;
    $touch_row = mysqli_fetch_assoc($touch_res);
    $article_touched = $touch_row['page_touched'];

    $redirect_title = $dab_row['redirect_title'];
    $disp_redirect_title = str_replace( '_', ' ', $redirect_title );
    $redirect_id = $dab_row['redirect_id'];

    $template_title = $dab_row['template_title'];
    $disp_template_title = str_replace( '_', ' ', $template_title );
    $template_id = $dab_row['template_id'];

    $dab_title = $dab_row['dab_title'];

    if ($template_started && $template_title != $prev_template_title) {
      print "</ul>\n";
      $template_started = false;
    }

    if ($redirect_started && $redirect_title != $prev_redirect_title) {
      print "</ul>\n";
      $redirect_started = false;
    }

    if ($dab_title != $redirect_title && $redirect_title != $prev_redirect_title && !$redirect_started) {
      $rtitle = mysqli_real_escape_string($enwiki, $redirect_title);
      $r_sql = "SELECT page_touched AS redirect_touched
                  FROM page
                 WHERE page_title = '$rtitle'
                   AND page_namespace = 0
               ";

      $r_res = mysqli_query($enwiki, $r_sql);
      $r_row = mysqli_fetch_assoc($r_res);
      $redirect_touched = $r_row['redirect_touched'];

      if (strcmp($base_time, $redirect_touched) < 0) print "<s>";
      print "<li><a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($redirect_title)."&redirect=no\" target=\"_blank\">$disp_redirect_title</a>";
      if (strcmp($base_time, $redirect_touched) < 0) print "</s>";
      print " (redirect)\n<ul>\n";
      $redirect_started = true;
    }

    if ($template_title != "" && $template_title != $prev_template_title && !$template_started) {

      $t_sql = "SELECT page_touched AS template_touched
                  FROM page
                 WHERE page_id = $template_id
               ";

      $t_res = mysqli_query($enwiki, $t_sql);
      $t_row = mysqli_fetch_assoc($t_res);
      $template_touched = $t_row['template_touched'];

      if (strcmp($base_time, $template_touched) < 0) print "<s>";
      print "<li><a href=\"//en.wikipedia.org/w/index.php?title=Template:".encodeTitle($template_title)."\" target=\"_blank\">Template:$disp_template_title</a>";
      if (strcmp($base_time, $template_touched) < 0) print "</s>";
      $template_started = true;
    }

    if ($template_started) {
      print "<li>(transcluded by) <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($article_title)."\" target=\"_blank\">$disp_article_title</a>\n";
    } else {
      if (strcmp($base_time, $article_touched) < 0) print "<s>";
      print "<li><a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($article_title)."\" target=\"_blank\">$disp_article_title</a>\n";
      if (strcmp($base_time, $article_touched) < 0) print "</s>";
    }

    $prev_redirect_title = $redirect_title;
    $prev_template_title = $template_title;
  }

  if ($template_started)  {
    print "</ul>\n";
  }

  if ($redirect_started)  {
    print "</ul>\n";
  }

  print "</ul>\n\n";
}

if (isset($_GET['fragment'])) {
	print "</body>\n";
	print "</html>";
} else {
  print_footer();
}


?>

