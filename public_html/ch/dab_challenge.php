<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$page_name = "dab_challenge.php";

$prev_month_no = -1;
$prev_month = "";

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if ($mysql) {

  $sql = "
               SELECT month_no,
                      month_name
                 FROM dab_hof
                ORDER BY year DESC, month_no DESC
                LIMIT 1
          ";

  $res = mysqli_query($mysql, $sql);

  if ($res && mysqli_num_rows($res) == 1) {

    $row = mysqli_fetch_assoc($res);
    $prev_month_no = $row['month_no'];
    $prev_month = $row['month_name'];

    if ($prev_month_no == 1)
      $month = "February";
    else if ($prev_month_no == 2)
      $month = "March";
    else if ($prev_month_no == 3)
      $month = "April";
    else if ($prev_month_no == 4)
      $month = "May";
    else if ($prev_month_no == 5)
      $month = "June";
    else if ($prev_month_no == 6)
      $month = "July";
    else if ($prev_month_no == 7)
      $month = "August";
    else if ($prev_month_no == 8)
      $month = "September";
    else if ($prev_month_no == 9)
      $month = "October";
    else if ($prev_month_no == 10)
      $month = "November";
    else if ($prev_month_no == 11)
      $month = "December";
    else if ($prev_month_no == 12)
      $month = "January";
  }
  else
    $month = date('F', time() - (24 * 60 * 60));

  print_header("DAB Challenge - $month", "", "../css/main.css");

  print "<p>Welcome to the <b>Monthly DAB Challenge</b>! The purpose of this competition
is to see who can fix the most
<a href=\"//en.wikipedia.org/wiki/Wikipedia:Disambiguation_pages_with_links\">links to disambiguation pages</a>
that existed as of $month 1st.</p>\n\n";

  print "<h3>How to score points</h3>\n<p>The scoring system is straightforward - make an article no longer link to one of the disambigs in the list, and you earn one point.</p>\n\n";

  print "<p>These types of edits will earn you points:
  <ul>
  <li> Repairing disambig links within an article
  <li> Removing disambig links from an article
  <li> Changing a redirect that points to a disambig to point to something else
  <li> Repairing or removing disambig links within a template
  </ul></p>\n\n";

  print "<p>Through redirect and template edits you can rack up points with a single edit. If a template with a disambig link is transcluded in 30 articles, and fixing that template causes all 30 articles to no longer link to the disambig, you receive 30 points.  (Note though that it usually takes a couple of days for template fixes to be reflected by the articles that transclude them, so be patient when waiting to see results.)</p>\n\n";

  print "<p>These types of edits will NOT earn you points:
  <ul>
  <li> Any fix that doesn't \"stick\" (that is, if an article you fixed is re-linked to the disambig, you lose your point)
  <li> Fixing article-to-disambig links that were created <i>after</i> this month's list was generated on $month 1st
  <li> Removing the {{disambig}} template so an article is no longer a disambig
  <li> Page moves that cause an article to no longer point to a disambig
  </ul></p>\n\n";

  print "<p>There are several compelling arguments I could make to justify this policy, but the simple truth is leaving out these scenarios made the programming a lot easier.</p>\n\n";

  print "<p>A final note on scoring: it isn't going to be perfect.  Some edits will be improperly attributed, and others may not be caught by the system.  But I've tried to keep this at a minimum, and don't expect it to ruin anyone's standings.</p>\n\n";

  print "<h3>A word of caution</h3>\n\n";

  print "<p>The purpose of this competition is to have fun, acknowledge the contributions of others, and improve Wikipedia.  Anyone who does not make constructive edits (for instance, by making hasty, uninformed edits that have to be fixed later) will be banned indefinitely from the leaderboards. As insurance against this, I've created the ability to view the dab-fixing efforts of others (see below).</p>\n\n";

  $curr_day = date('j', time());
  $curr_month_no = date('n', time());

  if ($curr_day == 1 && (($curr_month_no == $prev_month_no + 1) || ($curr_month_no == 1 && $prev_month_no == 12))) {
    print "<h3>Results for $prev_month</h3>\n\n";

    print "<p>These are the final results for DAB Challenge - $prev_month.  The top three finalists have earned their place in the <a href=\"//dplbot.toolforge.org/ch/dab_hall_of_fame.php\">Disambiguator Hall of Fame</a>.  $month's contest is already underway; the new leaderboard will be posted tomorrow.</p>\n\n";
  }
  else {
    print "<h3>Current leaderboard for $month</h3>\n\n";

    print "<p>This list, updated hourly, includes anyone who has fixed at least 10 links from the disambig list this month. The second link goes to a page that lists what articles the user has fixed, what disambig was involved, and the diff of the \"fixing\" edit.</p>\n\n";
  }

  $sql = "
             SELECT user,
                    count(*) AS count
               FROM ch_results
              GROUP BY user
             HAVING count >= 10
              ORDER BY count DESC, user
          ";

  $res = mysqli_query($mysql, $sql);

  $num = mysqli_num_rows($res);

  if ($num > 0) {
    printLeaderboardResults($res, $mysql);
  }
  else print "<p>There are no results in this query.</p>\n\n";

  print "<h3>The List for $month</h3>\n\n";

  print "<p>On the first of every month, a new list of <a href=\"//dplbot.toolforge.org/disambig_links.php\">disambiguation pages with links</a> is generated.
Listed below are those disambiguation pages from the eligible list that have
5 or more incoming links. The full list of Challenge links is available at
<a href=\"monthly_list.php\">this page</a>.
The number of links is updated once daily, so you can see what disambigs are still available.
If all links to a disambig are fixed, or if it is no longer a disambig, it will not be displayed.</p>";

  $sql = "
	             SELECT lc_title AS title,
	                    lc_amnt AS count
	               FROM monthly_list_count
                      WHERE lc_amnt >= 5
                   ORDER BY count DESC, title
          ";

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      printResultsLocal($res);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printLeaderboardResults($res, $mysql) {

  $number = 1;
  print "\n<table style=\"font-size:95%\">\n";
  while ($row = mysqli_fetch_assoc($res)) {
    $user = str_replace( ' ', '_', $row['user']);
    $count = $row['count'];
    $display_user = str_replace( '_', ' ', $user );
    print "<tr><td>&nbsp;&nbsp;".($number < 10 ? "&nbsp;" : "").$number++.". <a href=\"//en.wikipedia.org/wiki/User:".encodeTitle($user)."\">$display_user</a></td><td>&nbsp;&nbsp;</td><td align=\"right\"><a href=\"user_results.php?user=".encodeTitle($display_user)."\">$count fixed</a></td></tr>\n";
  }
  print "</table>\n\n";
  
}


function printResultsLocal($res) {

  $number = 1;
  print "\n\n<table style=\"font-size:95%\">\n";
  while ($row = mysqli_fetch_assoc($res)) {
    $title = $row['title'];
    $count = $row['count'];
    $disp_title = str_replace( '_', ' ', $title );
    print "<tr><td>&nbsp;&nbsp;&nbsp;".$number++.". <a href=\"//en.wikipedia.org/wiki/".encodeTitle($title)."\">$disp_title</a></td>\n";
    print "    <td align=\"right\">&nbsp;&nbsp;<a href=\"//en.wikipedia.org/w/index.php?title=Special:WhatLinksHere&target=".encodeTitle($title)."&namespace=0\">".number_format($count)." links</a></td></tr>\n";
  }
  print "</table><br />\n\n";
}

?>
