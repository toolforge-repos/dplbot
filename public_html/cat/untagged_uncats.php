<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

if (file_exists("$HOME_DIR/status/uu_last_good_run.php"))
  include_once("$HOME_DIR/status/uu_last_good_run.php");

list( $limit, $offset ) = check_limits();

$now = time();

$page_name = "untagged_uncats.php";
$page_title = "Untagged Uncategorized Articles";

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$enwiki = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

if ($mysql && $enwiki) {

  if (file_exists("$HOME_DIR/status/uu_last_good_run.php"))
    $base_time = $uu_begin_run_wiki;
  else
    $base_time = '99999999999999';

  $sql = "
                    SELECT uc_title AS title
                      FROM u_micro_list
                     WHERE NOT EXISTS
                              (
                               SELECT 1
                                 FROM deletion_targets
                                WHERE uc_id = del_id
                              )
                       AND is_tagged = 0
                     ORDER BY title
                    ";

  $sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

  print_header($page_title, "", "../css/main.css");
  $space = "&nbsp;&nbsp;&nbsp;";

  print "<table cellspacing=\"6\" style=\"font-size:95%\">\n\n";

  print "<tr><td colspan=\"2\">The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>. Replication lag is ".convert_time(get_replag($enwiki)).".</td></tr>\n\n";

  print "<tr><td colspan=\"2\">Because tagging uncategorized articles is often performed by bots or semi-automated tools such as <a href=\"//en.wikipedia.org/wiki/Wikipedia:AWB\">AutoWikiBrowser</a>, it is very important to make sure the list of untagged uncategorized articles is up to date. Towards that purpose, this page holds up to 5000 untagged uncategorized articles that whose status can be re-checked. After an article has been edited, it will drop out of the list with a page refresh, and on the next update a new article will be brought in from <a href=\"uncategorized_articles.php\">Uncategorized articles</a> to replace it.</td></tr>\n\n";

  # Update button
  if (file_exists("$HOME_DIR/status/uu_last_good_run.php")) {
    print "<tr><td width=\"45%\">".$space."This page was last updated ".convert_time($now - $uu_begin_run)." ago.</td>\n";
    print "<td><form action=\"/data/trigger_uncat_update.php\" method=\"get\">".$space."<input type=\"submit\" value=\"Update\"/><input type=\"hidden\" name=\"limit\" value=\"$limit\"/>".((is_numeric($offset) && $offset != 0) ? "<input type=\"hidden\" name=\"offset\" value=\"$offset\"/>" : "")."</form></td></tr>\n\n";
  }
  else
    print "<tr><td colspan=\"2\"><form action=\"/data/trigger_uncat_update.php\" method=\"get\"><input type=\"submit\" value=\"Update\"/><input type=\"hidden\" name=\"limit\" value=\"$limit\"/>".((is_numeric($offset) && $offset != 0) ? "<input type=\"hidden\" name=\"offset\" value=\"$offset\"/>" : "")."</form></td></tr>\n\n";
  # End update button

  print "<tr><td colspan=\"2\">For convenience, you can also download a bot- or AWB- friendly version of this list.  To get an up-to-date list, click Regenerate, which generates a fresh list excluding any article that's been edited since the last update, and after the page has reloaded, right click \"uncategorized articles list.txt\" to download. <br/>(<i>Save Link as...</i> in Firefox, <i>Save target as...</i> in IE)</td></tr>\n\n";

  # Regenerate list button
  if (file_exists("$HOME_DIR/public_html/data/uncategorized_articles_list.txt")) {
    $diff = $now - filemtime("$HOME_DIR/public_html/data/uncategorized_articles_list.txt");
    print "<tr><td width=\"45%\">".$space."Last list generation: ".convert_time($diff > 0 ? $diff : 0)." ago.<br/>".$space."<a href=\"/data/uncategorized_articles_list.txt\">uncategorized articles list.txt</a></td>\n<td><form action=\"/data/trigger_uncat_file.php\" method=\"get\"><input type=\"submit\" value=\"Regenerate\"/><input type=\"hidden\" name=\"limit\" value=\"$limit\"/>".((is_numeric($offset) && $offset != 0) ? "<input type=\"hidden\" name=\"offset\" value=\"$offset\"/>" : "")."</form></td></tr>\n\n";
  }
  else
    print "<tr><td colspan=\"2\"><form action=\"/data/trigger_uncat_file.php\" method=\"get\"><input type=\"submit\" value=\"Regenerate list\"/><input type=\"hidden\" name=\"limit\" value=\"$limit\"/>".((is_numeric($offset) && $offset != 0) ? "<input type=\"hidden\" name=\"offset\" value=\"$offset\"/>" : "")."</form></td></tr>\n\n";

  print "</table>\n\n";
  # End regenerate button

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNavLocal($limit, $num, $offset, $page_name);
      printResults($limit, $res, $num, $offset);
      printNavLocal($limit, $num, $offset, $page_name);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printNavLocal($limit, $num, $offset, $page_name) {

  $mo = "";

  if ($offset > 0) {
    $po = $offset - $limit;
    if ($po < 0) $po = 0;
    print "<p>View (<a href=\"$page_name?limit=$limit&offset=$po\">previous ".$limit."</a>) ";
  }
  else
    print "<p>View (previous ".$limit.") ";

  if ($num == $limit + 1) {
    $no = $offset + $limit;
    print "(<a href=\"$page_name?limit=$limit&offset=$no\">next ".$limit."</a>) ";
  }
  else
    print "(next ".$limit.") ";

  print "(<a href=\"$page_name?limit=20&offset=$offset\">20</a> ";
  print "| <a href=\"$page_name?limit=50&offset=$offset\">50</a> ";
  print "| <a href=\"$page_name?limit=100&offset=$offset\">100</a> ";
  print "| <a href=\"$page_name?limit=250&offset=$offset\">250</a> ";
  print "| <a href=\"$page_name?limit=500&offset=$offset\">500</a>)</p>";
}


function printFilteredResults($limit, $res, $num, $offset) {

  global $enwiki, $base_time;

  if ($num == $limit + 1) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      $title = $row['title'];
      $disp_title = str_replace( '_', ' ', $title );
      $ttitle = mysqli_real_escape_string($enwiki, $title);
      $query = "SELECT page_title
                  FROM page
                 WHERE page_title = '$ttitle'
                   AND page_touched < '$base_time'
                   AND page_namespace = 0
                   AND page_is_redirect = 0";
      $result = mysqli_query($enwiki, $query);
      if (!$result || mysqli_num_rows($result) == 0) // not a valid page
        continue;

      print "<li><a href=\"//en.wikipedia.org/wiki/".encodeTitle($title)."\">$disp_title</a><br/>\n";
    }
    print "</ol>\n\n";
  }
  else if ($num > 0) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
      $title = $row['title'];
      $disp_title = str_replace( '_', ' ', $title );
      print "<li><a href=\"//en.wikipedia.org/wiki/".encodeTitle($title)."\">$disp_title</a><br/>\n";
    }
    print "</ol>\n\n";
  }
}

?>

