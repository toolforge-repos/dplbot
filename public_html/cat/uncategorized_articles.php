<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

if (file_exists("$HOME_DIR/status/uc_last_good_run.php"))
  include_once("$HOME_DIR/status/uc_last_good_run.php");

list( $limit, $offset ) = check_limits();

$tagged_only = $untagged_only = false;

if (isset($_GET['templateFilter'])) {
  if ($_GET['templateFilter'] == "tagged")
      $tagged_only = true;
  else if ($_GET['templateFilter'] == "untagged")
      $untagged_only = true;
}

$now = time();

$page_name = "uncategorized_articles.php";
$page_title = "Uncategorized Articles";

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$enwiki = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

if ($mysql && $enwiki) {

  if (file_exists("$HOME_DIR/status/uc_last_good_run.php"))
    $base_time = $uc_begin_run_wiki;
  else
    $base_time = '99999999999999';

  $sql = "
                  SELECT uc_title AS title,
                         is_tagged
                    FROM uncategorized_articles
                   WHERE NOT EXISTS
                            (
                             SELECT *
                               FROM deletion_targets
                              WHERE uc_id = del_id
                            )";

  if ($tagged_only)
    $sql .= "
                     AND is_tagged = 1";
  else if ($untagged_only)
    $sql .= "
                     AND is_tagged = 0";

  $sql .= "
                   ORDER BY title
                  ";

  $sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

  print_header($page_title, "", "../css/main.css");
  $space = "&nbsp;&nbsp;&nbsp;";

  print "<table cellspacing=\"6\" style=\"font-size:95%\">\n\n";

  print "<tr><td colspan=\"2\">The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>. Replication lag is ".convert_time(get_replag($enwiki)).".</td></tr>\n\n";

  print "<tr><td colspan=\"2\">This is a list of all <a href=\"//en.wikipedia.org/wiki/Wikipedia:WikiProject_Categories/uncategorized\">uncategorized articles</a>. ";

  if (file_exists("$HOME_DIR/status/uc_last_good_run.php")) {
    include_once("$HOME_DIR/status/uc_last_good_run.php");
    print "This page normally is updated at least daily; the last update occurred ".convert_time($now - $uc_begin_run)." ago. ";
  }
  else
    print "This page normally is updated at least daily. ";


  print "Any article that has been edited since the last update will drop out of the list with a page refresh, and will not be displayed again before the next update.</td></tr>\n\n";

  # The info update form.
  print "<tr><td align=\"center\" colspan=\"2\"><form action=\"$page_name\" method=\"get\"><input type=\"hidden\" name=\"limit\" value=\"$limit\"/>";

  print "{{uncategorized}} template status <input type=\"radio\" name=\"templateFilter\" value=\"tagged\" ".($tagged_only ? "checked": "")."/> tagged";
  print "<input type=\"radio\" name=\"templateFilter\" value=\"untagged\" ".($untagged_only ? "checked": "")."/> untagged";
  print "<input type=\"radio\" name=\"templateFilter\" value=\"none\" ".((!$tagged_only && !$untagged_only) ? "checked": "")."/> both";

  print $space."<input type=\"submit\" value=\"Show\"/></form></td></tr>";
  # End form

  print "<tr><td colspan=\"2\">If you are here to place {{uncategorized}} tags on pages, check out <a href=\"untagged_uncats.php\">Untagged Uncats</a> - it holds a list of up to 5000 untagged uncategorized articles that can be updated on demand.</td></tr>\n\n";

  print "</table>\n\n";

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNavLocal($limit, $num, $offset, $page_name, $tagged_only, $untagged_only);
      printFilteredResults($limit, $res, $num, $offset);
      printNavLocal($limit, $num, $offset, $page_name, $tagged_only, $untagged_only);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printFilteredResults($limit, $res, $num, $offset) {

  global $enwiki, $base_time;

  if ($num == $limit + 1) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      $title = $row['title'];
      $disp_title = str_replace( '_', ' ', $title );
      $ttitle = mysqli_real_escape_string($enwiki, $title);
      $query = "SELECT page_title
                  FROM page
                 WHERE page_title = '$ttitle'
                   AND page_touched < '$base_time'
                   AND page_namespace = 0
                   AND page_is_redirect = 0";
      $result = mysqli_query($enwiki, $query);
      if (!$result || mysqli_num_rows($result) == 0) // not a valid page
        continue;

      print "<li><a href=\"//en.wikipedia.org/wiki/".encodeTitle($title)."\">$disp_title</a><br/>\n";
    }
    print "</ol>\n\n";
  }
  else if ($num > 0) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
      $title = $row['title'];
      $disp_title = str_replace( '_', ' ', $title );
      print "<li><a href=\"//en.wikipedia.org/wiki/".encodeTitle($title)."\">$disp_title</a><br/>\n";
    }
    print "</ol>\n\n";
  }
}



function printNavLocal($limit, $num, $offset, $page_name, $tagged_only, $untagged_only) {

  if ($tagged_only)
    $mo = "&templateFilter=tagged";
  else if ($untagged_only)
    $mo = "&templateFilter=untagged";
  else
    $mo = "";

  if ($offset > 0) {
    $po = $offset - $limit;
    if ($po < 0) $po = 0;
    print "<p>View (<a href=\"$page_name?limit=$limit&offset=$po".$mo."\">previous ".$limit."</a>) ";
  }
  else
    print "<p>View (previous ".$limit.") ";

  if ($num == $limit + 1) {
    $no = $offset + $limit;
    print "(<a href=\"$page_name?limit=$limit&offset=$no".$mo."\">next ".$limit."</a>) ";
  }
  else
    print "(next ".$limit.") ";

  print "(<a href=\"$page_name?limit=20&offset=".$offset.$mo."\">20</a> ";
  print "| <a href=\"$page_name?limit=50&offset=".$offset.$mo."\">50</a> ";
  print "| <a href=\"$page_name?limit=100&offset=".$offset.$mo."\">100</a> ";
  print "| <a href=\"$page_name?limit=250&offset=".$offset.$mo."\">250</a> ";
  print "| <a href=\"$page_name?limit=500&offset=".$offset.$mo."\">500</a>)</p>";
}

?>
