<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

include_once("$HOME_DIR/status/uc_last_good_run.php");
$base_time = $uc_begin_run_wiki;

$page_name = "categorized_articles.php";
$page_title = "Categorized Articles";

$sql = "
             SELECT c_title AS title
               FROM categorized_articles
              WHERE  NOT EXISTS
                       (
                        SELECT 1
                          FROM deletion_targets
                         WHERE c_id = del_id
                       )
              ORDER BY title
        ";

$sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$enwiki = get_db_con("enwiki_p", "enwiki.web.db.svc.wikimedia.cloud");

if ($mysql && $enwiki) {

  print_header($page_title, "", "../css/main.css");

  print "<table cellspacing=\"6\" style=\"font-size:95%\">\n\n";

  print "<tr><td colspan=\"2\">The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>. Replication lag is ".convert_time(get_replag($enwiki)).".</td></tr>\n\n";

  print "<tr><td colspan=\"2\">This is a list of all articles that are tagged as uncategorized but do have at least one unhidden category. ";

  if (file_exists("$HOME_DIR/status/uc_last_good_run.php")) {
    include_once("$HOME_DIR/status/uc_last_good_run.php");
    print "This page normally is updated at least daily; the last update occurred ".convert_time(time() - $uc_begin_run)." ago. ";
  }
  else
    print "This page normally is updated at least daily. ";

  print "Any article that has been edited since the last update will not appear in this list.</td></tr>\n\n";

  print "<tr><td colspan=\"2\">For convenience, you can also download a bot- or AWB- friendly version of this list.  To get an up-to-date list, click Regenerate, which removes any article that's been edited since the last update, and after the page has reloaded, right click \"categorized articles list.txt\" to download. <br/>(<i>Save Link as...</i> in Firefox, <i>Save target as...</i> in IE)</td></tr>\n\n";

  # Regenerate list button
  if (file_exists("$HOME_DIR/public_html/data/categorized_articles_list.txt")) {
    $diff = time() - filemtime("$HOME_DIR/public_html/data/categorized_articles_list.txt");
    print "<tr><td width=\"45%\">&nbsp;Last list generation: ".convert_time($diff > 0 ? $diff : 0)." ago.<br/>&nbsp;<a href=\"/data/categorized_articles_list.txt\">categorized articles list.txt</a></td>\n<td><form action=\"/data/trigger_cat_file.php\" method=\"get\"><input type=\"submit\" value=\"Regenerate\"/><input type=\"hidden\" name=\"limit\" value=\"$limit\"/>".((is_numeric($offset) && $offset != 0) ? "<input type=\"hidden\" name=\"offset\" value=\"$offset\"/>" : "")."</form></td></tr>\n\n";
  }
  else
    print "<tr><td colspan=\"2\"><form action=\"/data/trigger_cat_file.php\" method=\"get\"><input type=\"submit\" value=\"Regenerate list\"/><input type=\"hidden\" name=\"limit\" value=\"$limit\"/>".((is_numeric($offset) && $offset != 0) ? "<input type=\"hidden\" name=\"offset\" value=\"$offset\"/>" : "")."</form></td></tr>\n\n";

  print "</table>\n\n";
  # End regenerate button

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNav($limit, $num, $offset, $page_name);
      printFilteredResults($limit, $res, $num, $offset);
      printNav($limit, $num, $offset, $page_name);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printFilteredResults($limit, $res, $num, $offset) {

  global $enwiki, $base_time;

  if ($num == $limit + 1) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      $title = $row['title'];
      $disp_title = str_replace( '_', ' ', $title );
      $ttitle = mysqli_real_escape_string($enwiki, $title);
      $query = "SELECT page_title
                  FROM page
                 WHERE page_title = '$ttitle'
                   AND page_touched < '$base_time'
                   AND page_namespace = 0
                   AND page_is_redirect = 0";
      $result = mysqli_query($enwiki, $query);
      if (!$result || mysqli_num_rows($result) == 0) // not a valid page
        continue;

      print "<li><a href=\"//en.wikipedia.org/wiki/".encodeTitle($title)."\">$disp_title</a><br/>\n";
    }
    print "</ol>\n\n";
  }
  else if ($num > 0) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
      $title = $row['title'];
      $disp_title = str_replace( '_', ' ', $title );
      print "<li><a href=\"//en.wikipedia.org/wiki/".encodeTitle($title)."\">$disp_title</a><br/>\n";
    }
    print "</ol>\n\n";
  }
}


?>
