<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$user = $_GET['user'] ?? "";

if (trim($user) != "") {
  $page_title = "Disambigs created by $user";
  $h1_title = "Disambiguation links created by $user";
}
else {
  $user = "";
  $page_title = $h1_title = "Disambiguation links created by a user";
}

$display_user = str_replace('_', ' ', $user);

list( $limit, $offset ) = check_limits();

include_once("$HOME_DIR/status/dab_last_good_run.php");
include_once("$HOME_DIR/status/ru_last_good_run.php");

if (file_exists("$HOME_DIR/status/ru_last_good_run.php"))
  $base_time = $ru_begin_run_wiki;
else
  $base_time = "99999999999999";

$page_name = "user_dabs.php";

$tooldb = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
if (! $tooldb) {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Tool database connection error: ".mysqli_connect_error()."</p>\n\n";
  print_footer();
  die();
}

$wikidb = get_db_con("enwiki_p", "enwiki.web.db.svc.wikimedia.cloud");
if (! $wikidb) {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Wiki database connection error: ".mysqli_connect_error()."</p>\n\n";
  print_footer();
  die();
}

$euser = mysqli_real_escape_string($tooldb, $user);

print_header($page_title, $h1_title);

print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>. Replication lag is ".convert_time(get_replag($wikidb)).".</p>\n\n";

print "<p>This report lists all disambiguation links recently created by $user - that is, dablinks that were created within the last month.</p>\n\n";

print "<p>This report is generated twice daily, and the entries in the report are updated from time to time. This report was last generated ".convert_time(time() - $dab_begin_run)." ago, and last updated ".convert_time(time() - $ru_begin_run)." ago.</p>\n\n";

printDabRedirect($tooldb, $user, $display_user, $base_time);
printDabConvert($tooldb, $user, $display_user, $base_time);
printDabMove($tooldb, $user, $display_user, $base_time);
printDabTemplate($tooldb, $user, $display_user, $base_time);
printDabArticle($tooldb, $user, $display_user, $base_time);
printDabInt($tooldb, $user, $display_user, $base_time);
printDabRvv($tooldb, $user, $display_user, $base_time);

mysqli_close($tooldb);
print_footer();


function printDabRedirect($tooldb, $user, $display_user, $base_time) {
  global $wikidb, $euser;

  $sql = "
          SELECT redirect_id,
                 redirect_title,
                 dab_title,
                 count(*) AS article_count
            FROM recent_dabs
           WHERE user = '$euser'
             AND type = 1
           GROUP BY redirect_title
           ORDER BY article_count DESC, redirect_title
         ";

  $dab_res = mysqli_query($tooldb, $sql);

  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {

      $redirect_title = $dab_row['redirect_title'];
      $disp_redirect_title = str_replace( '_', ' ', $redirect_title);
      $dab_title = $dab_row['dab_title'];
      $disp_dab_title = str_replace( '_', ' ', $dab_title);
      $article_count = $dab_row['article_count'];
      $redirect_id = (int) $dab_row['redirect_id'];

      $touch_res = mysqli_query($wikidb, "
          SELECT page_touched
            FROM page
           WHERE page_id = $redirect_id
      ");
      if ($touch_res && mysqli_num_rows($touch_res) > 0) {
        $touch_row = mysqli_fetch_assoc($touch_res);
        $page_touched = $touch_row['page_touched'];
      } else {
        $page_touched = "19700101000000";
      }

      print "<li style=\"margin-bottom:0.3em;\">";
      print "Targeted ";
	    if (strcmp($base_time, $page_touched) < 0) print "<s>";
      print "<a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($redirect_title)."&redirect=no\">$disp_redirect_title</a>";
	    if (strcmp($base_time, $page_touched) < 0) print "</s>";
      print " to <a href=\"//en.wikipedia.org/wiki/".encodeTitle($dab_title)."\">$disp_dab_title</a> ($article_count dablink".($article_count == 1 ? "" : "s").")<br/>\n";
    }
    print "</ul>\n";
  }
}


function printDabConvert($tooldb, $user, $display_user, $base_time) {
  global $wikidb, $euser;

  $sql = "
          SELECT dab_id,
                 dab_title,
                 count(*) AS article_count
            FROM recent_dabs
           WHERE user = '$euser'
             AND type = 2
           GROUP BY dab_title
           ORDER BY article_count DESC, dab_title
         ";

  $dab_res = mysqli_query($tooldb, $sql);

  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {

      $dab_title = $dab_row['dab_title'];
      $disp_dab_title = str_replace( '_', ' ', $dab_title);
      $article_count = $dab_row['article_count'];
      $dab_id = (int) $dab_row['dab_id'];

      $touch_res = mysqli_query($wikidb, "
          SELECT page_touched
            FROM page
           WHERE page_id = $dab_id
      ");
      if ($touch_res && mysqli_num_rows($touch_res) > 0) {
        $touch_row = mysqli_fetch_assoc($touch_res);
        $page_touched = $touch_row['page_touched'];
      } else {
        $page_touched = "19700101000000";
      }

      print "<li style=\"margin-bottom:0.3em;\">";
      print "Converted ";
	    if (strcmp($base_time, $page_touched) < 0) print "<s>";
	    print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($dab_title)."\">$disp_dab_title</a>";
	    if (strcmp($base_time, $page_touched) < 0) print "</s>";
	    print " into a disambig ($article_count dablink".($article_count == 1 ? "" : "s").")<br/>\n";
    }

    print "</ul>\n";
  }
}


function printDabMove($tooldb, $user, $display_user, $base_time) {
  global $wikidb, $euser;
  global $notified_only, $unnotified_only, $span, $sqlLastDate;

  $sql = "
          SELECT mv_from,
                 mv_to,
                 count(*) AS article_count
            FROM recent_dabs, move_log
           WHERE user = '$euser'
             AND user = mv_user
             AND mv_to = dab_title
             AND dab_date = mv_date
             AND type = 3
           GROUP BY mv_to
           ORDER BY article_count DESC, mv_to
         ";

  $dab_res = mysqli_query($tooldb, $sql);

  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {

      $mv_from = $dab_row['mv_from'];
      $disp_mv_from = str_replace( '_', ' ', $mv_from);
      $mv_to = $dab_row['mv_to'];
      $disp_mv_to = str_replace( '_', ' ', $mv_to);
      $article_count = $dab_row['article_count'];

      print "<li style=\"margin-bottom:0.3em;\">";
      print "Moved <a href=\"//en.wikipedia.org/wiki/".encodeTitle($mv_from)."\">$disp_mv_from</a> to <a href=\"//en.wikipedia.org/wiki/".encodeTitle($mv_to)."\">$disp_mv_to</a> ($article_count dablink".($article_count == 1 ? "" : "s").")<br/>\n";
    }

    print "</ul>\n";
  }
}


function printDabTemplate($tooldb, $user, $display_user, $base_time) {
  global $wikidb, $euser;
  global $notified_only, $unnotified_only, $span, $sqlLastDate;

  $sql = "
          SELECT template_id,
                 template_title,
                 count(*) AS article_count
            FROM recent_dabs
           WHERE user = '$euser'
             AND type = 4
           GROUP BY template_id
           ORDER BY article_count DESC, template_title
         ";

  $dab_res = mysqli_query($tooldb, $sql);

  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {

      $template_id = (int) $dab_row['template_id'];
      $template_title = $dab_row['template_title'];
      $disp_template_title = str_replace( '_', ' ', $template_title);

      $touch_res = mysqli_query($wikidb, "
          SELECT page_touched
            FROM page
           WHERE page_id = $template_id
      ");
      if ($touch_res && mysqli_num_rows($touch_res) > 0) {
        $touch_row = mysqli_fetch_assoc($touch_res);
        $page_touched = $touch_row['page_touched'];
      } else {
        $page_touched = "19700101000000";
      }

		  $sql = "
		          SELECT COUNT(DISTINCT article_id) AS transclusion_count
		            FROM recent_dabs
		           WHERE user = '$euser'
		             AND template_id = $template_id
		             AND type = 4";

		  $trc_res = mysqli_query($tooldb, $sql);
		  $trc_row = mysqli_fetch_assoc($trc_res);
		  $transclusion_count = $trc_row['transclusion_count'];

      print "<li>";
	    if (strcmp($base_time, $page_touched) < 0) print "<s>";
      print "<a href=\"//en.wikipedia.org/wiki/Template:".encodeTitle($template_title)."\">Template:$disp_template_title</a>";
	    if (strcmp($base_time, $page_touched) < 0) print "</s>";
      print " ($transclusion_count transclusion".($transclusion_count == 1 ? "" : "s").")<br/>\n";

      $sql = "
              SELECT redirect_title
                FROM recent_dabs
               WHERE user = '$euser'
                 AND template_id = $template_id
                 AND type = 4
               GROUP BY redirect_title
              ";

      $red_res = mysqli_query($tooldb, $sql);

      if ($red_res) {

        $is_first = true;
        print "<ul style=\"list-style-type:none;\">\n <li style=\"margin-bottom:0.3em;\">";

        while ($red_row = mysqli_fetch_assoc($red_res)) {

          $redirect_title = $red_row['redirect_title'];
          $disp_redirect_title = str_replace( '_', ' ', $redirect_title);

          if (!$is_first) print ", ";
          else print "Added ";

          print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($redirect_title)."\">$disp_redirect_title</a>";

          $is_first = false;
        }

        print "\n</ul>\n";
      }
    }

    print "</ul>\n";
  }
}


function printDabArticle($tooldb, $user, $display_user, $base_time) {
  global $wikidb, $euser;

  $sql = "
          SELECT article_id,
                 article_title,
                 count(*) AS article_count
            FROM recent_dabs
           WHERE user = '$euser'
             AND type = 5
           GROUP BY article_id
           ORDER BY article_count DESC, article_title
         ";

  $dab_res = mysqli_query($tooldb, $sql);
  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {

      $article_id = $dab_row['article_id'];
      $article_title = $dab_row['article_title'];
      $disp_article_title = str_replace( '_', ' ', $article_title);
      $article_count = $dab_row['article_count'];

      $touch_res = mysqli_query($wikidb, "
          SELECT page_touched
            FROM page
           WHERE page_id = $article_id
      ");
      if ($touch_res && mysqli_num_rows($touch_res) > 0) {
        $touch_row = mysqli_fetch_assoc($touch_res);
        $page_touched = $touch_row['page_touched'];
      } else {
        $page_touched = "19700101000000";
      }

      print "<li>";
	    if (strcmp($base_time, $page_touched) < 0) print "<s>";
      print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($article_title)."\">$disp_article_title</a>";
	    if (strcmp($base_time, $page_touched) < 0) print "</s>";
      print "<br/>\n";

      $sql = "
              SELECT redirect_title
                FROM recent_dabs
               WHERE user = '$euser'
                 AND article_id = $article_id
                 AND type = 5
               ORDER BY redirect_title
             ";

      $red_res = mysqli_query($tooldb, $sql);

      if ($red_res) {

        $is_first = true;
        print "<ul style=\"list-style-type:none;\">\n <li style=\"margin-bottom:0.3em;\">";

        while ($red_row = mysqli_fetch_assoc($red_res)) {

          $redirect_title = $red_row['redirect_title'];
          $disp_redirect_title = str_replace( '_', ' ', $redirect_title);

          if (!$is_first) print ", ";
          else print "Added ";

          print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($redirect_title)."\">$disp_redirect_title</a>";

          $is_first = false;
        }

        print "\n</ul>\n";
      }
    }

    print "</ul>\n";
  }
}


function printDabInt($tooldb, $user, $display_user, $base_time) {
  global $wikidb, $euser;
  global $notified_only, $unnotified_only, $span, $sqlLastDate;

  $sql = "
          SELECT article_id,
                 article_title,
                 count(*) AS article_count
            FROM recent_dabs
           WHERE user = '$euser'
             AND type = 6
           GROUP BY article_id
           ORDER BY article_count DESC, article_title
         ";

  $dab_res = mysqli_query($tooldb, $sql);

  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {

      $article_id = $dab_row['article_id'];
      $article_title = $dab_row['article_title'];
      $disp_article_title = str_replace( '_', ' ', $article_title);
      $article_count = $dab_row['article_count'];

      $touch_res = mysqli_query($wikidb, "
          SELECT page_touched
            FROM page
           WHERE page_id = $article_id
      ");
      if ($touch_res && mysqli_num_rows($touch_res) > 0) {
        $touch_row = mysqli_fetch_assoc($touch_res);
        $page_touched = $touch_row['page_touched'];
      } else {
        $page_touched = "19700101000000";
      }

      print "<li>";
	    if (strcmp($base_time, $page_touched) < 0) print "<s>";
      print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($article_title)."\">$disp_article_title</a>";
	    if (strcmp($base_time, $page_touched) < 0) print "</s>";
      print "<br/>\n";

      $sql = "
              SELECT redirect_title
                FROM recent_dabs
               WHERE user = '$euser'
                 AND article_id = $article_id
                 AND type = 6
               ORDER BY redirect_title
             ";

      $red_res = mysqli_query($tooldb, $sql);

      if ($red_res) {

        $is_first = true;
        print "<ul style=\"list-style-type:none;\">\n <li style=\"margin-bottom:0.3em;\">";

        while ($red_row = mysqli_fetch_assoc($red_res)) {

          $redirect_title = $red_row['redirect_title'];
          $disp_redirect_title = str_replace( '_', ' ', $redirect_title);

          if (!$is_first) print ", ";
          else print "Added ";

          print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($redirect_title)."\">$disp_redirect_title</a>";

          $is_first = false;
        }

        print "\n</ul>\n";
      }
    }

    print "</ul>\n";
  }
}


function printDabRvv($tooldb, $user, $display_user, $base_time) {
  global $wikidb, $euser;
  global $notified_only, $unnotified_only, $span, $sqlLastDate;

  $sql = "
          SELECT article_id,
                 article_title,
                 count(*) AS article_count
            FROM recent_dabs
           WHERE user = '$euser'
             AND type = 7
           GROUP BY article_id
           ORDER BY article_count DESC, article_title
         ";

  $dab_res = mysqli_query($tooldb, $sql);

  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {

      $article_id = $dab_row['article_id'];
      $article_title = $dab_row['article_title'];
      $disp_article_title = str_replace( '_', ' ', $article_title);
      $article_count = $dab_row['article_count'];

      $touch_res = mysqli_query($wikidb, "
          SELECT page_touched
            FROM page
           WHERE page_id = $article_id
      ");
      if ($touch_res && mysqli_num_rows($touch_res) > 0) {
        $touch_row = mysqli_fetch_assoc($touch_res);
        $page_touched = $touch_row['page_touched'];
      } else {
        $page_touched = "19700101000000";
      }

      print "<li>";
	    if (strcmp($base_time, $page_touched) < 0) print "<s>";
      print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($article_title)."\">$disp_article_title</a>";
	    if (strcmp($base_time, $page_touched) < 0) print "</s>";
      print "<br/>\n";

      $sql = "
              SELECT redirect_title
                FROM recent_dabs
               WHERE user = '".str_replace("'", "\'", $user)."'
                 AND article_id = $article_id
                 AND type = 7
               ORDER BY redirect_title
             ";

      $red_res = mysqli_query($tooldb, $sql);

      if ($red_res) {

        $is_first = true;
        print "<ul style=\"list-style-type:none;\">\n <li style=\"margin-bottom:0.3em;\">";

        while ($red_row = mysqli_fetch_assoc($red_res)) {

          $redirect_title = $red_row['redirect_title'];
          $disp_redirect_title = str_replace( '_', ' ', $redirect_title);

          if (!$is_first) print ", ";
          else print "Added ";

          print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($redirect_title)."\">$disp_redirect_title</a>";

          $is_first = false;
        }

        print "\n</ul>\n";
      }
    }

    print "</ul>\n";
  }
}

?>

