<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

$ip_only = $registered_only = false;

$base_time = "99999999999999";

$usr = $_GET['usr'] ?? "";

if ($usr == "ip")
  $ip_only = true;
else if ($usr == "reg")
  $registered_only = true;

$dab_redirect = $dab_convert = $dab_move = false;
$dab_template = $dab_article = $dab_int = $dab_rvv = false;
$all_types = false;
$type = array();

$type = $_GET['type'] ?? null;

if (!isset($type) || count($type) == 0)
  $dab_article = true;
else {
  if (in_array(1, $type))
    $dab_redirect = true;

  if (in_array(2, $type))
    $dab_convert = true;

  if (in_array(3, $type))
    $dab_move = true;

  if (in_array(4, $type))
    $dab_template = true;

  if (in_array(5, $type))
    $dab_article = true;

  if (in_array(6, $type))
    $dab_int = true;

  if (in_array(7, $type))
    $dab_rvv = true;

  if ($dab_redirect && $dab_convert && $dab_move && $dab_template && $dab_article && $dab_int && $dab_rvv)
    $all_types = true;
  else if (!$dab_redirect && !$dab_convert && !$dab_move && !$dab_template && !$dab_article && !$dab_int && !$dab_rvv)
    $dab_article = true;
}

$notified_only = $unnotified_only = false;

$msg = $_GET['msg'] ?? "";

if ($msg == "y")
  $notified_only = true;
else if ($msg == "n")
  $unnotified_only = true;

$span = $_GET['span'] ?? "last";

if ($span != "week" && $span != "month" && $span != "day")
  $span = "last";

include_once("$HOME_DIR/status/dab_last_good_run.php");
include_once("$HOME_DIR/status/ru_last_good_run.php");

if (file_exists("$HOME_DIR/status/ru_last_good_run.php"))
  $base_time = $ru_begin_run_wiki;

$page_name = "dab_dashboard.php";
$page_title = "Dab Dashboard";

$phpLastDate = strtotime($dab_last_begin_run_wiki_lag);
$sqlLastDate = date('Y-m-d H-i-s', $phpLastDate);

$sql = "
             SELECT user,
                    count(*) AS count
               FROM recent_dabs";

if ($span == 'last')
  $sql .= "
              WHERE dab_date > '".$sqlLastDate."'";
else
  $sql .= "
              WHERE dab_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")";

$sql .= "
                AND is_fix = 0";

if ($ip_only)
  $sql .= "
                AND is_reg = 0";
else if ($registered_only)
  $sql .= "
                AND is_reg = 1";

if (!$all_types) {
  $is_first = true;

  $sql .= "
                AND type IN (";

  if ($dab_redirect) {
    $is_first = false;
    $sql .= "1";
  }
  if ($dab_convert) {
    if (!$is_first) $sql .= ", ";
    else $is_first = false;
    $sql .= "2";
  }
  if ($dab_move) {
    if (!$is_first) $sql .= ", ";
    else $is_first = false;
    $sql .= "3";
  }
  if ($dab_template) {
    if (!$is_first) $sql .= ", ";
    else $is_first = false;
    $sql .= "4";
  }
  if ($dab_article) {
    if (!$is_first) $sql .= ", ";
    else $is_first = false;
    $sql .= "5";
  }
  if ($dab_int) {
    if (!$is_first) $sql .= ", ";
    else $is_first = false;
    $sql .= "6";
  }
  if ($dab_rvv) {
    if (!$is_first) $sql .= ", ";
    else $is_first = false;
    $sql .= "7";
  }
  $sql .= ")";
}

if ($notified_only)
  $sql .= "
                AND is_msg = 1";
else if ($unnotified_only)
  $sql .= "
                AND is_msg = 0";

$sql .= "
              GROUP BY user
              ORDER BY count DESC, user
              LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

$tooldb = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$wikidb = get_db_con("enwiki_p", "enwiki.web.db.svc.wikimedia.cloud");

if ($tooldb && $wikidb) {

  print_header($page_title);

  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>. Replication lag is ".convert_time(get_replag($wikidb)).".</p>\n\n";

  print "<p>This report lists all recently created article dablinks - that is, dablinks that were created within the last month. Each dablink has been assigned a date, and, when possible, the editor who created the dablink and a type, defined below.</p>\n\n";

  print "<p>The main purpose of this report is identify those dablinks that are created by article edit (Type 5). There is no simple way for an editor to realize they've introduced dablinks to an article, unless they click each and every wikilink they create. This is easy to overlook. In the other methods of dablink creation (the other types below), the editor should be well aware that they are creating dablinks - with the exception of the Type 4 template edits, but templates with dablinks are well under control.</p>\n\n";

  print "<p>This report is generated twice daily, and the entries in the report are updated from time to time. This report was last generated ".convert_time(time() - $dab_begin_run)." ago, and last updated ".convert_time(time() - $ru_begin_run)." ago.</p>\n\n";

  # The Config form
  print "<form action=\"$page_name\" method=\"get\">\n\n";

  print "<blockquote>\n";
  print "<input type=\"checkbox\" name=\"type[]\" value=\"1\" ".($dab_redirect ? "checked": "")."/> <b>Type 1</b> - A redirect has been changed to point to a disambig\n";
  print "<br/><input type=\"checkbox\" name=\"type[]\" value=\"2\" ".($dab_convert ? "checked": "")."/> <b>Type 2</b> - A page has been converted into a disambig (usually by adding the {{disambig}} template)\n";
  print "<br/><input type=\"checkbox\" name=\"type[]\" value=\"3\" ".($dab_move ? "checked": "")."/> <b>Type 3</b> - A page has become a disambig due to a page move (e.g., moving [[X (disambiguation)]] to [[X]])\n";
  print "<br/><input type=\"checkbox\" name=\"type[]\" value=\"4\" ".($dab_template ? "checked": "")."/> <b>Type 4</b> - A template edit has introduced dablinks\n";
  print "<br/><input type=\"checkbox\" name=\"type[]\" value=\"5\" ".($dab_article ? "checked": "")."/> <b>Type 5</b> - An article edit has introduced dablinks\n";
  print "<br/><input type=\"checkbox\" name=\"type[]\" value=\"6\" ".($dab_int ? "checked": "")."/> <b>Type 6</b> - Probable INTDABLINK violation (hatnote, See also, disambiguation page edit)\n";
  print "<br/><input type=\"checkbox\" name=\"type[]\" value=\"7\" ".($dab_rvv ? "checked": "")."/> <b>Type 7</b> - A revert has re-introduced dablinks\n";
  print "</blockquote>\n\n";

  print "<p><center><input type=\"hidden\" name=\"limit\" value=\"$limit\"/>";

  print "<input type=\"radio\" name=\"usr\" value=\"ip\" ".($ip_only ? "checked": "")."/> Show IPs only";
  print "<input type=\"radio\" name=\"usr\" value=\"reg\" ".($registered_only ? "checked": "")."/> Show registered users only";
  print "<input type=\"radio\" name=\"usr\" value=\"all\" ".((!$ip_only && !$registered_only) ? "checked": "")."/> Show all users";

  print "<br/><br/><input type=\"radio\" name=\"msg\" value=\"y\" ".($notified_only ? "checked": "")."/> Show notified users only";
  print "<input type=\"radio\" name=\"msg\" value=\"n\" ".($unnotified_only ? "checked": "")."/> Show unnotified users only";
  print "<input type=\"radio\" name=\"msg\" value=\"all\" ".((!$notified_only && !$unnotified_only) ? "checked": "")."/> Show all users";

  print "<br/><br/><input type=\"radio\" name=\"span\" value=\"last\" ".($span == "last" ? "checked": "")."/> Found by latest scan only";
  print "<input type=\"radio\" name=\"span\" value=\"day\" ".($span == "day" ? "checked": "")."/> Last 24 hours";
  print "<input type=\"radio\" name=\"span\" value=\"week\" ".($span == "week" ? "checked": "")."/> Last week";
  print "<input type=\"radio\" name=\"span\" value=\"month\" ".($span == "month" ? "checked": "")."/> Last month";

  print "<br/><br/><input type=\"submit\" value=\"Apply\"/></center></p>\n";

  print "</form>\n\n";
  # End Config form

  $res = mysqli_query($tooldb, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNavLocal($limit, $num, $offset, $page_name);
      printResultsLocal($tooldb, $limit, $res, $num, $offset);
      printNavLocal($limit, $num, $offset, $page_name);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($tooldb));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($tooldb)."</p>\n\n";
  }

  mysqli_close($tooldb);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printNavLocal($limit, $num, $offset, $page_name) {

  global $ip_only, $registered_only;
  global $dab_redirect, $dab_convert, $dab_move;
  global $dab_template, $dab_article, $dab_int, $dab_rvv;
  global $notified_only, $unnotified_only, $span;

  if ($ip_only)
    $mo = "&usr=ip";
  else if ($registered_only)
    $mo = "&usr=reg";
  else
    $mo = "";

  if ($dab_redirect)
    $mo=$mo."&type[]=1";

  if ($dab_convert)
    $mo=$mo."&type[]=2";

  if ($dab_move)
    $mo=$mo."&type[]=3";

  if ($dab_template)
    $mo=$mo."&type[]=4";

  if ($dab_article)
    $mo=$mo."&type[]=5";

  if ($dab_int)
    $mo=$mo."&type[]=6";

  if ($dab_rvv)
    $mo=$mo."&type[]=7";

  if ($notified_only)
    $mo = $mo."&msg=y";
  else if ($unnotified_only)
    $mo = $mo."&msg=n";

  $mo=$mo."&span=$span";

  if ($offset > 0) {
    $po = $offset - $limit;
    if ($po < 0) $po = 0;
    print "<p>View (<a href=\"$page_name?limit=$limit&offset=$po".$mo."\">previous ".$limit."</a>) ";
  }
  else
    print "<p>View (previous ".$limit.") ";

  if ($num == $limit + 1) {
    $no = $offset + $limit;
    print "(<a href=\"$page_name?limit=$limit&offset=$no".$mo."\">next ".$limit."</a>) ";
  }
  else
    print "(next ".$limit.") ";

  print "(<a href=\"$page_name?limit=20&offset=".$offset.$mo."\">20</a> ";
  print "| <a href=\"$page_name?limit=50&offset=".$offset.$mo."\">50</a> ";
  print "| <a href=\"$page_name?limit=100&offset=".$offset.$mo."\">100</a> ";
  print "| <a href=\"$page_name?limit=250&offset=".$offset.$mo."\">250</a> ";
  print "| <a href=\"$page_name?limit=500&offset=".$offset.$mo."\">500</a>)</p>";
}


function printResultsLocal($tooldb, $limit, $res, $num, $offset) {

  if ($num == $limit + 1) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      printRowLocal($tooldb, $row);
    }
    print "</ol>\n\n";
  }
  else if ($num > 0) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
      printRowLocal($tooldb, $row);
    }
    print "</ol>\n\n";
  }
}


function printRowLocal($tooldb, $row) {

  global $dab_redirect, $dab_convert, $dab_move;
  global $dab_template, $dab_article, $dab_int, $dab_rvv;

  $user = $row['user'];
  $display_user = str_replace( '_', ' ', $user );
  $escuser = mysqli_real_escape_string($tooldb, $user);
  $count = $row['count'];

  print "<li>";
  print "<a href=\"//en.wikipedia.org/wiki/User:".encodeTitle($user).
        "\">User:$display_user</a> ($count dablink".($count == 1 ? "" : "s").")<br/>\n";

  if ($dab_redirect)
    printDabRedirect($tooldb, $escuser, $display_user);

  if ($dab_convert)
    printDabConvert($tooldb, $escuser, $display_user);

  if ($dab_move)
    printDabMove($tooldb, $escuser, $display_user);

  if ($dab_template)
    printDabTemplate($tooldb, $escuser, $display_user);

  if ($dab_article)
    printDabArticle($tooldb, $escuser, $display_user);

  if ($dab_int)
    printDabInt($tooldb, $escuser, $display_user);

  if ($dab_rvv)
    printDabRvv($tooldb, $escuser, $display_user);
}


function printDabRedirect($tooldb, $user, $display_user) {

  global $notified_only, $unnotified_only, $span, $sqlLastDate, $base_time;

  $sql = "
          SELECT redirect_id,
                 redirect_title,
                 dab_title,
                 count(*) AS article_count
            FROM recent_dabs
           WHERE user = '$user'
             AND type = 1
             AND is_fix = 0";

  if ($span == 'last')
    $sql .= "
             AND dab_date > '".$sqlLastDate."'";
  else
    $sql .= "
             AND dab_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")";

  if ($notified_only)
    $sql .= "
             AND is_msg = 1";
  else if ($unnotified_only)
    $sql .= "
             AND is_msg = 0";

  $sql .= "
           GROUP BY redirect_title
           ORDER BY article_count DESC, redirect_title
         ";

  $dab_res = mysqli_query($tooldb, $sql);

  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {
      $redirect_id = (int) $dab_row['redirect_id'];
      if (($check = page_check($redirect_id)) == false) {
        continue;
      }
      $redirect_title = $check['page_title'];
      $disp_redirect_title = str_replace( '_', ' ', $redirect_title);
      $dab_title = $dab_row['dab_title'];
      $disp_dab_title = str_replace( '_', ' ', $dab_title);
      $page_touched = $check['page_touched'];
      $article_count = $dab_row['article_count'];

      print "<li style=\"margin-bottom:0.3em;\">";
      print "Targeted ";
      if (strcmp($base_time, $page_touched) < 0) print "<s>";
      print "<a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($redirect_title)."&redirect=no\">$disp_redirect_title</a>";
      if (strcmp($base_time, $page_touched) < 0) print "</s>";
      print " to <a href=\"//en.wikipedia.org/wiki/".encodeTitle($dab_title)."\">$disp_dab_title</a> ($article_count dablink".($article_count == 1 ? "" : "s").")<br/>\n";
    }

    print "</ul>\n";
  }
}


function printDabConvert($tooldb, $user, $display_user) {

  global $notified_only, $unnotified_only, $span, $sqlLastDate, $base_time;

  $sql = "
          SELECT dab_id,
                 dab_title,
                 count(*) AS article_count
            FROM recent_dabs
           WHERE user = '$user'
             AND type = 2
             AND is_fix = 0";

  if ($span == 'last')
    $sql .= "
             AND dab_date > '".$sqlLastDate."'";
  else
    $sql .= "
             AND dab_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")";

  if ($notified_only)
    $sql .= "
             AND is_msg = 1";
  else if ($unnotified_only)
    $sql .= "
             AND is_msg = 0";

  $sql .= "
           GROUP BY dab_title
           ORDER BY article_count DESC, dab_title
         ";

  $dab_res = mysqli_query($tooldb, $sql);

  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {
      $dab_id = (int) $dab_row['dab_id'];
      if (($check = page_check($dab_id)) == false)
        continue;
      $dab_title = $check['page_title'];
      $disp_dab_title = str_replace( '_', ' ', $dab_title);
      $page_touched = $check['page_touched'];
      $article_count = $dab_row['article_count'];

      print "<li style=\"margin-bottom:0.3em;\">";
      print "Converted ";
      if (strcmp($base_time, $page_touched) < 0) print "<s>";
      print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($dab_title)."\">$disp_dab_title</a>";
      if (strcmp($base_time, $page_touched) < 0) print "</s>";
      print " into a disambig ($article_count dablink".($article_count == 1 ? "" : "s").")<br/>\n";
    }

    print "</ul>\n";
  }
}


function printDabMove($tooldb, $user, $display_user) {

  global $notified_only, $unnotified_only, $span, $sqlLastDate, $base_time;

  $sql = "
          SELECT mv_from,
                 mv_to,
                 count(*) AS article_count
            FROM recent_dabs, move_log
           WHERE user = '$user'
             AND user = mv_user
             AND mv_to = dab_title
             AND dab_date = mv_date
             AND type = 3
             AND is_fix = 0";

  if ($span == 'last')
    $sql .= "
             AND dab_date > '".$sqlLastDate."'";
  else
    $sql .= "
             AND dab_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")";

  if ($notified_only)
    $sql .= "
             AND is_msg = 1";
  else if ($unnotified_only)
    $sql .= "
             AND is_msg = 0";

  $sql .= "
           GROUP BY mv_to
           ORDER BY article_count DESC, mv_to
         ";

  $dab_res = mysqli_query($tooldb, $sql);

  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {

      $mv_from = $dab_row['mv_from'];
      $disp_mv_from = str_replace( '_', ' ', $mv_from);
      $mv_to = $dab_row['mv_to'];
      $disp_mv_to = str_replace( '_', ' ', $mv_to);
      $article_count = $dab_row['article_count'];

      print "<li style=\"margin-bottom:0.3em;\">";
      print "Moved <a href=\"//en.wikipedia.org/wiki/".encodeTitle($mv_from).
            "\">$disp_mv_from</a> to <a href=\"//en.wikipedia.org/wiki/".
            encodeTitle($mv_to)."\">$disp_mv_to</a> ($article_count dablink".
            ($article_count == 1 ? "" : "s").")<br/>\n";
    }

    print "</ul>\n";
  }
}


function printDabTemplate($tooldb, $user, $display_user) {

  global $notified_only, $unnotified_only, $span, $sqlLastDate, $base_time;

  $sql = "
          SELECT template_id,
                 page_title AS template_title,
                 count(*) AS article_count
            FROM recent_dabs
           WHERE user = '$user'
             AND type = 4
             AND is_fix = 0";

  if ($span == 'last')
    $sql .= "
             AND dab_date > '".$sqlLastDate."'";
  else
    $sql .= "
             AND dab_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")";

  if ($notified_only)
    $sql .= "
             AND is_msg = 1";
  else if ($unnotified_only)
    $sql .= "
             AND is_msg = 0";

  $sql .= "
           GROUP BY template_id
           ORDER BY article_count DESC, template_title
         ";

  $dab_res = mysqli_query($tooldb, $sql);

  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {

      $template_id = (int) $dab_row['template_id'];
      if (($check = page_check($template_id)) == false)
        continue;
      $template_title = $check['page_title'];
      $disp_template_title = str_replace( '_', ' ', $template_title);
      $page_touched = $check['page_touched'];

      $sql = "
              SELECT COUNT(DISTINCT article_id) AS transclusion_count
                FROM recent_dabs
               WHERE user = '$user'
                 AND template_id = $template_id
                 AND type = 4
                 AND is_fix = 0";

      if ($span == 'last')
        $sql .= "
                 AND dab_date > '".$sqlLastDate."'";
      else
        $sql .= "
                 AND dab_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")";

      if ($notified_only)
        $sql .= "
                 AND is_msg = 1";
      else if ($unnotified_only)
        $sql .= "
                 AND is_msg = 0";

      $trc_res = mysqli_query($tooldb, $sql);
      $trc_row = mysqli_fetch_assoc($trc_res);
      $transclusion_count = $trc_row['transclusion_count'];

      print "<li>";
      if (strcmp($base_time, $page_touched) < 0) print "<s>";
      print "<a href=\"//en.wikipedia.org/wiki/Template:".encodeTitle($template_title)."\">Template:$disp_template_title</a>";
      if (strcmp($base_time, $page_touched) < 0) print "</s>";
      print " ($transclusion_count transclusion".($transclusion_count == 1 ? "" : "s").")<br/>\n";

      $sql = "
              SELECT redirect_title
                FROM recent_dabs
               WHERE user = '$user'
                 AND template_id = $template_id
                 AND type = 4
                 AND is_fix = 0";

      if ($span == 'last')
        $sql .= "
                 AND dab_date > '".$sqlLastDate."'";
      else
        $sql .= "
                 AND dab_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")";

      if ($notified_only)
        $sql .= "
                 AND is_msg = 1";
      else if ($unnotified_only)
        $sql .= "
                 AND is_msg = 0";

      $sql .= "
               GROUP BY redirect_title
              ";

      $red_res = mysqli_query($tooldb, $sql);

      if ($red_res) {

        $is_first = true;
        print "<ul style=\"list-style-type:none;\">\n <li style=\"margin-bottom:0.3em;\">";

        while ($red_row = mysqli_fetch_assoc($red_res)) {

          $redirect_title = $red_row['redirect_title'];
          $disp_redirect_title = str_replace( '_', ' ', $redirect_title);

          if (!$is_first) print ", ";
          else print "Added ";

          print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($redirect_title)."\">$disp_redirect_title</a>";

          $is_first = false;
        }

        print "\n</ul>\n";
      }
    }

    print "</ul>\n";
  }
}


function printDabArticle($tooldb, $user, $display_user) {

  global $notified_only, $unnotified_only, $span, $sqlLastDate, $base_time;

  $sql = "
          SELECT article_id,
                 count(*) AS article_count
            FROM recent_dabs
           WHERE user = '$user'
             AND type = 5
             AND is_fix = 0";

  if ($span == 'last')
    $sql .= "
             AND dab_date > '".$sqlLastDate."'";
  else
    $sql .= "
             AND dab_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")";

  if ($notified_only)
    $sql .= "
             AND is_msg = 1";
  else if ($unnotified_only)
    $sql .= "
             AND is_msg = 0";

  $sql .= "
           GROUP BY article_id
           ORDER BY article_count DESC, article_title
         ";
//  print "printDabArticle: $sql<br>";
  $dab_res = mysqli_query($tooldb, $sql);

  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {

      $article_id = (int) $dab_row['article_id'];
      if (($check = page_check($article_id)) == false)
        continue;

      $article_title = $check['page_title'];
      $disp_article_title = str_replace( '_', ' ', $article_title);
      $page_touched = $check['page_touched'];
      $article_count = $dab_row['article_count'];

      print "<li>";
      if (strcmp($base_time, $page_touched) < 0) print "<s>";
      print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($article_title)."\">$disp_article_title</a>";
      if (strcmp($base_time, $page_touched) < 0) print "</s>";
      print "<br/>\n";

      $sql = "
              SELECT redirect_title
                FROM recent_dabs
               WHERE user = '$user'
                 AND is_fix = 0";

      if ($span == 'last')
        $sql .= "
                 AND dab_date > '".$sqlLastDate."'";
      else
        $sql .= "
                 AND dab_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")";

      if ($notified_only)
        $sql .= "
                 AND is_msg = 1";
      else if ($unnotified_only)
        $sql .= "
                 AND is_msg = 0";

      $sql .= "
                 AND article_id = $article_id
                 AND type = 5
               ORDER BY redirect_title
             ";

      $red_res = mysqli_query($tooldb, $sql);

      if ($red_res) {

        $is_first = true;
        print "<ul style=\"list-style-type:none;\">\n <li style=\"margin-bottom:0.3em;\">";

        while ($red_row = mysqli_fetch_assoc($red_res)) {

          $redirect_title = $red_row['redirect_title'];
          $disp_redirect_title = str_replace( '_', ' ', $redirect_title);

          if (!$is_first) print ", ";
          else print "Added ";

          print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($redirect_title)."\">$disp_redirect_title</a>";

          $is_first = false;
        }

        print "\n</ul>\n";
      }
    }

    print "</ul>\n";
  }
}


function printDabInt($tooldb, $user, $display_user) {

  global $notified_only, $unnotified_only, $span, $sqlLastDate, $base_time;

  $sql = "
          SELECT article_id,
                 count(*) AS article_count
            FROM recent_dabs
           WHERE user = '$user'
             AND type = 6
             AND is_fix = 0";

  if ($span == 'last')
    $sql .= "
             AND dab_date > '".$sqlLastDate."'";
  else
    $sql .= "
             AND dab_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")";

  if ($notified_only)
    $sql .= "
             AND is_msg = 1";
  else if ($unnotified_only)
    $sql .= "
             AND is_msg = 0";

  $sql .= "
           GROUP BY article_id
           ORDER BY article_count DESC, article_title
         ";

  $dab_res = mysqli_query($tooldb, $sql);

  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {

      $article_id = (int) $dab_row['article_id'];
      if (($check = page_check($article_id)) == false)
        continue;
      $article_title = $check['page_title'];
      $disp_article_title = str_replace( '_', ' ', $article_title);
      $page_touched = $check['page_touched'];
      $article_count = $dab_row['article_count'];

      print "<li>";
      if (strcmp($base_time, $page_touched) < 0) print "<s>";
      print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($article_title)."\">$disp_article_title</a>";
      if (strcmp($base_time, $page_touched) < 0) print "</s>";
      print "<br/>\n";

      $sql = "
              SELECT redirect_title
                FROM recent_dabs
               WHERE user = '$user'
                 AND is_fix = 0";

      if ($span == 'last')
        $sql .= "
                 AND dab_date > '".$sqlLastDate."'";
      else
        $sql .= "
                 AND dab_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")";

      if ($notified_only)
        $sql .= "
                 AND is_msg = 1";
      else if ($unnotified_only)
        $sql .= "
                 AND is_msg = 0";

      $sql .= "
                 AND article_id = $article_id
                 AND type = 6
               ORDER BY redirect_title
             ";

      $red_res = mysqli_query($tooldb, $sql);

      if ($red_res) {

        $is_first = true;
        print "<ul style=\"list-style-type:none;\">\n <li style=\"margin-bottom:0.3em;\">";

        while ($red_row = mysqli_fetch_assoc($red_res)) {

          $redirect_title = $red_row['redirect_title'];
          $disp_redirect_title = str_replace( '_', ' ', $redirect_title);

          if (!$is_first) print ", ";
          else print "Added ";

          print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($redirect_title)."\">$disp_redirect_title</a>";

          $is_first = false;
        }

        print "\n</ul>\n";
      }
    }

    print "</ul>\n";
  }
}


function printDabRvv($tooldb, $user, $display_user) {

  global $notified_only, $unnotified_only, $span, $sqlLastDate, $base_time;

  $sql = "
          SELECT article_id,
                 count(*) AS article_count
            FROM recent_dabs
           WHERE user = '$user'
             AND type = 7
             AND is_fix = 0";

  if ($span == 'last')
    $sql .= "
             AND dab_date > '".$sqlLastDate."'";
  else
    $sql .= "
             AND dab_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")";

  if ($notified_only)
    $sql .= "
             AND is_msg = 1";
  else if ($unnotified_only)
    $sql .= "
             AND is_msg = 0";

  $sql .= "
           GROUP BY article_id
           ORDER BY article_count DESC, article_title
         ";

  $dab_res = mysqli_query($tooldb, $sql);

  if ($dab_res && mysqli_num_rows($dab_res) > 0) {

    print "<ul>\n";

    while ($dab_row = mysqli_fetch_assoc($dab_res)) {

      $article_id = (int) $dab_row['article_id'];
      if (($check = page_check($article_id)) == false)
        continue;
      $article_title = $check['page_title'];
      $disp_article_title = str_replace( '_', ' ', $article_title);
      $page_touched = $check['page_touched'];
      $article_count = $dab_row['article_count'];

      print "<li>";
      if (strcmp($base_time, $page_touched) < 0) print "<s>";
      print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($article_title)."\">$disp_article_title</a>";
      if (strcmp($base_time, $page_touched) < 0) print "</s>";
      print "<br/>\n";

      $sql = "
              SELECT redirect_title
                FROM recent_dabs
               WHERE user = '$user'
                 AND is_fix = 0";

      if ($span == 'last')
        $sql .= "
                 AND dab_date > '".$sqlLastDate."'";
      else
        $sql .= "
                 AND dab_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")";

      if ($notified_only)
        $sql .= "
                 AND is_msg = 1";
      else if ($unnotified_only)
        $sql .= "
                 AND is_msg = 0";

      $sql .= "
                 AND article_id = $article_id
                 AND type = 7
               ORDER BY redirect_title
             ";

      $red_res = mysqli_query($tooldb, $sql);

      if ($red_res) {

        $is_first = true;
        print "<ul style=\"list-style-type:none;\">\n <li style=\"margin-bottom:0.3em;\">";

        while ($red_row = mysqli_fetch_assoc($red_res)) {

          $redirect_title = $red_row['redirect_title'];
          $disp_redirect_title = str_replace( '_', ' ', $redirect_title);

          if (!$is_first) print ", ";
          else print "Added ";

          print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($redirect_title)."\">$disp_redirect_title</a>";

          $is_first = false;
        }

        print "\n</ul>\n";
      }
    }

    print "</ul>\n";
  }
}


function page_check($page_id) {
  # return page_title and page_touched as an array, or false if not found
  global $wikidb;

  $result = mysqli_query($wikidb, "
      SELECT page_title, page_touched
        FROM page
       WHERE page_id = $page_id
  ");
  if ((! $result) || mysqli_num_rows($result) == 0) {
    return false;
  }
  $row = mysqli_fetch_assoc($result);
  return $row;
}

?>

