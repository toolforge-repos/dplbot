<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$user = $_GET['user'] ?? "";
$display_user = str_replace( '_', ' ', $user );
$span = $_GET['span'] ?? "day";

if (trim($user) != "") {
  if (trim($span) == "week") {
    $page_title = $h1_title = "Moves by ".str_replace("_", " ", $user)." in the last week";
  }
  else if (trim($span) == "month") {
    $page_title = $h1_title = "Moves by ".str_replace("_", " ", $user)." in the last month";
  }
  else {
    $span = "day";
    $page_title = $h1_title = "Moves by ".str_replace("_", " ", $user)." in the last 24 hours";
  }

  list( $limit, $offset ) = check_limits();

  $page_name = "user_moves.php";

  $tooldb = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
  if ($tooldb) {
    $escuser = mysqli_real_escape_string($tooldb, $user);
    $sql = "
                 SELECT mv_date,
                        mv_is_admin,
                        mv_from,
                        mv_to
                   FROM move_log
                  WHERE mv_user = '$escuser'
                    AND mv_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")
                  ORDER BY mv_date DESC
            ";

    $sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

    print_header($page_title, $h1_title, "../css/main.css");

    print "<p>This page tracks moves done by <a href=\"//en.wikipedia.org/wiki/User:".encodeTitle($user)."\">$display_user</a> for the last day, week, or month. </p>\n\n";

    if (file_exists("$HOME_DIR/status/mv_last_good_run.php")) {
      include_once("$HOME_DIR/status/mv_last_good_run.php");
      print "<p>This data is updated hourly; the last update occurred ".convert_time(time() - $mv_begin_run)." ago.</p>\n\n";
    }
    else
      print "<p>This data is updated hourly.</p>\n\n";

    print "<p>Back to <a href=\"move_stats.php?span=$span&limit=$limit\">Move Watch</a>.</p>\n\n";

    print "<p>\n";

    if ($span == "day") {
      print "<b>24 hours</b> &ndash; ";
    } else {
      print "<a href=\"$page_name?span=day&user=$user&limit=$limit&offset=$offset\">24 hours</a> &ndash; ";
    }
    if ($span == "week") {
      print "<b>Week</b> &ndash; ";
    } else {
      print "<a href=\"$page_name?span=week&user=$user&limit=$limit&offset=$offset\">Week</a> &ndash; ";
    }
    if ($span == "month") {
      print "<b>Month</b>\n";
    } else {
      print "<a href=\"$page_name?span=month&user=$user&limit=$limit&offset=$offset\">Month</a>\n";
    }

    print "</p>\n\n<code>$sql</code>";

    $res = mysqli_query($tooldb, $sql);

    if ($res) {

      $num = mysqli_num_rows($res);

      if ($num > 0) {
        print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
        printNavLocal($limit, $num, $offset, $page_name, $user, $span);
        printResultsLocal($limit, $res, $num, $offset);
        printNavLocal($limit, $num, $offset, $page_name, $user, $span);
      }
      else print "<p>There are no results in this query.</p>\n\n";

    }
    else {
      log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($tooldb));
      print "<p>Database error:<br/><br/>\n\n".mysqli_error($tooldb)."</p>\n\n";
    }

    mysqli_close($tooldb);
  }
  else {
    log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
    print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
  }
}
else {
  $page_title = $h1_title = "No user specified";
  print_header($page_title, $h1_title, "../css/main.css");
}

print_footer();


function printResultsLocal($limit, $res, $num, $offset) {
  global $page_name;

  if ($num == $limit + 1) {
    print "\n\n<table style=\"font-size:95%\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      printRowLocal($row);
    }
    print "</table>\n\n";
  }
  else if ($num > 0) {
    print "\n\n<table style=\"font-size:95%\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
      printRowLocal($row, $num);
    }
    print "</table>\n\n";
  }
}


function printRowLocal($row) {
  $date = $row['mv_date'];
  $phpDate = strtotime($date);
  $disp_date = date('H:i, F j, Y', $phpDate);
  $is_admin = $row['mv_is_admin'];
  $from = $row['mv_from'];
  $to = $row['mv_to'];
  $disp_from = str_replace( '_', ' ', $from );
  $disp_to = str_replace( '_', ' ', $to );
  print "<li>".$disp_date." &ndash; moved <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($from)."\">$disp_from</a> to ";
  print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($to)."\">$disp_to</a>\n";
}


function printNavLocal($limit, $num, $offset, $page_name, $user, $span) {

  if ($offset > 0) {
    $po = $offset - $limit;
    if ($po < 0) $po = 0;
    print "<p>View (<a href=\"$page_name?user=$user&span=$span&limit=$limit&offset=$po\">previous ".$limit."</a>) ";
  }
  else
    print "<p>View (previous ".$limit.") ";

  if ($num == $limit + 1) {
    $no = $offset + $limit;
    print "(<a href=\"$page_name?user=$user&span=$span&limit=$limit&offset=$no\">next ".$limit."</a>) ";
  }
  else
    print "(next ".$limit.") ";

  print "(<a href=\"$page_name?user=$user&span=$span&limit=20&offset=".$offset."\">20</a> ";
  print "| <a href=\"$page_name?user=$user&span=$span&limit=50&offset=".$offset."\">50</a> ";
  print "| <a href=\"$page_name?user=$user&span=$span&limit=100&offset=".$offset."\">100</a> ";
  print "| <a href=\"$page_name?user=$user&span=$span&limit=250&offset=".$offset."\">250</a> ";
  print "| <a href=\"$page_name?user=$user&span=$span&limit=500&offset=".$offset."\">500</a>)</p>";
}

?>

