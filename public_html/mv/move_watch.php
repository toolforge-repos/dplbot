<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$span = $_GET['span'] ?? "day";

if (trim($span) == "week") {
  $page_title = $h1_title = "Moves in the last week";
}
else if (trim($span) == "month") {
  $page_title = $h1_title = "Moves in the last month";
}
else {
  $span = "day";
  $page_title = $h1_title = "Moves in the last 24 hours";
}

list( $limit, $offset ) = check_limits();

/* To create a new page this is all you have to change */

$page_name = "move_watch.php";
$page_title = "Move Watch";

$sql = "
             SELECT mv_user AS user,
                    mv_is_admin AS is_admin,
                    count(*) AS count
               FROM move_log
              WHERE mv_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")
              GROUP BY mv_user
              ORDER BY count DESC, max(mv_date) DESC, user ASC
        ";

/* Finish customization section */

$sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

$tooldb = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$wikidb = get_db_con("enwiki_p", "enwiki.web.db.svc.wikimedia.cloud");

if ($tooldb && $wikidb) {

  print_header($page_title, "", "../css/main.css");

  print "<p>This page tracks moves done by a user for the last day, week, or month. The entries are sorted by (1) number of moves in the given time period, and then (2) order of recent activity. So if three users have made 10 moves, the user who moved an article most recently will be at the top of the 10 moves section.</p>\n\n";

  print "<p>The third column is edit count. The last column is the date the user joined the project (for non-admins). After each non-admin their three most recent moves are listed.</p>\n\n";

  if (file_exists("$HOME_DIR/status/mv_last_good_run.php")) {
    include_once("$HOME_DIR/status/mv_last_good_run.php");
    print "<p>This page normally is updated hourly; the last update occurred ".convert_time(time() - $mv_begin_run)." ago.</p>\n\n";
  }
  else
    print "<p>This page normally is updated hourly.</p>\n\n";

  print "<p>\n";

  if ($span == "day") {
    print "<b>24 hours</b> &ndash; ";
  } else {
    print "<a href=\"$page_name?span=day&limit=$limit\">24 hours</a> &ndash; ";
  }
  if ($span == "week") {
    print "<b>Week</b> &ndash; ";
  } else {
    print "<a href=\"$page_name?span=week&limit=$limit\">Week</a> &ndash; ";
  }
  if ($span == "month") {
    print "<b>Month</b>\n";
  } else {
    print "<a href=\"$page_name?span=month&limit=$limit\">Month</a>\n";
  }

  print "</p>\n\n";

  $res = mysqli_query($tooldb, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNavLocal($limit, $num, $offset, $page_name, $span);
      printResultsLocal($tooldb, $limit, $res, $num, $offset, $span);
      printNavLocal($limit, $num, $offset, $page_name, $span);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($tooldb));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($tooldb)."</p>\n\n";
  }

  mysqli_close($tooldb);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printResultsLocal($tooldb, $limit, $res, $num, $offset, $span) {
  global $page_name;

  if ($num == $limit + 1) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    //print "<tr><th/><th/><th align=\"right\">Edit count</th><th align=\"right\">Member since</th></tr>\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      if (printRowLocal($tooldb, $row, $number, $span, $limit))
        $number++;
    }
    print "</table>\n\n";
  }
  else if ($num > 0) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    //print "<tr><th/><th/><th align=\"right\">Edit count</th><th align=\"right\">Member since</th></tr>\n";
    while ($row = mysqli_fetch_assoc($res)) {
      if (printRowLocal($tooldb, $row, $number, $span, $limit))
        $number++;
    }
    print "</table>\n\n";
  }
}

function printRowLocal($tooldb, $row, $number, $span, $limit) {
  global $wikidb;
  
  $user = $row['user'];
  $escuser = mysqli_real_escape_string($wikidb, $user);
  $is_admin = $row['is_admin'];
  $u_result = mysqli_query($wikidb, "
      SELECT user_registration, user_editcount
        FROM user
       WHERE user_name = '$escuser'
  ");
  if ((!$u_result) || mysqli_num_rows($u_result) == 0)
    return false;
  $u_row = mysqli_fetch_assoc($u_result);
  $date = $u_row['user_registration'];
  $phpDate = strtotime($date);
  $disp_date = date('F j, Y', $phpDate);
  $count = $row['count'];
  $editcount = $u_row['user_editcount'];
  $display_user = str_replace( '_', ' ', $user );
  print "<tr><td>&nbsp;&nbsp;&nbsp;".$number.". <a href=\"//en.wikipedia.org/wiki/User:".encodeTitle($user)."\">$display_user</a></td>\n";
  print "    <td align=\"right\">&nbsp;&nbsp;&nbsp;<a href=\"//dplbot.toolforge.org/mv/user_moves.php?user=".encodeTitle($user)."&span=$span&limit=$limit\">$count move".($count > 1 ? "s" : "")."</a>&nbsp;&nbsp;&nbsp;</td>\n";
  if ($editcount < 500)
    print "    <td align=\"right\">&nbsp;&nbsp;&nbsp;<font color=\"red\">".number_format($editcount)."</font> </td>\n";
  else
    print "    <td align=\"right\">&nbsp;&nbsp;&nbsp;".number_format($editcount)." </td>\n";
  if ($is_admin == 1) {
    print "    <td align=\"right\"><font color=\"green\">Administrator</font></td>\n";
  } else {
    if ($phpDate > strtotime("-2 month"))
      print "    <td align=\"right\">&nbsp;&nbsp;&nbsp;<font color=\"red\">$disp_date</font></td>\n";
    else
      print "    <td align=\"right\">&nbsp;&nbsp;&nbsp;$disp_date</td>\n";
  }
  print "</tr>\n";

  if ($is_admin == 0) {

    $sql = "
                 SELECT mv_from,
                        mv_to
                   FROM move_log
                  WHERE mv_user = '$escuser'
                    AND mv_date > DATE_SUB(SYSDATE(), INTERVAL 1 ".strtoupper($span).")
                  ORDER BY mv_date DESC
                  LIMIT 3
            ";

    $mv_res = mysqli_query($tooldb, $sql);

    if ($mv_res) {

      while ($mv_row = mysqli_fetch_assoc($mv_res)) {

        $mv_from = $mv_row['mv_from'];
        $disp_mv_from = str_replace( '_', ' ', $mv_from );

        $mv_to = $mv_row['mv_to'];
        $disp_mv_to = str_replace( '_', ' ', $mv_to );

        print "<tr><td colspan=\"4\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n";
        print "<a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($mv_from)."\">$disp_mv_from</a> <b>=></b> ";
        print "<a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($mv_to)."\">$disp_mv_to</a>";
        print "</td></tr>\n";
      }
    }
  }
  return true;
}


function printNavLocal($limit, $num, $offset, $page_name, $span) {

  if ($offset > 0) {
    $po = $offset - $limit;
    if ($po < 0) $po = 0;
    print "<p>View (<a href=\"$page_name?span=$span&limit=$limit&offset=$po\">previous ".$limit."</a>) ";
  }
  else
    print "<p>View (previous ".$limit.") ";

  if ($num == $limit + 1) {
    $no = $offset + $limit;
    print "(<a href=\"$page_name?span=$span&limit=$limit&offset=$no\">next ".$limit."</a>) ";
  }
  else
    print "(next ".$limit.") ";

  print "(<a href=\"$page_name?span=$span&limit=20&offset=".$offset."\">20</a> ";
  print "| <a href=\"$page_name?span=$span&limit=50&offset=".$offset."\">50</a> ";
  print "| <a href=\"$page_name?span=$span&limit=100&offset=".$offset."\">100</a> ";
  print "| <a href=\"$page_name?span=$span&limit=250&offset=".$offset."\">250</a> ";
  print "| <a href=\"$page_name?span=$span&limit=500&offset=".$offset."\">500</a>)</p>";
}

?>

