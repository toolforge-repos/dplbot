<?php

putenv('TZ=UTC');

error_reporting(E_ERROR);

$title = "";

if (strlen($_SERVER['PATH_INFO']) > 1)
  $title = substr($_SERVER['PATH_INFO'], 1);

header("Location: //dplbot.toolforge.org/disambigs_in_an_article.php?title=".localEncodeTitle($title));


function localEncodeTitle($title) {

	$title = rawurlencode( $title );
	$title = preg_replace( '/%3[Aa]/', ':', $title );
	$title = preg_replace( '/%2[Ff]/', '/', $title );

	return $title;
}

?>
