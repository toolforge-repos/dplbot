<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

/* To create a new page this is all you have to change */

include_once("$HOME_DIR/status/dab_last_good_run.php");
$base_time = $dab_begin_run_wiki;

$page_name = "latest_dabs.php";
$page_title = "Today's Dablink Report";

$newDabs = isset($_GET['newDabs']) ? trim($_GET['newDabs']) : "";

$tooldb = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$wikidb = get_db_con("enwiki_p", "enwiki.web.db.svc.wikimedia.cloud");

if ($tooldb && $wikidb) {

  $sql = "
               SELECT dab_id,
                      dab_title,
                      is_new,
                      new_count AS count,
                      all_count
                 FROM latest_dabs
                ";

  if ($newDabs == "y")
    $sql .= "
                WHERE is_new = 1
                ORDER BY count DESC, dab_title
          ";
  else
    $sql .= "
                ORDER BY count DESC, all_count DESC, dab_title
          ";

  $sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

  print_header($page_title);

  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e')).
        "</b>. Replication lag is ".convert_time(get_replag($wikidb)).".</p>\n\n";

  print "<p>All article-to-disambiguation page links are stored on Wikimedia Labs. Whenever a new one is discovered, it is recorded with a date stamp. This is a list of dablinks that were recorded on the date of the most recent scan. A couple of points. (1) This could show yesterday's dablinks, if today's scan hasn't run yet. (2) All of the dablinks found on a given date are shown - even if there were multiple scans that day.</p>\n\n";

  print "<p>This page has ability to show only \"new\" disambiguation pages with links - that is, disambigs that are either brand new to Wikipedia, or didn't have any links before the most recent scan.</p>\n\n";

  if (file_exists("$HOME_DIR/status/dab_last_good_run.php")) {
    include_once("$HOME_DIR/status/dab_last_good_run.php");
    print "<p>This list is generated once daily; the last generation occurred ".convert_time(time() - $dab_begin_run)." ago. ";
  }
  else
    print "<p>This list is generated once daily. ";

  print "If a page has been edited since the last update, it will be <s>struck through</s>. This doesn't necessarily mean the dablink has been fixed.</p>\n\n";

  # Filter button
  print "<form action=\"$page_name\" method=\"get\">\n\n";

  print "<input type=\"hidden\" name=\"limit\" value=\"$limit\"/>\n";

  print "<table cellspacing=\"6\" style=\"font-size:95%\">\n\n";

  print "<tr><td>Only show disambiguation pages that<br/>are <b>new</b> or had <b>no links yesterday</b>.</td>\n";

  print "    <td valign=\"center\"><input type=\"checkbox\" name=\"newDabs\" value=\"y\" ".($newDabs == "y" ? "checked" : "unchecked")."/></td>\n";

  print "    <td valign=\"center\"><input type=\"submit\" value=\"Filter\"/></td></tr>\n";

  print "</table>\n\n</form>\n\n";
  # End filter button

  $res = mysqli_query($tooldb, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>. (For the sake of readability, a disambig's link list is only shown for disambigs with 100 or fewer links.)</p>\n\n";
      printNavLocal($limit, $num, $offset, $page_name, $newDabs);
      printResultsLocal($tooldb, $limit, $res, $newDabs, $num, $offset, $base_time);
      printNavLocal($limit, $num, $offset, $page_name, $newDabs);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($tooldb));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($tooldb)."</p>\n\n";
  }

  mysqli_close($tooldb);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();



function printNavLocal($limit, $num, $offset, $page_name, $newDabs) {

  if ($offset > 0) {
    $po = $offset - $limit;
    if ($po < 0) $po = 0;
    print "<p>View (<a href=\"$page_name?newDabs=$newDabs&limit=$limit&offset=$po\">previous ".$limit."</a>) ";
  }
  else
    print "<p>View (previous ".$limit.") ";

  if ($num == $limit + 1) {
    $no = $offset + $limit;
    print "(<a href=\"$page_name?newDabs=$newDabs&limit=$limit&offset=$no\">next ".$limit."</a>) ";
  }
  else
    print "(next ".$limit.") ";

  print "(<a href=\"$page_name?newDabs=$newDabs&limit=20&offset=".$offset."\">20</a> ";
  print "| <a href=\"$page_name?newDabs=$newDabs&limit=50&offset=".$offset."\">50</a> ";
  print "| <a href=\"$page_name?newDabs=$newDabs&limit=100&offset=".$offset."\">100</a> ";
  print "| <a href=\"$page_name?newDabs=$newDabs&limit=250&offset=".$offset."\">250</a> ";
  print "| <a href=\"$page_name?newDabs=$newDabs&limit=500&offset=".$offset."\">500</a>)</p>";
}

function printResultsLocal($tooldb, $limit, $res, $newDabs, $num, $offset, $base_time) {
  global $wikidb;

  print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
  if ($num == $limit + 1) {
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      $dab_id = (int) $row['dab_id'];
      $dab_res = mysqli_query($wikidb, "
          SELECT *
            FROM page_props
           WHERE pp_page = $dab_id
             AND pp_propname = 'disambiguation'
      ");
      if ($dab_res && mysqli_num_rows($dab_res) > 0) {
        // page still exists and is still a dab page
        printRowLocal($tooldb, $row, $newDabs, $base_time);
      }
    }
  }
  else if ($num > 0) {
    while ($row = mysqli_fetch_assoc($res)) {
      $dab_id = (int) $row['dab_id'];
      $dab_res = mysqli_query($wikidb, "
          SELECT *
            FROM page_props
           WHERE pp_page = $dab_id
             AND pp_propname = 'disambiguation'
      ");
      if ($dab_res && mysqli_num_rows($dab_res) > 0) {
        // page still exists and is still a dab page
        printRowLocal($tooldb, $row, $newDabs, $base_time);
      }
    }
  }
  print "</ol>\n\n";
}

function printRowLocal($tooldb, $row, $newDabs, $base_time) {
  global $wikidb;

  $dab_id = $row['dab_id'];
  $dab_title = $row['dab_title'];
  $count = $row['count'];
  $all_count = $row['all_count'];
  $display_dab_title = str_replace( '_', ' ', $dab_title );
  print "<li>";
  print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($dab_title)."\">$display_dab_title</a>";
  if ($newDabs == 'y')
    print " (<a href=\"dab_fix_list.php?title=".encodeTitle($dab_title)."\">".number_format($count)." link".($count == 1 ? "" : "s").")<br/>\n";
  else
    print " (<a href=\"dab_fix_list.php?title=".encodeTitle($dab_title)."\">".number_format($count)." new link".($count == 1 ? "" : "s")." of $all_count)<br/>\n";


  if ($count <= 100) {

    $sql = "
            SELECT article_title,
                   redirect_id,
                   redirect_title,
                   template_id,
                   template_title
              FROM latest_dab_links
             WHERE dab_id = $dab_id
             ORDER BY redirect_title, template_title, article_title
           ";

    $dab_res = mysqli_query($tooldb, $sql);

    if ($dab_res) {

      $prev_redirect_title = "";
      $prev_template_title = "";

      $redirect_started = false;
      $template_started = false;

      print "<ul>\n";

      # Replace with iterate array
      while ($dab_row = mysqli_fetch_assoc($dab_res)) {

        $article_title = $dab_row['article_title'];
        $disp_article_title = str_replace( '_', ' ', $article_title );
        $atitle = mysqli_real_escape_string($wikidb, $article_title);

        $touch_res = mysqli_query($wikidb, "
            SELECT page_touched AS article_touched
              FROM page
             WHERE page_title = '$atitle'
               AND page_namespace = 0
        ");
        $touch_row = mysqli_fetch_assoc($touch_res);
        $article_touched = $touch_row['article_touched'] ?? '20000101000000';

        $redirect_title = $dab_row['redirect_title'];
        $disp_redirect_title = str_replace( '_', ' ', $redirect_title );
        $redirect_id = (int) $dab_row['redirect_id'];

        $template_title = $dab_row['template_title'];
        $disp_template_title = str_replace( '_', ' ', $template_title );
        $template_id = (int) $dab_row['template_id'];

        if ($template_started && $template_title != $prev_template_title) {
          print "</ul>\n";
          $template_started = false;
        }

        if ($redirect_started && $redirect_title != $prev_redirect_title) {
          print "</ul>\n";
          $redirect_started = false;
        }

        if ($dab_title != $redirect_title && $redirect_title != $prev_redirect_title && !$redirect_started) {
          $rtitle = mysqli_real_escape_string($wikidb, $redirect_title);
          $sql = "
                   SELECT page_touched AS redirect_touched
                     FROM enwiki_p.page
                    WHERE page_title = '$rtitle'
                      AND page_namespace = 0
                 ";

          $r_res = mysqli_query($wikidb, $sql);
          $r_row = mysqli_fetch_assoc($r_res);
          $redirect_touched = $r_row['redirect_touched'] ?? '20000101000000';

          if (strcmp($base_time, $redirect_touched) < 0) print "<s>";
          print "<li><a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($redirect_title)."&redirect=no\">$disp_redirect_title</a>";
          if (strcmp($base_time, $redirect_touched) < 0) print "</s>";
          print " (redirect)\n<ul>\n";
          $redirect_started = true;
        }

        if ($template_title != "" && $template_title != $prev_template_title && !$template_started) {

          $sql = "
                   SELECT page_touched AS template_touched
                     FROM page
                    WHERE page_id = $template_id
                 ";

          $t_res = mysqli_query($wikidb, $sql);
          $t_row = mysqli_fetch_assoc($t_res);
          $template_touched = $t_row['template_touched'] ?? '20000101000000';

          if (strcmp($base_time, $template_touched) < 0) print "<s>";
          print "<li><a href=\"//en.wikipedia.org/w/index.php?title=Template:".encodeTitle($template_title)."\">Template:$disp_template_title</a>";
          if (strcmp($base_time, $template_touched) < 0) print "</s>";
          print "\n<ul>\n";
          $template_started = true;
        }

        if ($template_started) {
          print "<li>(transcluded by) <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($article_title)."\">$disp_article_title</a>\n";
        } else {
          if (strcmp($base_time, $article_touched) < 0) print "<s>";
          print "<li><a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($article_title)."\">$disp_article_title</a>\n";
          if (strcmp($base_time, $article_touched) < 0) print "</s>";
          print "\n";
        }

        $prev_redirect_title = $redirect_title;
        $prev_template_title = $template_title;
      }

      if ($template_started)  {
        print "</ul>\n";
      }

      if ($redirect_started)  {
        print "</ul>\n";
      }

      print "</ul>\n";
    }
  }
}

?>
