<?php

putenv('TZ=UTC');

$HOME_DIR = "/data/project/dplbot"."/dplbot";

error_reporting(E_ERROR);

$free_filename = "$HOME_DIR/status/uu_lock.txt";
$in_use_filename = "$HOME_DIR/status/uu_update.txt";

if (rename($free_filename, $in_use_filename)) {
  include_once("$HOME_DIR/scripts/user_uncat_update.php");
  rename($in_use_filename, $free_filename);
}
else if (file_exists($in_use_filename)) {
  if (time() - filectime($in_use_filename) > 600) {
    include_once("$HOME_DIR/scripts/user_uncat_update.php");
    rename($in_use_filename, $free_filename);
  }
}

header("Location: /cat/untagged_uncats.php?".$_SERVER['QUERY_STRING']);

?>

