<?php

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

$title = $_GET["title"];

$title = str_replace(" ", "_", trim($title));

$count = $_GET["count"];

$location = "Location: ../disambig_links.php?title=$title&count=$count&limit=$limit";

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if ($mysql) {
	
  $etitle = mysqli_real_escape_string($mysql, $title);

  if (is_numeric($count) && $count > 0) {
    $sql = "
            SELECT min(lc_title) as ct_title
              FROM dab_link_count
             WHERE lc_amnt = (
                              SELECT max(lc_amnt)
                                FROM dab_link_count
                               WHERE lc_amnt <= $count
                             );
           ";

    $ct_res = mysqli_query($mysql, $sql);
    
    if ($ct_res) {
      $row = mysqli_fetch_assoc($ct_res);
      $title = $row['ct_title'];
      $etitle = mysqli_real_escape_string($mysql, $title);
    }
  }

  $sql = "
          SELECT 1
            FROM dab_link_count
           WHERE lc_title = '$etitle'
         ";

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    if (mysqli_num_rows($res) == 0)
      $location .= "&offset=$offset&fail=y";

    else {

      $sql = "
              SELECT count(*) AS count
                FROM dab_link_count
               WHERE lc_amnt > (
                                SELECT lc_amnt
                                  FROM dab_link_count
                                 WHERE lc_title = '$etitle'
                               )
                  OR (
                      lc_amnt = (
                                 SELECT lc_amnt
                                   FROM dab_link_count
                                  WHERE lc_title = '$etitle'
                                )
                      AND lc_title < '$etitle'
                     )";

      $offset_res = mysqli_query($mysql, $sql);

      if ($offset_res) {
        $row = mysqli_fetch_assoc($offset_res);
        $offset = $row['count'];
      }

      $location .= "&offset=$offset";
    }

  } else
    $location .= "&offset=$offset&fail=y";
  mysqli_close($mysql);
}

header($location);
?>
