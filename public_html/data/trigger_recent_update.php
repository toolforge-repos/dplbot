<?php

putenv('TZ=UTC');

$HOME_DIR = "/data/project/dplbot"."/dplbot";

error_reporting(E_ERROR);

$id_filename = "$HOME_DIR/status/ru_".date("His", time())."_".rand(100, 199).".txt";

if (rename("$HOME_DIR/status/ru_lock.txt", $id_filename)) {
  include_once("$HOME_DIR/scripts/user_recent_update.php");
  rename($id_filename, "$HOME_DIR/status/ru_lock.txt");
}
else {
  $start = time();
  while (true) {
    sleep(1);
    if (file_exists("$HOME_DIR/status/ru_lock.txt"))
      break;
    if ((time() - $start) > 30)
      break;
  }
}

header("Location: $HOME_DIR/public_html/dab_edits.php?".$_SERVER['QUERY_STRING']);

?>