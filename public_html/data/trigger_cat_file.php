<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

error_reporting(E_ERROR);

if (file_exists("$HOME_DIR/status/uc_last_good_run.php")) {
  include_once("$HOME_DIR/status/uc_last_good_run.php");
  $base_time = $uc_begin_run_wiki;
}
else
  $base_time = '99999999999999';

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$enwiki = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

if ($mysql && $enwiki) {
  write_bot_data_local($mysql, $enwiki, $base_time);
  mysqli_close($mysql);
}
else {
  $fp = fopen("categorized_articles_list.txt", "w");
  fclose($fp);
  log_error(date("F j G:i", time()), "trigger_cat_file.php", "mysql connect", mysqli_connect_error());
}


header("Location: /cat/categorized_articles.php?".$_SERVER['QUERY_STRING']);


function write_bot_data_local($mysql, $enwiki, $base_time) {
  global $HOME_DIR;
  $sql = "
         SELECT c_title AS title
           FROM categorized_articles
          WHERE NOT EXISTS
                   (
                    SELECT 1
                      FROM deletion_targets
                     WHERE c_id = del_id
                   )
          ORDER BY title
         ";

  $result = mysqli_query($mysql, $sql);

  if ($result) {

    $fp = fopen("$HOME_DIR/public_html/data/categorized_articles_list.txt", "w");

    while ($row = mysqli_fetch_assoc($result)) {
      $title = str_replace( '_', ' ', $row['title'] );
      $etitle = mysqli_real_escape_string($enwiki, $row['title']);
      $query = "
            SELECT 1
              FROM page
             WHERE page_title = '$etitle'
               AND page_touched < '$base_time'
               AND page_namespace = 0
               AND page_is_redirect = 0
      ";
      $valid = mysqli_query($enwiki, $query);
      if ($valid && mysqli_num_rows($valid) == 1)
        fputs($fp, "[[$title]]\n");
      mysqli_free_result($valid);
    }

    fclose($fp);

    mysqli_free_result($result);
  }
  else {

    $fp = fopen("$HOME_DIR/public_html/data/categorized_articles_list.txt", "w");
    fclose($fp);

    log_error(date("F j G:i", time()), "trigger_cat_file.php", "write_bot_data_local", mysqli_error($mysql));
  }
}

?>
