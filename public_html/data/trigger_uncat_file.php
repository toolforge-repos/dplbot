<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

// error_reporting(E_ERROR);

$is_error = 0;

$free_filename = "$HOME_DIR/status/uu_lock.txt";
$in_use_filename = "$HOME_DIR/status/uu_update.txt";

if (rename($free_filename, $in_use_filename)) {
  include_once("$HOME_DIR/scripts/user_uncat_update.php");
  rename($in_use_filename, $free_filename);
}
else if (file_exists($in_use_filename)) {
  if (time() - filectime($in_use_filename) > 600) {
    include_once("$HOME_DIR/scripts/user_uncat_update.php");
    rename($in_use_filename, $free_filename);
  }
}
else {
  $is_error = 1;
}

if (!$is_error) {
  if (file_exists("$HOME_DIR/status/uu_last_good_run.php")) {
    include_once("$HOME_DIR/status/uu_last_good_run.php");
    $base_time = $uu_begin_run_wiki;
  } else {
    $base_time = '99999999999999';
  }
  $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
  $enwiki = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

  if ($mysql && $enwiki) {
    write_bot_data_local();
    mysqli_close($mysql);
    mysqli_close($enwiki);
  } else {
    log_error(date("F j G:i", time()), "trigger_uncat_file.php", "mysql connect", mysqli_connect_error());
  }
}

header("Location: /cat/untagged_uncats.php?".$_SERVER['QUERY_STRING']);


function write_bot_data_local() {
  
  global $HOME_DIR, $mysql, $enwiki, $base_time;

  $sql = "
         SELECT uc_title AS title
           FROM u_micro_list
          WHERE NOT EXISTS
                   (
                    SELECT 1
                      FROM deletion_targets
                     WHERE uc_id = del_id
                   )
            AND is_tagged = 0
          ORDER BY title
         ";

  $result = mysqli_query($mysql, $sql);

  if ($result) {

    $fp = fopen("$HOME_DIR/public_html/data/uncategorized_articles_list.txt", "w");

    while ($row = mysqli_fetch_assoc($result)) {
      $etitle = mysqli_real_escape_string($enwiki, $row['title']);
      $valid = mysqli_query ($enwiki, "
                SELECT 1
                  FROM page
                 WHERE page_title = '$etitle'
                   AND page_touched < '$base_time'
                   AND page_namespace = 0
                   AND page_is_redirect = 0
      ");
      if ($valid && mysqli_num_rows($valid) == 1) {
        $title = str_replace( '_', ' ', $row['title'] );
        fwrite($fp, "[[$title]]\n");
      }
      mysqli_free_result($valid);
    }

    fclose($fp);

    mysqli_free_result($result);
  }
  else {
    $fp = fopen("$HOME_DIR/public_html/data/uncategorized_articles_list.txt", "w");
    fclose($fp);

    log_error(date("F j G:i", time()), "trigger_uncat_file.php", "write_bot_data_local", mysqli_error($mysql));
  }

}

?>
