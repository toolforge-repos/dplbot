<?php

putenv('TZ=UTC');

include_once("/data/project/dplbot"."/dplbot/scripts/common.php");

if (file_exists("$HOME_DIR/status/dab_last_good_run.php"))
  include_once("$HOME_DIR/status/dab_last_good_run.php");

list( $limit, $offset ) = check_limits();

$page_name = "dabs_linking_dabs.php";
$page_title = "Disambig pages linking other disambig pages";

  if (file_exists("$HOME_DIR/status/dab_last_good_run.php"))
    $base_time = $dab_begin_run_wiki;
  else
    $base_time = "99999999999999";

$sql = "
         SELECT from_dab_id AS from_id,
                from_dab_title AS from_title
           FROM dab_to_dab_links
          GROUP BY from_dab_id
          ORDER BY from_dab_title ASC
        ";

$sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if ($mysql) {

  print_header($page_title);

  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>.<br/>\n\n";

  print "<p>Any page that has been edited (including null edits that do not show up in the edit history) since the last update will be struck through.</p>\n\n";

  if (file_exists("$HOME_DIR/status/dab_last_good_run.php")) {
    include_once("$HOME_DIR/status/dab_last_good_run.php");
    print "<p>This page normally is updated at least daily; the last update occurred ".convert_time(time() - $dab_begin_run)." ago.</p>\n\n";
  }
  else
    print "<p>This page normally is updated at least daily.</p>\n\n";

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNav($limit, $num, $offset, $page_name);
      printResultsLocal($mysql, $limit, $res, $num, $offset, $base_time);
      printNav($limit, $num, $offset, $page_name);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printResultsLocal($mysql, $limit, $res, $num, $offset, $base_time) {

  if ($num == $limit + 1) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
    	printRowLocal($mysql, $row, $base_time);
    }
    print "</ol>\n\n";
  }
  else if ($num > 0) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
    	printRowLocal($mysql, $row, $base_time);
    }
    print "</ol>\n\n";
  }
}

function printRowLocal($mysql, $row, $base_time) {
 
	$from_id = $row['from_id'];
	$from_title = $row['from_title'];
//	$page_touched = $row['page_touched'];
	$disp_from_title = str_replace( '_', ' ', $from_title );
	
	$sql = "
	        SELECT to_dab_title AS dab_title
	          FROM dab_to_dab_links
	         WHERE from_dab_id = $from_id
	         ORDER BY dab_title
	       ";
	
	$dab_res = mysqli_query($mysql, $sql);
	
	if ($dab_res) {
		
	  print "<li>";
//	  if (strcmp($base_time, $page_touched) < 0) print "<s>";
	  print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($from_title)."\">$disp_from_title</a>";
//	  if (strcmp($base_time, $page_touched) < 0) print "</s>";
	
	  print "<ul>\n";
	
	  while ($dab_row = mysqli_fetch_assoc($dab_res)) {
	
	    $dab_title = $dab_row['dab_title'];
	    $disp_dab_title = str_replace( '_', ' ', $dab_title );
	
	    print "<li><a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($dab_title)."\">$disp_dab_title</a><br/>\n";
	  }
	
	  print "</ul>\n";
	}
}

?>
