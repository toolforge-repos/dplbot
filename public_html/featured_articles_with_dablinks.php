<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

/* To create a new page this is all you have to change */

include_once("$HOME_DIR/status/dab_last_good_run.php");
$base_time = $dab_begin_run_wiki;

$page_name = "featured_articles_with_dablinks.php";
$page_title = "Featured and Good Articles with Dablinks";

/* Finish customization section */

$tooldb = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$wikidb = get_db_con("enwiki_p", "enwiki.web.db.svc.wikimedia.cloud");

if ($tooldb && $wikidb) {
  
  print_header($page_title, $page_title);
  
  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>. Replication lag is ".convert_time(get_replag($wikidb)).".</p>\n\n";
  
  print "<p>This is a list of featured and good articles that contain disambiguation links.</p>\n\n";
  
  print "<p>This page normally is updated at least daily; the last update completed ".convert_time(time() - $dab_finish_run)." ago. When the list is updated, any dablink that has been fixed will be removed from this list. If an article has been edited since the most recent update, its title will be <s>struck through</s>. That doesn't necessarily mean its dablinks have been fixed; if they haven't the strikethrough will be removed with the next update.</p>\n\n";

  $sql = "
             SELECT article_id,
                    article_title,
                    count(*) AS count
               FROM all_dab_links
              WHERE EXISTS
                          (
                           SELECT 1
                             FROM featured_and_good_articles
                            WHERE article_id = art_id
                          )
              GROUP BY article_id
              ORDER BY count DESC, article_title
        ";


  $sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);
  
  $res = mysqli_query($tooldb, $sql);
  
  if ($res) {
    
    $num = mysqli_num_rows($res);
    
    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNav($limit, $num, $offset, $page_name);
      printResultsLocal($tooldb, $limit, $res, $num, $offset, $base_time);
      printNav($limit, $num, $offset, $page_name);
    }
    else print "<p>There are no results in this query.</p>\n\n";
    
  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($tooldb));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($tooldb)."</p>\n\n";
  }
  
  mysqli_close($tooldb);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printResultsLocal($tooldb, $limit, $res, $num, $offset, $base_time) {

  if ($num == $limit + 1) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      printRowLocal($tooldb, $row, $base_time);
    }
    print "</ol>\n\n";
  }
  else if ($num > 0) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
      printRowLocal($tooldb, $row, $base_time);
    }
    print "</ol>\n\n";
  }
}

function printRowLocal($tooldb, $row, $base_time) {
  global $wikidb;

  $article_id = (int) $row['article_id'];
  $article_title = $row['article_title'];
  $count = $row['count'];
  $display_article_title = str_replace( '_', ' ', $article_title );
  
  $touch_res = mysqli_query($wikidb, "
      SELECT page_touched
        FROM page
       WHERE page_id = $article_id
  ");
  $touch_row = mysqli_fetch_assoc($touch_res);
  $article_touched = $touch_row['page_touched'];
  
  $sql = "
          SELECT 1
            FROM templatelinks, linktarget
           WHERE tl_from = $article_id
             AND tl_target_id = lt_id
             AND lt_namespace = 10
             AND lt_title = 'Dn'
         ";
  
  $dn_res = mysqli_query($wikidb, $sql);
         
  print "<li>";
  if (strcmp($base_time, $article_touched) < 0) print "<s>";
  print "<a href=\"//en.wikipedia.org/wiki/".encodeTitle($article_title)."\">$display_article_title</a>";
  print " (".number_format($count)." link".($count > 1 ? "s" : "").")\n";
  if (strcmp($base_time, $article_touched) < 0) print "</s>";
  print "\n";
  if (mysqli_num_rows($dn_res) > 0) print "<br>&nbsp;&nbsp;at least one {{dn}} present";
  
  $sql = "
          SELECT article_title,
                 dab_title,
                 redirect_title,
                 template_id,
                 template_title
            FROM all_dab_links
           WHERE article_id = $article_id
           ORDER BY template_title, redirect_title, dab_title
         ";
  
  $dab_res = mysqli_query($tooldb, $sql);
  
  if ($dab_res) {
    
    $prev_redirect_title = "";
    $prev_template_title = "";
    
    $redirect_started = false;
    $template_started = false;
  
    print "<ul>\n";
    
    # Replace with iterate array
    while ($dab_row = mysqli_fetch_assoc($dab_res)) {
      
      $article_title = $dab_row['article_title'];
      $disp_article_title = str_replace( '_', ' ', $article_title );
      
      $dab_title = $dab_row['dab_title'];
      $disp_dab_title = str_replace( '_', ' ', $dab_title );
      
      $redirect_title = $dab_row['redirect_title'];
      $disp_redirect_title = str_replace( '_', ' ', $redirect_title );
      
      $template_title = $dab_row['template_title'];
      $disp_template_title = str_replace( '_', ' ', $template_title );
      $template_id = $dab_row['template_id'];
      
      if ($template_started && $template_title != $prev_template_title) {
        print "</ul>\n";
        $template_started = false;
      }
      
      if ($redirect_started && $redirect_title != $prev_redirect_title) {
        print "</ul>\n";
        $redirect_started = false;
      }
      
      if ($dab_title != $redirect_title && $redirect_title != $prev_redirect_title && !$redirect_started) {
        print "<li><a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($redirect_title)."&amp;redirect=no\">$disp_redirect_title</a> (redirect)\n<ul>\n";
        $redirect_started = true;
      }
      
      if ($template_title != "" && $template_title != $prev_template_title && !$template_started) {
      
        $sql = "
                 SELECT page_touched AS template_touched
                   FROM page
                  WHERE page_id = $template_id
               ";
        
        $t_res = mysqli_query($wikidb, $sql);
        $t_row = mysqli_fetch_assoc($t_res);
        $template_touched = $t_row['template_touched'];
        
        if (strcmp($base_time, $template_touched) < 0) {
        print "<li><a href=\"//en.wikipedia.org/w/index.php?title=Template:".encodeTitle($template_title)."\"><s>Template:$disp_template_title</s></a>";
        } else {
        print "<li><a href=\"//en.wikipedia.org/w/index.php?title=Template:".encodeTitle($template_title)."\">Template:$disp_template_title</a>";
        }
        print "\n<ul>\n";
        $template_started = true;
      }
      
      print "<li><a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($dab_title)."\">$disp_dab_title</a>\n";
      
      $prev_redirect_title = $redirect_title;
      $prev_template_title = $template_title;
    }
    
    if ($template_started)  {
      print "</ul>\n";
    }
    
    if ($redirect_started)  {
      print "</ul>\n";
    }
  
    print "</ul>\n";
  }
}

?>

