<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

$page_name = "Verwaiste_Bilder.php";

$category = isset($_GET['cat']) ? trim($_GET['cat']) : "";

if ($category != "") {

  if (strtolower(substr($category, 0, 10)) == "kategorie:")
    $category = substr($category, 10);

  $cat_title = str_replace("_", " ", $category);
  $category = str_replace(" ", "_", $category);

  $page_title = "Verwaiste Bilder in Kategorie:$cat_title";
  $h1_title = "Verwaiste Bilder in <a href=\"//de.wikipedia.org/wiki/Kategorie:$category\">Kategorie:$cat_title</a>";
}
else {
  $cat_title = "";
  $page_title = $h1_title = "Verwaiste Bilder in einer bestimmten Kategorie";
}

$mysql = get_db_con("dewiki_p", "dewiki.web.db.svc.wikimedia.cloud");

if ($mysql) {

  print_header($page_title, $h1_title);

  print "<p>Dieses Tool gibt diejenigen Bilder in der Kategorie aus, die in keinem Wikipedia-Artikel eingebunden sind. Die Einbindung in Seiten anderer Namensräume (Diskussionsseiten, Benutzerseiten, usw.) wird dabei nicht berücksichtigt.</p>\n\n";

  print "<p>In der zweiten Spalte wird der Uploader angezeigt. Dort sind jeweils diejenigen Beträge des betreffenden Benutzers verlinkt, welche dieser direkt nach dem Hochladen des betreffenden Bildes gemacht hat. Anhand dieser Benutzerbeiträge kann oft nachvollzogen werden, für welchen Artikel ein Bild ursprünglich gedacht war. Auf diese Weise kann geprüft werden, ob ein Bild durch ein besseres ersetzt worden ist. In der Benutzerbeitragsliste ist das entsprechende Bild ganz unten aufgeführt, darüber stehen die nachfolgenden Beiträge.</p>\n\n";

  # Submit button
  print "<form action=\"$page_name\" method=\"get\">\n\n<p>\n\n";
  print "<br/><p>Gib die zu überprüfende Kategorie an (case sensitive)&nbsp;&nbsp;&nbsp;<input type=\"text\" value=\"$cat_title\" name=\"cat\" size=\"50\"/>\n\n";
  print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"Senden\"/>\n\n";
  print "</p><br/>\n\n</form>\n\n";
  # End submit button

  if ($category != "") {
    $ecat = mysqli_real_escape_string($mysql, $category);
    $sql = "
            SELECT 1
              FROM page
             WHERE page_title = '$ecat'
               AND page_namespace = 14
           ";

    $exists_check = mysqli_query($mysql, $sql);

    if ($exists_check) {

      if (mysqli_num_rows($exists_check) == 0)
        print "<p>Es existiert keine Kategorie mit dem Namen „".$cat_title."“. Wenn du denkst, dies sei ein Fehler, so überprüfe die Kategorieseite: <a href=\"//de.wikipedia.org/wiki/Kategorie:".encodeTitle($category)."\">$cat_title</a>.</p><br/>";

      else {
        // Get names of the images that are a member of the category.
        $sql = "
                SELECT a.page_title AS title
                  FROM page a, categorylinks
                 WHERE cl_to = '$ecat'
                   AND cl_from = a.page_id
                   AND a.page_namespace = 6
                   AND NOT EXISTS
                         (
                          SELECT 1
                            FROM imagelinks, page b
                           WHERE il_to = a.page_title
                             AND il_from = b.page_id
                             AND b.page_namespace = 0
                         )
              ";

        $sql .= " LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

        $res = mysqli_query($mysql, $sql);

        if ($res) {

          $num = mysqli_num_rows($res);

          if ($num > 0) {
            print "<p>Nachfolgend aufgelistet sind bis zu <b>".$limit."</b> Treffer, beginnend mit Nummer <b>".($offset + 1)."</b>.</p>\n\n";
            printNavLocal($limit, $num, $offset, $page_name, $category);
            printResultsLocal($mysql, $limit, $res, $num, $offset);
            printNavLocal($limit, $num, $offset, $page_name, $category);
          }
          else print "<p>Keine Treffer. Diese Kategorie enthält keine verwaisten Bilder.</p><br/>\n\n";

        }
        else {
          log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
          print "<p>Fehler bei der Datenbank:</p>\n\n<p>".mysqli_error($mysql)."</p>\n\n";
        }
      }
    }
    else {
      log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
      print "<p>Fehler bei der Datenbank:</p>\n\n<p>".mysqli_error($mysql)."</p>\n\n";
    }
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Fehler bei der Verbindung zur Datenbank: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printResultsLocal($mysql, $limit, $res, $num, $offset) {
  global $page_name;

  if ($num == $limit + 1) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      $title = $row['title'];
      $display_title = str_replace( '_', ' ', $title );
      print "<tr><td>&nbsp;&nbsp;&nbsp;".$number++.". <a href=\"//de.wikipedia.org/wiki/Datei:".encodeTitle($title)."\">$display_title</a></td>\n";
      print "    <td align=\"left\">&nbsp;&nbsp;&nbsp;&nbsp;".get_image_history($mysql, $title)."</td></tr>\n";
    }
    print "</table>\n\n";
  }
  else if ($num > 0) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
      $title = $row['title'];
      $display_title = str_replace( '_', ' ', $title );
      print "<tr><td>&nbsp;&nbsp;&nbsp;".$number++.". <a href=\"//de.wikipedia.org/wiki/Datei:".encodeTitle($title)."\">$display_title</a></td>\n";
      print "    <td align=\"left\">&nbsp;&nbsp;&nbsp;&nbsp;".get_image_history($mysql, $title)."</td></tr>\n";
    }
    print "</table>\n\n";
  }
}


function printNavLocal($limit, $num, $offset, $page_name, $category) {

  if ($offset > 0) {
    $po = $offset - $limit;
    if ($po < 0) $po = 0;
    print "<p>Zeige (<a href=\"$page_name?cat=$category&limit=$limit&offset=$po\">vorherige ".$limit."</a>) ";
  }
  else
    print "<p>Zeige (vorherige ".$limit.") ";

  if ($num == $limit + 1) {
    $no = $offset + $limit;
    print "(<a href=\"$page_name?cat=$category&limit=$limit&offset=$no\">nächste ".$limit."</a>) ";
  }
  else
    print "(nächste ".$limit.") ";

  print "(<a href=\"$page_name?cat=$category&limit=20&offset=".$offset."\">20</a> ";
  print "| <a href=\"$page_name?cat=$category&limit=50&offset=".$offset."\">50</a> ";
  print "| <a href=\"$page_name?cat=$category&limit=100&offset=".$offset."\">100</a> ";
  print "| <a href=\"$page_name?cat=$category&limit=250&offset=".$offset."\">250</a> ";
  print "| <a href=\"$page_name?cat=$category&limit=500&offset=".$offset."\">500</a>)</p>";
}

function get_image_history($mysql, $image_title) {

  // Now find the creator of the image.  We look in the oldimage table first, get only the oldest timestamp.
  $ititle = mysqli_real_escape_string($mysql, $image_title);
  $sql = "
          SELECT actor_name as oi_user_text,
                 oi_timestamp
            FROM oldimage, actor
           WHERE oi_name = '$ititle'
             AND oi_actor = actor_id
             AND oi_timestamp = (
                                 SELECT min(oi_timestamp)
                                   FROM oldimage
                                  WHERE oi_name = '$ititle'
                                )
         ";

  $old_images = mysqli_query($mysql, $sql);

  if ($old_images) {

    if (mysqli_num_rows($old_images) == 1) { // found an older revision - should only be one result due to min() function
      $row_oi_user = mysqli_fetch_assoc($old_images);
      $user_name = $row_oi_user['oi_user_text'];
      $creation_ts = date("YmdHis", strtotime($row_oi_user['oi_timestamp'])-1);
      return "<a href=\"//de.wikipedia.org/w/index.php?title=Special:Contributions&dir=prev&offset=$creation_ts&limit=20&contribs=user&target=$user_name\">$user_name</a>";
    } else {

      // single revision - get it from the image table
      $sql = "
              SELECT actor_name as img_user_text,
                     img_timestamp
                FROM image, actor
               WHERE img_name = '$ititle'
                 AND img_actor = actor_id
             ";

      $latest_image = mysqli_query($mysql, $sql);

      if ($latest_image) {

        if (mysqli_num_rows($latest_image) == 1) {
          $row_user = mysqli_fetch_assoc($latest_image);
          $user_name = $row_user['img_user_text'];
          $creation_ts = date("YmdHis", strtotime($row_user['img_timestamp'])-1);
          return "<a href=\"//de.wikipedia.org/w/index.php?title=Special:Contributions&dir=prev&offset=$creation_ts&limit=20&contribs=user&target=$user_name\">$user_name</a>";
        }
        else // probably hosted on Commons, then
          return "Lokale Bildbeschreibungsseite eines Commons-Bildes";

      }
    }
  }
}

?>