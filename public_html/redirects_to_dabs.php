<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

/* To create a new page this is all you have to change */

include_once("$HOME_DIR/status/dab_last_good_run.php");
$base_time = $dab_begin_run_wiki;

$page_name = "redirects_to_dabs.php";
$page_title = "Most Linked Redirects to Disambiguation Pages";

$sql = "
             SELECT redirect_title,
                    redirect_id,
                    dab_title,
                    count(*) AS count,
                    lc_amnt AS dab_count
               FROM all_dab_links, dab_link_count
              WHERE dab_id = lc_id
                AND redirect_title <> dab_title
                AND dab_title NOT LIKE '%_(disambiguation)'
              GROUP BY redirect_title
              ORDER BY count DESC, redirect_title ASC
        ";

/* Finish customization section */

$sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

$tooldb = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$wikidb = get_db_con("enwiki_p", "enwiki.web.db.svc.wikimedia.cloud");

if ($tooldb) {

  print_header($page_title);

  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>. Replication lag is ".convert_time(get_replag($wikidb)).".<br/>\n\n";

  $res = mysqli_query($tooldb, $sql);

  if ($res) {

    print "<code>\n\n".nl2br(str_replace(" ", "&nbsp;", $sql))."\n\n</code>\n\n";

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNav($limit, $num, $offset, $page_name);
      printResultsLocal($tooldb, $limit, $res, $num, $offset, $base_time);
      printNav($limit, $num, $offset, $page_name);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($tooldb));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($tooldb)."</p>\n\n";
  }

  mysqli_close($tooldb);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printResultsLocal($tooldb, $limit, $res, $num, $offset, $base_time) {

  if ($num == $limit + 1) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
    	$row = mysqli_fetch_assoc($res);
    	printRowLocal($tooldb, $row, $base_time);
    }
    print "</ol>\n\n";
  }
  else if ($num > 0) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
    	printRowLocal($tooldb, $row, $base_time);
    }
    print "</ol>\n\n";
  }
}

function printRowLocal($tooldb, $row, $base_time) {
  global $wikidb;
  
	$r_title = $row['redirect_title'];
	$r_id = (int) $row['redirect_id'];
	$dab_title = $row['dab_title'];
	$count = (int) $row['count'];
	$dab_count = (int) $row['dab_count'];
	$display_r_title = str_replace( '_', ' ', $r_title );
	$display_dab_title = str_replace( '_', ' ', $dab_title );
	
	$sql = "
	        SELECT page_touched
	          FROM page
	         WHERE page_id = $r_id
	       ";
	
	$r_res = mysqli_query($wikidb, $sql);
	
	if ($r_res) {
	
	  $r_row = mysqli_fetch_assoc($r_res);
	  $r_last = $r_row['page_touched'];
	
		print "<li>";
		if (strcmp($base_time, $r_last) < 0) print "<s>";
		print "<a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($r_title)."&redirect=no\">$display_r_title</a>";
		if (strcmp($base_time, $r_last) < 0) print "</s>";
		print " &rarr;";
		print " <a href=\"//en.wikipedia.org/w/index.php?title=".encodeTitle($dab_title)."\">$display_dab_title</a>";
		print " (<a href=\"//dplbot.toolforge.org/dab_fix_list.php?title=".encodeTitle($dab_title)."\">".number_format($count)." of ".number_format($dab_count)." link".($dab_count > 1 ? "s" : "")."</a>)\n";
	}
}

?>

