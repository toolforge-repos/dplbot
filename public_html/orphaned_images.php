<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

$page_name = "orphaned_images.php";

$allNS = isset($_GET['allNS']) ? trim($_GET['allNS']) : "";

$category = isset ($_GET['cat']) ? trim($_GET['cat']) : "";

if ($category != "") {

  if (strtolower(substr($category, 0, 9)) == "category:")
    $category = substr($category, 9);

  $cat_title = str_replace("_", " ", $category);
  $category = str_replace(" ", "_", $category);

  $page_title = "Orphaned images in Category:$cat_title";
  $h1_title = "Orphaned images in <a href=\"//en.wikipedia.org/wiki/Category:$category\">Category:$cat_title</a>";
}
else {
  $cat_title = "";
  $page_title = $h1_title = "Orphaned images in a given category";
}

$wikidb = get_db_con("enwiki_p", "enwiki.web.db.svc.wikimedia.cloud");

if ($wikidb) {

  print_header($page_title, $h1_title);

  print "<p>This is a list of images that are members of this category but aren't used in any English Wikipedia articles. An orphaned image is not used by any Wikipedia articles.  It is possible, though, for an orphaned image to be linked from other namespaces, such as an editor's User: page, which is perfectly acceptable in most situations (but not for non-free images).</p>\n\n";

  print "<p>Before listing an image here for deletion, check to make sure it really should be removed. If an article is linked from a non-article space using no-display syntax (such as [[:File:Example.jpg]] - note the leading colon), it will be listed as an orphan here.</p>\n\n";

  print "<p>The fourth column links to edits made by the user immediately after they created the image. Normally, the first thing a user does after creating an image is insert it into an article. Therefore looking at that editor's subsequent edits often tells what article the image was originally intended for. When knowing the identity of a replacement image is useful (such as in a deletion discussion), this can help locate the replacement image, if one exists.  If you click the link, the file creation is at the bottom of the list; each edit made after file creation is above it.</p>\n\n";

  # Filter button
  print "<form action=\"$page_name\" method=\"get\">\n\n";

  print "<input type=\"hidden\" name=\"limit\" value=\"$limit\"/>\n";

  print "<input type=\"hidden\" name=\"cat\" value=\"$cat_title\"/>\n";

  print "<table cellspacing=\"6\" style=\"font-size:95%\">\n\n";

  print "<tr><td>Only files that are orphans
                 in <b>all</b> namespaces<br/>
                 (User:, Talk:, Portal:, etc)</td>\n";

  print "    <td valign=\"center\"><input type=\"checkbox\" name=\"allNS\" value=\"y\" ".($allNS == "y" ? "checked" : "unchecked")."/></td>\n";

  print "    <td valign=\"center\"><input type=\"submit\" value=\"Filter\"/></td></tr>\n";

  print "</table>\n\n</form>\n\n";
  # End filter button

  # Submit button
  print "<form action=\"$page_name\" method=\"get\">\n\n<p>\n\n";

  print "<input type=\"hidden\" name=\"limit\" value=\"$limit\"/>\n";

  print "<input type=\"hidden\" name=\"allNS\" value=\"$allNS\"/>\n";

  print "<table cellspacing=\"6\" style=\"font-size:95%\">\n\n";

  print "<tr><td>Enter a category to check (case sensitive)&nbsp;&nbsp;&nbsp;<input type=\"text\" value=\"$cat_title\" name=\"cat\" size=\"50\"/></td>\n";

  print "<td>&nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"Submit\"/></td></tr>\n\n";

  print "</table>\n\n</form>\n\n";
  # End submit button

  if ($category != "") {

    $ctitle = mysqli_real_escape_string($wikidb, $category);
    $sql = "
            SELECT 1
              FROM page
             WHERE page_title = '$ctitle'
               AND page_namespace = 14
           ";

    $exists_check = mysqli_query($wikidb, $sql);

    if ($exists_check) {

      if (mysqli_num_rows($exists_check) == 0)
        print "<p>There is no category with the name \"$cat_title\".  If you believe this is an error, check the <a href=\"//en.wikipedia.org/wiki/Category:".encodeTitle($category)."\">$cat_title</a> page.</p><br/>";

      else {
        $res = getOrphanedImagesInCat($wikidb, $category, $allNS, $limit, $offset);

        if ($res) {

          $num = mysqli_num_rows($res);

          if ($num > 0) {
            print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
            printNavLocal($limit, $num, $offset, $page_name, $category, $allNS);
            printResultsLocal($wikidb, $limit, $res, $num, $offset);
            printNavLocal($limit, $num, $offset, $page_name, $category, $allNS);
          }
          else print "<p>Empty set. This category contains no orphaned images.</p><br/>\n\n";

        }
        else {
          log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($wikidb));
          print "<p>Database error:</p>\n\n<p>".mysqli_error($wikidb)."</p>\n\n";
        }
      }
    }
    else {
      log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($wikidb));
      print "<p>Database error:</p>\n\n<p>".mysqli_error($wikidb)."</p>\n\n";
    }
  }

  mysqli_close($wikidb);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function getOrphanedImagesInCat($wikidb, $category, $allNS, $limit, $offset) {

  $ctitle = mysqli_real_escape_string($wikidb, $category);
  $sql = "
          SELECT page_id AS id,
                 page_title AS title,
                 actor_name,
                 img_timestamp
            FROM page, image, actor, categorylinks
           WHERE cl_to = '$ctitle'
             AND cl_from = page_id
             AND img_name = page_title
             AND page_namespace = 6
             AND actor_id = img_actor
             AND NOT EXISTS
                   (
                    SELECT *
                      FROM pagelinks, linktarget
                     WHERE pl_from_namespace = 0
                       AND pl_target_id = lt_id
                       AND lt_namespace = 6
                       AND lt_title = img_name
                   )
             AND NOT EXISTS
                   (
                    SELECT *
                      FROM imagelinks
                     WHERE il_to = img_name";

  if ($allNS != "y")
    $sql .= "
                       AND il_from_namespace = 0";

  $sql .= "
                   )";

  $sql .= "
           LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

  return mysqli_query($wikidb, $sql);
}


function printResultsLocal($wikidb, $limit, $res, $num, $offset) {

  if ($num == $limit + 1) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    print "\n<tr><th/><th/><th>Notes</th><th/><th align=\"right\">Creation date</th><th/><th align=\"left\">Subsequent edits</th><th/><th align=\"right\">Orphan since</th></tr>\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      printRowLocal($wikidb, $row, $number);
      $number++;
    }
    print "</table>\n\n";
  }
  else if ($num > 0) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    print "\n<tr><th/><th/><th>Notes</th><th/><th align=\"right\">Creation date</th><th/><th align=\"left\">Subsequent edits</th><th/><th align=\"right\">Orphan since</th></tr>\n";
    while ($row = mysqli_fetch_assoc($res)) {
      printRowLocal($wikidb, $row, $number);
      $number++;
    }
    print "</table>\n\n";
  }
}


function printRowLocal($wikidb, $row, $number) {
  
  $id = $row['id'];
  $title = $row['title'];
  $user = $row['actor_name'];
  $list_timestamp = date("Ymd");
  $timestamp = $row['img_timestamp'] - 1;
  $since = $list_timestamp;
  $display_title = str_replace( '_', ' ', $title );
  print "<tr><td>&nbsp;&nbsp;&nbsp;".$number.". <a href=\"//en.wikipedia.org/wiki/File:".encodeTitle($title)."\">$display_title</a></td>\n";
  print "    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>\n";

  print "    <td align=\"left\">";
  $sql = "SELECT cl_from
            FROM categorylinks
           WHERE cl_from = $id
             AND (
                  cl_to = 'Candidates_for_speedy_deletion' OR
                  cl_to = 'Wikipedia_files_for_deletion' OR
                  cl_to = 'Wikipedia_files_on_Wikimedia_Commons' OR
                  cl_to = 'Wikipedia_files_with_unknown_source_for_deletion_in_dispute' OR
                  cl_to = 'All_Wikipedia_files_with_no_non-free_use_rationale' OR
                  cl_to = 'All_Wikipedia_files_with_unknown_copyright_status' OR
                  cl_to = 'All_Wikipedia_files_with_unknown_source' OR
                  cl_to = 'All_orphaned_non-free_use_Wikipedia_files' OR
                  cl_to = 'All_replaceable_non-free_use_Wikipedia_files'
                 )";

  $delres = mysqli_query($wikidb, $sql);

  if ($delres && mysqli_num_rows($delres) > 0) {
    print "Listed for deletion";
  } else {
    $sql = "SELECT cl_from
              FROM categorylinks
             WHERE cl_from = $id
               AND cl_to = 'Copy_to_Wikimedia_Commons'
           ";

    $cmnres = mysqli_query($wikidb, $sql);

    if ($cmnres && mysqli_num_rows($cmnres) > 0) {
      print "Move to Commons";
    } else {
      $sql = "SELECT tl_from
                FROM templatelinks, linktarget
               WHERE tl_from = $id
                 AND tl_target_id = lt_id
                 AND lt_namespace = 10
                 AND lt_title = 'Now_Commons'
             ";

      $nowres = mysqli_query($wikidb, $sql);

      if ($nowres && mysqli_num_rows($nowres) > 0) {
        print "Now Commons";
      }
    }
  }
  print "</td>\n";
  print "    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>\n";

  print "    <td align=\"right\">";
  $date = date_create_from_format('YmdHis', $timestamp);
  print date_format($date, 'F j, Y');
  print "</td>\n";
  print "    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>\n";

  print "    <td align=\"left\">";
  if ($user != "")
    print "<a href=\"//en.wikipedia.org/w/index.php?title=Special:Contributions&dir=prev&offset=$timestamp&limit=10&contribs=user&target=$user\">$user</a>";
  else
    print "Hosted on Commons";
  print "</td>\n";
  print "    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>\n";

  print "    <td align=\"right\">";
  $since_date = date_create_from_format('Ymd', $since);

  if ($since_date > date_sub(date_create(), date_interval_create_from_date_string('1 month'))) {
    print date_format($since_date, 'F j, Y');
  } else if ($since_date > date_sub(date_create(), date_interval_create_from_date_string('1 year'))) {
    print date_format($since_date, 'F, Y');
  } else {
    print date_format($since_date, 'Y');
  }

  print "</td>\n";

  print "</tr>\n";
}


function printNavLocal($limit, $num, $offset, $page_name, $category, $allNS) {

  if ($offset > 0) {
    $po = $offset - $limit;
    if ($po < 0) $po = 0;
    print "<p>View (<a href=\"$page_name?cat=$category&allNS=$allNS&limit=$limit&offset=$po\">previous ".$limit."</a>) ";
  }
  else
    print "<p>View (previous ".$limit.") ";

  if ($num == $limit + 1) {
    $no = $offset + $limit;
    print "(<a href=\"$page_name?cat=$category&allNS=$allNS&limit=$limit&offset=$no\">next ".$limit."</a>) ";
  }
  else
    print "(next ".$limit.") ";

  print "(<a href=\"$page_name?cat=$category&allNS=$allNS&limit=20&offset=".$offset."\">20</a> ";
  print "| <a href=\"$page_name?cat=$category&allNS=$allNS&limit=50&offset=".$offset."\">50</a> ";
  print "| <a href=\"$page_name?cat=$category&allNS=$allNS&limit=100&offset=".$offset."\">100</a> ";
  print "| <a href=\"$page_name?cat=$category&allNS=$allNS&limit=250&offset=".$offset."\">250</a> ";
  print "| <a href=\"$page_name?cat=$category&allNS=$allNS&limit=500&offset=".$offset."\">500</a>)</p>";
}

?>

