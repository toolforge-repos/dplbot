<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

list( $limit, $offset ) = check_limits();

$page_name = "articles_with_dab_links.php";
$page_title = "Articles With Multiple Dablinks";

$sql = "SELECT lc_title AS title,
               lc_amnt AS count
          FROM to_dab_link_count
         ORDER BY lc_amnt DESC, title
";

$sql = "$sql LIMIT ".( (is_numeric($offset) && $offset != 0) ? "$offset, " : "" ).($limit + 1);

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$enwiki = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

if ($mysql) {

  print_header($page_title);

  print "<p>The current time is <b>".str_replace( '_', ' ', date('F j, Y, G:i e'))."</b>. Replication lag is ".convert_time(get_replag($enwiki)).".<br/>\n\n";

  print "<p>This is a list of English Wikipedia articles with the most links to disambiguation pages. The second column links to a page that lists those disambiguation articles. ";

  if (file_exists("$HOME_DIR/status/dab_last_good_run.php")) {
    include_once("$HOME_DIR/status/dab_last_good_run.php");
    print "This page normally is updated at least daily; the last update occurred ".convert_time(time() - $dab_begin_run)." ago.</p>\n\n";
  }
  else
    print "This page normally is updated at least daily.</p>\n\n";

  # Submit button
  print "<form action=\"data/trigger_article_dab_links.php\" method=\"get\">\n\n<p>\n\n";

  print "<input type=\"hidden\" name=\"limit\" value=\"$limit\"/>\n";

  if (is_numeric($offset) && $offset != 0)
    print "<input type=\"hidden\" name=\"offset\" value=\"$offset\"/>\n";

  print "Article search (case sensitive) <input type=\"text\" name=\"title\" size=\"50\"/>\n\n";

  print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"Search\"/>\n\n";
  
  print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

  print "Jump to link count <input type=\"text\" name=\"count\" size=\"4\"/>\n\n";

  print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"Jump\"/>\n\n";

  if (isset($_GET['fail']) and $_GET['fail'] == "y") {
    $no_res_title = $_GET['title'] ?? "";
    $no_res_title_disp = str_replace("_", " ", trim($no_res_title));
    print "<p>No results for <a href=\"//en.wikipedia.org/wiki/".encodeTitle($no_res_title)."\">$no_res_title_disp</a>.</p>";
  }

  print "</p>\n\n</form>\n\n";
  # End submit button

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      print "<p>Showing below up to <b>".$limit."</b> results starting with #<b>".($offset + 1)."</b>.</p>\n\n";
      printNav($limit, $num, $offset, $page_name);
      printResultsLocal($limit, $res, $num, $offset);
      printNav($limit, $num, $offset, $page_name);
    }
    else print "<p>There are no results in this query.</p>\n\n";

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    print "<p>Database error:<br/><br/>\n\n".mysqli_error($mysql)."</p>\n\n";
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  print "<p>Database connection error: ".mysqli_connect_error()."</p>\n\n";
}

print_footer();


function printResultsLocal($limit, $res, $num, $offset) {

  if ($num == $limit + 1) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
    	printRowLocal($row, $number);
    	$number++;
    }
    print "</table>\n\n";
  }
  else if ($num > 0) {
    $number = $offset + 1;
    print "\n\n<table style=\"font-size:95%\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
    	printRowLocal($row, $number);
    	$number++;
    }
    print "</table>\n\n";
  }
}

function printRowLocal($row, $number) {

	$title = $row['title'];
	$count = $row['count'];
	$disp_title = str_replace( '_', ' ', $title );
	print "<tr><td>&nbsp;&nbsp;&nbsp;".$number.". <a href=\"//en.wikipedia.org/wiki/".encodeTitle($title)."\">$disp_title</a></td>\n";
	print "    <td align=\"right\">&nbsp;&nbsp;<a href=\"disambigs_in_an_article.php?title=".encodeTitle($title)."\">".number_format($count)." links</a></td>\n";
}

?>

