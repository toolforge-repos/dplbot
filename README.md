# dplbot

A Toolforge tool suite that maintains data about links to disambiguation pages
on enwiki and simplewiki, and a variety of other miscellaneous web tools.
All tools are accessible via https://dplbot.toolforge.org

For help or information, contact the project maintainers at
https://en.wikipedia.org/w/User_talk:DPL_bot

## Authors and acknowledgment

This project includes code originally written by Wikipedia user JaGa,
with contributions from Dispenser and R'n'B.

## License

GNU General Public License v3.0 (see file COPYING for full license terms)
