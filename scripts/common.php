<?php

/*  dplbot - Disambiguation page maintenance tools
    Copyright (C) 2023-2024 Wikipedia users JaGa, Dispenser, R'n'B

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

// Namespaces from includes/Defines.php
define('NS_MAIN', 0);
define('NS_TALK', 1);
define('NS_USER', 2);
define('NS_USER_TALK', 3);
define('NS_PROJECT', 4);
define('NS_PROJECT_TALK', 5);
define('NS_IMAGE', 6);
define('NS_IMAGE_TALK', 7);
define('NS_MEDIAWIKI', 8);
define('NS_MEDIAWIKI_TALK', 9);
define('NS_TEMPLATE', 10);
define('NS_TEMPLATE_TALK', 11);
define('NS_HELP', 12);
define('NS_HELP_TALK', 13);
define('NS_CATEGORY', 14);
define('NS_CATEGORY_TALK', 15);

$HOME_DIR = "/data/project/dplbot"."/dplbot";

function get_db_con($mysql_schema = "enwiki_p", $mysql_server = "enwiki.analytics.db.svc.wikimedia.cloud") {

  if (file_exists("/data/project/dplbot/.my.cnf")) {
    $contents = file_get_contents("/data/project/dplbot/.my.cnf");
    $lines = explode("\n", $contents);
    foreach ($lines AS $line) {
      $line = trim ($line);
      if (substr($line, 0, 8) != "password") {
        continue;
      }
      $line = explode("'", $line);
      $junk = array_shift($line);
      $mysql_password = array_shift($line);
    }
    $mysql_user = "s51290";
  } else {
    print "<h2>Unable to locate password file.</h2><br/><br/>\n";
  }

  if (!$mysql_con = mysqli_connect($mysql_server, $mysql_user, $mysql_password, $mysql_schema)) {
    print "<h2>Database connection error [$mysql_server]. Please try again later.</h2><br/><br/>\n";
    print mysqli_connect_error();
    print "server $mysql_server, user $mysql_user, password ********, schema $mysql_schema\n";
    return;
  }

  return $mysql_con;
}

function allowBots($bot, $text) {
  if (preg_match('/\{\{(nobots|bots\|allow=none|bots\|deny=all|bots\|optout=all|bots\|deny=.*?'.preg_quote($bot,'/').'.*?)\}\}/iS', $text))
    return false;
  if (preg_match('/\{\{(bots\|allow=all|bots\|allow=.*?'.preg_quote($bot,'/').'.*?)\}\}/iS', $text))
    return true;
  if (preg_match('/\{\{(bots\|allow=.*?)\}\}/iS', $text))
    return false;
  return true;
}

# Convert seconds to hours, minutes, seconds
# Don't display seconds unless requested
function convert_time ($time, $use_seconds = false) {
  if (is_numeric($time)) {
    $days = ($time - $time % 86400 ) / 86400;
    $hours = ($time - $days * 86400 - ($time - $days * 86400) % 3600) / 3600;
    $minutes = ($time - $days * 86400 - $hours * 3600 - ($time - $hours * 3600) % 60) / 60;
    $seconds = $time - $days * 86400 - $hours * 3600 - $minutes * 60;
  } else {
    syslog(LOG_WARNING, "Incorrect \$time value $time (".gettype($time).") received in convert_time.");
    return '';
  }

	if ($days == 1) {
		$days = 0;
		$hours += 24;
	}

	if ($seconds < 0) {
		$seconds = 0;
	}

	$result = '';

	if ($use_seconds) {
    if ($days == 0 && $hours == 0 && $minutes == 0)
	    $result = $seconds.($seconds == 1 ? " second" : " seconds");
	  else if ($days == 0) {
		  if ($hours == 0)
		    $result = $minutes.($minutes == 1 ? " minute " : " minutes ").$seconds.($seconds == 1 ? " second" : " seconds");
		  else
		    $result = $hours.($hours == 1 ? " hour " : " hours ").$minutes.($minutes == 1 ? " minute " : " minutes ").$seconds.($seconds == 1 ? " second" : " seconds");
	  } else
		  $result = $days." days ".$hours.($hours == 1 ? " hour " : " hours ").$minutes.($minutes == 1 ? " minute " : " minutes ").$seconds.($seconds == 1 ? " second" : " seconds");
	}
	else {
    if ($days == 0 && $hours == 0 && $minutes == 0)
	    $result = $seconds.($seconds == 1 ? " second" : " seconds");
	  else if ($days == 0) {
		  if ($hours == 0)
		    $result = $minutes.($minutes == 1 ? " minute" : " minutes");
		  else
		    $result = $hours.($hours == 1 ? " hour " : " hours ").$minutes.($minutes == 1 ? " minute" : " minutes");
	  } else
		  $result = $days." days ".$hours.($hours == 1 ? " hour " : " hours ").$minutes.($minutes == 1 ? " minute" : " minutes");
	}

	return $result;
}


function extract_date ($timestamp) {
  $strlen = strlen($timestamp);
  if ($strlen < 10) return "19700101";
  return str_replace('-', '', substr($timestamp, 0, 10));
}


function log_error ($date, $page_name, $from, $error) {
  global $HOME_DIR;
  
  $fp = fopen("$HOME_DIR/logs/error_log.txt", "a");
  fputs($fp, "$date  $page_name  $from  $error\n");
  fclose($fp);
}


function run_count_query ($mysql, $table, $where_clause = "") {

  $sql = "SELECT count(*) AS count
            FROM $table";

  if ($where_clause != "")
    $sql = $sql." WHERE ".$where_clause;

  $result = mysqli_query($mysql, $sql);

  if ($result) {
    $row = mysqli_fetch_assoc($result);
    $count = $row['count'];

    settype($count, "integer");

    return $count;
  }

  return 0;
}


function printNav($limit, $num, $offset, $page_name) {

  if ($offset > 0) {
    $po = $offset - $limit;
    if ($po < 0) $po = 0;
    print "<p>View (<a href=\"$page_name?limit=$limit&amp;offset=$po\">previous ".$limit."</a>) ";
  }
  else
    print "<p>View (previous ".$limit.") ";

  if ($num == $limit + 1) {
    $no = $offset + $limit;
    print "(<a href=\"$page_name?limit=$limit&amp;offset=$no\">next ".$limit."</a>) ";
  }
  else
    print "(next ".$limit.") ";

  print "(<a href=\"$page_name?limit=20&amp;offset=".$offset."\">20</a> ";
  print "| <a href=\"$page_name?limit=50&amp;offset=".$offset."\">50</a> ";
  print "| <a href=\"$page_name?limit=100&amp;offset=".$offset."\">100</a> ";
  print "| <a href=\"$page_name?limit=250&amp;offset=".$offset."\">250</a> ";
  print "| <a href=\"$page_name?limit=500&amp;offset=".$offset."\">500</a>)</p>";
}


function printResults($limit, $res, $num, $offset) {

  if ($num == $limit + 1) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    for ($counter = 1; $counter < $limit + 1; $counter++) {
      $row = mysqli_fetch_assoc($res);
      $title = $row['title'];
      $disp_title = str_replace( '_', ' ', $title );
      print "<li><a href=\"//en.wikipedia.org/wiki/".encodeTitle($title)."\">$disp_title</a><br/>\n";
    }
    print "</ol>\n\n";
  }
  else if ($num > 0) {
    print "<ol start=\"".($offset + 1)."\" class=\"special\">\n";
    while ($row = mysqli_fetch_assoc($res)) {
      $title = $row['title'];
      $disp_title = str_replace( '_', ' ', $title );
      print "<li><a href=\"//en.wikipedia.org/wiki/".encodeTitle($title)."\">$disp_title</a><br/>\n";
    }
    print "</ol>\n\n";
  }
}


function check_limits() {

  $limit = isset($_GET["limit"]) ? intval($_GET["limit"]) : 50;
  $offset = isset($_GET["offset"]) ? intval($_GET["offset"]) : 0;

  if( $limit <= 0 ) $limit = 50;
  if( $limit > 1000 ) $limit = 1000;
  if( $offset < 0 ) $offset = 0;

  return array( $limit, $offset );
}


# Make a string url-friendly
function encodeTitle($title) {

	$title = rawurlencode( $title );
	$title = preg_replace( '/%3[Aa]/', ':', $title );
	$title = preg_replace( '/%2[Ff]/', '/', $title );

	return $title;
}


function write_bot_data($mysql, $table, $base_time, $filename) {

  $sql = "SELECT o_title AS title
            FROM $table, enwiki_p.page
           WHERE o_id = page_id
             AND page_touched < '$base_time'
        ORDER BY title
           LIMIT 5000";

  $result = mysqli_query($mysql, $sql);

  if ($result) {

    $fp = fopen($filename, "w");

    while ($row = mysqli_fetch_assoc($result)) {
      $title = str_replace( '_', ' ', $row['title'] );
      fputs($fp, "[[".$title."]]\n");
    }

    fclose($fp);

    mysqli_free_result($result);
  }
  else {

    $fp = fopen($filename, "w");
    fclose($fp);

    log_error(date("F j G:i", time()), "common.php", "write_bot_data", mysqli_error($mysql));
  }

}


// See what the replication lag is between Toolforge replica and the live wiki
function get_replag($mysql, $db="enwiki_p") {

  $sql = "SELECT TIMESTAMPDIFF(SECOND, rc_timestamp, NOW()) AS replag
            FROM $db.recentchanges
	   ORDER BY rc_timestamp DESC
           LIMIT 1";

  $result = mysqli_query($mysql, $sql);

  if ($result == false) {
    return "ERROR on get_replag(): ".mysqli_error($mysql);
  }
  else {
    $row = mysqli_fetch_assoc($result);
    $replag = $row['replag'];

    if ($replag < 0) return 0;
    else return $replag;
  }
}


// See what the replication lag is between Toolforge replica and live wiki
function get_simplewiki_replag($mysql) {

	$sql = "SELECT TIMESTAMPDIFF(SECOND, rc_timestamp, NOW()) AS replag
	          FROM simplewiki_p.recentchanges
	      ORDER BY rc_timestamp DESC
	         LIMIT 1";

  $result = mysqli_query($mysql, $sql);

  if ($result == false) {
    return "ERROR on get_simplewiki_replag(): ".mysqli_error($mysql);
  }
  else {
    $row = mysqli_fetch_assoc($result);
    $replag = $row['replag'];

    if ($replag < 0) return 0;
    else return $replag;
  }
}


function print_header($page_title, $h1_header = "", $css = "css/main.css") {
  header_start($page_title, $css);
  header_end($page_title, $h1_header);
}


function print_js_header($page_title, $h1_header = "", $css = "css/main.css") {
  header_start($page_title, $css);
  header_js(); // Implemented in page
  header_end($page_title, $h1_header);
}

function header_start ($page_title, $css) {

  print "<!DOCTYPE html>\n";
  print "<html>\n";
  print "<head>\n";
  print "<title>$page_title - Wikimedia Toolforge - dplbot</title>\n";
  print "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n";
  print "<meta name=\"robots\" content=\"noindex,nofollow\" />\n";
  print "<link rel=\"stylesheet\" type=\"text/css\" href=\"$css\" />\n";
  print "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n";
}

function header_end ($page_title, $h1_header) {

  print "</head>\n\n";

  print "<body>\n\n";

  // Optional message box for notices to users
  if (false) {
    print '<div style="text-align: center; margin: 1em 15%; ';
    print 'background-color: #fafad2; padding: 10px; border: 1px solid;">
    DPL bot is currently undergoing some maintenance. This may cause short-term
    service outages and/or delays in data updates. If a page is unavailable, 
    please try again after a short wait. The management apologizes for any
    inconvenience.</div>';
  }
  if ($h1_header == "")
    print "<h1>$page_title</h1>\n\n";
  else if ($h1_header != "NOH1")
    print "<h1>$h1_header</h1>\n\n";
}


function print_footer() {

  print "<hr/><br/>\n";
  print "<span id=\"tsbanner\">\n";
  print "Contact: <a href=\"//en.wikipedia.org/wiki/User_talk:DPL bot\">the bot discussion page</a> on the English Wikipedia.<br />
Source code available <a href=\"https://gitlab.wikimedia.org/toolforge-repos/dplbot\">here</a>.\n";
  print "</span>\n\n";
  print "</body>\n";
  print "</html>";
}


function escape_chars($string) {
  $fc = substr($string, 0, 1);
  $fc = escape_all($fc);
  $string = substr($string, 1);
  $string = escape_all($string);
  $string = "[".$fc."|".strtolower($fc)."]".$string;
  $pattern = "/[ _]/";
  $string = preg_replace($pattern, "[ _]*", $string);
  return $string;
}


function escape_all($string) {
  $string = str_replace("/", "\/", $string);
  $string = str_replace("^", "\^", $string);
  $string = str_replace("$", "\$", $string);
  $string = str_replace(".", "\.", $string);
  $string = str_replace("[", "\[", $string);
  $string = str_replace("]", "\]", $string);
  $string = str_replace("|", "\|", $string);
  $string = str_replace("(", "\(", $string);
  $string = str_replace(")", "\)", $string);
  $string = str_replace("?", "\?", $string);
  $string = str_replace("*", "\*", $string);
  $string = str_replace("+", "\+", $string);
  $string = str_replace("=", "\=", $string);
  $string = str_replace("{", "\{", $string);
  $string = str_replace("}", "\}", $string);
  return $string;
}


function endswith ( $fullstring, $endstring ) {
# return true if $endstring matches the end of $fullstring

    return ( substr( $fullstring, -strlen( $endstring )) == $endstring );
}


function startswith ( $fullstring, $prefix ) {
# return true if $prefix matches the beginning of $fullstring

    return ( substr( $fullstring, 0, strlen( $prefix )) == $prefix );
}
