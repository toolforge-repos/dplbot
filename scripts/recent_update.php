<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$is_error = false;

$fp = fopen("$HOME_DIR/status/ru_results.php", "w");

fputs($fp, "<?PHP\n\n");

$tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

$time_begin = time();
$begin_run_wiki = date("YmdHis", $time_begin);
$begin_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_begin));

fputs($fp, "$"."ru_begin_run = $time_begin;\n" );
fputs($fp, "$"."ru_begin_run_wiki = \"$begin_run_wiki\";\n" );
fputs($fp, "$"."ru_begin_run_str = \"$begin_run_str\";\n\n" );

$tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
run_query($tooldb, "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

$ru_count1 = get_rd_count($tooldb, $fp);
mysqli_close($tooldb);

ru_main();

# Check recent_dabs again. If its unfixed count has changed, that can only mean a dab update has completed during execution.
$tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$ru_count2 = get_rd_count($tooldb, $fp);
mysqli_close($tooldb);

if ($ru_count1 != $ru_count2) {
  if (!$is_error) {
    log_error(date("F j G:i", time()), "recent_update.php", "edit conflict", "$ru_count1, then $ru_count2");
    fputs($fp, "\n$"."ru_error = \"Edit conflict: Before, recent_dabs had $ru_count1 rows. Now it has $ru_count2.  Aborting update.\";\n\n?>\n");
    fclose($fp);
    $is_error = true;
  }
}
else {
  ru_cleanup_recent_dabs();
}

ru_cleanup();

if (!$is_error) {
  $time_finish = time();

  $finish_run_wiki = date("YmdHis", $time_finish);
  $finish_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_finish));
  $total_time_str = convert_time($time_finish - $time_begin);

  fputs($fp, "\n$"."ru_finish_run = $time_finish;\n" );
  fputs($fp, "$"."ru_finish_run_wiki = \"$finish_run_wiki\";\n" );
  fputs($fp, "$"."ru_finish_run_str = \"$finish_run_str\";\n\n" );

  fputs($fp, "$"."ru_total_time_str = \"$total_time_str\";\n\n" );

  fputs($fp, "\n?>\n");
  fclose($fp);

  if (file_exists("$HOME_DIR/status/ru_last_good_run.php"))
    unlink("$HOME_DIR/status/ru_last_good_run.php");

  rename("$HOME_DIR/status/ru_results.php", "$HOME_DIR/status/ru_last_good_run.php");
}

else {
  log_error(date("F j G:i", time()), "recent_update.php", "mysql connect", mysqli_connect_error());
  fputs($fp, "\n$"."ru_error = \"".mysqli_connect_error()."\";\n\n?>\n");
  fclose($fp);
}


function ru_main ( ) {
  global $fp, $wikidb, $is_error;

  $begin = time();
  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  submit_query($tooldb,
    "CREATE TABLE IF NOT EXISTS recent_dabs (
       type tinyint NOT NULL default '-1',
       article_id int unsigned NOT NULL default '0',
       article_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       redirect_id int unsigned NOT NULL default '0',
       redirect_title varchar(255) binary NOT NULL default '',
       template_id int unsigned NOT NULL default '0',
       template_title varchar(255) binary NULL,
       rev_id int unsigned NOT NULL default 0,
       user varchar(255) binary NOT NULL default '',
       is_reg tinyint unsigned NOT NULL default '0',
       is_new tinyint unsigned NOT NULL default '0',
       is_msg tinyint unsigned NOT NULL default '0',
       is_fix tinyint unsigned NOT NULL default '0',
       msg_date DATETIME NULL,
       dab_date DATETIME NULL,
       UNIQUE INDEX u_rds (article_id, dab_id, redirect_id, template_id, type, dab_date)
     ) ENGINE=InnoDB"
  );

  $recent_dabs = []; // id => title, dab pages loaded from the recent_dabs table
  $ru_redirects = []; // redirect id => target id
  $batches = [];
  $batch = "";
  $count = 0;

  $rd_res = submit_query($tooldb, "SELECT DISTINCT dab_id FROM recent_dabs WHERE is_fix = 0");
  if ($is_error) return;
  while ($rd_row = mysqli_fetch_assoc($rd_res)) {
    if ($count > 0)
      $batch .= ",";
    $batch .= $rd_row['dab_id'];
    $count += 1;
    if ($count > 128) {
      $batches[] = $batch;
      $batch = "";
      $count = 0;
    }
  }
  if ($count > 0)
    $batches[] = $batch;
  mysqli_free_result($rd_res);
  mysqli_close($tooldb);
  
  foreach ($batches as $bat) {
    # validate each page still exists and is still a dab page
    $sql1 = "SELECT page_id AS dab_id, page_title AS dab_title
              FROM page
             WHERE page_id IN ($bat)
               AND page_namespace = 0
               AND page_is_redirect = 0
               AND EXISTS (
                    SELECT * FROM page_props
                     WHERE page_id = pp_page
                       AND pp_propname = 'disambiguation'
                   )
           ";
    $result1 = submit_query($wikidb, $sql1);
    if ($is_error) return;
    while ($row = mysqli_fetch_assoc($result1)) {
      $dab_id = (int) $row['dab_id'];
      $dab_title = $row['dab_title'];
      $dtitle = mysqli_real_escape_string($wikidb, $dab_title);
      $recent_dabs[$dab_id] = $dab_title;
      $ru_redirects[$dab_id] = $dab_id;
      # retrieve all redirects to this disambiguation page
      $sql2 = "SELECT page_id, page_title
                 FROM page, redirect
                WHERE rd_namespace = 0
                  AND rd_from = $dab_id
                  AND rd_title = '$dtitle'
                  AND page_id = rd_from
                  AND page_namespace = 0
                  AND page_is_redirect = 1";

      $res2 = submit_query($wikidb, $sql2);
      if ($is_error) return;
      while ($row2 = mysqli_fetch_assoc($res2)) {
        if (endswith ($row2['page_title'], "_(disambiguation)")
             || endswith ($row2['page_title'], "_(number)"))
          continue;
        $ru_redirects[(int) $row2['page_id']] = $dab_id;
        $recent_dabs[(int) $row2['page_id']] = $row2['page_title'];
      }
      mysqli_free_result($res2);
    }
    mysqli_free_result($result1);
  }

  fputs($fp, "$"."ru_proc_results['ru_prep_for_fetch'] = \"".convert_time(time() - $begin)."\";\n");

  # begin ru2_create_ru_pl
  $begin = time();

  # retrieve all article and template links to identified dab titles
  $ru_pagelinks = [];
  $ru_template_dab_links = [];
  $ru_template_ids = [];

  foreach ($recent_dabs as $dab_id => $dabtitle) {
    if ( endswith ($dabtitle, "_(disambiguation)")
         || endswith ($dabtitle, "_(number)") )
      continue;
    $dtitle = mysqli_real_escape_string($wikidb, $dabtitle);
    $sql = "SELECT page_id, page_namespace, page_title
              FROM page, pagelinks, linktarget
             WHERE lt_namespace = 0
               AND page_namespace IN (0, 10)
               AND page_is_redirect = 0
               AND page_id = pl_from
               AND pl_target_id = lt_id
               AND lt_title = '$dtitle'
    ";
    $result = submit_query($wikidb, $sql);
    if ($is_error) return;
    while ($row = mysqli_fetch_assoc($result)) {
      $pl_from = (int) $row['page_id'];
      if ($pl_from == $dab_id || $pl_from == $ru_redirects[$dab_id]) {
        # self-referential link
        continue;
      }
      if ($row['page_namespace'] == 0) {
        $ru_pagelinks[$pl_from.",".$dab_id] = array (
                'pl_from' => $pl_from,
                'pl_title' => $row['page_title'],
                'pl_to' => $dab_id,
                'pl_r' => $ru_redirects[$dab_id]
        );
      } else {
        $ru_template_dab_links[$pl_from.",".$dab_id] = array (
                't_id' => $pl_from,
                't_title' => $row['page_title'],
                'dab_id' => $dab_id,
                'r_id' => $ru_redirects[$dab_id]
        );
        $ru_template_ids[$pl_from] = $row['page_title'];
      }
    }
    mysqli_free_result($result);
  }

  fputs($fp, "$"."ru_proc_results['ru_create_ru_pl'] = \"".convert_time(time() - $begin)."\";\n");

  # begin remove_unlinked_templates
  $begin = time();
  #
  # Purpose: Delete templates that are not used by mainspace
  #          non-redirect articles
  #
  # 1: Loop through all templates that link to a dab
  #
  # 2: Loop through all pages that transclude this template
  #
  # 3: If any one transcluding page is a non-redirect
  #    mainspace article, put the template in the keep list
  #

  $keep_templates = [];
  foreach ($ru_template_ids as $t_id => $t_title) {
    $ttitle = mysqli_real_escape_string($wikidb, $t_title);
    $sql = "SELECT tl_from
              FROM templatelinks, linktarget
             WHERE tl_target_id = lt_id
               AND lt_title = '$ttitle'
               AND lt_namespace = 10 
               AND EXISTS (
                           SELECT * FROM page
                            WHERE page_id = tl_from
                              AND page_namespace = 0
                              AND page_is_redirect = 0
                          )
            ";
    $res = mysqli_query($wikidb, $sql);
    if ($res && mysqli_num_rows($res) > 0) {
      # we don't care about the results as long as there are one or more
      $keep_templates[$t_id] = $t_title;
    }
    if ($res) mysqli_free_result($res);
  }

  # remove any non-transcluded template links
  foreach ($ru_template_dab_links as $key => $tlink) {
    if (! isset($keep_templates[$tlink['t_id']]))
      unset($ru_template_dab_links[$key]);
  }

  fputs($fp, "$"."ru_proc_results['ru_remove_unlinked_templates'] = \"".convert_time(time() - $begin)."\";\n");

  # start find_noinclude_links
  $begin = time();
  #
  # Purpose: Delete templates that have their dab links
  #          in a section of the template (documentation)
  #          that does not get transcluded into the article
  #
  # 1: Loop through each template that links to a
  #    dab (from the newly pared-down list)
  #
  # 2: Loop through up to 4 articles that transclude
  #    this template
  #
  # 3: If any one of these articles is not also in
  #    ru_pagelinks, add it to the delete list
  #

  foreach ($ru_template_dab_links as $tkey => $tlink) {
    $ttitle = mysqli_real_escape_string($wikidb, $tlink['t_title']);
    $sql = "SELECT page_id
              FROM templatelinks, linktarget, page
             WHERE tl_target_id = lt_id
               AND lt_title = '$ttitle'
               AND lt_namespace = 10
               AND tl_from = page_id
               AND page_namespace = 0
               AND page_is_redirect = 0
               AND page_id <> {$tlink['dab_id']}
             LIMIT 4
           ";
    $res = submit_query($wikidb, $sql);
    if ($is_error) return;
    $flag = false;
    while ($row = mysqli_fetch_assoc($res)) {
      if (! isset($ru_pagelinks[$row['page_id'].",".$tlink['dab_id']])) {
        # found a transcluding article that is not in ru_pagelinks
        $flag = true;
        break;
      }
    }
    mysqli_free_result($res);
    if ($flag) {
      unset($ru_template_dab_links[$tkey]);
    }
  }

  fputs($fp, "$"."ru_proc_results['ru_find_noinclude_links'] = \"".convert_time(time() - $begin)."\";\n");

  # begin create_ru_dab_links
  $begin = time();

  # get all article-to-dab-via-template links
  $ru_article_template_links = [];

  foreach ($ru_template_dab_links as $tlink) {
    foreach ($ru_pagelinks as $plink) {
      if ($plink['pl_to'] == $tlink['dab_id']) {
        $ttitle = mysqli_real_escape_string($wikidb, $tlink['t_title']);
        $sql = "
          SELECT page_id, page_title
            FROM templatelinks, linktarget, page
           WHERE tl_target_id = lt_id
             AND lt_title = '$ttitle'
             AND lt_namespace = 10
             AND tl_from = page_id
             AND page_id = {$plink['pl_from']}
             AND page_namespace = 0
             AND page_is_redirect = 0
        ";
        $res = submit_query($wikidb, $sql);
        if ($is_error) return;
        while ($row = mysqli_fetch_assoc($res)) {
          $ru_article_template_links[$row['page_id'].",".$tlink['dab_id']] = array (
              'article_id' => (int) $row['page_id'],
              'article_title' => $row['page_title'],
              'template_id' => $tlink['t_id'],
              'template_title' => $tlink['t_title'],
              'dab_id' => $tlink['dab_id'],
              'dab_title' => $recent_dabs[$tlink['dab_id']]
          );
        }
        mysqli_free_result($res);
      }
    }
  }

  $tooldb = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  submit_query($tooldb, "DROP TABLE IF EXISTS ru_dab_links");

  submit_query($tooldb, "
    CREATE TABLE ru_dab_links (
      article_id int unsigned NOT NULL default '0',
      article_title varchar(255) binary NOT NULL default '',
      dab_id int unsigned NOT NULL default '0',
      dab_title varchar(255) binary NOT NULL default '',
      redirect_id int unsigned NOT NULL default '0',
      redirect_title varchar(255) binary NOT NULL default '',
      template_id int unsigned NULL,
      template_title varchar(255) binary NULL,
      INDEX (article_id),
      INDEX (dab_id),
      INDEX (template_id)
    ) ENGINE=InnoDB
  ");

  # populate ru_dab_links

  foreach ($ru_pagelinks as $plink) {
    $article_id = $plink['pl_from'];
    $atitle = mysqli_real_escape_string($tooldb, $plink['pl_title']);
    $dab_id = $plink['pl_to'];
    $dtitle = mysqli_real_escape_string($tooldb, $recent_dabs[$dab_id]);
    $redirect_id = $plink['pl_r'];
    $rtitle = mysqli_real_escape_string($tooldb, $recent_dabs[$redirect_id]);
    $template_id = (isset($ru_article_template_links[$article_id.",".$dab_id]) ?
                   $ru_article_template_links[$article_id.",".$dab_id]['template_id'] : 0);
    $template_title = (isset($ru_article_template_links[$article_id.",".$dab_id]) ?
                   $ru_article_template_links[$article_id.",".$dab_id]['template_title'] : '');
    $ttitle = mysqli_real_escape_string($tooldb, $template_title);
    $sql = "INSERT INTO ru_dab_links
                   (article_id, article_title, dab_id, dab_title,
                    redirect_id, redirect_title, template_id, template_title)
            VALUES ($article_id, '$atitle', $dab_id, '$dtitle',
                    $redirect_id, '$rtitle', $template_id, '$ttitle')";
    submit_query($tooldb, $sql);
  }
  mysqli_commit($tooldb);
  mysqli_close($tooldb);

  fputs($fp, "$"."ru_proc_results['ru_create_ru_dab_links'] = \"".convert_time(time() - $begin)."\";\n");
}


function ru_cleanup_recent_dabs ( ) {
  global $tooldb, $fp, $is_error;

  if ($is_error) return;  
  $begin = time();
  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  submit_query($tooldb, "
    UPDATE recent_dabs
       SET is_fix = 1
     WHERE NOT EXISTS
           (
            SELECT 1
              FROM ru_dab_links b
             WHERE recent_dabs.dab_id = b.dab_id
               AND recent_dabs.article_id = b.article_id
               AND recent_dabs.redirect_id = b.redirect_id
               AND recent_dabs.template_id = b.template_id
           )
  ");

  submit_query($tooldb, "DROP TABLE IF EXISTS ru_dab_links");

  if ($is_error) return;  
  mysqli_close($tooldb);
  fputs($fp, "$"."ru_proc_results['ru_cleanup_recent_dabs'] = \"".convert_time(time() - $begin)."\";\n");

}


function ru_cleanup ( ) {

  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS ru_dab_links");
  mysqli_close($tooldb);

}


function submit_query ($server, $sql) {
  global $fp, $is_error, $tooldb;

  if ($is_error)
    return false;
  
  $result = mysqli_query($server, $sql);

  if ($result == false) {
    log_error(date("F j G:i", $begin), "recent_update.php", $sql, mysqli_error($server));
    fputs($fp, "\n$"."ru_error = \"$sql: ".mysqli_error($server)."\";\n");
    fclose($fp);
    ru_cleanup();
    die(1);
  }
  return $result;
}


function run_query ($server, $sql) {
  global $fp, $is_error, $tooldb;

  if (!$is_error) {
    $begin = time();

    $result = submit_query($server, $sql);

    if ($result) {
      $duration = time() - $begin;

#      if ($duration > 1200)
#        log_slow_query(date("F j G:i", $begin), "recent_update.php", $sql, convert_time($duration));

      fputs($fp, "$"."ru_proc_results['$sql'] = \"".convert_time($duration)."\";\n");
    }
  }
  return $result;
}


function get_rd_count ($tooldb, $fp) {
  global $is_error;

  if ($is_error) 
    return 0;
  
  $sql = "SELECT count(*) AS count
            FROM recent_dabs
           WHERE is_fix = 0";

  $result = submit_query($tooldb, $sql);

  $row = mysqli_fetch_assoc($result);
  $count = (int) $row['count'];
  return $count;
}


function get_db_con_local($schema, $server) {
  global $fp;

  $conn = get_db_con($schema, $server);
  if (!$conn) {
    log_error(date("F j G:i", time()), "recent_update.php", "$server connect", mysqli_connect_error());
    fputs($fp, "\n$"."ru_error = \"".mysqli_connect_error()."\";\n\n?>\n");
    fclose($fp);
    die(1);
  }
  return $conn;
}


?>
