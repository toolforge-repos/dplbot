<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$fp = fopen("$HOME_DIR/status/mv_results.php", "w");
fputs($fp, "<?PHP\n\n");

$tooldb = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
if (! $tooldb) {
  log_error(date("F j G:i", time()), "mv_procs.php", "toolsdb connect", mysqli_connect_error());
  fputs($fp, "\n$"."dab_error = \"".mysqli_connect_error()."\";\n\n?>\n");
  fclose($fp);
  die(1);
}

$wikidb = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

if (! $wikidb) {
  log_error(date("F j G:i", time()), "mv_procs.php", "enwiki_p connect", mysqli_connect_error());
  fputs($fp, "\n$"."dab_error = \"".mysqli_connect_error()."\";\n\n?>\n");
  fclose($fp);
  die(1);
}

$time_begin = time();
$begin_run_wiki = date("YmdHis", $time_begin);
$begin_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_begin));

fputs($fp, "$"."mv_begin_run = $time_begin;\n" );
fputs($fp, "$"."mv_begin_run_wiki = \"$begin_run_wiki\";\n" );
fputs($fp, "$"."mv_begin_run_str = \"$begin_run_str\";\n\n" );

run_mv_query($tooldb, "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
run_mv_query($tooldb, "SET @@max_heap_table_size=268435456");
run_mv_query($tooldb, "SET @@tmp_table_size=268435456");
mv_prelim();
scan_move_log();
mv_cleanup();

$time_finish = time();

$finish_run_wiki = date("YmdHis", $time_finish);
$finish_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_finish));
$total_time_str = convert_time($time_finish - $time_begin);

fputs($fp, "\n$"."mv_finish_run = $time_finish;\n" );
fputs($fp, "$"."mv_finish_run_wiki = \"$finish_run_wiki\";\n" );
fputs($fp, "$"."mv_finish_run_str = \"$finish_run_str\";\n\n" );

fputs($fp, "$"."mv_total_time_str = \"$total_time_str\";\n\n" );

mysqli_close($tooldb);

fputs($fp, "\n?>\n");
fclose($fp);

if (file_exists("$HOME_DIR/status/mv_last_good_run.php"))
  unlink("$HOME_DIR/status/mv_last_good_run.php");

rename("$HOME_DIR/status/mv_results.php", "$HOME_DIR/status/mv_last_good_run.php");


function mv_prelim ( ) {
  global $fp, $tooldb;

  submit_query($tooldb,
    "CREATE TABLE IF NOT EXISTS admin_list (
       admin varchar(255) binary NOT NULL default '',
       PRIMARY KEY (admin)
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($tooldb,
    "CREATE TABLE IF NOT EXISTS move_log (
       mv_date DATETIME NULL,
       mv_user varchar(255) binary NULL,
       mv_is_admin BOOLEAN NOT NULL default 0,
       mv_from varchar(255) binary NOT NULL default '',
       mv_to varchar(255) binary NOT NULL default '',
       PRIMARY KEY (mv_from, mv_to)
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

}

function scan_move_log () {
  global $tooldb, $wikidb, $fp, $HOME_DIR;

  $begin = time();
  $allNS = array(
       1 => 'Talk:',
       2 => 'User:',
       3 => 'User_talk:',
       4 => 'Wikipedia:',
       5 => 'Wikipedia_talk:',
       6 => 'Project:',
       7 => 'Project_talk:',
       8 => 'MediaWiki:',
       9 => 'MediaWiki_talk:',
      10 => 'Template:',
      11 => 'Template_talk:',
      12 => 'Help:',
      13 => 'Help_talk:',
      14 => 'Category:',
      15 => 'Category_talk:',
     100 => 'Portal:',
     101 => 'Portal_talk:',
     108 => 'Book:',
     109 => 'Book_talk:',
     118 => 'Draft:',
     119 => 'Draft_talk:',
     446 => 'Education_Program:',
     447 => 'Education_Program_talk:',
     710 => 'TimedText:',
     711 => 'TimedText_talk:',
     828 => 'Module:',
     829 => 'Module_talk:',
    2300 => 'Gadget:',
    2301 => 'Gadget_talk:',
    2302 => 'Gadget_definition:',
    2303 => 'Gadget_definition_talk:'
  );

  if (file_exists("$HOME_DIR/status/mv_last_good_run.php")) {
    $last_run = filemtime("$HOME_DIR/status/mv_last_good_run.php");
  } else {
    $last_run = time() - 24*60*60; // default to last 24 hours
  }
  $last_ts = date("YmdHis", $last_run);

  # iterate through all move log entries since the last run

  #   note: log_namespace gives the namespace that the page was moved
  #         from; we have to parse the log params to find the namespace it
  #         was moved to.

  $log_res = submit_query($wikidb, "
      SELECT log_page,
             log_timestamp,
             actor_name as log_user_text,
             log_namespace,
             log_title,
             log_params
        FROM logging, actor_logging, page
       WHERE log_type='move'
         AND log_timestamp >= '$last_ts'
         AND actor_id = log_actor
         AND page_id = log_page
  ");

  while ($logitem = mysqli_fetch_assoc($log_res)) {
    $date = $logitem['log_timestamp'];
    $phpDate = strtotime($date);
    $sqlDate = date('Y-m-d H-i-s', $phpDate);

    $userName = $logitem['log_user_text'];

    $from_ns = $logitem['log_namespace'];
    $from_title = $logitem['log_title'];
    if ($from_ns > 0) {
      $from = $allNS[$from_ns].$from_title;
    } else {
      $from = $from_title;
    }

    $page_id = $logitem['log_page'];
      // note: we can't just look for this id in the page table,
      //       because page could have been moved multiple times
    $log_params = unserialize($logitem['log_params']);

    if (!isset($log_params['4::target'])) {
      print ("ERR log_params: ");
      var_dump ($log_params);
      continue;
    }
    $to = $log_params['4::target'];
      // this is full title of target, including namespace
    $mainNS = true;
    $pos = strpos($to, ":");
    if ($pos !== false) {
      foreach ($allNS as $ns) {
        if (startswith ($to, $ns)) {
          $mainNS = false;
          break;
        }
      }
    }

    if ($mainNS) {
      $esc_user = mysqli_real_escape_string($tooldb, $userName);
      $esc_from = mysqli_real_escape_string($tooldb, $from);
      $esc_to = mysqli_real_escape_string($tooldb, str_replace(' ', '_', $to));

      $is_admin = 0;

      $sql = "SELECT * FROM admin_list where admin = '$esc_user'";

      $result = submit_query($tooldb, $sql);
      if ($result && mysqli_num_rows($result) > 0) {
        $is_admin = 1;
      }

      $sql = "INSERT IGNORE INTO move_log (mv_date, mv_user, mv_is_admin, mv_from, mv_to)
                     VALUES ('$sqlDate', '$esc_user', $is_admin, '$esc_from', '$esc_to')";
      submit_query($tooldb, $sql);

      $sql = "INSERT IGNORE INTO all_moves (mv_date, mv_user, mv_is_admin, mv_from, mv_to)
                     VALUES ('$sqlDate', '$esc_user', $is_admin, '$esc_from', '$esc_to')";

      submit_query($tooldb, $sql);
    }
  }
  fputs($fp, "$"."mv_proc_results['scan_move_log'] = \"".convert_time(time() - $begin)."\";\n");
}


function mv_cleanup ( ) {
  global $tooldb, $fp;

  run_mv_query($tooldb,
    "DELETE FROM move_log
           WHERE mv_date < DATE_SUB(SYSDATE(), INTERVAL 31 DAY)",
    "mv_cleanup");
}


function submit_query ($server, $query) {
  global $fp;

  $result = mysqli_query($server, $query);

  if ($result == false) {
    log_error(date("F j G:i", $begin), "mv_procs.php", $query, mysqli_error($server));
    fputs($fp, "\n$"."mv_error = \"$query: ".mysqli_error($server)."\";\n");
    fclose($fp);
    die(1);
  }
  return $result;
}


function run_mv_query ($server, $sql, $nick = "") {
  global $fp;

  if ($nick == "")
    $nick = $sql;

  $begin = time();
  $result = submit_query($server, $sql);
  fputs($fp, "$"."mv_proc_results['$nick'] = \"".convert_time(time() - $begin)."\";\n");
}

?>
