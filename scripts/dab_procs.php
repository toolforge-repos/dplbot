<?php

// Define different dablink creation types
define('NOT_FOUND', 0);
define('RETARGET_DAB', 1);
define('CONVERT_TO_DAB', 2);
define('MOVE_DAB', 3);
define('TEMPLATE_DAB', 4);
define('ARTICLE_DAB', 5);
define('INT_DAB', 6);
define('RVV_DAB', 7);

define('EXCESSIVE', 7); // threshold for excessive dab links in an article

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$config="$HOME_DIR/.dplcredentials.cfg";
require ("SxWiki.php");

$sx=new SxWiki;
$sx->configFile=$config;
$sx->autoLogin = false;

$logging_enabled = true;

$is_error = false;

$last_run_begin = 9999999999;
$last_run_begin_wiki = "99999999999999";

if (file_exists("$HOME_DIR/status/dab_last_good_run.php")) {
  include_once("$HOME_DIR/status/dab_last_good_run.php");
  $last_run_begin = $dab_begin_run_lag;
  $last_run_begin_wiki = $dab_begin_run_wiki_lag;
}

$fp = fopen("$HOME_DIR/status/dab_results.php", "w");

if ($logging_enabled) $lp = fopen("$HOME_DIR/logs/dab_results.log", "w");
else $lp = "";

fputs($fp, "<?PHP\n\n");

$wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
$time_begin = time();
$time_begin_lag = $time_begin - get_replag($wikidb);
mysqli_close($wikidb);
$begin_run_wiki = date("YmdHis", $time_begin);
$begin_run_wiki_lag = date("YmdHis", $time_begin_lag);
$begin_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_begin));

fputs($fp, "$"."dab_last_begin_run_wiki_lag = \"$last_run_begin_wiki\";\n\n" );

fputs($fp, "$"."dab_begin_run = $time_begin;\n" );
fputs($fp, "$"."dab_begin_run_lag = $time_begin_lag;\n" );
fputs($fp, "$"."dab_begin_run_wiki = \"$begin_run_wiki\";\n" );
fputs($fp, "$"."dab_begin_run_wiki_lag = \"$begin_run_wiki_lag\";\n" );
fputs($fp, "$"."dab_begin_run_str = \"$begin_run_str\";\n\n" );

initialize_tables();
fetch_dab_pages();
create_results_list();
post_results_list();

$cleanup_begin = time();
dab_cleanup();

if (!$is_error) {

  $time_finish = time();
  fputs($fp, "$"."dab_proc_results['dab_cleanup'] = \"".convert_time($time_finish - $cleanup_begin)."\";\n");

  $finish_run_wiki = date("YmdHis", $time_finish);
  $finish_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_finish));
  $total_time_str = convert_time($time_finish - $time_begin);

  fputs($fp, "\n$"."dab_finish_run = $time_finish;\n" );
  fputs($fp, "$"."dab_finish_run_wiki = \"$finish_run_wiki\";\n" );
  fputs($fp, "$"."dab_finish_run_str = \"$finish_run_str\";\n\n" );

  fputs($fp, "$"."dab_total_time_str = \"$total_time_str\";\n\n" );

  fputs($fp, "\n?>\n");
  fclose($fp);

  if (file_exists("$HOME_DIR/status/dab_last_good_run.php"))
    unlink("$HOME_DIR/status/dab_last_good_run.php");

  rename("$HOME_DIR/status/dab_results.php", "$HOME_DIR/status/dab_last_good_run.php");
}


function submit_query ($server, $sql) {

  global $fp, $is_error;

  if ($is_error) {
    return;
  }

  try {
    $result = mysqli_query($server, $sql);
  } catch ( mysqli_sql_exception $ex ) {
      $result = false;
  }

  if ($result == false) {
    log_error(date("F j G:i", time()), "dab_procs.php", $sql, mysqli_error($server));
    fputs($fp, "\n$"."dab_error = \"$sql: ".mysqli_error($server)."\";\n");
    fclose($fp);
    $is_error = true;
  }

  return $result;

}


function submit_and_log_query ($server, $sql, $nick = "") {

  global $fp, $is_error;

  if ($is_error) {
    return;
  }

  if ($nick == "")
    $nick = $sql;

  $begin = time();

  $result = submit_query($server, $sql);

  if (! $is_error) {
    $duration = time() - $begin;
    fputs($fp,
      "$"."dab_proc_results['$nick'] = \"".convert_time($duration)."\";\n");
  }

  return $result;

}


function initialize_tables () {

  global $fp, $is_error;

  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
  submit_and_log_query($tooldb, "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

  $begin = time();
  submit_query($tooldb, "DROP TABLE IF EXISTS t_dabs_without_hats");
  submit_query($tooldb, "
    CREATE TABLE t_dabs_without_hats (
      dab_id int(10) unsigned NOT NULL default '0',
      dab_title varchar(255) binary NOT NULL default '',
      PRIMARY KEY pk_dwh (dab_id)
    ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC
  ");

  submit_query($tooldb, "DROP TABLE IF EXISTS t_all_dabs");
  submit_query($tooldb, "
    CREATE TABLE t_all_dabs (
      dab_id int(10) unsigned NOT NULL default '0',
      dab_title varchar(255) binary NOT NULL default '',
      PRIMARY KEY pk_dab (dab_id),
      UNIQUE KEY u_dab (dab_title)
    ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC
  ");

  submit_query($tooldb,
    "CREATE TABLE IF NOT EXISTS recent_dabs (
      type tinyint NOT NULL default '-1',
      article_id int unsigned NOT NULL default '0',
      article_title varchar(255) binary NOT NULL default '',
      dab_id int unsigned NOT NULL default '0',
      dab_title varchar(255) binary NOT NULL default '',
      redirect_id int unsigned NOT NULL default '0',
      redirect_title varchar(255) binary NOT NULL default '',
      template_id int unsigned NOT NULL default '0',
      template_title varchar(255) binary NULL,
      rev_id int unsigned NOT NULL default 0,
      user varchar(255) binary NOT NULL default '',
      is_reg tinyint unsigned NOT NULL default '0',
      is_new tinyint unsigned NOT NULL default '0',
      is_msg tinyint unsigned NOT NULL default '0',
      is_fix tinyint unsigned NOT NULL default '0',
      msg_date DATETIME NULL,
      dab_date DATETIME NULL,
      UNIQUE INDEX u_rds (article_id, dab_id, redirect_id, template_id, type, dab_date)
    ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC
  ");

  submit_query($tooldb,
    "DELETE FROM recent_dabs
           WHERE dab_date < DATE_SUB(SYSDATE(), INTERVAL 31 DAY)
  ");

  submit_query($tooldb, "DROP TABLE IF EXISTS t_template_dab_links");
  submit_query($tooldb,
    "CREATE TABLE t_template_dab_links (
      t_id int unsigned NOT NULL default '0',
      t_title varchar(255) binary NOT NULL default '',
      dab_id int unsigned NOT NULL default '0',
      dab_title varchar(255) binary NOT NULL default '',
      r_title varchar(255) binary NOT NULL default ''
    ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS all_dab_links_part2");
  submit_query($tooldb, "
    CREATE TABLE all_dab_links_part2 (
      article_id int unsigned NOT NULL default '0',
      article_title varchar(255) binary NOT NULL default '',
      dab_id int unsigned NOT NULL default '0',
      dab_title varchar(255) binary NOT NULL default '',
      redirect_id int unsigned NOT NULL default '0',
      redirect_title varchar(255) binary NOT NULL default '',
      template_id int unsigned NULL,
      template_title varchar(255) binary NULL,
      INDEX (article_id),
      INDEX (dab_id),
      INDEX (template_id)
    ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS t_dab_template_tc_count");
  submit_query($tooldb,
    "CREATE TABLE t_dab_template_tc_count (
       tc_id int unsigned NOT NULL default '0',
       tc_title varchar(255) binary NOT NULL default '',
       tc_amnt int unsigned NOT NULL default '0'
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS t_dab_link_count");
  submit_query($tooldb,
    "CREATE TABLE t_dab_link_count (
       lc_id int unsigned NOT NULL default '0',
       lc_title varchar(255) binary NOT NULL default '',
       lc_amnt int unsigned NOT NULL default '0',
       PRIMARY KEY pk_dlc (lc_id),
       UNIQUE INDEX u_dlc (lc_title)
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS t_named_dab_link_count");
  submit_query($tooldb,
    "CREATE TABLE t_named_dab_link_count (
       lc_title varchar(255) binary NOT NULL default '',
       lc_amnt int unsigned NOT NULL default '0',
       UNIQUE INDEX u_ndlc (lc_title)
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS t_dab_to_dab_links");
  submit_query($tooldb,
    "CREATE TABLE t_dab_to_dab_links (
       from_dab_id int unsigned NOT NULL default '0',
       from_dab_title varchar(255) binary NOT NULL default '',
       to_dab_id int unsigned NOT NULL default '0',
       to_dab_title varchar(255) binary NOT NULL default ''
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS t_to_dab_link_count");
  submit_query($tooldb,
    "CREATE TABLE t_to_dab_link_count (
       lc_id int unsigned NOT NULL default '0',
       lc_title varchar(255) binary NOT NULL default '',
       lc_amnt int unsigned NOT NULL default '0',
       PRIMARY KEY (lc_id)
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS tagged_excessive_dab_articles");
  submit_query($tooldb,
    "CREATE TABLE tagged_excessive_dab_articles (
       article_id int unsigned NOT NULL default '0',
       PRIMARY KEY (article_id)
     ) ENGINE=InnoDB"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS dab_template_titles");
  submit_query($tooldb,
    "CREATE TABLE dab_template_titles (
       template_title varchar(255) binary NOT NULL default '',
       UNIQUE KEY u_dtt (template_title)
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );
  submit_query($tooldb, "DROP TABLE IF EXISTS backup_all_dab_links");

  if ($is_error) return;
  mysqli_commit($tooldb);
  mysqli_close($tooldb);
  $duration = time() - $begin;
  fputs($fp,
    "$"."dab_proc_results['initialize_tables'] = \"".convert_time($duration)."\";\n");

}

function fetch_dab_pages () {
  global $fp, $lp;
  global $is_error, $logging_enabled;

  $dabtitles = [];      // id => title
  $dabredirects = [];   // from_id => to_id
  $dablinks = [];
  $templatelinks = [];

  if ($is_error) return;

  $pagetitles = []; # maps ids => titles for all pages containing a disambig link

  # Retrieve all pages in ns 0 with disambiguation property

  $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
  $sql = "SELECT page_id,
                 page_title
            FROM page, page_props
           WHERE pp_page = page_id
             AND pp_propname = 'disambiguation'
             AND page_namespace = 0
             AND page_is_redirect = 0
         ";
  $dabresult = submit_and_log_query($wikidb, $sql, "fetch_dab_pages");

  $begin = time();
  $values = "";
  $batches = [];
  $vcount = 0;

  while ($dabrow = mysqli_fetch_assoc($dabresult)) {
    $did = (int) $dabrow['page_id'];
    $dabtitles[$did] = $dabrow['page_title'];
    $dtitle = mysqli_real_escape_string($wikidb, $dabrow['page_title']);
    # pages that are not redirects point to themselves
    $dabredirects[$did] = $did;
    if ($values != "") {
      $values .= ",";
    }
    $values .= "($did, '$dtitle')";
    $vcount += 1;
    if ($vcount == 128) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  mysqli_free_result($dabresult);
  mysqli_close($wikidb);

  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
  foreach ($batches as $vstring) {
    $iresult = submit_query($tooldb,
      "INSERT INTO t_all_dabs (dab_id, dab_title)
            VALUES $vstring
      ");
    if ($is_error) return;
  }
  mysqli_commit($tooldb);
  mysqli_close($tooldb);

  $duration = time() - $begin;
  fputs($fp,
        "$"."dab_proc_results['store_all_dabs'] = \"".convert_time($duration)."\";\n");

  # Populate t_dabs_without_hats by retrieving all pages in disambig
  # categories and storing those that were not previously found
  # GROUP BY dab_id to eliminate duplication of any pages found in multiple categories

  $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
  $sql = "SELECT page_id AS dab_id,
                 page_title AS dab_title
            FROM categorylinks, page
           WHERE page_id = cl_from
             AND page_namespace = 0
             AND (
                  cl_to = 'Disambiguation_pages' OR
                  cl_to = 'All_disambiguation_pages' OR
                  cl_to = 'All_article_disambiguation_pages' OR
                  cl_to = 'Animal_common_name_disambiguation_pages' OR
                    cl_to = 'Bird_common_name_disambiguation_pages' OR
                    cl_to = 'Fish_common_name_disambiguation_pages' OR
                  cl_to = 'Broadcast_call_sign_disambiguation_pages' OR
                  cl_to = 'Buildings_and_structures_disambiguation_pages' OR
                    cl_to = 'Airport_disambiguation' OR
                    cl_to = 'Architectural_disambiguation_pages' OR
                    cl_to = 'Educational_institution_disambiguation_pages' OR
                    cl_to = 'Hospital_disambiguation_pages' OR
                    cl_to = 'Places_of_worship_disambiguation_pages' OR
                      cl_to = 'Church_building_disambiguation_pages' OR
                      cl_to = 'Synagogue_disambiguation_pages' OR
                  cl_to = 'Case_law_disambiguation_pages' OR
                  cl_to = 'Disambiguation_pages_with_Chinese_character_titles' OR
                  cl_to = 'Human_name_disambiguation_pages' OR
                  cl_to = 'Language_and_nationality_disambiguation_pages' OR
                  cl_to = 'Letter-number_combination_disambiguation_pages' OR
                  cl_to = 'Lists_of_ambiguous_numbers' OR
                  cl_to = 'Military_units_and_formations_disambiguation_pages' OR
                  cl_to = 'Place_name_disambiguation_pages' OR
                    cl_to = 'Country_subdivision_name_disambiguation_pages' OR
                      cl_to = 'Arrondissement_name_disambiguation_pages' OR
                      cl_to = 'Canton_name_disambiguation_pages' OR
                      cl_to = 'County_name_disambiguation_pages' OR
                        cl_to = 'United_States_county_disambiguation_pages' OR
                      cl_to = 'Department_name_disambiguation_pages' OR
                      cl_to = 'District_name_disambiguation_pages' OR
                      cl_to = 'Municipality_name_disambiguation_pages' OR
                      cl_to = 'Parish_name_disambiguation_pages' OR
                      cl_to = 'Province_name_disambiguation_pages' OR
                      cl_to = 'Region_name_disambiguation_pages' OR
                      cl_to = 'State_name_disambiguation_pages' OR
                      cl_to = 'Township_name_disambiguation_pages' OR
                        cl_to = 'Arkansas_township_disambiguation_pages' OR
                        cl_to = 'Illinois_township_disambiguation_pages' OR
                        cl_to = 'Indiana_township_disambiguation_pages' OR
                        cl_to = 'Iowa_township_disambiguation_pages' OR
                        cl_to = 'Kansas_township_disambiguation_pages' OR
                        cl_to = 'Michigan_township_disambiguation_pages' OR
                        cl_to = 'Minnesota_township_disambiguation_pages' OR
                        cl_to = 'Missouri_township_disambiguation_pages' OR
                        cl_to = 'Nebraska_township_disambiguation_pages' OR
                        cl_to = 'New_Jersey_township_disambiguation_pages' OR
                        cl_to = 'North_Carolina_township_disambiguation_pages' OR
                        cl_to = 'North_Dakota_township_disambiguation_pages' OR
                        cl_to = 'Ohio_township_disambiguation_pages' OR
                        cl_to = 'Oklahoma_township_disambiguation_pages' OR
                        cl_to = 'Pennsylvania_township_disambiguation_pages' OR
                        cl_to = 'South_Dakota_township_disambiguation_pages' OR
                    cl_to = 'Station_disambiguation_pages' OR
                  cl_to = 'Plant_common_name_disambiguation_pages' OR
                  cl_to = 'Political_party_disambiguation_pages' OR
                  cl_to = 'Science_disambiguation_pages' OR
                    cl_to = 'Biology_disambiguation_pages' OR
                      cl_to = 'Taxonomy_disambiguation_pages' OR
                        cl_to = 'Genus_disambiguation_pages' OR
                        cl_to = 'Latin_name_disambiguation_pages' OR
                        cl_to = 'Species_Latin_name_disambiguation_pages' OR
                          cl_to = 'Species_Latin_name_abbreviation_disambiguation_pages' OR
                    cl_to = 'Linguistics_disambiguation_pages' OR
                    cl_to = 'Mathematics_disambiguation_pages' OR
                  cl_to = 'Temple_name_disambiguation_pages' OR
                  cl_to = 'Title_and_name_disambiguation_pages' OR
                  cl_to = 'Transport_disambiguation_pages' OR
                    cl_to = 'Road_disambiguation_pages' OR
                      cl_to = 'Ohio_road_disambiguation_pages' OR
                    cl_to = 'Transport_route_disambiguation_pages' OR
                    cl_to = 'Ship_disambiguation_pages' OR
                    cl_to = 'Station_disambiguation_pages' OR
                  cl_to = 'Tropical_cyclone_disambiguation_pages' OR
                    cl_to = 'Atlantic_hurricane_disambiguation_pages' OR
                    cl_to = 'Australian_region_cyclone_disambiguation_pages' OR
                    cl_to = 'Pacific_hurricane_disambiguation_pages' OR
                    cl_to = 'North_Indian_cyclone_disambiguation_pages' OR
                    cl_to = 'South_Pacific_cyclone_disambiguation_pages' OR
                    cl_to = 'South-West_Indian_Ocean_cyclone_disambiguation_pages' OR
                    cl_to = 'Pacific_typhoon_disambiguation_pages')
         GROUP BY dab_id
  ";
  $dwhresult = submit_and_log_query($wikidb, $sql, "fetch_dabs_without_hats");
  if ($is_error) return;
  $values = "";
  while ($dwhrow = mysqli_fetch_assoc($dwhresult)) {
    if (! isset($dabtitles[$dwhrow['dab_id']] )) {
      # only insert pages that do not have the disambiguation property
      $dab_id = (int) $dwhrow['dab_id'];
      $cleantitle = mysqli_real_escape_string($wikidb, $dwhrow['dab_title']);
      if ($values != "")
        $values .= ",";
      $values .= "($dab_id, '$cleantitle')";
    }
  }
  mysqli_free_result($dwhresult);
  mysqli_close($wikidb);

  if ($values != "") {
    $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
    submit_query($tooldb, "
        INSERT INTO t_dabs_without_hats (dab_id, dab_title)
         VALUES $values
    ");
    if ($is_error) return;
    mysqli_commit($tooldb);
    mysqli_close($tooldb);
  }

  # Retrieve all ns 0 redirects to disambiguation pages

  $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
  $sql = "SELECT src.page_id as redir_id,
                 src.page_title as redir_title,
                 target.page_id as dab_id
            FROM page as target, page_props, redirect, page as src
           WHERE target.page_namespace = 0
             AND target.page_title = rd_title
             AND rd_namespace = 0
             AND pp_page = target.page_id
             AND pp_propname = 'disambiguation'
             AND rd_from = src.page_id
             AND src.page_namespace = 0
         ";
  $rdresult = submit_and_log_query($wikidb, $sql, "fetch_dab_redirects");
  if ($is_error) return;
  while ($rdrow = mysqli_fetch_assoc($rdresult)) {
    $dabtitles[$rdrow['redir_id']] = $rdrow['redir_title'];
    $dabredirects[$rdrow['redir_id']] = (int) $rdrow['dab_id'];
  }
  mysqli_free_result($rdresult);
  mysqli_close($wikidb);

  $dabindex = array_flip($dabtitles);

  # retrieve all incoming links to disambiguation titles (pages and redirects)
  # from namespaces 0 and 10 only
  $begin = time();
  $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
  $values = "";
  $vcount = 0;
  # group titles into batches for efficiency
  $batches = [];
  foreach ($dabtitles as $onetitle) {
    if ($vcount > 0)
      $values .= ",";
    $cleantitle = mysqli_real_escape_string($wikidb, $onetitle);
    $values .= "'$cleantitle'";
    $vcount += 1;
    if ($vcount == 64) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0) {
    $batches[] = $values;
  }
  foreach ($batches as $batch) {
    $query = "SELECT pl_from,
                     page_title,
                     page_latest,
                     lt_title AS pl_title,
                     pl_from_namespace AS pl_ns
                FROM pagelinks, linktarget, page
               WHERE page_id=pl_from
                 AND page_is_redirect = 0
                 AND pl_target_id = lt_id
                 AND lt_namespace=0
                 AND (pl_from_namespace=0 OR pl_from_namespace=10)
                 AND lt_title IN ($batch)";
    $sresult = submit_query($wikidb, $query);
    if ($is_error) return;
    while ($srow = mysqli_fetch_assoc($sresult)) {
      $pltitle = $srow['pl_title'];
      $pl_from = (int) $srow['pl_from'];
      if ( isset ($dabindex[$pltitle]) ) {
        $dab_id = $dabindex[$pltitle];
      } else {
        if ($logging_enabled)
          fputs($lp, print_r(" Dab title $pltitle not found.\n", true));
        $dab_id = null;
      }
      if ( isset ($dabredirects[$dab_id]) ) {
        $redir_id = $dabredirects[$dab_id];
      } else {
        if ($logging_enabled)
          fputs($lp, print_r(" Redirect id $dab_id not found.\n", true));
        $redir_id = null;
      }
      if ($srow['pl_ns'] == 0) {
        # skip any links from a page to itself, including via a redirect
        if ($pl_from != $redir_id and $pl_from != $dab_id) {
          $dablinks[$pl_from.",".$dab_id] = [
            'from_id' => $pl_from,
            'from_title' => $srow['page_title'],
            'rev_id' => (int) $srow['page_latest'],
            'links_to' => $dab_id,
            'dab_target' => $redir_id,
          ];
          $pagetitles[$pl_from] = $srow['page_title'];
        }
      } elseif ($srow['pl_ns'] == 10) {
        $templatelinks[$pl_from.",".$dab_id] = [
          'template_id' => $pl_from,
          'template_title' => $srow['page_title'],
          'rev_id' => (int) $srow['page_latest'],
          'links_to' => $dab_id,
          'dab_target' => $redir_id,
        ];
      } else {
        if ($logging_enabled)
          fputs($lp, print_r(" Invalid namespace {$srow['pl_ns']} in query result.\n", true));
      }
    }
    mysqli_free_result($sresult);
  }
  mysqli_close($wikidb);
  fputs($lp, "Initial template link count: ".count($templatelinks)."\n");
  $duration = time() - $begin;
  fputs($fp, "$"."dab_proc_results['dab_pl_r'] = \"".convert_time($duration)."\";\n");

  # drop any multiple dablinks to same dab target

  $t_dablinks_index = [];
  foreach($dablinks as $i => $onelink) {
    if (isset($t_dablinks_index[$onelink['from_id']])) {
      if (in_array($onelink['dab_target'], $t_dablinks_index[$onelink['from_id']], true) === false) {
        # from_id already in index, but this target is not
        $t_dablinks_index[$onelink['from_id']][] = $onelink['dab_target'];
      } else {
        # index already contains a link from this article to the target dab page
        unset($dablinks[$i]);
      }
    } else {
      # from_id not yet in index
      $t_dablinks_index[$onelink['from_id']] = array($onelink['dab_target']);
    }
  }

  # Delete templates that are not used by mainspace non-redirect articles

  $begin = time();

  # create a list of unique templates containing >=1 links to dab pages
  $templatelist = [];
  foreach ($templatelinks as $tlink) {
    $templatelist[$tlink['template_id']] = $tlink['template_title'];
  }
  fputs($lp, "Found ".count($templatelist)." unique templates containing dablinks.\n");

  $keeplist = [];

  # For each template, check whether any page that transcludes the template is
  # a non-redirect mainspace article
  $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
  foreach ($templatelist as $t_id => $t_title) {
    if ($t_title != "Pagetype") {
      $esctitle = mysqli_real_escape_string($wikidb, $t_title);
      $query = "SELECT 1
		  FROM templatelinks, linktarget
		 WHERE tl_target_id = lt_id
		   AND lt_title = '$esctitle'
		   AND lt_namespace = 10
	    AND EXISTS (
			SELECT * FROM page
			 WHERE page_id = tl_from
			   AND page_namespace = 0
			   AND page_is_redirect = 0
		       )
      ";
      $qresult = mysqli_query($wikidb, $query);
      if ($qresult && mysqli_num_rows($qresult) >= 1) {
	# a transcluding article was found
	$keeplist[$t_id] = $t_title;
      }
      if ($qresult)
	mysqli_free_result($qresult);
    }
  }
  mysqli_close($wikidb);
  foreach ($templatelinks as $i => $tlink) {
    if (! isset($keeplist[$tlink['template_id']])) {
      unset($templatelinks[$i]);
    }
  }
  fputs($lp, "After Pass 1, ".count($keeplist)." unique templates containing ".count($templatelinks)." links to dab pages.\n");
  $duration = time() - $begin;
  fputs($fp, "$"."dab_proc_results['keep_templates'] = \"".convert_time($duration)."\";\n");

  # Delete templates that have a dab link in a non-transcluded portion of text

  # For each template in list, get ids of up to 4 mainspace articles that
  # transclude the template; if none of those 4 articles is in $dablinks,
  # remove the template from the list

  $begin = time();

  $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
  foreach ($templatelinks as $tkey => $tlink) {
    $ttitle = mysqli_real_escape_string($wikidb, $tlink['template_title']);
    $query2 = "SELECT page_id
                 FROM templatelinks, linktarget, page
                WHERE tl_target_id = lt_id
                  AND lt_title='$ttitle'
                  AND lt_namespace = 10
                  AND tl_from = page_id
                  AND page_namespace = 0
                  AND page_is_redirect = 0
                  AND page_id <> {$tlink['links_to']}
                LIMIT 4";
    $q2result = submit_query($wikidb, $query2);
    if ($is_error) return;
    $found = false;
    while ($qrow = mysqli_fetch_assoc($q2result)) {
      if (! isset($dablinks[$qrow['page_id'].",".$tlink['links_to']])) {
        # found a transcluding article that is not in pagelinks
        $found = true;
        break;
      }
    }
    mysqli_free_result($q2result);
    if ($found) {
      unset($templatelinks[$tkey]);
    }
  }
  mysqli_close($wikidb);
  unset($t_dablinks_index);
  unset($keeplist);

  fputs($lp, "After Pass 2, ".count($templatelinks)." template links to dab pages.\n");

  $duration = time() - $begin;
  fputs($fp, "$"."dab_proc_results['remove_templates'] = \"".convert_time($duration)."\";\n");

  # store t_template_dab_links
  $begin = time();

  $vcount = 0;
  $values = "";
  $batches = [];
  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  foreach ($templatelinks as $tlink) {
    if ( endswith ( $dabtitles[$tlink['links_to']], "_(disambiguation)" ) )
      continue;
    if ( endswith ( $dabtitles[$tlink['links_to']], "_(number)") )
      continue;
    $ttitle = mysqli_real_escape_string($tooldb, $tlink['template_title']);
    $dtitle = mysqli_real_escape_string($tooldb, $dabtitles[$tlink['dab_target']]);
    $rtitle = mysqli_real_escape_string($tooldb, $dabtitles[$tlink['links_to']]);
    if ($vcount > 0) {
      $values .= ", ({$tlink['template_id']}, '$ttitle', {$tlink['dab_target']},
                      '$dtitle', '$rtitle')";
    } else {
      $values = "({$tlink['template_id']}, '$ttitle', {$tlink['dab_target']},
                      '$dtitle', '$rtitle')";
    }
    $vcount += 1;
    if ($vcount == 32) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  foreach ($batches as $batch) {
    $sql = "INSERT INTO t_template_dab_links
                        (t_id, t_title, dab_id, dab_title, r_title)
                 VALUES $batch";
    $result = submit_query($tooldb, $sql);
    if ($is_error) return;
  }
  mysqli_close($tooldb);

  $duration = time() - $begin;
  fputs($fp, "$"."dab_proc_results['template_dab_links'] = \"".convert_time($duration)."\";\n");

  # For each remaining template link, find all articles that transclude the
  # template and link to the disambiguation page

  $begin = time();
  $dab_article_template_links = [];
  foreach ($templatelinks as $tlink) {
    if ( endswith($dabtitles[$tlink['links_to']], "_(disambiguation)" ) )
      continue;
    if ( endswith($dabtitles[$tlink['links_to']], "_(number)") )
      continue;
    $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
    $ttitle = mysqli_real_escape_string($wikidb, $tlink['template_title']);
    $dtitle = mysqli_real_escape_string($wikidb, $dabtitles[$tlink['links_to']]);
    $sql = "SELECT tl_from
              FROM templatelinks, linktarget AS tt, pagelinks, linktarget AS pt
             WHERE tl_from = pl_from
               AND tl_from_namespace = 0
               AND tl_target_id = tt.lt_id
               AND tt.lt_namespace = 10
               AND tt.lt_title = '$ttitle'
               AND pl_from_namespace = 0
               AND pl_target_id = pt.lt_id
               AND pt.lt_namespace = 0
               AND pt.lt_title = '$dtitle'";
    $result = submit_query($wikidb, $sql);
    if ($is_error) return;
    while ($row = mysqli_fetch_assoc($result)) {
      $dab_article_template_links[] = [
        'link_from' => (int) $row['tl_from'],
        'link_to' => $tlink['links_to'],
        'dab_id' => $tlink['dab_target'],
        'template_id' => $tlink['template_id'],
        'template_title' => $tlink['template_title']
      ];
      foreach ($dablinks as $i => $dlink) {
        if ($dlink['from_id'] == (int) $row['tl_from'] &&
            $dlink['links_to'] == $tlink['links_to']) {
          $dablinks[$i]['template_id'] = $tlink['template_id'];
        }
      }
    }
    mysqli_free_result($result);
    mysqli_close($wikidb);
  }

  $duration = time() - $begin;
  fputs($fp, "$"."dab_proc_results['dab_article_template_links'] = \"".convert_time($duration)."\";\n");

  # pull everything together into all_dab_links_part2 table
  $begin = time();
  $values = "";
  $vcount = 0;
  $batches = [];
  $named_dab_link_counts = [];
  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  foreach ($dablinks as $link) {
    $dabpage_t = $dabtitles[$link['links_to']];
    if ( endswith ( $dabpage_t, "_(disambiguation)" )
        || endswith ( $dabpage_t, "_(number)") ) {
      if (isset ($named_dab_link_counts[$dabpage_t])) {
        $named_dab_link_counts[$dabpage_t] += 1;
      } else {
        $named_dab_link_counts[$dabpage_t] = 1;
      }
    } else {
      $atitle = mysqli_real_escape_string($tooldb, $link['from_title']);
      $dtitle = mysqli_real_escape_string($tooldb, $dabtitles[$link['dab_target']]);
      $rtitle = mysqli_real_escape_string($tooldb, $dabpage_t);
      $tid = 0;
      $ttitle = "";
      foreach ($dab_article_template_links as $tlink) {
        if ($tlink['link_from'] == $link['from_id'] && $tlink['link_to'] == $link['links_to']) {
          $tid = $tlink['template_id'];
          $ttitle = mysqli_real_escape_string($tooldb, $tlink['template_title']);
        }
      }
      if ($values != "")
        $values .= ", ";
      $values .= "({$link['from_id']}, '$atitle',
                  {$link['dab_target']}, '$dtitle',
                  {$link['links_to']}, '$rtitle',
                  $tid, '$ttitle')";
      $vcount += 1;
      if ($vcount == 32) {
        $batches[] = $values;
        $values = "";
        $vcount = 0;
      }
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  foreach ($batches as $v) {
    $sql = "INSERT INTO all_dab_links_part2
                        (article_id, article_title,
                         dab_id, dab_title,
                         redirect_id, redirect_title,
                         template_id, template_title)
                 VALUES $v";
    submit_query($tooldb, $sql);
    if ($is_error) return;
  }

  submit_query($tooldb, 
    "CREATE TABLE IF NOT EXISTS all_dab_links (
       article_id int unsigned NOT NULL default '0',
       article_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       redirect_id int unsigned NOT NULL default '0',
       redirect_title varchar(255) binary NOT NULL default '',
       template_id int unsigned NOT NULL default '0',
       template_title varchar(255) binary NULL,
       dab_date varbinary(8) NULL,
       INDEX i_adl (article_id),
       INDEX i_adl2 (dab_id),
       INDEX i_adl3 (dab_title),
       INDEX i_adl4 (redirect_id),
       INDEX i_adl5 (template_id),
       INDEX i_adl6 (dab_date)
     ) ROW_FORMAT=DYNAMIC
       ENGINE=InnoDB
  ");

  submit_query($tooldb, "DROP TABLE IF EXISTS t_all_dab_links");

  submit_query($tooldb,
    "CREATE TABLE t_all_dab_links (
       article_id int unsigned NOT NULL default '0',
       article_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       redirect_id int unsigned NOT NULL default '0',
       redirect_title varchar(255) binary NOT NULL default '',
       template_id int unsigned NOT NULL default '0',
       template_title varchar(255) binary NULL,
       dab_date varbinary(8) NULL,
       INDEX i_adl (article_id),
       INDEX i_adl2 (dab_id),
       INDEX i_adl3 (dab_title),
       INDEX i_adl4 (redirect_id),
       INDEX i_adl5 (template_id),
       INDEX i_adl6 (dab_date)
     ) ROW_FORMAT=DYNAMIC
       ENGINE=InnoDB AS
         SELECT a.article_id AS article_id,
                a.article_title AS article_title,
                a.dab_id AS dab_id,
                a.dab_title AS dab_title,
                a.redirect_id AS redirect_id,
                a.redirect_title AS redirect_title,
                a.template_id AS template_id,
                a.template_title AS template_title,
                b.dab_date AS dab_date
           FROM all_dab_links_part2 a LEFT OUTER JOIN all_dab_links b
             ON a.article_id = b.article_id
            AND a.dab_id = b.dab_id
            AND a.redirect_id = b.redirect_id
            AND a.template_id = b.template_id"
  );

  submit_query($tooldb,
    "UPDATE t_all_dab_links
        SET dab_date = DATE_FORMAT(SYSDATE(), '%Y%m%d')
      WHERE dab_date IS NULL"
  );
  mysqli_commit($tooldb);

  # save named dab link counts table
  $values = "";
  $vcount = 0;
  $batches = [];

  foreach ($named_dab_link_counts as $pg_title => $amt) {
    if ($vcount > 0)
      $values .= ", ";
    $ptitle = mysqli_real_escape_string($tooldb, $pg_title);
    $values .= "('$ptitle', $amt)";
    $vcount += 1;
    if ($vcount == 128) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  foreach($batches as $batch) {
    submit_query($tooldb,
      "INSERT INTO t_named_dab_link_count (lc_title, lc_amnt) VALUES $batch"
    );
  }

  submit_query($tooldb, "DROP TABLE IF EXISTS all_dab_links_part2");

  submit_query($tooldb, "DROP TABLE IF EXISTS t_all_dab_links_basic");

  submit_query($tooldb,
    "CREATE TABLE t_all_dab_links_basic (
        article_id int unsigned NOT NULL default '0',
        article_title varchar(255) binary NOT NULL default '',
        dab_id int unsigned NOT NULL default '0',
        dab_title varchar(255) binary NOT NULL default '',
        INDEX i_adlb (article_id),
        INDEX i_adlb2 (dab_id)
     ) ENGINE=InnoDB AS /* SLOW_OK */
       SELECT article_id,
              article_title,
              dab_id,
              dab_title
         FROM t_all_dab_links
        GROUP BY dab_id, article_id"
  );

  $duration = time() - $begin;
  fputs($fp, "$"."dab_proc_results['t_all_dab_links'] = \"".convert_time($duration)."\";\n");

  # populate t_dab_template_tc_count

  $begin = time();

  $transclusion_count = [];

  fputs($lp, "dab_article_template_links count = ".count($dab_article_template_links));
  foreach ($dab_article_template_links as $tlink) {
    if (isset ($transclusion_count[$tlink['template_id']] )) {
      $transclusion_count[$tlink['template_id']] += 1;
    } else {
      $transclusion_count[$tlink['template_id']] = 1;
    }
  }

  $values = "";
  $vcount = 0;
  $batches = [];
  foreach ($transclusion_count as $t_id => $tr_count) {
    if ($tr_count <= 0)
      continue;
    $ttitle = mysqli_real_escape_string($tooldb, $templatelist[$t_id]);
    if ($vcount > 0)
      $values .= ", ";
    $values .= "($t_id, '$ttitle', $tr_count)";
    $vcount += 1;
    if ($vcount == 128) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0)
    $batches[] = $values;
  foreach ($batches as $batch) {
    $sql = "INSERT INTO t_dab_template_tc_count (tc_id, tc_title, tc_amnt)
                 VALUES $batch";
    submit_query($tooldb, $sql);
  }

  $duration = time() - $begin;
  fputs($fp, "$"."dab_proc_results['create_template_tc_count'] = \"".convert_time($duration)."\";\n");

  # create link count tables

  $begin = time();

  $dab_link_count = [];
  foreach ($dablinks as $link) {
    if ( endswith ( $dabtitles[$link['links_to']], "_(disambiguation)" ) )
      continue;
    if ( endswith ( $dabtitles[$link['links_to']], "_(number)") )
      continue;
    if ( isset ( $dab_link_count[$link['dab_target']] )) {
      $dab_link_count[$link['dab_target']] += 1;
    } else {
      $dab_link_count[$link['dab_target']] = 1;
    }
  }

  $values = "";
  $vcount = 0;
  $batches = [];
  foreach($dab_link_count as $dab_id => $link_count) {
    if ($link_count <= 0)
      continue;
    $dtitle = mysqli_real_escape_string($tooldb, $dabtitles[$dab_id]);
    if ($dtitle == '')
      continue;
    if ($vcount > 0)
      $values .= ", ";
    $values .= "($dab_id, '$dtitle', $link_count)";
    $vcount += 1;
    if ($vcount == 128) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  foreach ($batches as $batch) {
    $sql = "INSERT INTO t_dab_link_count (lc_id, lc_title, lc_amnt) VALUES $batch";
    submit_query($tooldb, $sql);
  }

  submit_query($tooldb, "DROP TABLE IF EXISTS t_latest_dabs");

  submit_query($tooldb,
    "CREATE TABLE t_latest_dabs (
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       is_new tinyint unsigned NOT NULL default '0',
       new_count int unsigned NOT NULL default '0',
       all_count int unsigned NOT NULL default '0',
       PRIMARY KEY pk_ld (dab_id),
       UNIQUE INDEX u_ld (dab_title)
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC AS
         SELECT dab_id,
                dab_title,
                0 AS is_new,
                count(*) AS new_count,
                lc_amnt AS all_count
           FROM t_all_dab_links_basic a, t_dab_link_count
          WHERE dab_id = lc_id
            AND EXISTS
                (
                 SELECT 1
                   FROM t_all_dab_links b
                  WHERE a.dab_id = b.dab_id
                    AND a.article_id = b.article_id
                    AND dab_date = DATE_FORMAT(SYSDATE(), '%Y%m%d')
                )
          GROUP BY dab_id"
  );

  submit_query($tooldb,
    "UPDATE t_latest_dabs
        SET is_new = 1
      WHERE new_count = all_count"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS t_latest_dab_links");

  submit_query($tooldb,
    "CREATE TABLE t_latest_dab_links (
       article_id int unsigned NOT NULL default '0',
       article_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       redirect_id int unsigned NOT NULL default '0',
       redirect_title varchar(255) binary NOT NULL default '',
       template_id int unsigned NOT NULL default '0',
       template_title varchar(255) binary NULL,
       INDEX i_ldl (article_id),
       INDEX i_ldl2 (dab_id),
       INDEX i_ldl3 (redirect_id),
       INDEX i_ldl4 (template_id)
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC AS
         SELECT article_id,
                article_title,
                dab_id,
                dab_title,
                redirect_id,
                redirect_title,
                template_id,
                template_title
           FROM t_all_dab_links
          WHERE dab_date = DATE_FORMAT(SYSDATE(), '%Y%m%d')"
  );

  # create dab-to-dab links table

  $values = "";
  $vcount = 0;
  $batches = [];
  foreach ($dablinks as $link) {
    if ( endswith ( $dabtitles[$link['links_to']], "_(disambiguation)" ) )
      continue;
    if ( endswith ( $dabtitles[$link['links_to']], "_(number)" ) )
      continue;
    if (isset ($dabtitles[$link['from_id']])) {
      # link appears on a disambiguation page

      $ftitle = mysqli_real_escape_string($tooldb, $link['from_title']);
      $dtitle = mysqli_real_escape_string($tooldb, $dabtitles[$link['dab_target']]);
      if ($values != "")
        $values .= ", ";
      $values .= "({$link['from_id']}, '$ftitle', {$link['dab_target']}, '$dtitle')";
      $vcount += 1;
      if ($vcount == 32) {
        $batches[] = $values;
        $values = "";
        $vcount = 0;
      }
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  foreach ($batches as $v) {
    $sql = "INSERT INTO t_dab_to_dab_links
            (from_dab_id, from_dab_title, to_dab_id, to_dab_title)
            VALUES $v";
    submit_query($tooldb, $sql);
  }

  # count dab links that are not via a template
  $to_dab_link_count = [];

  $dab_link_count = [];
  foreach ($dablinks as $link) {
    if ( endswith ( $dabtitles[$link['links_to']], "_(disambiguation)" ) )
      continue;
    if ( endswith ( $dabtitles[$link['links_to']], "_(number)") )
      continue;
    if ( isset ($link['template_id']) )
      continue;
    if ( isset ( $to_dab_link_count[$link['from_id']] )) {
      $to_dab_link_count[$link['from_id']] += 1;
    } else {
      $to_dab_link_count[$link['from_id']] = 1;
    }
  }

  $values = "";
  $vcount = 0;
  $batches = [];
  foreach($to_dab_link_count as $pg_id => $link_count) {
    if ($link_count <= 0)
      continue;
    $ptitle = mysqli_real_escape_string($tooldb, $pagetitles[$pg_id]);
    if ($vcount > 0)
      $values .= ", ";
    $values .= "($pg_id, '$ptitle', $link_count)";
    $vcount += 1;
    if ($vcount == 128) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  foreach ($batches as $batch) {
    $sql = "INSERT INTO t_to_dab_link_count (lc_id, lc_title, lc_amnt) VALUES $batch";
    submit_query($tooldb, $sql);
  }

  submit_query($tooldb, "DROP TABLE IF EXISTS t_excessive_dab_links");

  submit_query($tooldb,
    "CREATE TABLE t_excessive_dab_links (
       lc_id int unsigned NOT NULL default '0',
       lc_title varchar(255) binary NOT NULL default '',
       lc_amnt int unsigned NOT NULL default '0',
       PRIMARY KEY (lc_id)
     ) ENGINE=InnoDB AS
          SELECT lc_id,
                 lc_title,
                 lc_amnt
            FROM t_to_dab_link_count
           WHERE lc_amnt >= ".EXCESSIVE
  );
  mysqli_close($tooldb);

  # check for articles already containing Template:Dablinks

  $values = "";
  $vcount = 0;
  $batches = [];
  $tagged_pages = [];

  $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
  $result = submit_query($wikidb,
    "SELECT tl_from
       FROM templatelinks, linktarget
      WHERE tl_target_id = lt_id
        AND lt_namespace = 10
        AND lt_title = 'Dablinks'
        AND tl_from_namespace = 0
      GROUP BY tl_from"
  );
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($result)) {
    $tagged_pages[$row['tl_from']] = true;
    if ($vcount > 0)
      $values .= ", ";
    $values .= "({$row['tl_from']})";
    $vcount += 1;
    if ($vcount == 128) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  mysqli_free_result($result);
  mysqli_close($wikidb);

  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  foreach ($batches as $batch) {
    $sql = "INSERT INTO tagged_excessive_dab_articles (article_id) VALUES $batch";
    submit_query($tooldb, $sql);
  }

  submit_query($tooldb, "DROP TABLE IF EXISTS t_excessive_dab_links_fixed");

  submit_query($tooldb,
    "CREATE TABLE t_excessive_dab_links_fixed (
       lc_id int unsigned NOT NULL default '0',
       lc_title varchar(255) binary NOT NULL default '',
       lc_amnt int unsigned NOT NULL default '0',
       PRIMARY KEY (lc_id)
     ) ENGINE=InnoDB AS
          SELECT lc_id,
                 lc_title,
                 lc_amnt
            FROM t_to_dab_link_count
           WHERE lc_amnt < ".EXCESSIVE."
             AND EXISTS
                 (
                  SELECT 1
                    FROM tagged_excessive_dab_articles
                   WHERE article_id = lc_id
                 )"
  );

  # insert any article that contains template but no longer contains any links
  $values = "";
  $vcount = 0;
  $batches = [];

  foreach ($tagged_pages as $pg_id => $tag) {
    if (! isset($to_dab_link_count[$pg_id]) ) {
      if (! array_key_exists($pg_id, $pagetitles)) {
          continue;
      }
      $ptitle = mysqli_real_escape_string($tooldb, $pagetitles[$pg_id]);
      if ($vcount > 0)
        $values .= ", ";
      $values .= "($pg_id, '$ptitle', 0)";
      $vcount += 1;
      if ($vcount == 128) {
        $batches[] = $values;
        $values = "";
        $vcount = 0;
      }
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  foreach ($batches as $batch) {
    submit_query($tooldb,
      "INSERT IGNORE INTO t_excessive_dab_links_fixed
              (lc_id, lc_title, lc_amnt) VALUES $batch"
    );
  }

  submit_query($tooldb, "DROP TABLE IF EXISTS tagged_excessive_dab_articles");

  $duration = time() - $begin;
  fputs($fp, "$"."dab_proc_results['create_dab_link_count'] = \"".convert_time($duration)."\";\n");

  # prep for fetch
  $begin = time();

  submit_query($tooldb, "DROP TABLE IF EXISTS recent_dabs_update");

  submit_query($tooldb,
    "CREATE TABLE recent_dabs_update (
       type tinyint NOT NULL default '-1',
       article_id int unsigned NOT NULL default '0',
       article_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       redirect_id int unsigned NOT NULL default '0',
       redirect_title varchar(255) binary NOT NULL default '',
       template_id int unsigned NOT NULL default '0',
       template_title varchar(255) binary NULL,
       rev_id int unsigned NOT NULL default 0,
       user varchar(255) binary NOT NULL default '',
       is_reg tinyint unsigned NOT NULL default '0',
       is_new tinyint unsigned NOT NULL default '0',
       dab_date DATETIME NULL,
       UNIQUE INDEX u_lr (article_id, dab_id, redirect_id, template_id, type, dab_date)
     ) ENGINE=InnoDB"
  );

  # Exclude dabs created in the morning run
  submit_query($tooldb, "DROP TABLE IF EXISTS very_latest_dab_links");

  submit_query($tooldb,
    "CREATE TABLE very_latest_dab_links (
       article_id int unsigned NOT NULL default '0',
       article_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       redirect_id int unsigned NOT NULL default '0',
       redirect_title varchar(255) binary NOT NULL default '',
       template_id int unsigned NOT NULL default '0',
       template_title varchar(255) binary NULL,
       INDEX i_vll (article_id),
       INDEX i_vll2 (dab_id),
       INDEX i_vll3 (redirect_id),
       INDEX i_vll4 (template_id)
     ) ENGINE=InnoDB AS
          SELECT article_id,
                 article_title,
                 dab_id,
                 dab_title,
                 redirect_id,
                 redirect_title,
                 template_id,
                 template_title
            FROM t_latest_dab_links a
           WHERE NOT EXISTS
                 (
                  SELECT 1
                    FROM all_dab_links b
                   WHERE a.article_id = b.article_id
                     AND a.dab_id = b.dab_id
                     AND a.redirect_id = b.redirect_id
                     AND a.template_id = b.template_id
                     AND dab_date = DATE_FORMAT(SYSDATE(), '%Y%m%d')
                 )"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS very_latest_dabs");

  submit_query($tooldb,
    "CREATE TABLE very_latest_dabs (
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       PRIMARY KEY pk_vld (dab_id),
       UNIQUE INDEX u_vld (dab_title)
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC AS
          SELECT dab_id,
                 dab_title
            FROM very_latest_dab_links
           GROUP BY dab_id"
  );
  mysqli_close($tooldb);

  $dabtemplatetitles = [];
  $alldabtemplatetitles = "";

  # get titles of all disambiguation templates
  $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
  $result = submit_query($wikidb,
    "SELECT page_title AS template_title
       FROM page, categorylinks
      WHERE page_id = cl_from
        AND page_namespace = 10
        AND cl_to = 'Disambiguation_message_boxes'
      GROUP BY page_title"
  );
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($result)) {
    if ($row['template_title'] == "Wikipedia_disambiguation")
      continue;
    $dabtemplatetitles[] = $row['template_title'];
    if ($alldabtemplatetitles != "")
      $alldabtemplatetitles .= ",";
    $alldabtemplatetitles .= "'{$row['template_title']}'";
  }
  mysqli_free_result($result);
  # add titles of redirects to disambiguation templates
  $result = submit_query($wikidb,
    "SELECT page_title AS template_title
       FROM page, redirect
      WHERE page_id = rd_from
        AND rd_namespace = 10
        AND rd_title IN ( $alldabtemplatetitles )"
  );
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($result)) {
    $dabtemplatetitles[] = $row['template_title'];
  }
  mysqli_free_result($result);
  mysqli_close($wikidb);

  $values = "";
  $vcount = 0;
  $batches = [];
  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  foreach ($dabtemplatetitles as $t_title) {
    $ttitle = mysqli_real_escape_string($tooldb, $t_title);
    if ($vcount > 0)
      $values .= ", ";
    $values .= "('$ttitle')";
    $vcount += 1;
    if ($vcount == 128) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  foreach ($batches as $batch) {
    submit_query($tooldb,
      "INSERT IGNORE INTO dab_template_titles (template_title) VALUES $batch"
    );
  }
  mysqli_commit($tooldb);
  mysqli_close($tooldb);

  $duration = time() - $begin;
  fputs($fp, "$"."dab_proc_results['prep_for_fetch'] = \"".convert_time($duration)."\";\n");
}


function create_results_list () {

  global $sx;
  global $last_run_begin, $last_run_begin_wiki, $begin_run_wiki_lag;
  global $fp, $lp;
  global $is_error, $logging_enabled;

  if ($is_error) return;

  $begin = time();
  $is_first = true;
  $dab_start_time = time();

  # All disambig templates and their redirects

  $t_titles = array();

  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
  $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

  $sql = "SELECT template_title
            FROM dab_template_titles";
  $ttres = submit_query($tooldb, $sql);
  if ($is_error) return;

  while ($ttrow = mysqli_fetch_assoc($ttres))
    $t_titles[] = $ttrow['template_title'];
  mysqli_free_result($ttres);

  # Get the list of new dabs and iterate the list.
  $sql = "SELECT dab_id,
                 dab_title
            FROM very_latest_dabs
           ORDER BY dab_title";

  $result = submit_query($tooldb, $sql);
  if ($is_error) return;

  # START WHILE LOOP FOR EACH DAB
  while ($row = mysqli_fetch_assoc($result)) {

    $dab_id = $row['dab_id'];
    $dab_title = $row['dab_title'];

    if ($logging_enabled) {
      if (!$is_first) {
        $time_diff = time() - $dab_start_time;
        fputs($lp, print_r("    Total time: $time_diff second".($time_diff == 1 ? "" : "s")."\n", true));
      }
      fputs($lp, print_r("\nBegin while loop: $dab_title\n", true));
    }

    $is_first = false;
    $dab_start_time = time();
    $checked_templates = array();

    # Disambig and all its redirects
    $dab_titles_full = array($dab_title);
    $dab_titles = array();
    mysqli_close($wikidb);
    $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

    $dtitle = mysqli_real_escape_string($wikidb, $dab_title);
    $sql = "SELECT page_title
              FROM page, redirect
             WHERE rd_namespace = 0
               AND rd_title = '$dtitle'
               AND rd_from = page_id
               AND page_namespace = 0";

    $dtres = submit_query($wikidb, $sql);
    if ($is_error) return;

    while ($dtrow = mysqli_fetch_assoc($dtres))
      $dab_titles_full[] = $dtrow['page_title'];

    foreach($dab_titles_full AS $dt) {
      if ($dt != "" && !endswith($dt, "_(disambiguation)"))
        $dab_titles[] = $dt;
    }

    # Get the redirects for this dab
    $sql = "SELECT redirect_id,
                   redirect_title
              FROM very_latest_dab_links
             WHERE dab_id = $dab_id
               AND dab_id != redirect_id
             GROUP BY redirect_id
             ORDER BY redirect_title";

    $dr_res = submit_query($tooldb, $sql);
    if ($is_error) return;

    $del_redirects = array();

    while ($dr_row = mysqli_fetch_assoc($dr_res)) {

      $redirect_id = $dr_row['redirect_id'];
      $redirect_title = $dr_row['redirect_title'];

      #
      # STEP 1 - FIND REDIRECTS RETARGETED TO DISAMBIGS
      #
      if ($logging_enabled)
        fputs($lp, print_r(" Redirect check\n", true));

      $results = get_rev_id($redirect_title);

      if(find_user(0, 0, $redirect_id, 0, $dab_titles_full, $results[0], $results[1], $results[2], RETARGET_DAB, $redirect_title, false))
        $del_redirects[] = $redirect_id;
    }

    foreach($del_redirects AS $del_r) {

      $sql = "DELETE FROM very_latest_dab_links
                    WHERE redirect_id = $del_r";

      submit_query($tooldb, $sql);
    }

    # See if this is a brand new disambig
    $dtitle = mysqli_real_escape_string($tooldb, $dab_title);
    $sql = "SELECT 1
              FROM all_dabs
             WHERE dab_id = ".$dab_id."
               AND dab_title = '$dtitle'";

    $new_check = submit_query($tooldb, $sql);

    if ($new_check && mysqli_num_rows($new_check) == 0) {

      #
      # STEP 2 - FIND PAGES CONVERTED INTO DISAMBIGS
      #
      if ($logging_enabled)
        fputs($lp, print_r(" Conversion check\n", true));

      $results = get_rev_id($dab_title);

      if (find_user(0, $dab_id, 0, 0, $t_titles, $results[0], $results[1], $results[2], CONVERT_TO_DAB, $dab_title, false, "\{\{", "(\||\}\})"))
        continue;

      #
      # STEP 3 - FIND PAGEMOVE DABS
      #
      if ($logging_enabled)
        fputs($lp, print_r(" Dab move check\n", true));

      $dtitle = mysqli_real_escape_string($tooldb, $dab_title);
      $sql = "SELECT mv_user,
                     DATE_FORMAT(mv_date, '%Y%m%d%H%i%s') AS mv_date
                FROM move_log, all_dabs
               WHERE mv_to = '$dtitle'
                 AND mv_date > FROM_UNIXTIME(".$last_run_begin.")
                 AND mv_from = dab_title
               ORDER BY mv_date DESC
               LIMIT 1";

      $pmres = submit_query($tooldb, $sql);

      if ($pmres && mysqli_num_rows($pmres) == 1) {

        $pmrow = mysqli_fetch_assoc($pmres);
        $user = $pmrow['mv_user'];
        $date = $pmrow['mv_date'];

        $start_rev_id = $page_id = "";
        # get the earliest revision of this page since $last_run_begin_wiki
        $sql2 = "SELECT rev_id
                   FROM revision, page
                  WHERE page_id = rev_page
                    AND page_namespace = 0
                    AND page_title = '$dtitle'
                    AND rev_timestamp >= $last_run_begin_wiki
               ORDER BY rev_timestamp ASC
                  LIMIT 1";
        $result = submit_query($wikidb, $sql2);
        if ($result && mysqli_num_rows($result) == 1) {
          $row = mysqli_fetch_assoc($result);
          $start_rev_id = (int) $row['rev_id'];
        }

        store_results(MOVE_DAB, 0, $dab_id, 0, 0, $start_rev_id, $user, false, "", $date);

        continue;
      }

      if ($logging_enabled)
        fputs($lp, print_r(" ERROR: New dab but did not determine user\n", true));

    } // end work with new disambigs

    if ($logging_enabled)
      fputs($lp, print_r("  Begin inner loop for ".$dab_title."\n", true));

    # Get the list of new dabs and iterate the list.
    $sql = "SELECT article_id,
                   article_title,
                   redirect_id,
                   redirect_title,
                   template_id,
                   template_title
              FROM very_latest_dab_links
             WHERE dab_id = $dab_id
             ORDER BY article_title";

    $scan_res = submit_query($tooldb, $sql);

    if ($scan_res && mysqli_num_rows($scan_res) > 0) {

      while ($scan_row = mysqli_fetch_assoc($scan_res)) {

        $article_id = (int) $scan_row['article_id'];
        $article_title = $scan_row['article_title'];
        $redirect_id = (int) $scan_row['redirect_id'];
        $redirect_title = $scan_row['redirect_title'];
        $template_id = (int) $scan_row['template_id'];
        $template_title = $scan_row['template_title'];

        $sql = "SELECT 1
                  FROM recent_dabs_update
                 WHERE article_id = $article_id
                   AND dab_id = $dab_id
                   AND redirect_id = $redirect_id
                   AND template_id = $template_id";

        $dup_check = submit_query($tooldb, $sql);

        if ($dup_check && mysqli_num_rows($dup_check) > 0)
          continue;

        if ($logging_enabled)
          fputs($lp, print_r("   ARTICLE: $article_title\n", true));

        #
        # STEP 4 - FIND TEMPLATES THAT HAVE BEEN EDITED TO CONTAIN DISAMBIGS
        #
        if ($template_id != 0) {

          if ($logging_enabled)
            fputs($lp, print_r("    Template check\n", true));

          if (!in_array($template_id, $checked_templates)) {

            $checked_templates[] = $template_id;

            $results = get_rev_id("Template:".$template_title);

            find_user(0, $dab_id, 0, $template_id, $dab_titles, $results[0], $results[1], $results[2], TEMPLATE_DAB, "Template:".$template_title, false);
          }

          continue;
        }

        #
        # STEP 5 - FIND ARTICLES THAT HAVE BEEN EDITED TO CONTAIN DISAMBIGS
        #
        if ($logging_enabled)
          fputs($lp, print_r("    Article check\n", true));

        $results = get_rev_id($article_title);

        if (find_user($article_id, $dab_id, $redirect_id, $template_id, $dab_titles, $results[0], $results[1], $results[2], ARTICLE_DAB, $article_title, true))
          continue;

        store_results(NOT_FOUND, $article_id, $dab_id, $redirect_id, $template_id, 0, "", false, "", $begin_run_wiki_lag);

      } // end inner while loop of all links to a single dab
    }

  } // end while loop of all dabs
  mysqli_free_result($result);
  mysqli_close($tooldb);
  mysqli_close($wikidb);

  if ($logging_enabled) {
    $time_diff = time() - $dab_start_time;
    fputs($lp, print_r("    Total time: ".$time_diff." second".($time_diff == 1 ? "" : "s")."\n", true));
  }

  fputs($fp, "$"."dab_proc_results['create_results_list'] = \"".convert_time(time() - $begin)."\";\n");
}


function find_user($article_id, $dab_id, $redirect_id, $template_id, $all_titles, $end_timestamp, $new_article, $ns_move_date, $type, $to_scan, $is_article_search, $start = "(\[\[)", $end = "(\||\]\]|#)") {

  global $sx, $lp;
  global $logging_enabled;

  $start_timestamp = "now";
  $found_rev_id = $found_user = $found_timestamp = "";
  $prev_rev_id = $prev_user = $prev_timestamp = $prev_comment = "";
  $content = "";
  $query_continue = true;
  $counter = 0;
  $is_match = false;

  if ($is_article_search && is_disambig($article_id))
    $type = INT_DAB;

  if ($logging_enabled)
    fputs($lp, print_r("    Find User: Working on ".$to_scan."\n", true));

  // N.B. this function is forced to use the API because it accesses revision text
  while ($query_continue) {
    # get all revisions since $end_timestamp, starting with most recent
    $request_vars = array(
            'action' => 'query',
            'prop' => 'revisions',
            'titles' => $to_scan,
            'rvprop' => 'ids|user|timestamp|content|comment',
            'rvdir' => 'older',
            'rvstart' => $start_timestamp,
            'rvend' => $end_timestamp,
            'rvlimit' => 15,
            'rawcontinue' => '',
            'format' => 'php'
      );

    if(!$array = $sx->callAPI($request_vars))
//      die("SxWiki error. [463]");
      break;

    if(isset($array['query-continue']) && isset($array['query-continue']['revisions']) && isset($array['query-continue']['revisions']['rvcontinue'])) {
      $query_continue = true;
      $start_timestamp = $array['query-continue']['revisions']['rvcontinue'];
      $bar = strpos($start_timestamp, '|');
      if ($bar !== false)
        $start_timestamp = substr($start_timestamp, 0, $bar);
    }
    else {
      $query_continue = false;
    }

    $page_id = "";

    if(isset($array['query']['pages']))
      $page_id = key($array['query']['pages']);

    if(isset($array['query']['pages'][$page_id]['revisions'])) {

      $revisions = $array['query']['pages'][$page_id]['revisions'];

      $size = count($revisions);
      $inner_counter = 0;

      foreach($revisions as $rev) {

        if (! isset ($rev['*']))
          continue;
        // get the content of the revision
        $content = $rev['*'];
        $is_match = false;
        $inner_counter++;

        if ($logging_enabled)
          fputs($lp, print_r("     Loop\n", true));

        foreach($all_titles as $t) {
          // see if text of current revision contains a link to $t
          $safe_title = escape_chars($t);
          $pattern = "/{$start}\s*{$safe_title}\s*{$end}/";
          if(preg_match($pattern, $content)) {
            if ($logging_enabled)
              fputs($lp, print_r("      match: $pattern\n", true));
            $is_match = true;
            if ($is_article_search && is_seealso($pattern, $content)) {
              $type = INT_DAB;
              break;
            }
          }
        }

        if ($is_article_search) {
          $s_start = "(\{\{[^\[]*\|)";
          $s_end = "(\}\}|\|[^\]]*\}\}|#[^\]]*\}\}|\{\{!\}\}[^\]]*\}\})";
          foreach($all_titles as $t) {
            $safe_title = escape_chars($t);
            $pattern = "/{$s_start}\s*{$safe_title}\s*{$s_end}/";
            if(preg_match($pattern, $content)) {
              if ($logging_enabled)
                fputs($lp, print_r("      2nd match: $pattern\n", true));
              $is_match = true;
              $type = INT_DAB;
            }
          }
        }

        if (!$is_match) {
          if ($counter == 0) {
            break 2;
          }
          $found_user = $prev_user;
          $found_rev_id = $prev_rev_id;
          $found_timestamp = $prev_timestamp;
          if ($is_article_search && is_rvv($prev_comment))
            $type = RVV_DAB;
          break 2;
        }
        else {
          if (!$query_continue && $size == $inner_counter && $new_article) {
            if ($logging_enabled)
              fputs($lp, print_r("      match on last revision: query_continue ".($query_continue ? "true" : "false" )." size $size inner_counter $inner_counter\n", true));
            $found_user = array_key_exists('user', $rev) ? $rev['user'] : "";
            $found_rev_id = $rev['revid'];
            $found_timestamp = $rev['timestamp'];
            if ($is_article_search && is_rvv($rev['comment']))
              $type = RVV_DAB;
          } else {
            $prev_user = array_key_exists('user', $rev) ? $rev['user'] : "";
            $prev_rev_id = $rev['revid'];
            $prev_timestamp = $rev['timestamp'];
            $prev_comment = array_key_exists('comment', $rev) ? $rev['comment'] : "";
          }
        }

        $counter++;
      }  // end revision array foreach
    }
  }

  if (!empty($found_user)) {
    store_results($type, $article_id, $dab_id, $redirect_id, $template_id, $found_rev_id, $found_user, $new_article, $ns_move_date, $found_timestamp);
    return true;
  }

  if ($logging_enabled)
    fputs($lp, print_r("     not storing - is_match: ".($is_match ? "true" : "false")." found_user: $found_user type: $type\n", true));
  return false;
}

#
# Possible values for type:
# NOT_FOUND
# RETARGET_DAB
# CONVERT_TO_DAB
# MOVE_DAB
# TEMPLATE_DAB
# ARTICLE_DAB
# INT_DAB
# RVV_DAB
#
function store_results($type, $article_id, $dab_id, $redirect_id, $template_id, $revid, $user, $new_article, $ns_move_date, $date) {

  global $lp;
  global $logging_enabled;

  $phpDate;

  if ($ns_move_date != "")
    $phpDate = strtotime($ns_move_date);
  else
    $phpDate = strtotime($date);

  $sqlDate = date('Y-m-d H-i-s', $phpDate);

  if ($logging_enabled)
    if ($type == 0)
      fputs($lp, print_r("    NOT FOUND - Store results: $article_id $dab_id $redirect_id $template_id $sqlDate type: $type\n", true));
    else
      fputs($lp, print_r("    SUCCESS - Store results: $article_id $dab_id $redirect_id $template_id $sqlDate type: $type\n", true));

  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
  $uname = mysqli_real_escape_string($tooldb, $user);
  if ($revid) {
    if ($type == RETARGET_DAB) {

      $sql = "INSERT IGNORE INTO recent_dabs_update
		     SELECT $type AS type,
			    article_id,
			    article_title,
			    dab_id,
			    dab_title,
			    redirect_id,
			    redirect_title,
			    template_id,
			    template_title,
			    $revid AS rev_id,
			    '$uname' AS user,
			    0 AS is_reg,
			    ".($new_article ? 1 : 0)." AS is_new,
			    '$sqlDate' AS dab_date
		       FROM very_latest_dab_links
		      WHERE redirect_id = $redirect_id";

      submit_query($tooldb, $sql);

    } else if ($type == CONVERT_TO_DAB || $type == MOVE_DAB) {

      $sql = "INSERT IGNORE INTO recent_dabs_update
		     SELECT $type AS type,
			    article_id,
			    article_title,
			    dab_id,
			    dab_title,
			    redirect_id,
			    redirect_title,
			    template_id,
			    template_title,
			    $revid AS rev_id,
			    '$uname' AS user,
			    0 AS is_reg,
			    ".($new_article ? 1 : 0)." AS is_new,
			    '$sqlDate' AS dab_date
		       FROM very_latest_dab_links
		      WHERE dab_id = $dab_id";

      submit_query($tooldb, $sql);

    } else if ($type == TEMPLATE_DAB) {

      $sql = "INSERT IGNORE INTO recent_dabs_update
		     SELECT $type AS type,
			    article_id,
			    article_title,
			    dab_id,
			    dab_title,
			    redirect_id,
			    redirect_title,
			    template_id,
			    template_title,
			    $revid AS rev_id,
			    '$uname' AS user,
			    0 AS is_reg,
			    ".($new_article ? 1 : 0)." AS is_new,
			    '$sqlDate' AS dab_date
		       FROM very_latest_dab_links
		      WHERE template_id = $template_id
			AND dab_id = $dab_id";

      submit_query($tooldb, $sql);

    } else if ($type == ARTICLE_DAB || $type == INT_DAB || $type == RVV_DAB) {

      $sql = "INSERT IGNORE INTO recent_dabs_update
		     SELECT $type AS type,
			    article_id,
			    article_title,
			    dab_id,
			    dab_title,
			    redirect_id,
			    redirect_title,
			    template_id,
			    template_title,
			    $revid AS rev_id,
			    '$uname' AS user,
			    0 AS is_reg,
			    ".($new_article ? 1 : 0)." AS is_new,
			    '$sqlDate' AS dab_date
		       FROM very_latest_dab_links
		      WHERE article_id = $article_id
			AND dab_id = $dab_id
			AND redirect_id = $redirect_id
			AND template_id = $template_id";

      submit_query($tooldb, $sql);

    } else if ($type == NOT_FOUND) {

      $sql = "INSERT IGNORE INTO recent_dabs_update
		     SELECT $type AS type,
			    article_id,
			    article_title,
			    dab_id,
			    dab_title,
			    redirect_id,
			    redirect_title,
			    template_id,
			    template_title,
			    $revid AS rev_id,
			    '$uname' AS user,
			    0 AS is_reg,
			    ".($new_article ? 1 : 0)." AS is_new,
			    '$sqlDate' AS dab_date
		       FROM very_latest_dab_links
		      WHERE article_id = $article_id
			AND dab_id = $dab_id
			AND redirect_id = $redirect_id
			AND template_id = $template_id";

      submit_query($tooldb, $sql);
    }
  }
  mysqli_close($tooldb);
}


function is_seealso($pattern, $content) {

  global $logging_enabled, $lp;

  $matches = $postmatches = "";
  $seealso = "/(==+)\s*See also\s*==+/i";

  if (preg_match($seealso, $content, $matches, PREG_OFFSET_CAPTURE)) {
    $offset = $matches[0][1] + strlen($matches[0][0]);
    $content = substr($content, $offset);

    $nextsection = "/^".$matches[1][0]."[^=]/m";

    if (preg_match($nextsection, $content, $postmatches, PREG_OFFSET_CAPTURE)) {
      $content = substr($content, 0, $postmatches[0][1]);
    }

    if (preg_match($pattern, $content)) {
      if ($logging_enabled)
        fputs($lp, print_r("     See also match: true\n", true));
      return true;
    }
  }

  return false;
}


function is_disambig($article_id) {

  global $logging_enabled, $lp;

  $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
  $sql = "SELECT 1 FROM page_props
           WHERE pp_page = $article_id
             AND pp_propname = 'disambiguation'";
  $res = mysqli_query($wikidb, $sql);

  if ($res && mysqli_num_rows($res) > 0) {
    if ($logging_enabled)
      fputs($lp, print_r("     Dab check: true\n", true));
    mysqli_close($wikidb);
    return true;
  }

  mysqli_close($wikidb);
  return false;
}


function is_rvv($comment) {

  global $logging_enabled, $lp;

  if (empty($comment)) return false;

  $rvv_words = array('Revert', 'Undid revision', 'vandal', 'restore', 'restoring', 'rvv', 'Wikipedia:HG', 'Wikipedia:TW', 'merge', 'split');

  if ($logging_enabled)
    fputs($lp, print_r("      check comment: $comment\n", true));

  foreach($rvv_words as $r) {
    $pattern = "/".$r."/i";
    if(preg_match($pattern, $comment)) {
      if ($logging_enabled)
        fputs($lp, print_r("        match: $pattern\n", true));
      return true;
    }
  }

  return false;
}


function get_rev_id($title) {

  global $is_error;
  global $last_run_begin_wiki, $lp;
  global $logging_enabled;

  $page_id = "";
  $end_rev_id = 0;
  $end_timestamp = 0;
  $new_article = true;
  $ns_move_date = "";

  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
  $etitle = mysqli_real_escape_string($tooldb, $title);

  $sql = "SELECT DATE_FORMAT(mv_date, '%Y%m%d%H%i%s') AS mv_date
            FROM move_log
           WHERE mv_date > STR_TO_DATE('".$last_run_begin_wiki."','%Y%m%d%H%i%s')
             AND mv_to = '$etitle'
             AND (
                  mv_from LIKE 'User:%' OR
                  mv_from LIKE 'User_talk:%' OR
                  mv_from LIKE 'Wikipedia:%' OR
                  mv_from LIKE 'Wikipedia_talk:%'
                 )
           ORDER BY mv_date DESC
           LIMIT 1";

  $mv_res = submit_query($tooldb, $sql);

  if ($mv_res && mysqli_num_rows($mv_res) == 1) {

    $mv_row = mysqli_fetch_assoc($mv_res);
    $ns_move_date = $mv_row['mv_date'];
    mysqli_free_result($mv_res);

    if ($logging_enabled)
      fputs($lp, print_r("    found new article moved in from other NS\n", true));

  } else {
    # get info about the last revision older than $last_run_begin_wiki
    $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
    $sql = "SELECT rev_id, rev_timestamp
              FROM revision, page
             WHERE page_id = rev_page
               AND page_namespace = 0
               AND page_title = '$etitle'
               AND rev_timestamp <= $last_run_begin_wiki
          ORDER BY rev_timestamp DESC
             LIMIT 1";
    $result = submit_query($wikidb, $sql);

    if ($result && mysqli_num_rows($result) == 1) {
      $row = mysqli_fetch_assoc($result);
      $end_rev_id = (int) $row['rev_id'];
      $end_timestamp = $row['rev_timestamp'];
      $new_article = false;
      mysqli_free_result($result);
    }
    mysqli_close($wikidb);
  }
  mysqli_close($tooldb);

  if ($logging_enabled)
    fputs($lp, print_r("    end_rev_id: $end_rev_id end_timestamp: $end_timestamp new_article: ".($new_article ? "true" : "false")."\n", true));

  return array($end_timestamp, $new_article, $ns_move_date);
}


function post_results_list ( ) {

  global $fp, $is_error;

  if ($is_error) return;
  $begin = time();
  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  submit_query($tooldb, "DROP TABLE IF EXISTS dab_template_titles");
  submit_query($tooldb, "DROP TABLE IF EXISTS very_latest_dabs");
  submit_query($tooldb, "DROP TABLE IF EXISTS very_latest_dab_links");

  submit_query($tooldb,
    "UPDATE recent_dabs_update
        SET is_reg = 1
      WHERE user NOT REGEXP '^(([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3}([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])$'
        AND user <> ''"
  );

  submit_query($tooldb,
    "INSERT IGNORE INTO recent_dabs
            SELECT type,
                   article_id,
                   article_title,
                   dab_id,
                   dab_title,
                   redirect_id,
                   redirect_title,
                   template_id,
                   template_title,
                   rev_id,
                   user,
                   is_reg,
                   is_new,
                   0 AS is_msg,
                   0 AS is_fix,
                   null AS msg_date,
                   dab_date
              FROM recent_dabs_update"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS recent_dabs_update");

  if ($is_error) return;

  submit_query($tooldb, "DROP TABLE IF EXISTS dabs_without_hats");
  submit_query($tooldb, "RENAME TABLE t_dabs_without_hats TO dabs_without_hats");

  submit_query($tooldb, "DROP TABLE IF EXISTS all_dabs");
  submit_query($tooldb, "RENAME TABLE t_all_dabs TO all_dabs");
  if ($is_error) return;

  submit_query($tooldb, "RENAME TABLE all_dab_links TO backup_all_dab_links");
  submit_query($tooldb, "RENAME TABLE t_all_dab_links TO all_dab_links");
  if ($is_error) {
    mysqli_query($tooldb, "RENAME TABLE backup_all_dab_links TO all_dab_links");
    return;
  }
  submit_query($tooldb, "DROP TABLE IF EXISTS backup_all_dab_links");

  submit_query($tooldb, "DROP TABLE IF EXISTS all_dab_links_basic");
  submit_query($tooldb, "RENAME TABLE t_all_dab_links_basic TO all_dab_links_basic");

  submit_query($tooldb, "DROP TABLE IF EXISTS template_dab_links");
  submit_query($tooldb, "RENAME TABLE t_template_dab_links TO template_dab_links");

  submit_query($tooldb, "DROP TABLE IF EXISTS dab_template_tc_count");
  submit_query($tooldb, "RENAME TABLE t_dab_template_tc_count TO dab_template_tc_count");

  submit_query($tooldb, "DROP TABLE IF EXISTS dab_link_count");
  submit_query($tooldb, "RENAME TABLE t_dab_link_count TO dab_link_count");

  submit_query($tooldb, "DROP TABLE IF EXISTS latest_dabs");
  submit_query($tooldb, "RENAME TABLE t_latest_dabs TO latest_dabs");

  submit_query($tooldb, "DROP TABLE IF EXISTS latest_dab_links");
  submit_query($tooldb, "RENAME TABLE t_latest_dab_links TO latest_dab_links");

  submit_query($tooldb, "DROP TABLE IF EXISTS to_dab_link_count");
  submit_query($tooldb, "RENAME TABLE t_to_dab_link_count TO to_dab_link_count");

  submit_query($tooldb, "DROP TABLE IF EXISTS excessive_dab_links");
  submit_query($tooldb, "RENAME TABLE t_excessive_dab_links TO excessive_dab_links");

  submit_query($tooldb, "DROP TABLE IF EXISTS excessive_dab_links_fixed");
  submit_query($tooldb,
    "RENAME TABLE t_excessive_dab_links_fixed TO excessive_dab_links_fixed");

  submit_query($tooldb, "DROP TABLE IF EXISTS dab_to_dab_links");
  submit_query($tooldb, "RENAME TABLE t_dab_to_dab_links TO dab_to_dab_links");

  submit_query($tooldb, "DROP TABLE IF EXISTS named_dab_link_count");
  submit_query($tooldb, "RENAME TABLE t_named_dab_link_count TO named_dab_link_count");

  mysqli_close($tooldb);
  if ($is_error) return;
  fputs($fp, "$"."dab_proc_results['final_steps'] = \"".convert_time(time() - $begin)."\";\n");

}


function dab_cleanup ( ) {

  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  # note: don't call submit_query in this function, to avoid infinite recursion

  mysqli_query($tooldb, "DROP TABLE IF EXISTS dab_template_titles");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS recent_dabs_update");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_all_dab_links_basic");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_all_dab_links");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_dab_to_dab_links");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_excessive_dab_links_fixed");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS tagged_excessive_dab_articles");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_excessive_dab_links");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_to_dab_link_count");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS very_latest_dabs");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS very_latest_dab_links");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_latest_dab_links");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_latest_dabs");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_dab_link_count");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_dab_template_tc_count");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS all_dab_links_part2");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS template_ids");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS all_dab_links_part");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_template_dab_links");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_dabs_without_hats");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_all_dabs");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_named_dab_link_count");

  mysqli_close($tooldb);
}


function get_db_con_local($schema, $server) {
  global $fp;

  $conn = get_db_con($schema, $server);
  if (!$conn) {
    log_error(date("F j G:i", time()), "dab_procs.php", "$server connect", mysqli_connect_error());
    fputs($fp, "\n$"."dab_error = \"".mysqli_connect_error()."\";\n\n?>\n");
    fclose($fp);
    die(1);
  }
  return $conn;
}
