<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");
require("$HOME_DIR/scripts/SxWiki.php");

$sx=new SxWiki;
$config="$HOME_DIR/.dplcredentials.cfg";
$sx->configFile=$config;

$is_error = false;

$fp = fopen("$HOME_DIR/status/mo_results.php", "w");

fputs($fp, "<?PHP\n\n");

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if (! $mysql) {
  log_error(date("F j G:i", time()), "mo_procs.php", "toolsdb connect", mysqli_connect_error());
  fputs($fp, "\n$"."dab_error = \"".mysqli_connect_error()."\";\n\n?>\n");
  fclose($fp);
  $is_error = true;
}

$enwiki = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

if (! $enwiki) {
  log_error(date("F j G:i", time()), "mo_procs.php", "enwiki_p connect", mysqli_connect_error());
  fputs($fp, "\n$"."dab_error = \"".mysqli_connect_error()."\";\n\n?>\n");
  fclose($fp);
  $is_error = true;
}

if (!$is_error) {

  $time_begin = time();
  $begin_run_wiki = date("YmdHis", $time_begin);
  $begin_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_begin));

  fputs($fp, "$"."mo_begin_run = $time_begin;\n" );
  fputs($fp, "$"."mo_begin_run_wiki = \"$begin_run_wiki\";\n" );
  fputs($fp, "$"."mo_begin_run_str = \"$begin_run_str\";\n\n" );

  submit_and_log_query($mysql, "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
  submit_and_log_query($mysql, "SET @@max_heap_table_size=2147483648");
  submit_and_log_query($mysql, "SET @@tmp_table_size=2147483648");

  mo_prep();
  get_redirects();
  create_mo_pl();
  create_template_only_list($sx);
  finish_monthly_list();
  mo_rename();
  $begin = time();
}

if (!$is_error) {
  $duration = time() - $begin;
  fputs($fp, 
    "$"."mo_proc_results['mo_cleanup'] = \"".convert_time($duration)."\";\n");

  $time_finish = time();

  $finish_run_wiki = date("YmdHis", $time_finish);
  $finish_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_finish));
  $total_time_str = convert_time($time_finish - $time_begin);

  fputs($fp, "\n$"."mo_finish_run = $time_finish;\n" );
  fputs($fp, "$"."mo_finish_run_wiki = \"$finish_run_wiki\";\n" );
  fputs($fp, "$"."mo_finish_run_str = \"$finish_run_str\";\n\n" );

  fputs($fp, "$"."mo_total_time_str = \"$total_time_str\";\n\n" );
  mysqli_close($mysql);

  fputs($fp, "\n?>\n");
  fclose($fp);

  if (file_exists("$HOME_DIR/status/mo_last_good_run.php"))
    unlink("$HOME_DIR/status/mo_last_good_run.php");

  rename("$HOME_DIR/status/mo_results.php", "$HOME_DIR/status/mo_last_good_run.php");
}


function mo_prep ( ) {
  global $mysql, $enwiki, $fp, $is_error;
  global $mo_articles;

  $begin = time();

  submit_query($mysql, "DROP TABLE IF EXISTS mo_articles");

/* mo_articles holds the ids and titles of all disambiguation pages
   that will be counted for this month's contest, using the current
   contents of the dab_link_count table (produced by dab_procs.php)
*/

  submit_query($mysql,
    "CREATE TABLE mo_articles (
       mo_id int unsigned NOT NULL default '0',
       mo_title varchar(255) binary NOT NULL default '',
       PRIMARY KEY pk_mo (mo_id)
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC AS
          SELECT lc_id AS mo_id,
                 lc_title AS mo_title
            FROM dab_link_count
           ORDER BY lc_amnt DESC, lc_title ASC
    "
  );

  $mo_articles = [];
  $res = submit_query($mysql, "SELECT mo_id, mo_title FROM mo_articles");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($res)) {
    $mo_articles[(int) $row['mo_id']] = $row['mo_title'];
  }

  submit_query($mysql, "DROP TABLE IF EXISTS mo_redirect_list");

  submit_query($mysql,
    "CREATE TABLE mo_redirect_list (
       mo_r_id int unsigned NOT NULL default '0',
       mo_r_title varchar(255) binary NOT NULL default '',
       rev_id int unsigned NOT NULL default '0',
       mo_r_target int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       UNIQUE KEY u_mrl (mo_r_title), 
       INDEX i_mrl2 (dab_title)
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($mysql, "DROP TABLE IF EXISTS mo_pl");

  submit_query($mysql,
    "CREATE TABLE mo_pl (
       pl_from int unsigned NOT NULL default '0',
       rev_id int unsigned NOT NULL default '0',
       pl_to int unsigned NOT NULL default '0',
       pl_r int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default ''
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($mysql, "DROP TABLE IF EXISTS mo_template_pl");
    
  submit_query($mysql,
    "CREATE TABLE mo_template_pl (
       t_id int unsigned NOT NULL default '0',
       rev_id int unsigned NOT NULL default '0',
       t_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default ''
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($mysql, "DROP TABLE IF EXISTS mo_monthly_list_part");

  submit_query($mysql,
    "CREATE TABLE mo_monthly_list_part (
       article_id int unsigned NOT NULL default '0',
       article_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       redirect_id int unsigned NOT NULL default '0',
       redirect_title varchar(255) binary NOT NULL default ''
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($mysql, "DROP TABLE IF EXISTS mo_article_template_links");

  submit_query($mysql,
    "CREATE TABLE mo_article_template_links (
       article_id int unsigned NOT NULL default '0',
       article_rev_id int unsigned NOT NULL default '0',
       article_title varchar(255) binary NOT NULL default '',
       template_id int unsigned NOT NULL default '0',
       template_rev_id int unsigned NOT NULL default '0',
       template_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default ''
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($mysql, "DROP TABLE IF EXISTS mo_t_template_only_articles");

  submit_query($mysql,
    "CREATE TABLE mo_t_template_only_articles (
       article_id int unsigned NOT NULL default '0',
       article_rev_id int unsigned NOT NULL default '0',
       template_id int unsigned NOT NULL default '0',
       template_rev_id int unsigned NOT NULL default '0',
       template_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default ''
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($mysql, "DROP TABLE IF EXISTS mo_t_template_also_articles");

  submit_query($mysql,
    "CREATE TABLE mo_t_template_also_articles (
       article_id int unsigned NOT NULL default '0',
       article_rev_id int unsigned NOT NULL default '0',
       template_id int unsigned NOT NULL default '0',
       template_rev_id int unsigned NOT NULL default '0',
       template_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default ''
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  if ($is_error) return;
  $duration = time() - $begin;
  fputs($fp, 
    "$"."mo_proc_results['mo_prep'] = \"".convert_time($duration)."\";\n");
}


function get_redirects ( ) {
  global $mysql, $enwiki, $fp, $is_error;
  global $mo_articles, $redirtitles, $redirects, $r_revids, $mo_link_candidates;

  # retrieve all redirects to dab pages on the contest list
  $begin = time();

  $batches = [];
  $batch = "(";
  $count = 0;

  $redirtitles = [];     // will map id -> title
  $redirects = [];       // will map redir id -> target id
  $r_revids = [];        // will map redir id -> page_latest revision

  foreach($mo_articles as $art_id => $mtitle) {
    $redirects[$art_id] = $art_id; // non-redirects map to themselves
    if ($count > 0)
      $batch .= ",";
    $mtitle = mysqli_real_escape_string($enwiki, $mtitle);
    $batch .= "'$mtitle'";
    $count += 1;
    if ($count >= 128) {
      $batches[] = $batch . ")";
      $batch = "(";
      $count = 0;
    }
  }
  if ($count > 0)
    $batches[] = $batch . ")";
  $dabindex = array_flip($mo_articles);

  foreach($batches as $bat) {
    $sql2 = "SELECT rd_from as oc_id, 
                    rd_title as dab_title, 
                    page_title as oc_title,
                    page_latest
               FROM redirect, page
              WHERE page_namespace = 0
                AND rd_namespace = 0
                AND rd_from != 0
                AND rd_title != ''
                AND rd_title IN $bat
                AND page_id = rd_from
              GROUP BY rd_from";
    $res2 = submit_query($enwiki, $sql2);
    if ($is_error) return;
    while ($row2 = mysqli_fetch_assoc($res2)) {
      $redirtitles[(int) $row2['oc_id']] = $row2['oc_title'];
      $redirects[(int) $row2['oc_id']] = $dabindex[$row2['dab_title']];
      $r_revids[(int) $row2['oc_id']] = (int) $row2['page_latest'];
    }
    mysqli_free_result($res2);
  }
  unset($dabindex);

  # store the mo_redirect_list table

  $batches = [];
  $batch = "";
  $count = 0;

  foreach ($redirtitles as $r_id => $r_title) {
    if (endswith ($r_title, "_(disambiguation)") || endswith ($r_title, "_(number)"))
      continue;
    $rtitle = mysqli_real_escape_string($mysql, $r_title);
    $dtitle = mysqli_real_escape_string($mysql, $mo_articles[$redirects[$r_id]]);
    if ($count > 0)
      $batch .= ",";
    $batch .= "($r_id, '$rtitle', {$r_revids[$r_id]}, {$redirects[$r_id]}, '$dtitle')";
    $count += 1;
    if ($count >= 128) {
      $batches[] = $batch;
      $batch = "";
      $count = 0;
    }
  }
  if ($count > 0)
    $batches[] = $batch;

  foreach ($batches as $bat) {
    $sql = "INSERT INTO mo_redirect_list
                       (mo_r_id, mo_r_title, rev_id, mo_r_target, dab_title)
                 VALUES $bat";
    submit_query($mysql, $sql);
    if ($is_error) return;
  }

  # mo_link_candidates merges mo_articles and redir_titles, and omits INTDABLINKs

  $mo_link_candidates = [];
  foreach ($mo_articles as $dab_id => $dab_title) {
    if (endswith ($dab_title, "_(disambiguation)") ||
        endswith ($dab_title, "_(number)"))
      continue;
    $mo_link_candidates[$dab_id] = $dab_title;
  }
  foreach ($redirtitles as $rd_id => $rd_title) {
    if (endswith ($rd_title, "_(disambiguation)") || 
        endswith ($rd_title, "_(number)"))
      continue;
    $mo_link_candidates[$rd_id] = $rd_title;
  }

  $duration = time() - $begin;
  fputs($fp, 
    "$"."mo_proc_results['get_redirects'] = \"".convert_time($duration)."\";\n");
}


function create_mo_pl ( ) {
  global $mysql, $enwiki, $fp, $is_error;
  global $mo_articles, $redirtitles, $redirects, $r_revids, $mo_link_candidates;

  $begin = time();

  # get all incoming links from ns 0 or 10 to contest candidates

  $article_links = [];
  $template_links = [];

  $batches = [];
  $batch = "";
  $count = 0;

  $titleindex = array_flip($mo_link_candidates);

  foreach ($mo_link_candidates as $target_title) {
    if ($count > 0)
      $batch .= ",";
    $ttitle = mysqli_real_escape_string($enwiki, $target_title);
    $batch .= "'$ttitle'";
    $count += 1;
    if ($count >= 128) {
      $batches[] = "($batch)";
      $batch = "";
      $count = 0;
    }
  }
  if ($count > 0) {
    $batches[] = "($batch)";
  }

  foreach ($batches as $bat) {
    $sql = "SELECT pl_from, lt_title AS pl_title, page_title, page_latest, pl_from_namespace AS ns
              FROM pagelinks, linktarget, page
             WHERE lt_namespace = 0
               AND pl_from_namespace IN (0, 10)
               AND pl_from = page_id
               AND pl_target_id = lt_id
               AND page_namespace = pl_from_namespace
               AND page_is_redirect = 0
               AND lt_title IN $bat";

    $result = submit_query($enwiki, $sql);
    if ($is_error) return;
    while ($row = mysqli_fetch_assoc($result)) {
      $target = $titleindex[$row['pl_title']];
      #   Remove self-referential links
      if ($row['pl_from'] == $target || $row['pl_from'] == $redirects[$target])
        continue;
      if ($row['ns'] == 0) {
        $article_links[] = array("from_id" => (int) $row['pl_from'],
                                 "from_title" => $row['page_title'],
                                 "target_id" => $target,
                                 "target_title" => $row['pl_title'],
                                 "rev_id" => (int) $row['page_latest'],
                                 "dab_id" => $redirects[$target]
                                );
      } else if ($row['ns'] == 10) {
        $template_links[] = array("t_id" => (int) $row['pl_from'],
                                  "t_title" => $row['page_title'],
                                  "target_id" => $target,
                                  "target_title" => $row['pl_title'],
                                  "rev_id" => (int) $row['page_latest'],
                                  "dab_id" => $redirects[$target]
                                 );
      }
    }
    mysqli_free_result($result);
  }

  # save the mo_pl table

  $batches = [];
  $batch = "";
  $count = 0;

  foreach ($article_links as $alink) {
    $dtitle = mysqli_real_escape_string($mysql, $mo_articles[$alink['dab_id']]);
    if ($count > 0)
      $batch .= ",";
    $batch .= "({$alink['from_id']}, {$alink['rev_id']}, {$alink['dab_id']}, {$alink['target_id']}, '$dtitle')";
    $count += 1;
    if ($count >= 128) {
      $batches[] = $batch;
      $batch = "";
      $count = 0;
    }
  }

  foreach ($batches as $bat) {
    mysqli_close($mysql);
    $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
    $sql = "INSERT INTO mo_pl (pl_from, rev_id, pl_to, pl_r, dab_title) VALUES $bat";
    submit_query($mysql, $sql);
    if ($is_error) return;
  }

  # save the mo_template_pl table

  $batches = [];
  $batch = "";
  $count = 0;

  foreach ($template_links as $tlink) {
    $dtitle = mysqli_real_escape_string($mysql, $mo_articles[$tlink['dab_id']]);
    $ttitle = mysqli_real_escape_string($mysql, $tlink['t_title']);
    if ($count > 0)
      $batch .= ",";
    $batch .= "({$tlink['t_id']}, {$tlink['rev_id']}, '$ttitle', {$tlink['dab_id']}, '$dtitle')";
    $count += 1;
    if ($count >= 128) {
      $batches[] = $batch;
      $batch = "";
      $count = 0;
    }
  }

  foreach ($batches as $bat) {
    $sql = "INSERT INTO mo_template_pl 
                   (t_id, rev_id, t_title, dab_id, dab_title) VALUES $bat";
    submit_query($mysql, $sql);
    if ($is_error) return;
  }

  $duration = time() - $begin;
  fputs($fp, 
    "$"."mo_proc_results['create_mo_pl'] = \"".convert_time($duration)."\";\n");

  #   Create the monthly_list_part
  #   table before we eliminate the redirects

  $begin = time();

  #   mo_monthly_list_part is links to dab pages in mo_articles

  $m_batches = [];
  $m_batch = "";
  $m_count = 0;
  foreach ($article_links as $link) {
    $atitle = mysqli_real_escape_string($mysql, $link['from_title']);
    $dtitle = mysqli_real_escape_string($mysql, $mo_articles[$link['dab_id']]);
    $rtitle = mysqli_real_escape_string($mysql, $link['target_title']);
    if (isset($mo_articles[$link['dab_id']])) {
      if ($m_count > 0)
        $m_batch .= ",";
      $m_batch .= "({$link['from_id']}, '$atitle', {$link['dab_id']},
                    '$dtitle', {$link['target_id']}, '$rtitle')";
      $m_count += 1;
      if ($m_count >= 128) {
        $m_batches[] = $m_batch;
        $m_batch = "";
        $m_count = 0;
      }
    }
  }

  if ($m_count > 0)
    $m_batches[] = $m_batch;

  foreach ($m_batches as $mbat) {
    $sql = "INSERT INTO mo_monthly_list_part
                        (article_id, article_title, dab_id, 
                         dab_title, redirect_id, redirect_title)
                 VALUES $mbat";
    submit_query($mysql, $sql);
    if ($is_error) return;
  }

  # Make the mo_pl table unique with a group by statement

  submit_query($mysql, "DROP TABLE IF EXISTS mo_pl_unique");

  submit_query($mysql, 
    "CREATE TABLE mo_pl_unique (
       pl_from int unsigned NOT NULL default '0',
       rev_id int unsigned NOT NULL default '0',
       pl_to int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       UNIQUE INDEX (pl_from, rev_id, pl_to)
     ) ENGINE=InnoDB AS
         SELECT pl_from,
                rev_id,
                pl_to,
                dab_title
           FROM mo_pl
       GROUP BY pl_from, rev_id, pl_to, dab_title"
  );

  submit_query($mysql, "DROP TABLE IF EXISTS mo_pl");
  submit_query($mysql, "RENAME TABLE mo_pl_unique TO mo_pl");
    
  submit_query($mysql, "DROP TABLE IF EXISTS mo_template_ids");

    #
    # List of templates (id and title) that link
    # to disambigs.
    #

  submit_query($mysql,
    "CREATE TABLE mo_template_ids (
       t_id int unsigned NOT NULL default '0',
       t_title varchar(255) binary NOT NULL default '',
       PRIMARY KEY pk_tid (t_id)
     ) ENGINE=InnoDB AS
          SELECT t_id,
                 t_title
            FROM mo_template_pl
           GROUP BY t_id, t_title"
  );

  if ($is_error) return;

  $duration = time() - $begin;
  fputs($fp, 
    "$"."mo_proc_results['mo_pl_cleanup'] = \"".convert_time($duration)."\";\n");

  # drop any multiple dablinks to same dab target

  $t_dablinks_index = [];
  foreach($article_links as $i => $onelink) {
    if (isset($t_dablinks_index[$onelink['from_id']])) {
      if (in_array($onelink['dab_id'], $t_dablinks_index[$onelink['from_id']], true) === false) {
        # from_id is already in index, but this target is not
        $t_dablinks_index[$onelink['from_id']][] = $onelink['dab_id'];
      } else {
        # index already contains a link from this article to the target dab page
        unset($article_links[$i]);
      }
    } else {
      # from_id not yet in index
      $t_dablinks_index[$onelink['from_id']] = array($onelink['dab_id']);
    }
  }

  # Delete templates that are not used by mainspace non-redirect articles

  $begin = time();

  # create a list of unique templates containing >=1 links to dab pages

  $templatelist = [];
  $keeplist = [];

  foreach ($template_links as $tlink) {
    $templatelist[$tlink['t_id']] = $tlink['t_title'];
  }

  # For each template, check whether any page that transcludes the template is
  # a non-redirect mainspace article
  foreach ($templatelist as $t_id => $t_title) {
    $esctitle = mysqli_real_escape_string($enwiki, $t_title);
    $query = "SELECT 1 FROM templatelinks, linktarget
               WHERE tl_target_id = lt_id
                 AND lt_title = '$esctitle'
                 AND lt_namespace = 10
          AND EXISTS (SELECT * FROM page
                       WHERE page_id = tl_from
                         AND page_namespace = 0
                         AND page_is_redirect = 0)
    ";
    if ($t_title != "Pagetype") {
      $qresult = submit_query($enwiki, $query);
        if ($is_error) return;
        if (mysqli_num_rows($qresult) >= 1) {
          # a transcluding article was found
          $keeplist[$t_id] = $t_title;
        }
      if ($qresult)
        mysqli_free_result($qresult);
    }
  }
  foreach ($template_links as $i => $tlink) {
    if (! isset($keeplist[$tlink['t_id']])) {
      unset($template_links[$i]);
    }
  }

  $duration = time() - $begin;
  fputs($fp, "$"."mo_proc_results['mo_remove_unlinked_templates'] = \"".convert_time($duration)."\";\n");

  # Delete templates that have a dab link in a non-transcluded portion of text

  # For each template in list, get ids of up to 4 mainspace articles that
  # transclude the template; if none of those 4 articles is in $dablinks,
  # remove the template from the list

  $begin = time();

  $removelist = [];

  foreach ($keeplist as $t_id => $t_title) {
    $esctitle = mysqli_real_escape_string($enwiki, $t_title);
    $query2 = "SELECT page_id 
                 FROM templatelinks, linktarget, page
                WHERE tl_target_id = lt_id
                  AND lt_title='$esctitle'
                  AND lt_namespace = 10
                  AND tl_from = page_id
                  AND page_namespace = 0
                  AND page_is_redirect = 0
                LIMIT 4";
    $q2result = submit_query($enwiki, $query2);
    if ($is_error) return;
    $found = false;
    while ($qrow = mysqli_fetch_assoc($q2result)) {
      if (isset($t_dablinks_index[(int) $qrow['page_id']])) { // FIXME
        $found = true;
        break;
      }
    }
    mysqli_free_result($q2result);
    if (! $found) {
      $removelist[$t_id] = true;
    }
  }
  foreach ($template_links as $i => $tlink) {
    if (isset($removelist[$tlink['t_id']])) {
      unset($template_links[$i]);
    }
  }

  $duration = time() - $begin;
  fputs($fp, "$"."mo_proc_results['mo_find_noinclude_links'] = \"".convert_time($duration)."\";\n");

  unset($t_dablinks_index);
  unset($removelist);

  # populate mo_article_template_links

  $begin = time();

  # retrieve the articles that transclude each of the remaining templates

  $batches = [];
  $batch = "";
  $count = 0;

  foreach ($template_links as $t_id => $tlink) {
    $ttitle = mysqli_real_escape_string($enwiki, $tlink['t_title']);
    $rtitle = mysqli_real_escape_string($enwiki, $tlink['target_title']);
    $dtitle = mysqli_real_escape_string($enwiki, $mo_articles[$tlink['dab_id']]);
    $sql = "SELECT page_id, page_latest, page_title
              FROM page, templatelinks, linktarget AS tt, pagelinks, linktarget AS pt
             WHERE tl_target_id = tt.lt_id
               AND tt.lt_title = '$ttitle'
               AND tt.lt_namespace = 10
               AND tl_from = page_id
               AND pl_from = page_id
               AND pl_target_id = pt.lt_id
               AND pt.lt_namespace = 0
               AND pt.lt_title = '$rtitle'
               AND page_namespace = 0
               AND page_is_redirect = 0";
    $result = submit_query($enwiki, $sql);
    if ($is_error) return;
    while ($row = mysqli_fetch_assoc($result)) {
      $atitle = mysqli_real_escape_string($enwiki, $row['page_title']);
      if ($count > 0)
        $batch .= ",";
      $batch .= "({$row['page_id']}, {$row['page_latest']}, '$atitle', $t_id,
                  {$tlink['rev_id']}, '$ttitle', {$tlink['dab_id']}, '$dtitle')";
      $count += 1;
      if ($count >= 32) {
        $batches[] = $batch;
        $batch = "";
        $count = 0;
      }
    }
    if ($count > 0)
      $batches[] = $batch;
    mysqli_free_result($result);
  }
  mysqli_close($mysql);
  $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  foreach ($batches as $bat) {
    $sql = "INSERT INTO mo_article_template_links
                        (article_id, article_rev_id, article_title, template_id,
                         template_rev_id, template_title, dab_id, dab_title)
            VALUES $bat";
    submit_query($mysql, $sql);
    if ($is_error) return;
  }

  $duration = time() - $begin;
  fputs($fp, "$"."mo_proc_results['mo_article_template_links'] = \"".convert_time($duration)."\";\n");
}

function create_template_only_list ($sx) {
  global $mysql, $enwiki, $is_error, $fp;

  $begin = time();
  $start = "(\[\[)";
  $end = "(\||\]\]|#)";

  $sql = "SELECT dab_id,
                 dab_title
            FROM mo_article_template_links
           GROUP BY dab_id, dab_title
           ORDER BY dab_title";

  $result = mysqli_query($mysql, $sql);

  while ($row = mysqli_fetch_assoc($result)) {

    $dab_id = $row['dab_id'];
    $dab_title = str_replace("_", " ", $row['dab_title']);
    $all_titles = "";

    #
    # all_titles contains the title of the DAB and its redirects. 
    # This is the set of titles we will be looking for using 
    # preg_match(). The escape_chars() method in common.php
    # adds upper and lowercase starting characters, and
    # all possible <space> vs. <underscore> combinations
    # to the matching pattern.
    #
    $all_titles = array($dab_title);

    $sql = "SELECT mo_r_title
              FROM mo_redirect_list
             WHERE mo_r_target = $dab_id";

    try {
      $res_redirs = mysqli_query($mysql, $sql);
    } catch (Error $e) {
      $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
      $res_redirs = mysqli_query($mysql, $sql);
    }

    while ($redir_row = mysqli_fetch_assoc($res_redirs)) {
      array_push($all_titles, $redir_row['mo_r_title']);
    }

    #print_r($all_titles);

    # Get each article-to-template row and investigate.
    $sql = "SELECT article_id,
                   article_rev_id,
                   article_title,
                   template_id,
                   template_rev_id,
                   template_title
              FROM mo_article_template_links
             WHERE dab_id = $dab_id";

    $res_at_links = mysqli_query($mysql, $sql);

    while ($at_row = mysqli_fetch_assoc($res_at_links)) {

      #
      # $a_id = the id of the article that linked to the template
      # $a_rev_id = the rev_id of the article when the monthly scan was run
      # $t_id = the id of the template
      # $t_rev_id = the rev_id of the template when the monthly scan was run
      # $t_title = the title of the template
      #
      $a_id = $at_row['article_id'];
      $a_rev_id = $at_row['article_rev_id'];
      $a_title = $at_row['article_title'];
      $t_id = $at_row['template_id'];
      $t_rev_id = $at_row['template_rev_id'];
      $t_title = $at_row['template_title'];

      $request_vars = array(
              'action' => 'query',
              'prop' => 'revisions',
              'titles' => $a_title,
              'rvprop' => 'ids|user|content',
              'rvstartid' => $a_rev_id,
              'rvendid' => $a_rev_id,
              'format' => 'php'
      );

      #print_r($request_vars);

      if(!$array = $sx->callAPI($request_vars))
        die("SxWiki error.");

      if(isset($array['query']['pages']))
        $page_id = key($array['query']['pages']);
      else
        $page_id = 0;

      if(isset($array['query']['pages'][$page_id]['revisions'][0])) {

        $rev = $array['query']['pages'][$page_id]['revisions'][0];
        $content = $rev['*'];
        $is_match = false;

        foreach($all_titles as $t) {
          $safe_title = escape_chars($t);
          $pattern = "/{$start}\s*{$safe_title}\s*{$end}/";
          if(preg_match($pattern, $content, $matches)) {
	          #print_r("Dab: $dab_title");
	          #print_r($matches);
            $is_match = true;
          }
        }
        
        if (!$is_match) {
        	$start = "(\{\{[^\[]*\|)";
        	$end = "(\}\}|\|[^\]]*\}\}|#[^\]]*\}\}|\{\{!\}\}[^\]]*\}\})";
	        foreach($all_titles as $t) {
	          $safe_title = escape_chars($t);
	          $pattern = "/{$start}\s*{$safe_title}\s*{$end}/";
	          if(preg_match($pattern, $content, $matches)) {
	          	#print_r("Dab: $dab_title");
	          	#print_r($matches);
	            $is_match = true;
	          }
	        }
        }
        
        $esc_t_title = str_replace("'", "\'", $t_title);
        $esc_dab_title = str_replace("'", "\'", $dab_title);
        
        if ($is_match) {
          add_template_also_row($mysql, $a_id, $a_rev_id, $t_id, $t_rev_id, $esc_t_title, $dab_id, $esc_dab_title);
        } else {
          add_template_only_row($mysql, $a_id, $a_rev_id, $t_id, $t_rev_id, $esc_t_title, $dab_id, $esc_dab_title);
        }
      }
    }
  }

  fputs($fp, "$"."mo_proc_results['create_template_only_list'] = \"".convert_time(time() - $begin)."\";\n");
}


function add_template_also_row($mysql, $a_id, $a_rev_id, $t_id, $t_rev_id, $t_title, $dab_id, $dab_title) {

  try {
    mysqli_close($mysql);
  } catch (Error $e) {
    // do nothing
  }
  $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
  $sql = "INSERT INTO mo_t_template_also_articles
                      (article_id, article_rev_id, template_id, template_rev_id, template_title, dab_id, dab_title)
               VALUES ($a_id, $a_rev_id, $t_id, $t_rev_id, '$t_title', $dab_id, '$dab_title')";

  $result = mysqli_query($mysql, $sql);

  if ($result == false)
    log_error(date("F j G:i", time()), "mo_procs.php", $sql, mysqli_error($mysql));
}


function add_template_only_row($mysql, $a_id, $a_rev_id, $t_id, $t_rev_id, $t_title, $dab_id, $dab_title) {

  try {
    mysqli_close($mysql);
  } catch (Error $e) {
    // do nothing
  }
  $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
  $sql = "INSERT INTO mo_t_template_only_articles
                      (article_id, article_rev_id, template_id, template_rev_id, template_title, dab_id, dab_title)
               VALUES ($a_id, $a_rev_id, $t_id, $t_rev_id, '$t_title', $dab_id, '$dab_title')";

  $result = mysqli_query($mysql, $sql);

  if ($result == false)
    log_error(date("F j G:i", time()), "mo_procs.php", $sql, mysqli_error($mysql));
}


function finish_monthly_list ( ) {

  global $mysql, $fp, $is_error;

  $begin = time();
  try {
    mysqli_close($mysql);
  } catch (Error $e) {
    // do nothing
  }
  $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  submit_query ($mysql, "DROP TABLE IF EXISTS mo_article_template_links");

  submit_query ($mysql,
    "DELETE FROM mo_pl
       WHERE EXISTS
           (
            SELECT 1
              FROM mo_t_template_only_articles
             WHERE pl_from = article_id
               AND pl_to = dab_id
           )
  ");
    
  submit_query ($mysql, "DROP TABLE IF EXISTS t_monthly_list_full");
    
  submit_query ($mysql,
    "CREATE TABLE t_monthly_list_full (
       article_id int unsigned NOT NULL default '0',
       article_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       redirect_id int unsigned NOT NULL default '0',
       redirect_title varchar(255) binary NOT NULL default '',
       template_id int unsigned NULL,
       template_title varchar(255) binary NULL,
       INDEX i_mlf (dab_id)
     ) ENGINE=InnoDB AS
         SELECT a.article_id AS article_id,
                a.article_title AS article_title,
                a.dab_id AS dab_id,
                a.dab_title AS dab_title,
                a.redirect_id AS redirect_id,
                a.redirect_title AS redirect_title,
                b.template_id AS template_id,
                b.template_title AS template_title
           FROM mo_monthly_list_part a LEFT OUTER JOIN mo_t_template_only_articles b
             ON a.article_id = b.article_id
            AND a.dab_id = b.dab_id"
  );
    
  submit_query ($mysql,
    "UPDATE t_monthly_list_full
        SET template_id = 0
      WHERE template_id IS NULL"
  );
    
  submit_query ($mysql,
    "UPDATE t_monthly_list_full
        SET template_title = ''
      WHERE template_title IS NULL"
  );
    
  submit_query ($mysql, "DROP TABLE IF EXISTS mo_monthly_list_part");
    
  submit_query ($mysql,
    "DELETE FROM t_monthly_list_full
           WHERE NOT EXISTS
                   (
                    SELECT 1
                      FROM mo_pl
                     WHERE article_id = pl_from
                       AND dab_id = pl_to
                   )
             AND NOT EXISTS
                   (
                    SELECT 1
                      FROM mo_t_template_only_articles o
                     WHERE t_monthly_list_full.article_id = o.article_id
                       AND t_monthly_list_full.dab_id = o.dab_id
                   )"
  );
    
  submit_query ($mysql, "DROP TABLE IF EXISTS mo_monthly_list_full_unique");

  submit_query ($mysql,
    "CREATE TABLE mo_monthly_list_full_unique (
       article_id int unsigned NOT NULL default '0',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default ''
     ) ENGINE=InnoDB AS
         SELECT article_id,
                dab_id,
                dab_title
           FROM t_monthly_list_full
       GROUP BY article_id, dab_id"
  );
    
  submit_query ($mysql, "DROP TABLE IF EXISTS t_monthly_list_count");

  submit_query ($mysql,
    "CREATE TABLE t_monthly_list_count (
       lc_id int unsigned NOT NULL default '0',
       lc_title varchar(255) binary NOT NULL default '',
       lc_amnt int unsigned NOT NULL default '0'
     ) ENGINE=InnoDB AS
         SELECT dab_id as lc_id,
                dab_title as lc_title,
                count(*) as lc_amnt
           FROM mo_monthly_list_full_unique
       GROUP BY lc_id
       ORDER BY lc_amnt DESC, lc_title"
  );  /* removed overlength index        UNIQUE INDEX u_mlc (lc_title)  */
    
  submit_query ($mysql, "DROP TABLE IF EXISTS mo_monthly_list_full_unique");
    
  if ($is_error) return;
  fputs($fp, "$"."mo_proc_results['finish_monthly_list'] = \"".convert_time(time() - $begin)."\";\n");
}


function mo_rename ( ) {

  global $mysql, $fp, $is_error;

  $begin = time();

  try {
    mysqli_close($mysql);
  } catch (Error $e) {
    // do nothing
  }
  $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  submit_query($mysql, "DROP TABLE IF EXISTS bonus_list");
  submit_query($mysql, "DROP TABLE IF EXISTS bonus_list_full");
  submit_query($mysql, "DROP TABLE IF EXISTS bonus_list_count");

  submit_query($mysql, "DROP TABLE IF EXISTS mo_template_only_articles");
  submit_query($mysql,
    "RENAME TABLE mo_t_template_only_articles TO mo_template_only_articles");

  submit_query($mysql, "DROP TABLE IF EXISTS mo_template_also_articles");
  submit_query($mysql, 
    "RENAME TABLE mo_t_template_also_articles TO mo_template_also_articles");
  	
  submit_query($mysql, "DROP TABLE IF EXISTS monthly_list_full");
  submit_query($mysql, "RENAME TABLE t_monthly_list_full TO monthly_list_full");
  	
  submit_query($mysql, "DROP TABLE IF EXISTS monthly_list_count");
  submit_query($mysql, "RENAME TABLE t_monthly_list_count TO monthly_list_count");
  	
  if ($is_error) return;
  fputs($fp, "$"."mo_proc_results['mo_rename'] = \"".convert_time(time() - $begin)."\";\n");

}


function mo_cleanup ( ) {

  global $mysql;

  try {
    mysqli_close($mysql);
  } catch (Error $e) {
    // do nothing
  }
  $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  mysqli_query($mysql, "DROP TABLE IF EXISTS t_monthly_list_count");
  mysqli_query($mysql, "DROP TABLE IF EXISTS mo_monthly_list_full_unique");
  mysqli_query($mysql, "DROP TABLE IF EXISTS t_monthly_list_full");
  mysqli_query($mysql, "DROP TABLE IF EXISTS mo_t_template_also_articles");
  mysqli_query($mysql, "DROP TABLE IF EXISTS mo_t_template_only_articles");
  mysqli_query($mysql, "DROP TABLE IF EXISTS mo_article_template_links");
  mysqli_query($mysql, "DROP TABLE IF EXISTS mo_del_rows");
  mysqli_query($mysql, "DROP TABLE IF EXISTS mo_template_keep_ids");
  mysqli_query($mysql, "DROP TABLE IF EXISTS mo_template_ids");
  mysqli_query($mysql, "DROP TABLE IF EXISTS mo_pl_unique");
  mysqli_query($mysql, "DROP TABLE IF EXISTS mo_monthly_list_part");
  mysqli_query($mysql, "DROP TABLE IF EXISTS mo_template_pl");
  mysqli_query($mysql, "DROP TABLE IF EXISTS mo_redirects");

}


function submit_query ($server, $sql) {

  global $fp, $is_error;

  if ($is_error) {
    return;
  }

  $result = mysqli_query($server, $sql);

  if ($result == false) {
    log_error(date("F j G:i", time()), "mo_procs.php", $sql, mysqli_error($server));
    fputs($fp, "\n$"."mo_error = \"$sql: ".mysqli_error($server)."\";\n");
    fclose($fp);
    $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
    mo_cleanup();
    $is_error = true;
  }

  return $result;

}


function submit_and_log_query ($server, $sql, $nick = "") {
  
  global $fp, $is_error;

  if ($is_error) {
    return;
  }

  if ($nick == "")
    $nick = $sql;

  $begin = time();

  $result = submit_query($server, $sql);

  if (! $is_error) {
    $duration = time() - $begin;
    fputs($fp, 
      "$"."mo_proc_results['$nick'] = \"".convert_time($duration)."\";\n");
  }

  return $result;

}
?>