#! /usr/bin/python3
"Place notices on talk pages of users who have recently created dablinks."

import datetime
import locale
import pathlib
import traceback
import MySQLdb
import pywikibot

locale.setlocale(locale.LC_ALL, "")
MIN_EDIT_COUNT = 100
MIN_NOTIFY_INTERVAL = "'6 18' DAY_HOUR"  # next message delayed to 7th day
MIN_CHECK_INTERVAL = "9 DAY"
INLINE_DAB_LIMIT = 3

def convert_interval(interval, use_seconds = False):
    "Convert a timedelta object to a string"
    # Don't display seconds unless requested, or if interval < one minute

    def plural(number):
        return "s" if number != 1 else ""

    days = interval.days
    hours = interval.seconds // 3600
    minutes = (interval.seconds % 3600) // 60
    seconds = max(0, interval.seconds % 60)
    if days == 1:
        days = 0
        hours += 24
    result = ''
    if days == 0 and hours == 0 and minutes == 0:
        result = f"{seconds} second{plural(seconds)}"
    elif use_seconds:
        if days == 0:
            if hours == 0:
                result = f"{minutes} minute{plural(minutes)} {seconds} second{plural(seconds)}"
            else:
                result = f"{hours} hour{plural(hours)} {minutes} minute{plural(minutes)} {seconds} second{plural(seconds)}"
        else:
            result = f"{days} day{plural(days)} {hours} hour{plural(hours)} {minutes} minute{plural(minutes)} {seconds} second{plural(seconds)}"
    else:
        if days == 0:
            if hours == 0:
                result = f"{minutes} minute{plural(minutes)}"
            else:
                result = f"{hours} hour{plural(hours)} {minutes} minute{plural(minutes)}"
        else:
            result = f"{days} day{plural(days)} {hours} hour{plural(hours)} {minutes} minute{plural(minutes)}"
    return result

class UserNotifier:
    "Send talk page notices to users who have created new dablinks."

    def __init__(self):
        self.logging_enabled = True
        if self.logging_enabled:
            self.logfile = pathlib.Path("/data/project/dplbot",
                    "dplbot","logs/user_notify_results.log").open("w", encoding="utf-8")
        else:
            self.logfile = None
        self.tooldb = MySQLdb.connect(db='s51290__dpl_p',
                          host='tools.db.svc.wikimedia.cloud',
                          read_default_file='/data/project/dplbot/.my.cnf',
                          charset="utf8mb4")
        self.tcursor = self.tooldb.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        self.wikidb = MySQLdb.connect(db='enwiki_p',
                          host='enwiki.analytics.db.svc.wikimedia.cloud',
                          read_default_file='/data/project/dplbot/.my.cnf',
                          charset="utf8mb4")
        self.wcursor = self.wikidb.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        self.tcursor.execute(b"""
            CREATE TABLE IF NOT EXISTS notified_users (
                username varchar(255) binary NOT NULL default '',
                PRIMARY KEY pk_user (username)
            ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC
        """)

    def log(self, message):
        if self.logging_enabled:
            self.logfile.write(message)

    def is_too_early(self, user):
        "Return True iff user is in cooldown period"
        euser = self.tooldb.escape_string(user.encode("utf-8"))
        res = self.tcursor.execute(f"""
                    SELECT MAX(msg_date) AS msg_date
                      FROM recent_dabs
                     WHERE user = '{euser.decode("utf-8")}'
                       AND type = 5
                       AND msg_date > DATE_SUB(SYSDATE(), INTERVAL {MIN_NOTIFY_INTERVAL})
            """.encode("utf-8"))
        if res > 0:
            row = self.tcursor.fetchone()
            msg_date = row['msg_date'] # this will be a datetime.datetime object, or None
            if msg_date is None:
                return False
            self.log("  Editor has been recently notified ({convert_interval(datetime.datetime.now() - msg_date)} ago)\n")
            return True
        return False

    def is_bot(self, user):
        "Return True iff user is a bot account."
        euser = self.wikidb.escape_string(user.encode("utf-8")).decode("utf-8")
        res = self.wcursor.execute(f"""
                SELECT 1
                  FROM user, user_groups
                 WHERE user_name = '{euser}'
                   AND user_id = ug_user
                   AND ug_group = 'bot'
            """.encode("utf-8"))
        if res > 0:
            self.log("  Bot account - no notification\n")
            return True
        return False

    def is_whitelisted(self, user):
        whitelist = ('Woohookitty',
                     'JaGa',
                     "R'n'B",
                     'SchreiberBike',
                     'LittleWink',
                     'JustAGal',
                     'BD2412',
                     'Niceguyedc',
                     'PKT',
                     'TimBentley',
                     'Suriel1981',
                     'Ketiltrout',
                     'Tassedethe',
                     'Rcsprinter123',
                     'William Avery',
                     'JamesAM',
                     'StAnselm',
                     'Vegaswikian',
                     'GoingBatty',
                     'ShelfSkewed',
                     'Ulric1313',
                     'The Banner',
                     'Maunus',
                     'Gryllida'
                    )
        if user in whitelist:
            self.log(f"        whitelist match: {user}\n")
            return True
        return False

    def is_blocked_indef(self, user):
        euser = self.wikidb.escape_string(user.encode("utf-8")).decode("utf-8")
        res = self.wcursor.execute(f"""
                  SELECT ipb_expiry
                    FROM user, ipblocks
                   WHERE user_name = '{euser}'
                     AND user_id = ipb_user
            """.encode("utf-8"))
        if res > 0:
            row = self.wcursor.fetchone()
            expiry = row['ipb_expiry']
            if expiry is None:
                return False
            if expiry.strip() != "infinity":
                return False
            self.log("  Blocked indef - no notification\n")
            return True
        return False

    def user_talk_title(self, user, euser):
        """Return title of user's User talk: page.
        
        euser is the user name, escaped and encoded as a bytes object."""
        # check whether user talk page is a redirect
        res = self.wcursor.execute(f"""
                SELECT page_id
                  FROM page
                 WHERE page_title = '{euser.decode("utf-8")}'
                   AND page_is_redirect = 1
                   AND page_namespace = 3
            """.encode("utf-8"))
        if res > 0:
            # yes, see if target is also a user talk page
            row = self.wcursor.fetchone()
            page_id = row['page_id']
            rdres = self.wcursor.execute(f"""
                    SELECT rd_title
                      FROM redirect
                     WHERE rd_from = {page_id}
                       AND rd_namespace = 3
                """.encode("utf-8"))
            if rdres > 0:
                rdrow = self.wcursor.fetchone()
                rd_title = "User talk:" + rdrow['rd_title'].decode("utf-8").replace("_", " ")
                self.log(f"  Redirect detected, target: {rd_title}\n")
                return rd_title
            self.log("  Redirect detected, but doesn't redirect to a User Talk page\n")
            return ""
        return "User talk:" + user.replace("_", " ")

    def edit_count(self, user):
        count = 0
        euser = self.wikidb.escape_string(user.encode("utf-8")).decode("utf-8")
        res = self.wcursor.execute(f"""
                SELECT user_editcount
                  FROM user
                 WHERE user_name = '{euser}'
            """.encode("utf-8"))
        if res > 0:
            row = self.wcursor.fetchone()
            count = row['user_editcount']
            self.log("  Edit count: {count}\n")
        return count

    def title_list(self, dab_list):
        """Convert a list of article titles to a string format"""
        ct = 1
        size = len(dab_list)
        result = ""
        for dab in dab_list:
            if ct > 1:
                if ct == size:
                    result += " and "
                else:
                    result += ", "
            result += f'[[{dab.replace("_", " ")}]]'
            ct += 1
        return result

    def run(self):
        try:
            s = pywikibot.Site()
            today = datetime.date.today()
            # get list of users who have recently created dab links
            self.tcursor.execute(f"""
                    SELECT user
                      FROM recent_dabs
                     WHERE type = 5
                       AND is_msg = 0
                       AND is_reg = 1
                       AND is_fix = 0
                       AND dab_date > DATE_SUB(SYSDATE(), INTERVAL {MIN_CHECK_INTERVAL})
                     GROUP BY user
                     ORDER BY user
            """.encode("utf-8"))
            for row in self.tcursor.fetchall():
                self.wcursor.execute(b"SELECT lag FROM heartbeat_p.heartbeat WHERE shard='s1'")
                lag = self.wcursor.fetchone()['lag']
                started = datetime.datetime.now() - datetime.timedelta(seconds=float(lag))
                user = row['user']
                if not isinstance(user, str):
                    user = user.decode("utf-8")
                euser = self.tooldb.escape_string(row['user'].encode("utf-8"))
                self.log(f"\n\nNotify user: {user}\n")
                disp_user_talk = self.user_talk_title(user, euser)
                known_res = self.tcursor.execute(
                    b"SELECT username FROM notified_users WHERE username = '%b'"
                    % euser
                )
                known_user = (known_res > 0)
                if not disp_user_talk:
                    continue
                if self.is_too_early(user):
                    continue
                if self.is_bot(user):
                    continue
                if self.is_whitelisted(user):
                    continue
                if self.is_blocked_indef(user):
                    continue
                if self.edit_count(user) < MIN_EDIT_COUNT:
                    self.log("  User below edit count threshold\n")
                    continue
                usertalk = pywikibot.Page(s, disp_user_talk)
                try:
                    if not usertalk.botMayEdit():
                        self.log(f"  Blocked by {{{{nobots}}}} template on {disp_user_talk}\n")
                        continue
                except pywikibot.exceptions.NoPageError:
                    self.log("  No user talk page exists; creating new one.\n")
                # Build message
                article_list = []
                article_count = 0
                inline_style = True
                msg_hdr = f"==Disambiguation link notification for {today.strftime('%B')} {today.day} ==\n\n"
                msg_hdr_long = "Hi. Thank you for your recent edits. An automated process has detected that you've added some links pointing to [[Wikipedia:Disambiguation|disambiguation pages]]. Such links are [[WP:INTDABLINK|usually incorrect]], since a disambiguation page is merely a list of unrelated topics with similar titles. <small>(Read the [[User:DPL bot/Dablink notification FAQ|FAQ]]{{*}} Join us at the [[Wikipedia:Disambiguation pages with links|DPL WikiProject]].)</small>"
                msg_hdr_short = "An automated process has detected that you recently added links to disambiguation pages."
                msg_body = ""
                msg_ftr = "\n\nIt's OK to remove this message. Also, to stop receiving these messages, follow these [[User:DPL bot|opt-out instructions]]. Thanks, --~~~~"
                msg_ftr_short = "\n\n([[User:DPL bot|Opt-out instructions]].) --~~~~"

                # get list of recent dab links by user
                article_count = self.tcursor.execute(b"""
                        SELECT article_id,
                               article_title,
                               count(*) AS dab_count
                          FROM recent_dabs
                         WHERE user = '%b'
                           AND is_msg = 0
                           AND is_fix = 0
                           AND type = 5
                           AND dab_date > DATE_SUB(SYSDATE(), INTERVAL %b)
                      GROUP BY article_id
                      ORDER BY dab_count DESC, article_title
                    """ % (euser, MIN_CHECK_INTERVAL.encode("utf-8"))
                )
                self.log(f"  User dab edit count: {article_count}\n")
                if article_count > 0:
                    if article_count > 1:
                        inline_style = False
                    for ct_row in self.tcursor.fetchall():
                        article_id = ct_row['article_id']
                        article_title = ct_row['article_title']
                        disp_article_title = article_title.replace('_', ' ')
                        if not isinstance(disp_article_title, str):
                            disp_article_title = disp_article_title.decode("utf-8")
                        self.log(f" ARTICLE: {disp_article_title}\n")

                        # get the timestamp of this article's latest revision
                        page = pywikibot.Page(s, disp_article_title)
                        try:
                            last_touched = page.latest_revision.timestamp
                        except pywikibot.exceptions.NoPageError:
                            self.log(f"  Page {disp_article_title} does not exist\n")
                            continue
                        if last_touched > started:
                            self.log(f"  Page {disp_article_title} has been edited recently, and replag doesn't allow us to see the changes:\n")
                            self.log(f"   (last page edit {last_touched!s}; replica current to {started!s})\n")
                            continue

                        # get the list of dablinks added by user to the current article
                        self.tcursor.execute(b"""
                                SELECT redirect_title
                                  FROM recent_dabs
                                 WHERE user = '%b'
                                   AND article_id = %d
                                   AND is_msg = 0
                                   AND is_fix = 0
                                   AND type = 5
                                   AND dab_date > DATE_SUB(SYSDATE(), INTERVAL %b)
                            """ % (euser, article_id, MIN_CHECK_INTERVAL.encode("utf-8"))
                        )
                        dab_list = []
                        updated_dab_list = []
                        article_list = []
                        for msg_row in self.tcursor.fetchall():
                            if isinstance(msg_row['redirect_title'], str):
                                dab_list.append(msg_row['redirect_title'])
                            else:
                                dab_list.append(msg_row['redirect_title'].decode("utf-8"))
                        self.log(f" dablink list, before: {dab_list!s}\n")
                        for dab_title in dab_list:
                            # make sure the link still is in the database
                            dtitle = self.wikidb.escape_string(dab_title.encode("utf-8"))
                            chk_res = self.wcursor.execute(b"""
                                    SELECT 1
                                      FROM pagelinks, linktarget
                                     WHERE pl_from = %d
                                       AND pl_target_id = lt_id
                                       AND lt_namespace = 0
                                       AND lt_title = '%b'
                                """ % (article_id, dtitle))
                            if chk_res > 0:
                                updated_dab_list.append(dab_title)
                        self.log(f" dablink list, after: {updated_dab_list!s}\n")

                        updated_size = len(updated_dab_list)
                        if updated_size > 0:
                            article_list.append(article_id)
                            if inline_style and updated_size > INLINE_DAB_LIMIT:
                                inline_style = False
                            if inline_style:
                                if not known_user:
                                    msg_body = "Hi. Thank you for your recent edits. "
                                else:
                                    msg_body = ""
                                msg_body += f"An automated process has detected that when you recently edited [[{disp_article_title}]], you added "
                                if updated_size == 1:
                                    msg_body += "a link pointing to the disambiguation page "
                                else:
                                    msg_body += "links pointing to the disambiguation pages "
                                msg_body += self.title_list(updated_dab_list)
                                if known_user:
                                    msg_body += ". "
                                else:
                                    msg_body += ". Such links are [[WP:INTDABLINK|usually incorrect]], since a disambiguation page is merely a list of unrelated topics with similar titles. <small>(Read the [[User:DPL bot/Dablink notification FAQ|FAQ]]{{*}} Join us at the [[Wikipedia:Disambiguation pages with links|DPL WikiProject]].)</small>"
                            else:
                                msg_body += f"\n:[[{disp_article_title}]]\n"
                                if updated_size == 1:
                                    msg_body += "::added a link pointing to "
                                else:
                                    msg_body += "::added links pointing to "
                                msg_body += self.title_list(updated_dab_list)
                        else:
                            self.log(" couldn't find any dabs in pagelinks\n")
                # end loop of articles edited by user

                if msg_body == "":
                    continue
                if inline_style:
                    if known_user:
                        msg = msg_hdr + msg_body + msg_ftr_short
                    else:
                        msg = msg_hdr + msg_body + msg_ftr
                else:
                    if known_user:
                        msg = msg_hdr + msg_hdr_short + msg_body + msg_ftr_short
                    else:
                        msg = msg_hdr + msg_hdr_long + msg_body + msg_ftr

                content = usertalk.text
                if content.strip() == "":
                    usertalk.text = msg
                else:
                    usertalk.text = content + "\n\n" + msg
                try:
                    usertalk.save(summary=
                        "dablink notification message (see the [[User:DPL bot/Dablink notification FAQ|FAQ]])",
                        minor=False)
                except:
                    if self.logging_enabled:
                        self.logfile.write("  ** Saving user talk page failed.\n")
                        traceback.print_exc(file=self.logfile)
                    continue
                if not known_user:
                    self.tcursor.execute(b"INSERT IGNORE INTO notified_users (username) VALUES ('%b')" % euser)
                for page_id in article_list:
                    self.tcursor.execute(b"""
                            UPDATE recent_dabs
                               SET is_msg = 1,
                                   msg_date = SYSDATE()
                             WHERE article_id = %d
                               AND type = 5
                               AND is_fix = 0
                               AND user = '%b'
                               AND dab_date > DATE_SUB(SYSDATE(), INTERVAL %s)
                        """ % (page_id, euser, MIN_CHECK_INTERVAL.encode("utf-8"))
                    )
                self.tooldb.commit()
        finally:
            if self.logging_enabled:
                self.logfile.close()
            self.tcursor.close()
            self.wcursor.close()
