<?php

putenv('TZ=UTC');


include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");
$config="$HOME_DIR/.dplcredentials.cfg";
require("$HOME_DIR/scripts/SxWiki.php");

$logging_enabled = true;

$is_error = false;

$last_run_begin = 9999999999;
$last_run_begin_wiki = "99999999999999";

if (file_exists("$HOME_DIR/status/sdab_last_good_run.php")) {
  include_once("$HOME_DIR/status/sdab_last_good_run.php");
  $last_run_begin = $sdab_begin_run_lag;
  $last_run_begin_wiki = $sdab_begin_run_wiki_lag;
}

$fp = fopen("$HOME_DIR/status/sdab_results.php", "w");

if ($logging_enabled) $lp = fopen("$HOME_DIR/logs/sdab_results.log", "w");
else $lp = "";

fputs($fp, "<?PHP\n\n");

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if (! $mysql) {
  log_error(date("F j G:i", time()), "sdab_procs.php", "toolsdb connect", mysqli_connect_error());
  fputs($fp, "\n$"."sdab_error = \"".mysqli_connect_error()."\";\n\n?>\n");
  fclose($fp);
  $is_error = true;
}

$replica = get_db_con("simplewiki_p", "simplewiki.analytics.db.svc.wikimedia.cloud");

if (! $replica) {
  log_error(date("F j G:i", time()), "sdab_procs.php", "simplewiki_p connect", mysqli_connect_error());
  fputs($fp, "\n$"."sdab_error = \"".mysqli_connect_error()."\";\n\n?>\n");
  fclose($fp);
  $is_error = true;
}

if (! $is_error) {
  $time_begin = time();
  $time_begin_lag = $time_begin - get_replag($replica, "simplewiki_p");
  $begin_run_wiki = date("YmdHis", $time_begin);
  $begin_run_wiki_lag = date("YmdHis", $time_begin_lag);
  $begin_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_begin));

  fputs($fp, "$"."sdab_last_begin_run_wiki_lag = \"$last_run_begin_wiki\";\n\n" );

  fputs($fp, "$"."sdab_begin_run = $time_begin;\n" );
  fputs($fp, "$"."sdab_begin_run_lag = $time_begin_lag;\n" );
  fputs($fp, "$"."sdab_begin_run_wiki = \"$begin_run_wiki\";\n" );
  fputs($fp, "$"."sdab_begin_run_wiki_lag = \"$begin_run_wiki_lag\";\n" );
  fputs($fp, "$"."sdab_begin_run_str = \"$begin_run_str\";\n\n" );

  submit_and_log_query($mysql, "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
  submit_and_log_query($mysql, "SET @@max_heap_table_size=2147483648");
  submit_and_log_query($mysql, "SET @@tmp_table_size=2147483648");

  initialize_tables();
  fetch_dab_pages();
  post_results_list();

  $cleanup_begin = time();
  dab_cleanup();
}

if (!$is_error) {

  $time_finish = time();
  fputs($fp, "$"."sdab_proc_results['sdab_cleanup'] = \"".convert_time($time_finish - $cleanup_begin)."\";\n");

  $finish_run_wiki = date("YmdHis", $time_finish);
  $finish_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_finish));
  $total_time_str = convert_time($time_finish - $time_begin);

  fputs($fp, "\n$"."sdab_finish_run = $time_finish;\n" );
  fputs($fp, "$"."sdab_finish_run_wiki = \"$finish_run_wiki\";\n" );
  fputs($fp, "$"."sdab_finish_run_str = \"$finish_run_str\";\n\n" );

  fputs($fp, "$"."sdab_total_time_str = \"$total_time_str\";\n\n" );

  mysqli_close($mysql);
  mysqli_close($replica);

  fputs($fp, "\n?>\n");
  fclose($fp);

  if (file_exists("$HOME_DIR/status/sdab_last_good_run.php"))
    unlink("$HOME_DIR/status/sdab_last_good_run.php");

  rename("$HOME_DIR/status/sdab_results.php", "$HOME_DIR/status/sdab_last_good_run.php");
}


function submit_query ($server, $sql) {

  global $fp, $is_error;

  if ($is_error) {
    return;
  }

  $result = mysqli_query($server, $sql);

  if ($result == false) {
    log_error(date("F j G:i", time()), "sdab_procs.php", $sql, mysqli_error($server));
    fputs($fp, "\n$"."sdab_error = \"$sql: ".mysqli_error($server)."\";\n");
    fclose($fp);
    $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
    dab_cleanup();
    $is_error = true;
  }

  return $result;

}


function submit_and_log_query ($server, $sql, $nick = "") {

  global $fp, $is_error;

  if ($is_error) {
    return;
  }

  if ($nick == "")
    $nick = $sql;

  $begin = time();

  $result = submit_query($server, $sql);

  if (! $is_error) {
    $duration = time() - $begin;
    fputs($fp,
      "$"."sdab_proc_results['$nick'] = \"".convert_time($duration)."\";\n");
  }

  return $result;

}


function initialize_tables () {

  global $fp, $is_error;
  global $mysql;

  $begin = time();

  submit_query($mysql, "
    CREATE TABLE IF NOT EXISTS s_all_dab_links (
        article_id int unsigned NOT NULL default '0',
        article_title varchar(255) binary NOT NULL default '',
        dab_id int unsigned NOT NULL default '0',
        dab_title varchar(255) binary NOT NULL default '',
        redirect_id int unsigned NOT NULL default '0',
        redirect_title varchar(255) binary NOT NULL default '',
        template_id int unsigned NOT NULL default '0',
        template_title varchar(255) binary NULL,
        dab_date varbinary(8) NULL,
        INDEX i_sadl (article_id),
        INDEX i_sadl2 (dab_id),
        INDEX i_sadl3 (dab_title),
        INDEX i_sadl4 (redirect_id),
        INDEX i_sadl5 (template_id),
        INDEX i_sadl6 (dab_date)
    ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC
  ");

  submit_query($mysql, "DROP TABLE IF EXISTS t_s_all_dabs");

  submit_query($mysql, "
    CREATE TABLE t_s_all_dabs (
      dab_id int(10) unsigned NOT NULL default '0',
      dab_title varchar(255) binary NOT NULL default '',
      PRIMARY KEY pk_dab (dab_id),
      UNIQUE KEY u_dab (dab_title)
    ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC
  ");

  submit_query($mysql, "DROP TABLE IF EXISTS t_s_template_dab_links");

  submit_query($mysql,
    "CREATE TABLE t_s_template_dab_links (
      t_id int unsigned NOT NULL default '0',
      t_title varchar(255) binary NOT NULL default '',
      dab_id int unsigned NOT NULL default '0',
      dab_title varchar(255) binary NOT NULL default '',
      r_title varchar(255) binary NOT NULL default ''
    ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($mysql, "DROP TABLE IF EXISTS s_all_dab_links_part2");

  submit_query($mysql, "
    CREATE TABLE s_all_dab_links_part2 (
      article_id int unsigned NOT NULL default '0',
      article_title varchar(255) binary NOT NULL default '',
      dab_id int unsigned NOT NULL default '0',
      dab_title varchar(255) binary NOT NULL default '',
      redirect_id int unsigned NOT NULL default '0',
      redirect_title varchar(255) binary NOT NULL default '',
      template_id int unsigned NULL,
      template_title varchar(255) binary NULL,
      INDEX (article_id),
      INDEX (dab_id),
      INDEX (template_id)
    ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($mysql, "DROP TABLE IF EXISTS t_sdab_template_tc_count");

  submit_query($mysql,
    "CREATE TABLE t_sdab_template_tc_count (
       tc_id int unsigned NOT NULL default '0',
       tc_title varchar(255) binary NOT NULL default '',
       tc_amnt int unsigned NOT NULL default '0'
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($mysql, "DROP TABLE IF EXISTS t_sdab_link_count");

  submit_query($mysql,
    "CREATE TABLE t_sdab_link_count (
       lc_id int unsigned NOT NULL default '0',
       lc_title varchar(255) binary NOT NULL default '',
       lc_amnt int unsigned NOT NULL default '0',
       PRIMARY KEY pk_dlc (lc_id),
       UNIQUE INDEX u_dlc (lc_title)
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($mysql, "DROP TABLE IF EXISTS t_s_to_dab_link_count");

  submit_query($mysql,
    "CREATE TABLE t_s_to_dab_link_count (
       lc_id int unsigned NOT NULL default '0',
       lc_title varchar(255) binary NOT NULL default '',
       lc_amnt int unsigned NOT NULL default '0',
       PRIMARY KEY (lc_id)
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  if ($is_error) return;
  mysqli_commit($mysql);
  $duration = time() - $begin;
  fputs($fp,
    "$"."sdab_proc_results['initialize_tables'] = \"".convert_time($duration)."\";\n");

}

function fetch_dab_pages () {
  global $mysql, $replica;
  global $fp, $lp;
  global $is_error, $logging_enabled;

  $dabtitles = [];      // id => title
  $dabredirects = [];   // from_id => to_id
  $dablinks = [];
  $templatelinks = [];

  if ($is_error) return;

  $pagetitles = []; # maps ids => titles for all pages containing a disambig link

  # Retrieve all pages in ns 0 with disambiguation property

  $sql = "SELECT page_id,
                 page_title
            FROM page, page_props
           WHERE pp_page = page_id
             AND pp_propname = 'disambiguation'
             AND page_namespace = 0
             AND page_is_redirect = 0
         ";
  $dabresult = submit_and_log_query($replica, $sql, "fetch_dab_pages");

  $begin = time();
  $values = "";
  $batches = [];
  $vcount = 0;

  while ($dabrow = mysqli_fetch_assoc($dabresult)) {
    $did = (int) $dabrow['page_id'];
    $dabtitles[$did] = $dabrow['page_title'];
    $dtitle = mysqli_real_escape_string($mysql, $dabrow['page_title']);
    # pages that are not redirects point to themselves
    $dabredirects[$did] = $did;
    if ($values != "") {
      $values .= ",";
    }
    $values .= "($did, '$dtitle')";
    $vcount += 1;
    if ($vcount == 128) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  foreach ($batches as $vstring) {
    $iresult = submit_query($mysql,
      "INSERT INTO t_s_all_dabs (dab_id, dab_title)
            VALUES $vstring
      ");
    if ($is_error) return;
  }
  mysqli_commit($mysql);
  mysqli_free_result($dabresult);

  $duration = time() - $begin;
  fputs($fp,
        "$"."sdab_proc_results['store_all_dabs'] = \"".convert_time($duration)."\";\n");

  # Retrieve all ns 0 redirects to disambiguation pages

  $sql = "SELECT src.page_id as redir_id,
                 src.page_title as redir_title,
                 target.page_id as dab_id
            FROM page as target, page_props, redirect, page as src
           WHERE target.page_namespace = 0
             AND target.page_title = rd_title
             AND rd_namespace = 0
             AND pp_page = target.page_id
             AND pp_propname = 'disambiguation'
             AND rd_from = src.page_id
             AND src.page_namespace = 0
         ";
  $rdresult = submit_and_log_query($replica, $sql, "fetch_dab_redirects");
  if ($is_error) return;
  while ($rdrow = mysqli_fetch_assoc($rdresult)) {
    $dabtitles[$rdrow['redir_id']] = $rdrow['redir_title'];
    $dabredirects[$rdrow['redir_id']] = (int) $rdrow['dab_id'];
  }
  mysqli_free_result($rdresult);

  $dabindex = array_flip($dabtitles);

  # retrieve all incoming links to disambiguation titles (pages and redirects)
  # from namespaces 0 and 10 only
  $begin = time();
  $values = "";
  $vcount = 0;
  # group titles into batches for efficiency
  $batches = [];
  foreach ($dabtitles as $onetitle) {
    if ($vcount > 0)
      $values .= ",";
    $cleantitle = mysqli_real_escape_string($replica, $onetitle);
    $values .= "'$cleantitle'";
    $vcount += 1;
    if ($vcount == 128) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0) {
    $batches[] = $values;
  }
  foreach ($batches as $batch) {
    $query = "SELECT pl_from,
                     page_title,
                     page_latest,
                     lt_title as pl_title,
                     pl_from_namespace as pl_ns
                FROM pagelinks, linktarget, page
               WHERE page_id=pl_from
                 AND pl_target_id = lt_id
                 AND page_is_redirect = 0
                 AND (pl_from_namespace=0 OR pl_from_namespace=10)
                 AND lt_namespace=0
                 AND lt_title IN ($batch)";
    $sresult = submit_query($replica, $query);
    if ($is_error) return;
    while ($srow = mysqli_fetch_assoc($sresult)) {
      $pltitle = $srow['pl_title'];
      $pl_from = (int) $srow['pl_from'];
      if ( isset ($dabindex[$pltitle]) ) {
        $dab_id = $dabindex[$pltitle];
      } else {
        if ($logging_enabled)
          fputs($lp, print_r(" Dab title $pltitle not found.\n", true));
        $dab_id = null;
      }
      if ( isset ($dabredirects[$dab_id]) ) {
        $redir_id = $dabredirects[$dab_id];
      } else {
        if ($logging_enabled)
          fputs($lp, print_r(" Redirect id $dab_id not found.\n", true));
        $redir_id = null;
      }
      if ($srow['pl_ns'] == 0) {
        # skip any links from a page to itself, including via a redirect
        if ($pl_from != $redir_id and $pl_from != $dab_id) {
          $dablinks[$pl_from.",".$dab_id] = [
            'from_id' => $pl_from,
            'from_title' => $srow['page_title'],
            'rev_id' => (int) $srow['page_latest'],
            'links_to' => $dab_id,
            'dab_target' => $redir_id,
          ];
          $pagetitles[$pl_from] = $srow['page_title'];
        }
      } elseif ($srow['pl_ns'] == 10) {
        $templatelinks[$pl_from.",".$dab_id] = [
          'template_id' => $pl_from,
          'template_title' => $srow['page_title'],
          'rev_id' => (int) $srow['page_latest'],
          'links_to' => $dab_id,
          'dab_target' => $redir_id,
        ];
      } else {
        if ($logging_enabled)
          fputs($lp, print_r(" Invalid namespace {$srow['pl_ns']} in query result.\n", true));
      }
    }
    mysqli_free_result($sresult);
  }
  fputs($lp, "Initial template link count: ".count($templatelinks)."\n");
  $duration = time() - $begin;
  fputs($fp, "$"."sdab_proc_results['dab_pl_r'] = \"".convert_time($duration)."\";\n");

  # drop any multiple dablinks to same dab target

  $t_dablinks_index = [];
  foreach($dablinks as $i => $onelink) {
    if (isset($t_dablinks_index[$onelink['from_id']])) {
      if (in_array($onelink['dab_target'], $t_dablinks_index[$onelink['from_id']], true) === false) {
        # from_id already in index, but this target is not
        $t_dablinks_index[$onelink['from_id']][] = $onelink['dab_target'];
      } else {
        # index already contains a link from this article to the target dab page
        unset($dablinks[$i]);
      }
    } else {
      # from_id not yet in index
      $t_dablinks_index[$onelink['from_id']] = array($onelink['dab_target']);
    }
  }

  # Delete templates that are not used by mainspace non-redirect articles

  $begin = time();

  # create a list of unique templates containing >=1 links to dab pages
  $templatelist = [];
  foreach ($templatelinks as $tlink) {
    $templatelist[$tlink['template_id']] = $tlink['template_title'];
  }
  fputs($lp, "Found ".count($templatelist)." unique templates containing dablinks.\n");

  $keeplist = [];

  # For each template, check whether any page that transcludes the template is
  # a non-redirect mainspace article
  foreach ($templatelist as $t_id => $t_title) {
    $esctitle = mysqli_real_escape_string($replica, $t_title);
    $query = "SELECT 1
                FROM templatelinks, linktarget
               WHERE tl_target_id = lt_id
                 AND lt_title = '$esctitle'
                 AND lt_namespace = 10
          AND EXISTS (
                      SELECT * FROM page
                       WHERE page_id = tl_from
                         AND page_namespace = 0
                         AND page_is_redirect = 0
                     )
    ";
    $qresult = mysqli_query($replica, $query);
    if ($qresult && mysqli_num_rows($qresult) >= 1) {
      # a transcluding article was found
      $keeplist[$t_id] = $t_title;
    }
    if ($qresult)
      mysqli_free_result($qresult);
  }
  foreach ($templatelinks as $i => $tlink) {
    if (! isset($keeplist[$tlink['template_id']])) {
      unset($templatelinks[$i]);
    }
  }
  fputs($lp, "After Pass 1, ".count($keeplist)." unique templates containing ".count($templatelinks)." links to dab pages.\n");
  $duration = time() - $begin;
  fputs($fp, "$"."sdab_proc_results['keep_templates'] = \"".convert_time($duration)."\";\n");

  # Delete templates that have a dab link in a non-transcluded portion of text

  # For each template in list, get ids of up to 4 mainspace articles that
  # transclude the template; if none of those 4 articles is in $dablinks,
  # remove the template from the list

  $begin = time();

  foreach ($templatelinks as $tkey => $tlink) {
    $ttitle = mysqli_real_escape_string($replica, $tlink['template_title']);
    $query2 = "SELECT page_id
                 FROM templatelinks, linktarget, page
                WHERE tl_from = page_id 
                  AND tl_target_id = lt_id
                  AND lt_title = '$ttitle'
                  AND lt_namespace = 10
                  AND page_namespace = 0
                  AND page_is_redirect = 0
                  AND page_id <> {$tlink['links_to']}
                LIMIT 4";
    $q2result = submit_query($replica, $query2);
    if ($is_error) return;
    $found = false;
    while ($qrow = mysqli_fetch_assoc($q2result)) {
      if (! isset($dablinks[$qrow['page_id'].",".$tlink['links_to']])) {
        # found a transcluding article that is not in pagelinks
        $found = true;
        break;
      }
    }
    mysqli_free_result($q2result);
    if ($found) {
      unset($templatelinks[$tkey]);
    }
  }

  unset($t_dablinks_index);
  unset($keeplist);

  fputs($lp, "After Pass 2, ".count($templatelinks)." template links to dab pages.\n");

  $duration = time() - $begin;
  fputs($fp, "$"."sdab_proc_results['remove_templates'] = \"".convert_time($duration)."\";\n");

  # store t_s_template_dab_links
  $begin = time();

  $vcount = 0;
  $values = "";
  $batches = [];

  foreach ($templatelinks as $tlink) {
    if ( endswith ( $dabtitles[$tlink['links_to']], "_(disambiguation)" ) )
      continue;
    if ( endswith ( $dabtitles[$tlink['links_to']], "_(number)") )
      continue;
    $ttitle = mysqli_real_escape_string($mysql, $tlink['template_title']);
    $dtitle = mysqli_real_escape_string($mysql, $dabtitles[$tlink['dab_target']]);
    $rtitle = mysqli_real_escape_string($mysql, $dabtitles[$tlink['links_to']]);
    if ($vcount > 0) {
      $values .= ", ({$tlink['template_id']}, '$ttitle', {$tlink['dab_target']},
                      '$dtitle', '$rtitle')";
    } else {
      $values = "({$tlink['template_id']}, '$ttitle', {$tlink['dab_target']},
                      '$dtitle', '$rtitle')";
    }
    $vcount += 1;
    if ($vcount == 32) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  foreach ($batches as $batch) {
    $sql = "INSERT INTO t_s_template_dab_links
                        (t_id, t_title, dab_id, dab_title, r_title)
                 VALUES $batch";
    $result = submit_query($mysql, $sql);
    if ($is_error) return;
  }

  $duration = time() - $begin;
  fputs($fp, "$"."sdab_proc_results['s_template_dab_links'] = \"".convert_time($duration)."\";\n");

  # For each remaining template link, find all articles that transclude the
  # template and link to the disambiguation page

  $begin = time();
  $dab_article_template_links = [];
  foreach ($templatelinks as $tlink) {
    if ( endswith($dabtitles[$tlink['links_to']], "_(disambiguation)" ) )
      continue;
    if ( endswith($dabtitles[$tlink['links_to']], "_(number)") )
      continue;
    $ttitle = mysqli_real_escape_string($replica, $tlink['template_title']);
    $dtitle = mysqli_real_escape_string($replica, $dabtitles[$tlink['links_to']]);
    $sql = "SELECT tl_from
              FROM templatelinks, linktarget AS tlt, pagelinks, linktarget as plt
             WHERE tl_from = pl_from
               AND tl_from_namespace = 0
               AND tl_target_id = tlt.lt_id
               AND tlt.lt_namespace = 10
               AND tlt.lt_title = '$ttitle'
               AND pl_from_namespace = 0
               AND pl_target_id = plt.lt_id
               AND plt.lt_namespace = 0
               AND plt.lt_title = '$dtitle'";
    $result = submit_query($replica, $sql);
    if ($is_error) return;
    while ($row = mysqli_fetch_assoc($result)) {
      $dab_article_template_links[] = [
        'link_from' => (int) $row['tl_from'],
        'link_to' => $tlink['links_to'],
        'dab_id' => $tlink['dab_target'],
        'template_id' => $tlink['template_id'],
        'template_title' => $tlink['template_title']
      ];
      foreach ($dablinks as $i => $dlink) {
        if ($dlink['from_id'] == (int) $row['tl_from'] &&
            $dlink['links_to'] == $tlink['links_to']) {
          $dablinks[$i]['template_id'] = $tlink['template_id'];
        }
      }
    }
    mysqli_free_result($result);
  }
  $duration = time() - $begin;
  fputs($fp, "$"."sdab_proc_results['dab_article_template_links'] = \"".convert_time($duration)."\";\n");

  # pull everything together into s_all_dab_links_part2 table
  $begin = time();
  $values = "";
  $vcount = 0;
  $batches = [];
  $named_dab_link_counts = [];
  foreach ($dablinks as $link) {
    $dabpage_t = $dabtitles[$link['links_to']];
    if ( endswith ( $dabpage_t, "_(disambiguation)" )
        || endswith ( $dabpage_t, "_(number)") ) {
      if (isset ($named_dab_link_counts[$dabpage_t])) {
        $named_dab_link_counts[$dabpage_t] += 1;
      } else {
        $named_dab_link_counts[$dabpage_t] = 1;
      }
    } else {
      $atitle = mysqli_real_escape_string($mysql, $link['from_title']);
      $dtitle = mysqli_real_escape_string($mysql, $dabtitles[$link['dab_target']]);
      $rtitle = mysqli_real_escape_string($mysql, $dabpage_t);
      $tid = 0;
      $ttitle = "";
      foreach ($dab_article_template_links as $tlink) {
        if ($tlink['link_from'] == $link['from_id'] && $tlink['link_to'] == $link['links_to']) {
          $tid = $tlink['template_id'];
          $ttitle = mysqli_real_escape_string($mysql, $tlink['template_title']);
        }
      }
      if ($values != "")
        $values .= ", ";
      $values .= "({$link['from_id']}, '$atitle',
                  {$link['dab_target']}, '$dtitle',
                  {$link['links_to']}, '$rtitle',
                  $tid, '$ttitle')";
      $vcount += 1;
      if ($vcount == 32) {
        $batches[] = $values;
        $values = "";
        $vcount = 0;
      }
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  mysqli_close($mysql);
  $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  foreach ($batches as $v) {
    $sql = "INSERT INTO s_all_dab_links_part2
                        (article_id, article_title,
                         dab_id, dab_title,
                         redirect_id, redirect_title,
                         template_id, template_title)
                 VALUES $v";
    submit_query($mysql, $sql);
    if ($is_error) return;
  }

  submit_query($mysql, "DROP TABLE IF EXISTS t_all_dab_links");

  submit_query($mysql,
    "CREATE TABLE t_all_dab_links (
       article_id int unsigned NOT NULL default '0',
       article_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       redirect_id int unsigned NOT NULL default '0',
       redirect_title varchar(255) binary NOT NULL default '',
       template_id int unsigned NOT NULL default '0',
       template_title varchar(255) binary NULL,
       dab_date varbinary(8) NULL,
       INDEX i_adl (article_id),
       INDEX i_adl2 (dab_id),
       INDEX i_adl3 (dab_title),
       INDEX i_adl4 (redirect_id),
       INDEX i_adl5 (template_id),
       INDEX i_adl6 (dab_date)
     ) ROW_FORMAT=DYNAMIC
       ENGINE=InnoDB AS
         SELECT a.article_id AS article_id,
                a.article_title AS article_title,
                a.dab_id AS dab_id,
                a.dab_title AS dab_title,
                a.redirect_id AS redirect_id,
                a.redirect_title AS redirect_title,
                a.template_id AS template_id,
                a.template_title AS template_title,
                b.dab_date AS dab_date
           FROM s_all_dab_links_part2 a LEFT OUTER JOIN s_all_dab_links b
             ON a.article_id = b.article_id
            AND a.dab_id = b.dab_id
            AND a.redirect_id = b.redirect_id
            AND a.template_id = b.template_id"
  );

  submit_query($mysql,
    "UPDATE t_all_dab_links
        SET dab_date = DATE_FORMAT(SYSDATE(), '%Y%m%d')
      WHERE dab_date IS NULL"
  );

  submit_query($mysql, "DROP TABLE IF EXISTS s_all_dab_links_part2");

  submit_query($mysql, "DROP TABLE IF EXISTS t_s_all_dab_links_basic");

  submit_query($mysql,
    "CREATE TABLE t_s_all_dab_links_basic (
        article_id int unsigned NOT NULL default '0',
        article_title varchar(255) binary NOT NULL default '',
        dab_id int unsigned NOT NULL default '0',
        dab_title varchar(255) binary NOT NULL default '',
        INDEX i_adlb (article_id),
        INDEX i_adlb2 (dab_id)
     ) ENGINE=InnoDB AS /* SLOW_OK */
       SELECT article_id,
              article_title,
              dab_id,
              dab_title
         FROM t_all_dab_links
        GROUP BY dab_id, article_id"
  );

  $duration = time() - $begin;
  fputs($fp, "$"."sdab_proc_results['t_all_dab_links'] = \"".convert_time($duration)."\";\n");

  # populate t_sdab_template_tc_count

  $begin = time();

  $transclusion_count = [];

  fputs($lp, "dab_article_template_links count = ".count($dab_article_template_links));
  foreach ($dab_article_template_links as $tlink) {
    if (isset ($transclusion_count[$tlink['template_id']] )) {
      $transclusion_count[$tlink['template_id']] += 1;
    } else {
      $transclusion_count[$tlink['template_id']] = 1;
    }
  }

  $values = "";
  $vcount = 0;
  $batches = [];
  foreach ($transclusion_count as $t_id => $tr_count) {
    if ($tr_count <= 0)
      continue;
    $ttitle = mysqli_real_escape_string($mysql, $templatelist[$t_id]);
    if ($vcount > 0)
      $values .= ", ";
    $values .= "($t_id, '$ttitle', $tr_count)";
    $vcount += 1;
    if ($vcount == 128) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0)
    $batches[] = $values;
  foreach ($batches as $batch) {
    $sql = "INSERT INTO t_sdab_template_tc_count (tc_id, tc_title, tc_amnt)
                 VALUES $batch";
    submit_query($mysql, $sql);
  }

  $duration = time() - $begin;
  fputs($fp, "$"."sdab_proc_results['create_template_tc_count'] = \"".convert_time($duration)."\";\n");

  # create link count tables

  $begin = time();

  $dab_link_count = [];
  foreach ($dablinks as $link) {
    if ( endswith ( $dabtitles[$link['links_to']], "_(disambiguation)" ) )
      continue;
    if ( endswith ( $dabtitles[$link['links_to']], "_(number)") )
      continue;
    if ( isset ( $dab_link_count[$link['dab_target']] )) {
      $dab_link_count[$link['dab_target']] += 1;
    } else {
      $dab_link_count[$link['dab_target']] = 1;
    }
  }

  $values = "";
  $vcount = 0;
  $batches = [];
  foreach($dab_link_count as $dab_id => $link_count) {
    if ($link_count <= 0)
      continue;
    $dtitle = mysqli_real_escape_string($mysql, $dabtitles[$dab_id]);
    if ($vcount > 0)
      $values .= ", ";
    $values .= "($dab_id, '$dtitle', $link_count)";
    $vcount += 1;
    if ($vcount == 128) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  foreach ($batches as $batch) {
    $sql = "INSERT INTO t_sdab_link_count (lc_id, lc_title, lc_amnt) VALUES $batch";
    submit_query($mysql, $sql);
  }

  # count dab links that are not via a template
  $to_dab_link_count = [];

  $dab_link_count = [];
  foreach ($dablinks as $link) {
    if ( endswith ( $dabtitles[$link['links_to']], "_(disambiguation)" ) )
      continue;
    if ( endswith ( $dabtitles[$link['links_to']], "_(number)") )
      continue;
    if ( isset ($link['template_id']) )
      continue;
    if ( isset ( $to_dab_link_count[$link['from_id']] )) {
      $to_dab_link_count[$link['from_id']] += 1;
    } else {
      $to_dab_link_count[$link['from_id']] = 1;
    }
  }

  $values = "";
  $vcount = 0;
  $batches = [];
  foreach($to_dab_link_count as $pg_id => $link_count) {
    if ($link_count <= 0)
      continue;
    $ptitle = mysqli_real_escape_string($mysql, $pagetitles[$pg_id]);
    if ($vcount > 0)
      $values .= ", ";
    $values .= "($pg_id, '$ptitle', $link_count)";
    $vcount += 1;
    if ($vcount == 128) {
      $batches[] = $values;
      $values = "";
      $vcount = 0;
    }
  }
  if ($vcount > 0)
    $batches[] = $values;

  foreach ($batches as $batch) {
    $sql = "INSERT INTO t_s_to_dab_link_count (lc_id, lc_title, lc_amnt) VALUES $batch";
    submit_query($mysql, $sql);
  }

  $duration = time() - $begin;
  fputs($fp, "$"."sdab_proc_results['create_dab_link_count'] = \"".convert_time($duration)."\";\n");

  mysqli_commit($mysql);
}


function post_results_list ( ) {

  global $mysql, $fp, $is_error;

  if ($is_error) return;
  $begin = time();

  submit_query($mysql, "DROP TABLE IF EXISTS s_all_dabs");
  submit_query($mysql, "RENAME TABLE t_s_all_dabs TO s_all_dabs");

  submit_query($mysql, "DROP TABLE IF EXISTS s_all_dab_links");
  submit_query($mysql, "RENAME TABLE t_all_dab_links TO s_all_dab_links");

  submit_query($mysql, "DROP TABLE IF EXISTS s_all_dab_links_basic");
  submit_query($mysql, "RENAME TABLE t_s_all_dab_links_basic TO s_all_dab_links_basic");

  submit_query($mysql, "DROP TABLE IF EXISTS s_template_dab_links");
  submit_query($mysql, "RENAME TABLE t_s_template_dab_links TO s_template_dab_links");

  submit_query($mysql, "DROP TABLE IF EXISTS sdab_template_tc_count");
  submit_query($mysql, "RENAME TABLE t_sdab_template_tc_count TO sdab_template_tc_count");

  submit_query($mysql, "DROP TABLE IF EXISTS sdab_link_count");
  submit_query($mysql, "RENAME TABLE t_sdab_link_count TO sdab_link_count");

  submit_query($mysql, "DROP TABLE IF EXISTS s_to_dab_link_count");
  submit_query($mysql, "RENAME TABLE t_s_to_dab_link_count TO s_to_dab_link_count");

  if ($is_error) return;
  fputs($fp, "$"."sdab_proc_results['final_steps'] = \"".convert_time(time() - $begin)."\";\n");

}


function dab_cleanup ( ) {

  global $mysql;

  # note: don't call submit_query in this function, to avoid infinite recursion

  mysqli_query($mysql, "DROP TABLE IF EXISTS t_s_all_dab_links_basic");
  mysqli_query($mysql, "DROP TABLE IF EXISTS t_all_dab_links");
  mysqli_query($mysql, "DROP TABLE IF EXISTS t_s_to_dab_link_count");
  mysqli_query($mysql, "DROP TABLE IF EXISTS t_sdab_link_count");
  mysqli_query($mysql, "DROP TABLE IF EXISTS t_sdab_template_tc_count");
  mysqli_query($mysql, "DROP TABLE IF EXISTS s_all_dab_links_part2");
  mysqli_query($mysql, "DROP TABLE IF EXISTS t_s_template_dab_links");
  mysqli_query($mysql, "DROP TABLE IF EXISTS t_s_all_dabs");

}

?>