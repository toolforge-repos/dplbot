#!/bin/sh
today=$(date +%F)
mysqldump -h tools.db.svc.eqiad.wmflabs s51290__dpl_p all_dabs all_moves disambig_titles > /data/project/dplbot/dbbackups/${today}_all.sql
mysqldump -h tools.db.svc.eqiad.wmflabs s51290__dpl_p disambig_links > /data/project/dplbot/dbbackups/${today}_dl.sql
twoago=$(date -d "2 days ago" +%F)
rm /data/project/dplbot/dbbackups/${twoago}_all.sql
rm /data/project/dplbot/dbbackups/${twoago}_dl.sql
