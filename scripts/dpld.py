#! /usr/bin/python3

"""Daemon to update wiki pages when other processes update data files."""

# Copyright (C) 2023 Wikipedia users JaGa, Dispenser, R'n'B

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime
import pathlib
import threading
import time
from update_leaderboard import LeaderboardThread
from update_hof import HallOfFameThread
from bot_dablinks import DablinksNotifier
from bot_incoming_links import IncomingLinksNotifier
from bot_user_notify import UserNotifier
from daily_disambig import DailyDisambigWriter
from dabmaintbot import DabMaintBot

def dab_procs_notify(last_update):
    "Send all the post-dab_procs messages"
    try:
        DablinksNotifier().run()
    except Exception as e1:
        print(f"Error in DablinksNotifier: {type(e1)=}, {e1=}")
    try:
        IncomingLinksNotifier().run()
    except Exception as e2:
        print(f"Error in IncomingLinksNotifier: {type(e2)=}, {e2=}")
    try:
        UserNotifier().run()
    except Exception as e3:
        print(f"Error in UserNotifier: {type(e3)=}, {e3=}")
    if datetime.date.fromtimestamp(last_update) < datetime.date.today():
        # only update the Daily Disambig once per calendar day
        try:
            DailyDisambigWriter().run()
        except Exception as e4:
            print(f"Error in DailyDisambigWriter: {type(e4)=}, {e4=}")
        if datetime.date.today().isoweekday() == 7:
            # only update maintenance pages on Sunday
            try:
                DabMaintBot().run()
            except Exception as e5:
                print(f"Error in DabMaintBot: {type(e5)=}, {e5=}")

short_leaderboard_path = pathlib.Path("/data/project/dplbot",
        "dplbot", "data", "sl.txt")
try:
    last_sl_update = short_leaderboard_path.stat().st_mtime + 1
except FileNotFoundError:
    last_sl_update = int(time.time()) + 1

hall_of_fame_path = pathlib.Path("/data/project/dplbot",
        "dplbot", "data", "hof.txt")
try:
    last_hof_update = hall_of_fame_path.stat().st_mtime + 1
except FileNotFoundError:
    last_hof_update = int(time.time()) + 1

dab_results_path = pathlib.Path("/data/project/dplbot",
        "dplbot", "status", "dab_last_good_run.php")
try:
    last_dab_update = dab_results_path.stat().st_mtime + 1
except FileNotFoundError:
    last_dab_update = int(time.time()) + 1

# main loop
while True:     # program is intended to run continuously
    # check for short leaderboard updates
    try:
        sl_timestamp = short_leaderboard_path.stat().st_mtime
    except FileNotFoundError:
        sl_timestamp = last_sl_update - 1 # don't run if file is gone
    if sl_timestamp > last_sl_update:
        LeaderboardThread(short_leaderboard_path).start()
        last_sl_update = sl_timestamp + 1
    # check for hall of fame updates
    try:
        hof_timestamp = hall_of_fame_path.stat().st_mtime
    except FileNotFoundError:
        hof_timestamp = last_hof_update - 1 # don't run if file is gone
    if hof_timestamp > last_hof_update:
        HallOfFameThread(hall_of_fame_path).start()
        last_hof_update = hof_timestamp + 1
    # check for dab_procs completion
    try:
        dab_timestamp = dab_results_path.stat().st_mtime
    except FileNotFoundError:
        dab_timestamp = last_dab_update - 1 # don't run if file is gone
    if dab_timestamp > last_dab_update:
        threading.Thread(target=dab_procs_notify, args=[last_dab_update]).start()
        last_dab_update = dab_timestamp + 1

    time.sleep(60)
