#! /usr/bin/python3

"Save the Short Leaderboard page"

import threading
import pywikibot

class LeaderboardThread(threading.Thread):
    def __init__(self, leaderboard_path):
        threading.Thread.__init__(self)
        self.site = pywikibot.Site()
        self.path = leaderboard_path
        self.page = pywikibot.Page(self.site, "User:JaGa/Short leaderboard")

    def run(self):
        """Read data from file and write to wiki"""
        with self.path.open("r") as src:
            self.page.text = src.read()
            self.page.save(summary="Update Dab Challenge results")
