<?php

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if ($mysql) {

  $sql = "
          CREATE TABLE IF NOT EXISTS dab_hof (
            month_name varchar(30) binary NOT NULL default '',
            month_no tinyint unsigned NOT NULL default '0',
            year smallint unsigned NOT NULL default '0',
            user1 varchar(255) binary NOT NULL default '',
            count1 mediumint unsigned NOT NULL default '0',
            user2 varchar(255) binary NOT NULL default '',
            count2 mediumint unsigned NOT NULL default '0',
            user3 varchar(255) binary NOT NULL default '',
            count3 mediumint unsigned NOT NULL default '0',
            user4 varchar(255) binary NOT NULL default '',
            count4 mediumint unsigned NOT NULL default '0',
            bonus varchar(255) binary NULL default '',
            count5 mediumint unsigned NULL default '0'
          ) ENGINE=InnoDB
         ";

  mysqli_query($mysql, $sql);

  $users[0] = $users[1] = $users[2] = $users[3] = $users[4] = "";
  $counts[0] = $counts[1] = $counts[2] = $counts[3] = $counts[4] = "";

  $sql = "
            SELECT user,
                   count(*) AS count
              FROM ch_results
             GROUP BY user
            HAVING count >= 10
             ORDER BY count DESC, user
             LIMIT 4
         ";

  $res = mysqli_query($mysql, $sql);

  $counter = 0;

  while ($row = mysqli_fetch_assoc($res)) {
    $users[$counter] = str_replace("'", "\'", $row['user']);
    $counts[$counter++] = $row['count'];
  }
  
  $y = time() - (10 * 24 * 60 * 60);

  $sql = "INSERT INTO dab_hof (month_name, month_no, year, user1, count1, user2, count2, user3, count3, user4, count4, bonus, count5)
                 VALUES ('".date('F', $y)."', ".date('n', $y).", ".date('Y', $y).",
                         '{$users[0]}', {$counts[0]}, '{$users[1]}', {$counts[1]},
                         '{$users[2]}', {$counts[2]}, '{$users[3]}', {$counts[3]},
                         '', 0)"; // last two entries replace the now-defunct bonus list count
  mysqli_query($mysql, $sql);

  $outfp = fopen("$HOME_DIR/data/hof.txt", "w");
  
  $sql = "SELECT *
            FROM dab_hof
        ORDER BY year DESC, month_no DESC
  ";

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      printResultsLocal($res, $outfp);
    }
    else fputs($outfp, "There are no results in this query.\n\n");

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    fputs($outfp, "Database error: ".mysqli_error($mysql)."</p>\n\n");
  }

  fclose($outfp);
  mysqli_close($mysql);
}
else
  log_error(date("F j G:i", time()), "hof_cmds.php", "mysql connect", mysqli_connect_error());


function printResultsLocal($res, $fp) {
	
	fputs($fp, "[[File:Super Disambiguator's Barnstar.png|thumb|160px|The Super Disambiguator's Barnstar, awarded to the winners of the Disambiguation Pages with Links monthly challenges.]]\n");

  fputs($fp, "The [[Wikipedia:Disambiguation pages with links#DAB Challenge leaderboard|Monthly DAB Challenge]] began in April 2009 to find the greatest, most dedicated fixers of [[Wikipedia:Disambiguation pages with links|links to disambiguation pages]]. Listed here are the winners of each month's competition, hallowed be their usernames.\n\n");
  
  fputs($fp, "{|style=\"background: transparent; margin: auto; \" cellpadding=\"2\" align=\"left\" \n");
  fputs($fp, "|- align=\"left\" \n");
  fputs($fp, "! Contest\n");
  fputs($fp, "! colspan=2 | First place\n");
  fputs($fp, "! colspan=2 | Second place\n");
  fputs($fp, "! colspan=2 | Third place\n");
  fputs($fp, "! colspan=2 | Fourth place\n");
  fputs($fp, "! colspan=2 | Bonus list champion\n");
  
  while ($row = mysqli_fetch_assoc($res)) {
    $month_name = $row['month_name'];
    $year = $row['year'];
    $user1 = $row['user1'];
    $count1 = $row['count1'];
    $user2 = $row['user2'];
    $count2 = $row['count2'];
    $user3 = $row['user3'];
    $count3 = $row['count3'];
    $user4 = $row['user4'];
    $count4 = $row['count4'];
    $bonus = $row['bonus'];
    $count5 = $row['count5'];
    fputs($fp, "|- align=\"left\"\n");
    fputs($fp, "| $month_name $year &nbsp;&nbsp;&nbsp;&nbsp;");
    fputs($fp, "|| [[User:$user1|$user1]] &nbsp;&nbsp;&nbsp; || $count1 &nbsp;&nbsp;&nbsp; ");
    fputs($fp, "|| [[User:$user2|$user2]] &nbsp;&nbsp;&nbsp; || $count2 &nbsp;&nbsp;&nbsp; ");
    fputs($fp, "|| [[User:$user3|$user3]] &nbsp;&nbsp;&nbsp; || $count3 &nbsp;&nbsp;&nbsp; ");
    fputs($fp, "|| [[User:$user4|$user4]] &nbsp;&nbsp;&nbsp; || $count4 &nbsp;&nbsp;&nbsp; ");
    if ($count5 > 0) {
      fputs($fp, "|| [[User:$bonus|$bonus]] &nbsp;&nbsp;&nbsp; || $count5 &nbsp;&nbsp;&nbsp; \n");
    } else {
      fputs($fp, "|| || \n");
    }
  }
  
  fputs($fp, "|}\n");
}

function run_query ($mysql, $sql) {

  $result = mysqli_query($mysql, $sql);

  if ($result == false)
    log_error(date("F j G:i", time()), "hof_cmds.php", $sql, mysqli_error($mysql));
}

?>