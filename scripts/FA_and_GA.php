<?php

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
$enwiki = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

if ($mysql && $enwiki) {

	$sql = "DROP TABLE IF EXISTS t_featured_and_good_articles";

  mysqli_query($mysql, $sql);

	$sql = "CREATE TABLE t_featured_and_good_articles (
            art_id int unsigned NOT NULL default '0',
            art_title varchar(255) binary NOT NULL default '',
            PRIMARY KEY (art_id)
          ) ENGINE=InnoDB";

  mysqli_query($mysql, $sql);

  $sql = "SELECT page_id AS art_id,
                 page_title AS art_title
            FROM page, categorylinks
           WHERE page_namespace = 0
             AND page_id = cl_from
             AND (
                  cl_to = 'Featured_articles' OR
                  cl_to = 'Good_articles'
                 )";

  $result = mysqli_query($enwiki, $sql);
  if ($result) {
    while ($row = mysqli_fetch_assoc($result)) {
      $id = (int) $row['art_id'];
      $title = mysqli_real_escape_string($mysql, $row['art_title']);
      mysqli_query($mysql, 
        "INSERT IGNORE INTO t_featured_and_good_articles 
                       (art_id, art_title) 
                VALUES ($id, '$title')
        ");
    }
    mysqli_free_result($result);

    $sql = "DROP TABLE IF EXISTS featured_and_good_articles";

    mysqli_query($mysql, $sql);

    $sql = "RENAME TABLE t_featured_and_good_articles TO featured_and_good_articles";

    mysqli_query($mysql, $sql);
  }
}

?>