#! /usr/bin/python3
"Manage templates on disambiguation pages with many incoming links"
import datetime
import locale
import pathlib
import re
import MySQLdb
import pywikibot

locale.setlocale(locale.LC_ALL, "")
ADD_THRESHOLD = 30
RMV_THRESHOLD = 25

class IncomingLinksNotifier:
    def __init__(self):
        self.logging_enabled = True
        if self.logging_enabled:
            p = pathlib.Path("/data/project/dplbot", "dplbot",
                    "logs/incoming_links_results.log")
            self.logfile = p.open("w", encoding="utf8")
        else:
            self.logfile = None
        toolsdb = MySQLdb.connect(db='s51290__dpl_p',
                          host='tools.db.svc.wikimedia.cloud',
                          read_default_file='/data/project/dplbot/.my.cnf',
                          charset="utf8")
        self.tcursor = toolsdb.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        self.wikidb = MySQLdb.connect(db='enwiki_p',
                          host='enwiki.analytics.db.svc.wikimedia.cloud',
                          read_default_file='/data/project/dplbot/.my.cnf',
                          charset="utf8")
        self.wcursor = self.wikidb.cursor(cursorclass=MySQLdb.cursors.DictCursor)

    def log(self, message):
        if self.logging_enabled:
            self.logfile.write(message)

    def is_dab(self,title):
        "Return True iff 'title' is a dab page"
        etitle = str(self.wikidb.escape_string(title), encoding="utf-8")
        try:
            result = self.wcursor.execute(f"""
                SELECT 1
                  FROM page
                 WHERE page_title = '{etitle}'
                   AND page_namespace = 0
                   AND EXISTS (
                                SELECT *
                                  FROM page_props
                                 WHERE pp_page = page_id
                                   AND pp_propname = 'disambiguation'
                              )
            """.encode("utf-8"))
            if result > 0:
                self.log("  is_dab: true\n")
                return True
            self.log("  is_dab: false\n")
            return False
        except KeyError:
            print(f"KeyError: title={title}, etitle={etitle}")
            return False

    def unique_link_count(self, title):
        "Return number of non-intentional incoming links to dab page"
        etitle = str(self.wikidb.escape_string(title), encoding="utf-8")
        if etitle.endswith("(disambiguation)"):
            is_named_dab = True
            link_candidates = "("
        else:
            is_named_dab = False
            link_candidates = f"('{etitle}'"
        redirect_result = self.wcursor.execute(f"""
              SELECT page_title as r_title
                FROM redirect, page
               WHERE rd_title = '{etitle}'
                 AND rd_namespace = 0
                 AND rd_from != 0
                 AND page_id = rd_from
                 AND page_namespace = 0
                 AND page_title != ''
                 AND page_is_redirect = 1
                 AND page_title NOT LIKE '%_(disambiguation)'
               GROUP BY r_title
        """.encode("utf-8"))
        count_redirect = 0
        if redirect_result:
            for row in self.wcursor.fetchall():
                r_title = str(self.wikidb.escape_string(row['r_title']), encoding="utf-8")
                if is_named_dab and count_redirect == 0:
                    link_candidates += f"'{r_title}'"
                else:
                    link_candidates += f", '{r_title}'"
                count_redirect += 1
        link_candidates += ")"
        if link_candidates == "()":
            return 0
        count_sql = f"""
              SELECT pl_from
                FROM pagelinks, linktarget, page
               WHERE lt_title IN {link_candidates}
                 AND lt_namespace = 0
                 AND pl_target_id = lt_id
                 AND pl_from = page_id
                 AND page_namespace = 0
                 AND page_title NOT LIKE '%_(disambiguation)'
               GROUP BY pl_from
        """.encode("utf-8")
        try:
            count_res = self.wcursor.execute(count_sql)
        except Exception as e:
            print(count_sql)
            raise e
        article_links = max(count_res - count_redirect, 0)
        self.log(f"  num links: {article_links}\n")
        return article_links

    def run(self):
        try:
            s = pywikibot.Site()
            # check all dab pages with at least ADD_THRESHOLD incoming links
            self.tcursor.execute(f"""
                    SELECT lc_id
                      FROM dab_link_count
                     WHERE lc_amnt > {ADD_THRESHOLD}
            """.encode("utf-8"))
            id_list = [("%d" % row['lc_id']) for row in self.tcursor.fetchall()]
            # find those that do not have {{incoming links}} tag
            self.wcursor.execute(f"""
                    SELECT page_id, page_title
                      FROM page
                     WHERE page_namespace=0
                       AND page_is_redirect=0
                       AND page_id IN ({",".join(id_list)})
                       AND EXISTS
                           (
                                SELECT * FROM page_props
                                 WHERE pp_page = page_id
                                   AND pp_propname = 'disambiguation'
                           )
                       AND NOT EXISTS
                           (
                                SELECT * from templatelinks, linktarget
                                 WHERE tl_target_id = lt_id
                                   AND lt_namespace = 10
                                   AND lt_title = 'Incoming_links'
                                   AND tl_from = page_id
                           )
            """.encode("utf-8"))
            for row in self.wcursor.fetchall():
                self.wcursor.execute(b"SELECT lag FROM heartbeat_p.heartbeat WHERE shard='s1'")
                lag = self.wcursor.fetchone()['lag']
                started = datetime.datetime.now() - datetime.timedelta(seconds=float(lag))
                title = row['page_title'].decode("utf8").replace('_', ' ')
                p = pywikibot.Page(s, title)
                self.log(f"Add template: {title} id: {row['page_id']}\n")
                if self.unique_link_count(row['page_title']) < ADD_THRESHOLD:
                    self.log("  too few links: aborted.\n")
                    continue
                try:
                    last_touched = p.latest_revision.timestamp
                    if last_touched > started:
                        self.log(f"  Page {title} has been edited recently, and replag doesn't allow us to see the changes:\n")
                        self.log(f"   (last page edit {last_touched!s}; replica current to {started!s})\n")
                        continue
                except pywikibot.exceptions.NoPageError:
                    self.log(f"  Page {title} does not exist\n")
                    continue
                if not p.botMayEdit():
                    self.log(f"  Blocked by {{{{nobots}}}} template on {title}\n")
                    continue
                content = "{{incoming links|date=" + datetime.date.today().strftime("%B %Y") + "}}\n" + p.text
                self.log(f"  Submitting content for {title}\n")
                p.text = content
                p.save(summary=f"Robot: adding incoming links template; {ADD_THRESHOLD}" +
                               " or more articles link here (see the [[Template:Dablinks/FAQ|FAQ]])",
                       minor=True)
            # find pages that do contain the {{incoming links}} template and see if they still have links
            self.wcursor.execute(b"""
                    SELECT tl_from, page_title
                      FROM templatelinks, linktarget, page
                     WHERE tl_target_id = lt_id
                       AND lt_namespace = 10
                       AND lt_title = 'Incoming_links'
                       AND tl_from = page_id
                       AND page_namespace = 0
            """)
            pages = {}
            for row in self.wcursor.fetchall():
                pages[row['tl_from']] = row['page_title']
            for page_id in pages:
                result = self.tcursor.execute(f"""
                        SELECT lc_amnt
                          FROM dab_link_count
                         WHERE lc_id = {page_id}
                """.encode("utf-8"))
                if result > 0:
                    amnt = self.tcursor.fetchone()['lc_amnt']
                if result == 0 or amnt < RMV_THRESHOLD or not self.is_dab(pages[page_id]):
                    self.wcursor.execute(b"SELECT lag FROM heartbeat_p.heartbeat WHERE shard='s1'")
                    lag = self.wcursor.fetchone()['lag']
                    started = datetime.datetime.now() - datetime.timedelta(seconds=float(lag))
                    page_title = pages[page_id].decode("utf8").replace('_', ' ')
                    p = pywikibot.Page(s, page_title)
                    self.log(f"Remove template: {page_title} id: {page_id}\n")
                    if self.unique_link_count(pages[page_id]) >= RMV_THRESHOLD:
                        self.log("  too many links -- aborted\n")
                        continue
                    try:
                        last_touched = p.latest_revision.timestamp
                    except pywikibot.exceptions.NoPageError:
                        self.log(f"  Page {page_title} does not exist\n")
                        continue
                    if last_touched > started:
                        self.log(f"  Page {page_title} has been edited recently, and replag doesn't allow us to see the changes:\n")
                        self.log(f"   (last page edit {last_touched!s}; replica current to {started!s})\n")
                        continue
                    if not p.botMayEdit():
                        self.log(f"  Blocked by {{{{nobots}}}} template on {page_title}\n")
                        continue
                    pattern = re.compile(r"\{\{\s*[Ii]ncoming[ _]*links\s*(\|.*?)?\}\}\s*")
                    content = pattern.sub('', p.text)
                    self.log(f"  Submitting content for {page_title}\n")
                    p.text = content
                    p.save(summary="Robot: removing incoming links template; fewer than " +
                                f"{RMV_THRESHOLD} links (see the [[Template:Dablinks/FAQ|FAQ]])",
                           minor=True)
        finally:
            if self.logging_enabled:
                self.logfile.close()
            self.tcursor.close()
            self.wcursor.close()
