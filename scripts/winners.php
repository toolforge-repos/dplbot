<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$page_name = "winners.php";

$fp = fopen("$HOME_DIR/data/sl.txt", "w");

$sql = "
             SELECT *
               FROM dab_hof
              ORDER BY year DESC, month_no DESC
              LIMIT 1
        ";

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if ($mysql) {

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      printResultsLocal($res, $fp);
    }
    else fputs($fp, "There are no results in this query.\n\n");

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    fputs($fp, "Database error: ".mysqli_error($mysql)."</p>\n\n");
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  fputs($fp, "Database connection error: ".mysqli_connect_error()."\n\n");
}

fclose($fp);


function printResultsLocal($res, $fp) {

  $row = mysqli_fetch_assoc($res);
  $month_name = $row['month_name'];
  fputs($fp, "Congratulations to ".$month_name."'s winners!  They have earned their place in the [[Wikipedia:Disambiguation_pages_with_links/Disambiguator_Hall_of_Fame|Disambiguator Hall of Fame]].\n\n");
  fputs($fp, "{| border=\"0\" style=\"background:transparent;\"\n");
  $user1 = $row['user1'];
  $count1 = $row['count1'];
  $user2 = $row['user2'];
  $count2 = $row['count2'];
  $user3 = $row['user3'];
  $count3 = $row['count3'];
  $user4 = $row['user4'];
  $count4 = $row['count4'];
  $bonus = $row['bonus'];
  $count5 = $row['count5'];
  fputs($fp, "|-\n|'''First place'''\n|&nbsp;&nbsp;&nbsp;\n|[[User:$user1|$user1]]\n|&nbsp;&nbsp;&nbsp;\n|align=\"right\"|[https://dplbot.toolforge.org/ch/user_results.php?user=".encodeTitle($user1)." $count1 fixed]\n");
  fputs($fp, "|-\n|'''Second place'''\n|&nbsp;&nbsp;&nbsp;\n|[[User:$user2|$user2]]\n|&nbsp;&nbsp;&nbsp;\n|align=\"right\"|[https://dplbot.toolforge.org/ch/user_results.php?user=".encodeTitle($user2)." $count2 fixed]\n");
  fputs($fp, "|-\n|'''Third place'''\n|&nbsp;&nbsp;&nbsp;\n|[[User:$user3|$user3]]\n|&nbsp;&nbsp;&nbsp;\n|align=\"right\"|[https://dplbot.toolforge.org/ch/user_results.php?user=".encodeTitle($user3)." $count3 fixed]\n");
  fputs($fp, "|-\n|'''Fourth place'''\n|&nbsp;&nbsp;&nbsp;\n|[[User:$user4|$user4]]\n|&nbsp;&nbsp;&nbsp;\n|align=\"right\"|[https://dplbot.toolforge.org/ch/user_results.php?user=".encodeTitle($user4)." $count4 fixed]\n");
  if ($bonus !== "") {
    fputs($fp, "|-\n|'''Bonus list champion'''\n|&nbsp;&nbsp;&nbsp;\n|[[User:$bonus|$bonus]]\n|&nbsp;&nbsp;&nbsp;\n|align=\"right\"|[https://dplbot.toolforge.org/ch/user_results.php?user=".encodeTitle($bonus)." $count5 fixed]\n");
  }
  fputs($fp, "|}\n\n(This month's contest is already underway. The new leaderboard will be posted tomorrow.)\n");
}

?>