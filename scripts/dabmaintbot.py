#!/usr/bin/python3
"""
dabmaintbot - Bot to update link counts on
[[en:Wikipedia:Disambiguation pages maintenance]]
"""

import datetime
import locale
import re
import MySQLdb
import pywikibot
from pywikibot import pagegenerators
from daily_disambig import clean

locale.setlocale(locale.LC_ALL, '')

# Constants:
ACTIVE_CUTOFF = 100
HISTORY_LEN = 6
THREAD_LIMIT = 100

started_at = datetime.datetime.now()

def increasing(seq):
    '''Return True iff seq is uniformly increasing (from last to first).'''
    for index in range( len(seq) - 1 ):
        if seq[index] <= seq[index+1]:
            return False
    return True

def fmt(num):
    return locale.format_string("%i", num, grouping=True)

def datekey(date):
    return date.strftime("%Y-%m-%d")

class DabMaintBot:
    def __init__(self):
        pass

    def run(self):
        s = pywikibot.Site()
        s.login()
        #input pages
        maint_page = pywikibot.Page(s,
                        "Wikipedia:Disambiguation pages with links/Current list")
        problem_page = pywikibot.Page(s,
                        "Wikipedia:Disambiguation pages with links/problems")
        #output pages
        result_page = pywikibot.Page(s,
                        "Wikipedia:Disambiguation pages with links/Current list")
        problem_result = pywikibot.Page(s,
                        "Wikipedia:Disambiguation pages with links/problems")
        fixed_pages = 0
        fixed_links = 0
        problems = []
        today = datetime.date.today()

        with MySQLdb.connect(db='s51290__dpl_p',
                           host="tools.db.svc.wikimedia.cloud",
              read_default_file='/data/project/dplbot/.my.cnf',
                        charset="utf8mb4") as db:
            cursor = db.cursor()
            cursor.execute("""\
                SELECT dt_title, dl_count /* SLOW_OK */
                FROM disambig_titles JOIN disambig_links ON dt_id=dl_id
                WHERE left(dt_title, 1)<>"#" AND dl_date = CURDATE();
            """)
            tooldata = dict((clean(row[0]), row[1]) for row in cursor.fetchall())

        m_text = maint_page.get()
        active_r = re.compile(
            r"^# (?:'''&bull; )?\[\[(.+)\]\] *\(([0-9]*) *" +
            r"\[\[Special:Whatlinkshere/(?:.+)\|links\]\]\) *" +
            r"(?:\((?:(?:new)|(?:[-+][0-9]+))\))? *" +
            r"(?:<!-- history (.*?)-->)? *(.*?) *(?:''')? *$", re.M)
        # the groups matched by this regex are:
        # 1.  the title of a disambiguation page
        # 2.  the number of links found last time the bot ran (may be empty)
        # 3.  the history of the page's link count (may be empty), consisting of a
        #     space-separated string of numbers
        # 4.  any notes added by users at the end of the line

        inactive_r = re.compile(
            r'^# \[\[(.+)\]\] \(([0-9]+)\) history ([0-9 ]*):(.*) *$', re.M)
        # the groups matched by this regex are the same as for active_r

        # lists are demarcated by HTML comments

        # Step 1: Collect all links and histories from the last scan

        start_mark = "<!-- section title="
        end_mark = "<!-- end section -->"
        marker = 0
        new_text = []
        disambiglinks = {}
        total_count = [0, 0, 0, 0]
        sections = []
        diffs = []
        while True:
            section_start = m_text.find(start_mark, marker)
            if section_start == -1:
                break
            title_mark = section_start + len(start_mark)
            section_title = m_text[title_mark:
                                   m_text.find(" -->\n", title_mark)]
            section_marker = title_mark + len(section_title) + len(" -->\n")
            if section_marker >= len(m_text):
                pywikibot.output(
                    "ERROR: cannot locate section title in %s" % section_title)
                raise RuntimeError

            section_end = m_text.find(end_mark, section_marker)
            if section_end == -1:
                pywikibot.output(
                    "ERROR: cannot locate end of section %s" % section_title)
                raise RuntimeError
            marker = section_end
            sections.append((section_title, section_marker, section_end))
            sectionnumber = len(sections) - 1

            match_data = {}
            for m in active_r.finditer(m_text, section_marker, section_end):
                title = m.group(1)
                if '#' in title:
                    title = title[:title.index('#')]
                if not title:
                    print("Ignoring %s" % m.group(1).encode("ascii", "xmlcharrefreplace"))
                    continue
                match_data[title] = (m.group(2), m.group(3), m.group(4))

            # search for inactive listings, which should always follow active ones
            for m in inactive_r.finditer(m_text, section_marker, section_end):
                title = m.group(1)
                if '#' in title:
                    title = title[:title.index('#')]
                if not title:
                    print("Ignoring %s" % m.group(1).encode("ascii", "xmlcharrefreplace"))
                    continue
                match_data[title] = (m.group(2), m.group(3), m.group(4))

            print("Found %s pages to update in '%s' on current list."\
                  % (len(match_data), section_title))

            for link_page in pagegenerators.PreloadingGenerator(
                              (pywikibot.Page(s, title) for title in match_data),
                              250):
                try:
                    item = match_data[link_page.title()]
                except KeyError:
                    continue
                while link_page.isRedirectPage():
                    title = link_page.getRedirectTarget().title(with_section=False)
                    link_page = pywikibot.Page(s, title)
                if not link_page.isDisambig():
                    continue
                link_page_title = link_page.title(with_section=False)
                if link_page_title in disambiglinks:
                    continue
                try:
                    count = tooldata[link_page_title]
                    del tooldata[link_page_title] # remove used pages
                except:
                    count = sum(1 for bl in link_page.backlinks(namespaces=[0]))
                if item[1]:
                    history = item[1]
                else:
                    history = ''
                disambiglinks[link_page_title] = {
                    'section': sectionnumber,
                    'title': link_page_title,
                    'count': count,
                    'history_text': history,
                    'trailing_text': item[2].strip()
                }

        # Step 2.  Add links from toolserver page that aren't already in the
        #          collection

        print("%i new pages being added to report" % sum(1 for k in tooldata
                                                         if tooldata[k]>=50))
        for link_page_title in tooldata:
            if tooldata[link_page_title] >= 50:
                disambiglinks[link_page_title] = {
                    'section': 0,
                    # All new articles go into 'general' until classified
                    'title': link_page_title,
                    'count': tooldata[link_page_title],
                    'history_text': '',
                    'trailing_text': ''
                }

        # Step 3.  Get all the refcount results off the queue

        # Step 4.  Sort links by section and count, and output page
        marker = 0
        for (number, (section_name, section_marker, section_end)
             ) in enumerate(sections):
            section_links = [link for link in list(disambiglinks.values())
                             if link['section'] == number]
            section_links.sort(key=lambda i:i['count'], reverse=True)
            section_count = [0, 0]
            new_text.append(m_text[marker:section_marker])
            active = True
            for link in section_links:
                if link['count'] < ACTIVE_CUTOFF and active:
                    active = False
                    new_text.append("<!-- Inactive articles:\n")
                if link['history_text']:
                    history = [int(n) for n in link['history_text'].split(" ")]
                else:
                    history = []
                history = [link['count']] + history
                while len(history) > HISTORY_LEN:
                    del history[-1]
                if len(history) == 1:
                    link['diff'] = 'new'
                else:
                    link['diff'] = "%+i" % (history[0] - history[1])
                    diffs.append( (history[0]-history[1], link['title']) )
                    if history[0] < history[1]:
                        fixed_pages += 1
                        fixed_links += (history[1] - history[0])
                link['history_text'] = " ".join(str(x) for x in history)
                # discard items that have no significant growth
                max_links = max(history)
                if len(history) == HISTORY_LEN:
                    max_growth = max((history[i-1] - history[i])
                                      for i in range(1, len(history)))
                    max_growth = max(0, max_growth)
                    if max_links + 5 * max_growth < ACTIVE_CUTOFF / 2:
                        continue

                if active:
                    section_count[0] += 1
                    section_count[1] += link['count']
                    item = (
"[[%(title)s]] (%(count)i [[Special:Whatlinkshere/%(title)s|links]]) " +
"(%(diff)s)<!-- history %(history_text)s--> %(trailing_text)s") % link
                    # bullet items that have shown unusual or persistent increases
                    if (len(history) > 1 and
                            history[0]-history[1] > ACTIVE_CUTOFF / 2
                       ) or (
                            len(history) == HISTORY_LEN and
                            increasing(history) and
                            history[0] - history[-1] > ACTIVE_CUTOFF
                       ):
                        prefix = "'''&bull; "
                        suffix = "'''"
                        item = item.rstrip("'")
                        problems.append(
"* [[%(title)s]] (%(count)i [[Special:Whatlinkshere/%(title)s|links]]) (%(diff)s)\n"
                            % link)
                    else:
                        prefix = suffix = ""
                    new_text.append("# %s%s%s\n" % (prefix, item, suffix))
                else:
                    total_count[2] += 1
                    total_count[3] += link['count']
                    new_text.append(
"# [[%(title)s]] (%(count)i) history %(history_text)s: %(trailing_text)s\n"
                        % link)
            if not active:
                new_text.append("-->\n")
            marker = section_end
            new_text.append(
                "\n Section '%s' contains %i links to %i active articles.\n" %
                (section_name, section_count[1], section_count[0]))
            total_count[0] += section_count[0]
            total_count[1] += section_count[1]

        diffs.sort()
        statistics_point = m_text.find("|}")
        if statistics_point >= 0:
            text = m_text[marker:statistics_point]
            text = re.sub(r"(?s)<!--banner-->.*?<!--/banner-->",
"""<!--banner-->
'''''Since last week, at least %s links to %s pages have been fixed!'''''
<!--/banner-->"""
                              % (fmt(fixed_links), fmt(fixed_pages)), text)
            top10 = ["\n===Top 10 increases==="]
            for item in reversed(diffs[-10:]):
                top10.append("# [[%s]] (%+i)" % (item[1], item[0]))
            top10.append("===Top 10 decreases===")
            for item in diffs[:10]:
                top10.append("# [[%s]] (%+i)" % (item[1], item[0]))
            top10.append("<!--/banner-->")
            text = text.replace("<!--/banner-->", "\n".join(top10))
            new_text.append(text)
            marker = statistics_point
            new_text.append("|-\n")
            today = datetime.date.today()
            new_text.append("| %4i-%02i-%02i || %s || %s || %s || %s\n"
                            % (today.year, today.month, today.day,
                               fmt(total_count[0]+total_count[2]),
                               fmt(total_count[0]),
                               fmt(total_count[1]+total_count[3]),
                               fmt(total_count[1])))

        new_text.append(m_text[marker:])
        result_page.text = "".join(new_text)
        result_page.save(summary="Disambiguation page maintenance script")
        prob_text = problem_page.get()
        header_start = prob_text.index("<noinclude>")
        header_end = prob_text.index("</noinclude>") + len("</noinclude>")
        problem_result.text = (prob_text[header_start:header_end] + "\n" +
                      "".join(problems))
        problem_result.save(summary="Disambiguation page maintenance script")

# vim: ai ts=4 sts=4 et sw=4
