<?php

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

if ($mysql) {
  run_query($mysql, "TRUNCATE TABLE ch_results");
  run_query($mysql, "TRUNCATE TABLE ch_no_fixer");
}
else
  log_error(date("F j G:i", time()), "trunc_cmds.php", "mysql connect", mysqli_connect_error());


function run_query ($mysql, $sql) {

  $result = mysqli_query($mysql, $sql);

  if ($result == false)
    log_error(date("F j G:i", time()), "trunc_cmds.php", $sql, mysqli_error($mysql));
}
?>