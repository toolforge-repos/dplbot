<?php

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
if (! $mysql) {
  log_error(date("F j G:i", time()), "admin_list.php", "toolsdb connect", mysqli_connect_error());
  print mysqli_connect_error()."\n";
}

$enwiki = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
if (! $enwiki) {
  log_error(date("F j G:i", time()), "admin_list.php", "enwiki connect", mysqli_connect_error());
  print mysqli_connect_error()."\n";
}

if ($mysql && $enwiki) {

  mysqli_query($mysql, "DROP TABLE IF EXISTS t_admin_list");

  $result = mysqli_query($mysql, "
     CREATE TABLE t_admin_list (
            admin varchar(255) binary NOT NULL default '',
            PRIMARY KEY (admin)
          ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC
  ");
  if ($result == false) {
    print mysqli_error($mysql)."\n";
  }

  $result = mysqli_query($enwiki, "
          SELECT user_name AS admin
            FROM user, user_groups
           WHERE ug_group = 'sysop'
             AND user_id = ug_user
           GROUP BY admin
  ");

  if ($result) {
    while ($row = mysqli_fetch_assoc($result)) {
      $admin = mysqli_real_escape_string($mysql, $row['admin']);
      mysqli_query($mysql, "INSERT INTO t_admin_list (admin) VALUES ('$admin')");
    }
    mysqli_free_result($result);

    $sql = "DROP TABLE admin_list";
    mysqli_query($mysql, $sql);

    $sql = "RENAME TABLE t_admin_list TO admin_list";
    mysqli_query($mysql, $sql);
  } else {
    print mysqli_error($enwiki)."\n";
  }
}

?>