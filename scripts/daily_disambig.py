#!/usr/bin/python3
# -*- coding: utf-8  -*-

# Copyright (c) 2013-2023 Russell Blau

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime
import locale
import re
import time
from warnings import filterwarnings
import MySQLdb
import pywikibot
import pywikibot.config

locale.setlocale(locale.LC_ALL, "")
filterwarnings('ignore', category = MySQLdb.Warning)

CONTESTSIZE = 1000
ONE_DAY = datetime.timedelta(days=1)

def datekey(date):
    return date.strftime("%Y-%m-%d")

def timestamp():
    return str(datetime.datetime.now().time())

def clean(title):
    if isinstance(title, str):
        return title.replace("_", " ")
    return title.decode("utf-8").replace("_", " ")

class DailyDisambigWriter:
    """Publish the Daily Disambig."""
    def __init__(self):
        pywikibot.config.maxlag = 0

    def getdb(self):
        while True:
            try:
                db = MySQLdb.connect(db='s51290__dpl_p',
                             host="tools.db.svc.wikimedia.cloud",
                             read_default_file='/data/project/dplbot/.my.cnf',
                             charset="utf8mb4")
                self.cursor = db.cursor()
                self.cursor._defer_warnings = True
                self.dcursor = db.cursor(cursorclass=MySQLdb.cursors.DictCursor)
                self.dcursor._defer_warnings = True
                return db
            except MySQLdb.OperationalError:
                pywikibot.log("Database operational error; retrying. "
                    + timestamp())
                time.sleep(5*60)
                continue

    def run(self):
        s = pywikibot.Site()
        offset = 0
        today = datetime.date.today()
        yesterday = today - ONE_DAY
        tomorrow = today + ONE_DAY
        weekago = today - datetime.timedelta(days=7)
        monthago = today - datetime.timedelta(days=30)
        pywikibot.log("  Starting daily disambig script for " + datekey(today))
        # get today's disambig stats from the Toolforge user database
        while True:
            try:
                db = self.getdb()
                pywikibot.log("Checking dab_link_count status. " + timestamp())
                self.dcursor.execute("""
                    SHOW TABLE STATUS LIKE 'dab_link_count'
                """)
                row = self.dcursor.fetchone()
                lastupdate = row['Create_time']
                if lastupdate.date() != today:
                    pywikibot.output(
                        "Error: no page update today. " + timestamp())
                    raise SystemExit(-1)
                # get list of all dates on which dab data was updated
                pywikibot.log("Retrieving update dates. " + timestamp())
                self.cursor.execute(
                    "SELECT DISTINCT dl_date FROM disambig_links /* SLOW_OK */")
                updates = [row[0] for row in self.cursor.fetchall()]
                # copy new page titles into database
                pywikibot.log("Storing today's new page titles. " + timestamp())
                self.cursor.execute(f"""
                    INSERT IGNORE INTO disambig_titles (dt_title, dt_joined)
                       SELECT DISTINCT lc_title, DATE('{datekey(today)}') 
                                  FROM dab_link_count
                    """.encode("utf-8")
                )
                pywikibot.log("Storing today's link counts. " + timestamp())
                self.cursor.execute(
                    f"""INSERT IGNORE INTO disambig_links (dl_id, dl_date, dl_count)
                        SELECT dt_id, DATE('{datekey(today)}'), lc_amnt
                        FROM dab_link_count
                        JOIN disambig_titles ON dt_title = lc_title
                        /*SLOW_OK*/
                    """.encode("utf-8")
                )
                db.commit()
                updates.append(today)
                break

            except MySQLdb.OperationalError:
                pywikibot.log("Database operational error; retrying. "
                    + timestamp())
                time.sleep(5*60)
                continue

            finally:
                db.close()

        while monthago not in updates:
            monthago -= ONE_DAY
        while weekago not in updates:
            weekago -= ONE_DAY
        while yesterday not in updates:
            yesterday -= ONE_DAY
        daily = pywikibot.Page(s,
            "Wikipedia:Disambiguation pages with links/The Daily Disambig")
        stats = pywikibot.Page(s,
            "Wikipedia:Disambiguation pages with links/The Daily Disambig/Stats")

        while True:
            try:
                db = self.getdb()
                table1 = []
                pywikibot.log("Selecting today's link counts from database. "
                              + timestamp())
                self.cursor.execute(
                    f"""SELECT dt_title, dl_count
                        FROM disambig_titles JOIN disambig_links ON dt_id=dl_id
                        WHERE dl_date = DATE('{datekey(today)}') AND LEFT(dt_title, 1) <> "#"
                        /* SLOW_OK */
                    """.encode("utf-8")
                )
                todaysitems = list(self.cursor.fetchall())
                for n in (1, 10, 25, 50, 100, 200, 400):
                    dpln = list(filter(lambda row: row[1] >= n, todaysitems))
                    table1.append(f"{len(dpln):,}")
                    table1.append(f"{sum(v for k, v in dpln):,}")
                if table1[0] == "0":
                    pywikibot.log("No data retrieved; retrying. "
                        + timestamp())
                    time.sleep(5*60)
                    continue
                break
            except MySQLdb.OperationalError:
                pywikibot.log("Database operational error; retrying. "
                  + timestamp())
                time.sleep(5*60)
            finally:
                db.close()

        # store today's counts for table 3
        pywikibot.log("Starting articlelinks query. " + timestamp())
        articlelinks = {}
        for n in range(5, 0, -1):
            while True:
                try:
                    db = self.getdb()
                    self.cursor.execute("""
                        SELECT COUNT(*) FROM to_dab_link_count
                        WHERE lc_amnt >= %i;
                    """ % n)
                    row = self.cursor.fetchone()
                    articlelinks[n] = int(row[0])
                    break
                except MySQLdb.OperationalError:
                    pywikibot.log("Database operational error; retrying. "
                        + timestamp())
                    time.sleep(5*60)
                finally:
                    db.close()

        todaydate = today.strftime("%B %d, %Y")
        if todaydate not in stats.text:
            pywikibot.log("Updating today's stats. " + timestamp())
            row1 = '|- align="right"\n| ' + todaydate + " || " \
                   + " || ".join(table1) + '\n<!--end Table 1-->'
            stats.text = stats.text.replace("<!--end Table 1-->", row1)
            table2 = []
            todaysitems.sort(key=lambda i:i[1])
            for n in (10, 25, 50, 100, 250, 500, 1000):
                table2.append(f"{sum(item[1] for item in todaysitems[-n:]):,}")
            row2 = '|- align="right"\n| ' + todaydate + " || " \
                   + " || ".join(table2) + '\n<!--end Table 2-->'
            stats.text = stats.text.replace("<!--end Table 2-->", row2)
            row3 = '|- align="right"\n| ' + todaydate + " || " \
                   + " || ".join(f"{articlelinks[z]:,}" for z in range(5, 0, -1)) \
                   + '\n<!--end Table 3-->'
            stats.text = stats.text.replace("<!--end Table 3-->", row3)
            stats.save("Updating Daily Disambig stats", minor=False)
        else:
            pywikibot.log("Stats already updated. " + timestamp())

        table1 = stats.text[ stats.text.find("=== Table 1 ===")
                             : stats.text.find("<!--end Table 1-->")]
        table2 = stats.text[ stats.text.find("=== Table 2 ===")
                             : stats.text.find("<!--end Table 2-->")]
        table3 = stats.text[ stats.text.find("=== Table 3 ===")
                             : stats.text.find("<!--end Table 3-->")]
        tabledata = {}

        # Table 1 has 14 data columns after the date
        for row1 in re.finditer(
r'''\|- *align="right"\n\| *(?P<month>\w+) (?P<day>\d+), (?P<year>\d+) *\|\| *'''
r'''([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+) *\|\| *'''
r'''([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+) *\|\| *'''
r'''([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+)\n''',
                table1):
            day = datetime.datetime.strptime(
                "%(month)s %(day)s, %(year)s" % row1.groupdict(),
                "%B %d, %Y")
            tabledata[day.date()] = [int(ct.replace(",",""))
                                     for ct in row1.groups()[3:]]

        # Table 2 has 7 data columns after the date
        for row2 in re.finditer(
r'''\|- *align="right"\n\| *(?P<month>\w+) (?P<day>\d+), (?P<year>\d+) *\|\| *'''
r'''([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+) *\|\| *'''
r'''([\d,]+) *\|\| *([\d,]+)\n''',
                table2):
            day = datetime.datetime.strptime(
                "%(month)s %(day)s, %(year)s" % row2.groupdict(),
                "%B %d, %Y"
            )
            tabledata[day.date()].extend(
                int(ct.replace(",","")) for ct in row2.groups()[3:]
            )

        # Table 3 has 5 data columns after the date
        for row3 in re.finditer(
r'''\|- *align="right"\n\| *(?P<month>\w+) (?P<day>\d+), (?P<year>\d+) *\|\| *'''
r'''([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+) *\|\| *([\d,]+)\n''',
                table3):
            day = datetime.datetime.strptime(
                "%(month)s %(day)s, %(year)s" % row3.groupdict(),
                "%B %d, %Y"
            )
            tabledata[day.date()].extend(
                int(ct.replace(",","")) for ct in row3.groups()[3:]
            )

        table1rows = []
        table2rows = []
        table3rows = []
        dates = [(today - datetime.timedelta(days=ago))
                for ago in (28, 21, 14, 7, 6, 5, 4, 3, 2, 1, 0)]
        first = datetime.date(today.year, today.month, 1) # first day of this month
        while first not in updates:
            first = first + ONE_DAY # find first day with data
        if first not in dates:
            dates.append(first)
        for monthsago in (1, 2, 3, 6, 12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 132, 144):
            m = today.month - monthsago
            y = today.year
            while m < 1:
                m += 12
                y -= 1
            olddate = datetime.date(y, m, 1) # first day of month X months ago
            while olddate not in updates:
                olddate = olddate + ONE_DAY
            if olddate not in dates:
                dates.append(olddate)
        dates.sort()
        for then in dates:
            if then not in tabledata:
                continue
            table1rows.append('|- align="right"\n| '
                              + then.strftime("%B %d, %Y") + " || "
                              + " || ".join(f"{n:,}" for n in tabledata[then][:14]))
            table2rows.append('|- align="right"\n| '
                              + then.strftime("%B %d, %Y") + " || "
                              + " || ".join(f"{n:,}" for n in tabledata[then][14:21]))
            if len(tabledata[then]) < 26:
                continue
            table3rows.append('|- align="right"\n| '
                              + then.strftime("%B %d, %Y") + " || "
                              + " || ".join(f"{n:,}" for n in tabledata[then][21:]))
        table1text = "\n".join(table1rows)
        table2text = "\n".join(table2rows)
        table3text = "\n".join(table3rows)

        # compute DAB Challenge progress
        lastmonth = first - ONE_DAY
        while lastmonth not in updates:
            lastmonth = lastmonth - ONE_DAY
        lastday = lastmonth.strftime("%B ") + str(int(lastmonth.strftime("%d")))
        firstday = first.strftime("%B ") + str(int(first.strftime("%d")))

        pywikibot.log(("Starting contest query. ") + timestamp())
        while True:
            try:
                db = self.getdb()
                self.cursor.execute("""\
                    SELECT dt_title,
                           first.dl_count,
                           IFNULL(
                               (SELECT today.dl_count
                                FROM disambig_links AS today
                                WHERE today.dl_id = first.dl_id
                                      AND today.dl_date=DATE('%s')
                               ), 0)
                      FROM disambig_links AS first
                      JOIN disambig_titles ON first.dl_id = dt_id
                     WHERE first.dl_date=DATE('%s') AND LEFT(dt_title, 1) <> "#"
                     ORDER BY first.dl_count DESC
                    /* SLOW_OK */
                """ % (datekey(today), datekey(lastmonth)))
                contest_all_pages = list(self.cursor.fetchall())
                break
            except MySQLdb.OperationalError:
                pywikibot.log("Database operational error; retrying. "
                    + timestamp())
                time.sleep(5*60)
            finally:
                db.close()
        contest_original_links = sum(v[1] for v in contest_all_pages)
        contest_current_links = sum(v[2] for v in contest_all_pages)
        contest_current_pages = sum(1 for v in contest_all_pages if v[2])
        contest_progress = (100.0 * (contest_original_links - contest_current_links)
                           / contest_original_links)
        contest_original_links = f"{contest_original_links:,}"
        contest_current_links = f"{contest_current_links:,}"
        contest_current_pages = f"{contest_current_pages:,}"

        # compute today's absolute and percentage changes
        pywikibot.log("Starting one-day changes query. " + timestamp())
        while True:
            try:
                db = self.getdb()
                self.cursor.execute(
                     """SELECT dt_title, today.dl_count, today.dl_count - yesterday.dl_count
                        FROM disambig_titles
                            JOIN disambig_links AS today on dt_id=today.dl_id
                            JOIN disambig_links AS yesterday on dt_id=yesterday.dl_id
                        WHERE today.dl_date = DATE('%s')
                              AND yesterday.dl_date = DATE('%s')
                              AND LEFT(dt_title, 1) <> "#"
                        /* SLOW_OK */
                     """ % (datekey(today), datekey(yesterday))
                )
                break
            except MySQLdb.OperationalError:
                pywikibot.log("Database operational error; retrying. "
                  + timestamp())
                time.sleep(5*60)
            finally:
                db.close()
        abschange = []
        for row in self.cursor.fetchall():
            try:
                abschange.append((clean(row[0]), row[1], row[2]))
            except:
                pywikibot.log("Error decoding " + str(row))
        abschange = sorted(abschange, key=lambda vec:(vec[2], vec[0], vec[1]))
        absinc = "\n".join("* [[%s]]: %i links (+%i)" % item
                           for item in reversed(abschange[-10:]))
        absdec = "\n".join("* [[%s]]: %i links (%i)" % item
                           for item in abschange[:10])
        pctchange = sorted(abschange,
                           key=lambda vec:(float(vec[2])/(vec[1]-vec[2]),
                                           vec[0], vec[1]))
        pctinc = "\n".join("* [[%s]]: %i links (+%i)" % item
                           for item in reversed(pctchange[-10:])
                           if item not in abschange[-10:])
        pctdec = "\n".join("* [[%s]]: %i links (%i)" % item
                           for item in pctchange[:10]
                           if item not in abschange[:10])

        # compute week's absolute and percentage changes
        pywikibot.log("Starting 7-day changes query. "
                      + timestamp())
        while True:
            try:
                db = self.getdb()
                self.cursor.execute("""
                    SELECT dt_title, today.dl_count, today.dl_count - weekago.dl_count
                      FROM disambig_titles
                      JOIN disambig_links AS today on dt_id=today.dl_id
                      JOIN disambig_links AS weekago on dt_id=weekago.dl_id
                     WHERE today.dl_date = DATE('%s')
                       AND weekago.dl_date = DATE('%s')
                       AND LEFT(dt_title, 1) <> "#"
                           /* SLOW_OK */
                    """ % (datekey(today), datekey(weekago))
                )
                weekabschange = sorted(((clean(row[0]), row[1], row[2])
                                       for row in self.cursor.fetchall()),
                                key=lambda vec:(vec[2], vec[0], vec[1]))

                break
            except MySQLdb.OperationalError:
                pywikibot.log("Database operational error; retrying. "
                  + timestamp())
                time.sleep(5*60)
        weekabsinc = "\n".join("* [[%s]]: %i links (+%i)" % item
                               for item in reversed(weekabschange[-10:]))
        weekabsdec = "\n".join("* [[%s]]: %i links (%i)" % item
                               for item in weekabschange[:10])
        weekpctchange = sorted(weekabschange,
                               key=lambda vec:(float(vec[2])/(vec[1]-vec[2]),
                                               vec[0], vec[1]))
        weekpctinc = "\n".join("* [[%s]]: %i links (+%i)" % item
                               for item in reversed(weekpctchange[-10:])
                               if item not in weekabschange[-10:])
        weekpctdec = "\n".join("* [[%s]]: %i links (%i)" % item
                               for item in weekpctchange[:10]
                               if item not in weekabschange[:10])

        # compute month's absolute and percentage changes
        pywikibot.log("Starting 30-day changes query. "
                      + timestamp())
        while True:
            try:
                db = self.getdb()
                self.cursor.execute("""
                    SELECT dt_title, today.dl_count, today.dl_count - monthago.dl_count
                      FROM disambig_titles
                      JOIN disambig_links AS today on dt_id=today.dl_id
                      JOIN disambig_links AS monthago on dt_id=monthago.dl_id
                     WHERE today.dl_date = DATE('%s')
                       AND monthago.dl_date = DATE('%s')
                       AND LEFT(dt_title, 1) <> "#"
                           /* SLOW_OK */
                """ % (datekey(today), datekey(monthago)))
                monthabschange = sorted(((clean(row[0]), row[1], row[2])
                                        for row in self.cursor.fetchall()),
                                    key=lambda vec:(vec[2], vec[0], vec[1]))
                break
            except MySQLdb.OperationalError:
                pywikibot.log("Database operational error; retrying. " + timestamp())
                time.sleep(5*60)

        monthabsinc = "\n".join("* [[%s]]: %i links (+%i)" % item
                               for item in reversed(monthabschange[-10:]))
        monthabsdec = "\n".join("* [[%s]]: %i links (%i)" % item
                               for item in monthabschange[:10])
        monthpctchange = sorted(monthabschange,
                               key=lambda vec:(float(vec[2])/(vec[1]-vec[2]),
                                               vec[0], vec[1]))
        monthpctinc = "\n".join("* [[%s]]: %i links (+%i)" % item
                               for item in reversed(monthpctchange[-10:])
                               if item not in monthabschange[-10:])
        monthpctdec = "\n".join("* [[%s]]: %i links (%i)" % item
                               for item in monthpctchange[:10]
                               if item not in monthabschange[:10])

        # find today's new and gone disambig pages
        pywikibot.log("Starting newpages query. " + timestamp())
        while True:
            try:
                db = self.getdb()
                newpages = []
                newvalues = []
                self.cursor.execute("""
                    SELECT dt_title, today.dl_count, dt_id
                      FROM disambig_titles
                      JOIN disambig_links as today ON dt_id=today.dl_id
                     WHERE today.dl_date = DATE('%s')
                       AND LEFT(dt_title, 1) <> "#"
                       AND (SELECT yesterday.dl_count
                              FROM disambig_links as yesterday
                             WHERE today.dl_id = yesterday.dl_id
                               AND yesterday.dl_date = DATE('%s')
                           ) IS NULL
                  ORDER BY today.dl_count DESC, dt_title
                           /* SLOW_OK */
                """ % (datekey(today), datekey(yesterday)))
                for row in self.cursor.fetchall():
                    try:
                        newpages.append((clean(row[0]), int(row[1])))
                    except:
                        continue
                    title = re.sub(r"\\", r"\\\\", row[0])
                    title = re.sub(r"'", r"\'", title)
                    newvalues.append("(%i, '%s', DATE('%s'))"
                                     % (int(row[2]), title, datekey(today)))
                break
            except MySQLdb.OperationalError:
                pywikibot.log("Database operational error; retrying. " + timestamp())
                time.sleep(5*60)
            finally:
                db.close()
        with self.getdb() as db:
            nvstart = 0
            while nvstart < len(newvalues):
                self.cursor.execute(
                    "REPLACE INTO disambig_titles (dt_id, dt_title, dt_joined) VALUES %s"
                     % ",".join(newvalues[nvstart:nvstart+100])
                )
                nvstart += 100
            db.commit()
        newcount = f"{len(newpages):,}"
        newtop5 = "\n".join("* [[%s]]: %i links" % item
                            for item in newpages[:5])
        newlist = "\n".join("* [[%s]]: %i links" % item
                            for item in newpages[5:600])
        pywikibot.log("Starting gone-pages query. " + timestamp())
        while True:
            try:
                db = self.getdb()
                gonepages = []
                gonevalues = []
                self.cursor.execute("""
                    SELECT dt_title, yesterday.dl_count, dt_id
                      FROM disambig_titles
                      JOIN disambig_links AS yesterday ON dt_id=yesterday.dl_id
                     WHERE yesterday.dl_date = DATE('%s')
                       AND LEFT(dt_title, 1) <> "#"
                       AND (SELECT today.dl_count
                              FROM disambig_links as today
                             WHERE yesterday.dl_id = today.dl_id
                               AND today.dl_date = DATE('%s')
                           ) IS NULL
                  ORDER BY yesterday.dl_count DESC, dt_title
                          /* SLOW_OK */
                """ % (datekey(yesterday), datekey(today)))
                for row in self.cursor.fetchall():
                    try:
                        gonepages.append((clean(row[0]), int(row[1])))
                    except:
                        print("Error decoding" + repr(row[0]))
                        continue
                    title = re.sub(r"\\", r"\\\\", row[0])
                    title = re.sub(r"'", r"\'", title)
                    gonevalues.append("(%i, '%s', NULL)" % (int(row[2]), title))
                break
            except MySQLdb.OperationalError:
                pywikibot.log("Database operational error; retrying. "
                  + timestamp())
                time.sleep(5*60)
            finally:
                db.close()
        if gonevalues:
            with self.getdb() as db:
                self.cursor.execute(
                    "REPLACE INTO disambig_titles (dt_id, dt_title, dt_joined) VALUES %s"
                    % ",".join(gonevalues)
                )
                db.commit()
        gonecount = f"{len(gonepages):,}"
        gonetop5 = "\n".join("* [[%s]]: had %i links" % item
                             for item in gonepages[:5])
        gonelist = "\n".join("* [[%s]]: had %i links" % item
                             for item in gonepages[5:600])

        # prepare page text
        pywikibot.log("Preparing page text. " + timestamp())
        daily.text = """\
{{shortcut|WP:TDD}}
{{redirect|WP:TDD|information on using talk pages|Wikipedia:Talk dos and don'ts}}
'''The Daily Disambig''' is a regular update of trends and changes in the
world of 
[[Wikipedia:Disambiguation pages with links|disambiguation pages with links]].
Please contact [[User talk:R'n'B|R'n'B]] with any feature requests, comments, or
suggestions.

All information on this page is derived from the wonderful
[//dplbot.toolforge.org/disambig_links.php Toolforge reports]
generated by [[User:JaGa]], and therefore is based on that report's
methodology.

== Running statistics ==

:'''For complete historical statistics, see [[/Stats|the subpage]]'''

=== Table 1 ===

{|class="wikitable" id="dd1"
|+ Pages and links by number of links to disambig page
! Date
! colspan=2 | DPL-All
! colspan=2 | DPL-10
! colspan=2 | DPL-25
! colspan=2 | DPL-50
! colspan=2 | DPL-100
! colspan=2 | DPL-200
! colspan=2 | DPL-400
|-
!
! Pages !! Links
! Pages !! Links
! Pages !! Links
! Pages !! Links
! Pages !! Links
! Pages !! Links
! Pages !! Links
%(table1text)s
|}

Explanation: "DPL-All" is all disambiguation pages with links;
"DPL-50" is all disambiguation pages with 50 or more links, etc.  The
columns under each of these headings show, respectively, how many
disambiguation pages have that number of links, and how many total
article links exist to these disambiguation pages.

=== Table 2 ===

{| class="wikitable" id="dd2"
|+ Number of links to most-linked disambig pages
! Date
! Top 10
! Top 25
! Top 50
! Top 100
! Top 250
! Top 500
! Top 1000
%(table2text)s
|}

This table simply shows how many article links exist to the 10, 25,
etc. disambiguation pages with the greatest number of links.

=== Table 3 ===

{| class="wikitable" id="dd3"
|+ Count of articles by number of links to disambig pages contained
! Date
! 5 or more
! 4 or more
! 3 or more
! 2 or more
! 1 or more
%(table3text)s
|}

== Today's highlights ==

'''DAB Challenge progress''':
* On %(lastday)s, the pages on the \
[//dplbot.toolforge.org/ch/monthly_list.php Monthly List] had \
%(contest_original_links)s links. Today, %(contest_current_pages)s pages with \
%(contest_current_links)s links remain &mdash; \
'''%(contest_progress)3.1f%% completed'''
; %(newcount)s disambig pages joined the list today, including&#58;
%(newtop5)s
{{hidden begin|toggle=left|title=&nbsp;... and more}}
%(newlist)s
{{hidden end}}

; %(gonecount)s disambig pages left the list today, including&#58;
%(gonetop5)s
{{hidden begin|toggle=left|title=&nbsp;... and more}}
%(gonelist)s
{{hidden end}}

=== Changes over 1 day ===
{| class="wikitable"
|+ Largest absolute changes in link count (one day)
|-
! Increases !! Decreases
|-
|
%(absinc)s
|
%(absdec)s
|}

{| class="wikitable"
|+ Large percentage changes in link count (one day)
|-
! Increases !! Decreases
|-
|
%(pctinc)s
|
%(pctdec)s
|}

=== Changes over 7 days ===
{| class="wikitable"
|+ Largest absolute changes in link count (last 7 days)
|-
! Increases !! Decreases
|-
|
%(weekabsinc)s
|
%(weekabsdec)s
|}

{| class="wikitable"
|+ Large percentage changes in link count (last 7 days)
|-
! Increases !! Decreases
|-
|
%(weekpctinc)s
|
%(weekpctdec)s
|}

=== Changes over 30 days ===
{| class="wikitable"
|+ Largest absolute changes in link count (last 30 days)
|-
! Increases !! Decreases
|-
|
%(monthabsinc)s
|
%(monthabsdec)s
|}

{| class="wikitable"
|+ Large percentage changes in link count (last 30 days)
|-
! Increases !! Decreases
|-
|
%(monthpctinc)s
|
%(monthpctdec)s
|}

{{Disambiguation pages with links/The Daily Disambig-footer}}
        """ % locals()
        daily.save("Updating the Daily Disambig", minor=False)

        # Update list of recently added pages
        recentlist = pywikibot.Page(s,
            "Wikipedia:Disambiguation pages with links/The Daily Disambig/Recently added")
        startday = min(d for d in updates if d > weekago)
        startdate = datekey(startday)
        pagelist = []
        offset = 0
        while True:
            try:
                db = self.getdb()
                r = self.cursor.execute(f'''
                    SELECT /* SLOW_OK */ dt_title, dt_joined, dl_count
                      FROM disambig_titles
                      JOIN disambig_links ON (dt_id=dl_id AND dl_date=DATE('{datekey(today)}'))
                     WHERE dt_joined >= DATE('{startdate}')
                     LIMIT {offset}, 5000
                ''')
                if r == 0:
                    break
                offset += r
                for row in self.cursor.fetchall():
                    pagelist.append((row[1], -int(row[2]), clean(row[0]))) # date, -links, title
            except MySQLdb.OperationalError:
                pywikibot.log("Database operational error; retrying. " + timestamp())
                time.sleep(5*60)
            finally:
                db.close()
        pagelist.sort()
        recenttable = ['|-\n| [[%s]] || %s || <span style="display:none">%05i</span>[[Special:WhatLinksHere/%s|%i links]]'
                         % (item[2], datekey(item[0]), -item[1], item[2], -item[1])
                       for item in pagelist]
        recentlist.text = """\
This is a list of disambiguation pages with links that have recently appeared \
on ''The Daily Disambig'' as pages joining the list, and that still have \
incoming links as of today's edition.

{| class="wikitable sortable"
|+ Recently added disambiguation pages with links
|-
! Disambiguation page !! Date added !! Current link count
%s
|}

""" % "\n".join(recenttable)
        recentlist.save("Updating list", minor=False)

        if tomorrow.day == 1:
            # Generate new monthly page on the last calendar day of preceding month
            month = tomorrow.strftime("%B")
            year = tomorrow.year
            thismonth = today.month
            thisyear = today.year
            if thismonth == tomorrow.month:  # TEST BLOCK - should never be true
                nextmonth = thismonth + 1
                nextyear = thisyear
                if nextmonth > 12:
                    nextmonth -= 12
                    nextyear += 1
                tomorrow = tomorrow.replace(month=nextmonth, year=nextyear, day=1)
                month = tomorrow.strftime("%B")
                year = tomorrow.year
            firstofmonth = []
            firstofmonth.append(today.replace(day=1))
            for n in range(1, 4):
                # store first day of each of previous 3 months
                thismonth -= 1
                if thismonth == 0:
                    thisyear -= 1
                    thismonth = 12
                firstofmonth.append(
                        firstofmonth[n-1].replace(year=thisyear, month=thismonth))

            listpages = [pywikibot.Page(s,
                            "Wikipedia:Disambiguation pages with links/"
                            + m.strftime('%B %Y')
                        ) for m in firstofmonth]
            pagelinks = [set(p.linkedPages(namespaces=0)) for p in listpages]
            pywikibot.log(f"Starting monthly top-{CONTESTSIZE} query. {timestamp()}")
            while True:
                try:
                    db = self.getdb()
                    self.cursor.execute("""\
                        SELECT dt_title, dl_count /*SLOW_OK*/
                          FROM disambig_titles
                          JOIN disambig_links ON dl_id = dt_id
                         WHERE dl_date = DATE('%s') and left(dt_title, 1)<>"#"
                      ORDER BY dl_count DESC, dt_title ASC
                         LIMIT %i
                        """ % (datekey(today), CONTESTSIZE)
                    )
                    dabpages = [(clean(row[0]), int(row[1]))
                                for row in self.cursor.fetchall()]
                    break
                except MySQLdb.OperationalError:
                    pywikibot.log("Database operational error; retrying. "
                      + timestamp())
                    time.sleep(5*60)
                finally:
                    db.close()
            count = sum(number for (title, number) in dabpages)
            fmtcount = f"{count:,}"
            for i in range(len(dabpages)):
                (ttl, nmb) = dabpages[i]
                pg = pywikibot.Page(s, ttl)
                if pg in pagelinks[0]:
                    if pg in pagelinks[1]:
                        if pg in pagelinks[2]:
                            if pg in pagelinks[3]:
                                prefix = "[[File:Rotating red star.gif|18px|Large rotating red star]]"
                            else:
                                prefix = "[[File:Red star2.png|13px|First of three red stars]][[File:Red star2.png|13px|Second of three red stars]][[File:Red star2.png|13px|Third of three red stars]]"
                        else:
                            prefix = "[[File:Red star2.png|13px|First of two red stars]][[File:Red star2.png|13px|Second of two red stars]]"
                    else:
                        prefix = "[[File:Red star2.png|13px|One red star]]"
                else:
                    prefix = ""
                dabpages[i] = (ttl, nmb, prefix)
            newlist = "\n".join(
                    f"# {p}[[{t}]]: {n:,} [[Special:Whatlinkshere/{t}|links]]"
                for (t, n, p) in dabpages)
            newtitle = f"Wikipedia:Disambiguation pages with links/{month} {year}"
            pagetext = f"""\
=={month} {year}==

===Progress===
From the [//dplbot.toolforge.org/disambig_links.php?limit={CONTESTSIZE}&offset=0 \
top {CONTESTSIZE} disambiguation pages] as of {todaydate}, out of a total \
of {fmtcount} links, approximately 0 have currently been fixed.

{{{{Progress bar|0|total={count}|width=60%}}}}
<br />
:'''PLEASE READ BEFORE MAKING ANY EDITS''': Kindly do not edit anything \
between the start of the line and the character string \
"''<nowiki>|links]]</nowiki>''".  You may insert any additional information \
after this point.  When an item is finished, please cut and paste it, \
''without revision'', to the end of the "Done" section. Thank you.

:The link counts reflect the number of links from main namespace articles as \
of the [//dplbot.toolforge.org/disambig_links.php tool report] \
date shown above. They are unlikely to be accurate at any later date; however, \
we keep the original count intact as a way of tracking our progress.  Please \
do not change the link totals!

'''Legend'''
: [[File:Red star2.png|13px|One red star]] Page was on list last month
: [[File:Red star2.png|13px|First of two red stars]][[File:Red star2.png|13px|Second of two red stars]] Page was on list both of past two months
: [[File:Red star2.png|13px|First of three red starts]][[File:Red star2.png|13px|Second of three red stars]][[File:Red star2.png|13px|Third of three red stars]] Page was on list all of past three months
: [[File:Rotating red star.gif|18px|Large rotating red star]] Page has been on list all of past four or more months

===To do===
<!-- SO THAT OTHER USERS CAN MAINTAIN THE PROGRESS BAR ABOVE, PLEASE DO NOT \
DELETE OR EDIT THE LINK COUNTS IN THE LIST BELOW.  If the count is seriously \
wrong, do not delete it but note your correction after the "Whatlinkshere" \
link. -->

{newlist}

===Done===
<div style="background:#ffe4e1;">
:'''Please add new entries to the bottom'''. It makes it easier to see how \
we are progressing.
<!--Striking through is ''not'' necessary-->

"""
            p = pywikibot.Page(s, newtitle)
            p.put(pagetext, "Creating new monthly list", minorEdit=False)

if __name__ == "__main__":
    DailyDisambigWriter().run()
