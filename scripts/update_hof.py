#! /usr/bin/python3

"Update the Hall of Fame page"

import threading
import pywikibot

class HallOfFameThread(threading.Thread):
    def __init__(self, hof_path):
        threading.Thread.__init__(self)
        self.site = pywikibot.Site()
        self.path = hof_path
        self.page = pywikibot.Page(self.site,
                "Wikipedia:Disambiguation pages with links/Disambiguator Hall of Fame")

    def run(self):
        """Read data from file and write to wiki"""
        with self.path.open("r") as src:
            self.page.text = src.read()
            self.page.save(summary="Update Dab Challenge Hall of Fame")
