<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$is_error = false;

$fp = fopen("status/uu_results.php", "w");

fputs($fp, "<?PHP\n\n");

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
if (! $mysql) {
  log_error(date("F j G:i", time()), "uncat_update.php", "toolsdb connect", mysqli_connect_error());
  fputs($fp, "\n$"."dab_error = \"".mysqli_connect_error()."\";\n\n?>\n");
  fclose($fp);
  $is_error = true;
}

$enwiki = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

if (! $enwiki) {
  log_error(date("F j G:i", time()), "uncat_update.php", "enwiki_p connect", mysqli_connect_error());
  fputs($fp, "\n$"."dab_error = \"".mysqli_connect_error()."\";\n\n?>\n");
  fclose($fp);
  $is_error = true;
}

if (! $is_error) {

  $time_begin = time();
  $begin_run_wiki = date("YmdHis", $time_begin);
  $begin_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_begin));

  fputs($fp, "$"."uu_begin_run = $time_begin;\n" );
  fputs($fp, "$"."uu_begin_run_wiki = \"$begin_run_wiki\";\n" );
  fputs($fp, "$"."uu_begin_run_str = \"$begin_run_str\";\n\n" );

  $uu_count1 = get_uc_count($mysql, $fp);

  run_query($mysql, "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
  uu_build();
//  run_query("CALL uu1_build()", $fp);

  # Check uncategorized_articles again. If its count has changed, that can only mean a major update has completed during execution.
  $uu_count2 = get_uc_count($mysql, $fp);

  if ($uu_count1 != $uu_count2) {
    if (!$is_error) {
      log_error(date("F j G:i", time()), "uncat_update.php", "edit conflict", "$uu_count1, then $uu_count2");
      fputs($fp, "\n$"."uu_error = \"Edit conflict: Before, uncategorized_articles had $uu_count1 rows. Now it has $uu_count2.  Aborting update.\";\n\n?>\n");
      fclose($fp);
      $is_error = true;
    }
  }
  else {
    uu_rename();
//    run_query("CALL uu2_rename()", $fp);
  }

  uu_cleanup();
//  run_query("CALL uu_cleanup()", $fp);

  if (!$is_error) {
    $time_finish = time();

    $finish_run_wiki = date("YmdHis", $time_finish);
    $finish_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_finish));
    $total_time_str = convert_time($time_finish - $time_begin);

    fputs($fp, "\n$"."uu_finish_run = $time_finish;\n" );
    fputs($fp, "$"."uu_finish_run_wiki = \"$finish_run_wiki\";\n" );
    fputs($fp, "$"."uu_finish_run_str = \"$finish_run_str\";\n\n" );

    fputs($fp, "$"."uu_total_time_str = \"$total_time_str\";\n\n" );

    mysqli_close($mysql);

    fputs($fp, "\n?>\n");
    fclose($fp);

    if (file_exists("status/uu_last_good_run.php"))
      unlink("status/uu_last_good_run.php");

    rename("status/uu_results.php", "status/uu_last_good_run.php");
  }
}
else {
  log_error(date("F j G:i", time()), "uncat_update.php", "mysql connect", mysqli_connect_error());
  fputs($fp, "\n$"."uu_error = \"".mysqli_connect_error()."\";\n\n?>\n");
  fclose($fp);
}


function uu_build ( ) {
  global $mysql, $enwiki, $fp, $is_error;
  
  $begin = time();
  
  submit_query($mysql, "
    CREATE TABLE IF NOT EXISTS deletion_targets (
      del_id int unsigned NOT NULL default '0',
      PRIMARY KEY pk_del (del_id)
    ) ENGINE=InnoDB
  ");
  
  # retrieve untagged uncategorized articles
  
  $all_uncategorized_articles = [];
  $result = submit_query($mysql, "
    SELECT uc_id
      FROM uncategorized_articles
     WHERE is_tagged = 0
       AND NOT EXISTS
               (
                SELECT *
                  FROM deletion_targets
                 WHERE uc_id = del_id
               )
     LIMIT 5000
  ");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($result)) {
    # validate article and retrieve title
    $uc_id = (int) $row['uc_id'];
    $sub_res = submit_query($enwiki, "
      SELECT page_title
        FROM page
       WHERE page_id = $uc_id
         AND page_namespace = 0
         AND page_is_redirect = 0
    ");
    if ($is_error) return;
    if (mysqli_num_rows($sub_res) == 1) {
      $data = mysqli_fetch_assoc($sub_res);
      if ($data['page_title'] != "Main_Page")
        $all_uncategorized_articles[$uc_id] = $data['page_title'];
    }
    mysqli_free_result($sub_res);
  }
  mysqli_free_result($result);
  
  # check all links contained on uncategorized articles
  
  $uu_cl = [];
  foreach ($all_uncategorized_articles as $uc_id => $uc_title) {
    $result = submit_query($enwiki, "
      SELECT cl_from, page_id
        FROM categorylinks, page
       WHERE cl_from = $uc_id
         AND cl_to = page_title
         AND page_namespace = 14
         AND page_is_redirect = 0
    ");
    if ($is_error) return;
    while ($row = mysqli_fetch_assoc($result)) {
      $uu_cl[] = array(
        "cl_from" => (int) $row['cl_from'],
        "cl_to" => (int) $row['page_id']
      );
    }
    mysqli_free_result($result);
  }
  
  # remove hidden categories from uu_cl
  $hidden_categories = [];
  $result = submit_query($enwiki, "
    SELECT page_id
      FROM page_props, page
     WHERE page_id = pp_page
       AND pp_propname = 'hiddencat'
       AND page_namespace = 14
       AND page_is_redirect = 0
       AND page_title != 'All_disambiguation_pages'
  ");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($result)) {
    $hidden_categories[ (int) $row['page_id'] ] = true;
  }
  mysqli_free_result($result);
  foreach ($uu_cl as $key => $link) {
    if (isset($hidden_categories[$link['cl_to']]))
      unset($uu_cl[$key]);
  }
  
  #remove stub categories from uu_cl
  $stub_categories = [];
  $result = submit_query($enwiki, "
    SELECT cl_from
      FROM categorylinks, page
     WHERE cl_to = 'Stub_categories'
       AND page_id = cl_from
       AND page_namespace = 14
       AND page_is_redirect = 0
  ");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($result)) {
    $stub_categories[ (int) $row['cl_from'] ] = true;
  }
  mysqli_free_result($result);
  foreach ($uu_cl as $key => $link) {
    if (isset($stub_categories[$link['cl_to']]))
      unset($uu_cl[$key]);
  }
  
  $already_tagged = [];
  # get articles already tagged as uncategorized
  $res = submit_query($enwiki, "
         SELECT cl_from 
           FROM categorylinks
          WHERE cl_to = 'All_uncategorized_pages'
  ");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($res)) {
    $already_tagged[(int) $row['cl_from']] = true;
  }
  mysqli_free_result($res);

  # or containing a related template
  $res = submit_query($enwiki, "
         SELECT tl_from 
           FROM templatelinks, linktarget
          WHERE tl_target_id = lt_id
            AND lt_namespace = 10
            AND (
                 lt_title = 'Uncategorized_stub' OR
                 lt_title = 'Improve_categories'
                )  
  ");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($res)) {
    $already_tagged[(int) $row['tl_from']] = true;
  }
  mysqli_free_result($res);
  
  $cl_from_list = [];
  foreach ($uu_cl as $link)
    $cl_from_list[$link['cl_from']] = true;

  submit_query($mysql, "DROP TABLE IF EXISTS t_u_micro_list");

  submit_query($mysql, "
    CREATE TABLE t_u_micro_list (
      uc_id int unsigned NOT NULL default '0',
      uc_title varchar(255) binary NOT NULL default '',
      is_tagged tinyint unsigned NOT NULL default '0',
      PRIMARY KEY pk_uc (uc_id)
    ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC
  ");
  if ($is_error) return;
  
  # populate table
  foreach ($all_uncategorized_articles as $uc_id => $uc_title) {
    if (! isset($cl_from_list[$uc_id])) {
      $tagged = (isset($already_tagged[$uc_id]) ? 1 : 0);
      $utitle = mysqli_real_escape_string($mysql, $uc_title);
      submit_query($mysql, "
        INSERT IGNORE INTO t_u_micro_list
               (uc_id, uc_title, is_tagged)
        VALUES ($uc_id, '$utitle', $tagged)
      ");
      if ($is_error) return;
    }
  }
  fputs($fp, "$"."uu_proc_results['uu_build'] = \"".convert_time(time() - $begin)."\";\n");
}


function uu_rename ( ) {
  global $mysql, $fp, $is_error;
  
  $begin = time();
  submit_query($mysql, "DROP TABLE IF EXISTS u_micro_list");
  submit_query($mysql, "RENAME TABLE t_u_micro_list TO u_micro_list");
  if ($is_error) return;
  fputs($fp, "$"."uu_proc_results['uu_rename'] = \"".convert_time(time() - $begin)."\";\n");
}


function uu_cleanup ( ) {
  global $mysql;
  mysqli_query($mysql, "DROP TABLE IF EXISTS t_u_micro_list");
}


function submit_query ($server, $sql) {
  global $mysql, $fp, $is_error;

  $begin = time();
  $result = mysqli_query($server, $sql);

  if ($result == false) {
    log_error(date("F j G:i", $begin), "uncat_update.php", $sql, mysqli_error($server));
    fputs($fp, "\n$"."uu_error = \"$sql: ".mysqli_error($server)."\";\n");
    fclose($fp);
	  $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
    uu_cleanup();
    $is_error = true;
  }
  return $result;
}


function run_query ($server, $sql) {
  global $fp, $is_error;

  if (!$is_error) {
	  $begin = time();

    $result = submit_query($server, $sql);

    if ($result == false || $is_error) {
      $is_error = true;
    }
    else {
      fputs($fp, "$"."uu_proc_results['$sql'] = \"".convert_time(time() - $begin)."\";\n");
    }
  }
}


function get_uc_count ($mysql, $fp) {
  global $is_error;

  if (!$is_error) {
    $sql = "SELECT count(*) AS count
              FROM uncategorized_articles";

    $result = mysqli_query($mysql, $sql);

    if ($result) {
      $row = mysqli_fetch_assoc($result);
      $count = $row['count'];

      settype($count, "integer");

      return $count;
    }
    else {
      mysqli_query($mysql, "CALL uu_cleanup()");
      log_error(date("F j G:i", time()), "uncat_update.php", $sql, mysqli_error($mysql));
      fputs($fp, "\n$"."uu_error = \"$sql: ".mysqli_error($mysql)."\";\n");
      fclose($fp);
      $is_error = true;
      return 0;
    }
  }
  return 0;
}

?>
