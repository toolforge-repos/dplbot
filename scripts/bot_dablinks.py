#! /usr/bin/python3
"Manage templates on articles that contain multiple links to dab pages."

import datetime
import locale
import pathlib
import re
import MySQLdb
import pywikibot

locale.setlocale(locale.LC_ALL, "")
ADD_THRESHOLD = 7
RMV_THRESHOLD = 4

class DablinksNotifier:
    def __init__(self):
        self.logging_enabled = True
        if self.logging_enabled:
            p = pathlib.Path("/data/project/dplbot", "dplbot", "logs", "dablinks_results.log")
            self.logfile = p.open("w", encoding="utf8")
        else:
            self.logfile = None
        toolsdb = MySQLdb.connect(db='s51290__dpl_p',
                          host='tools.db.svc.wikimedia.cloud',
                          read_default_file='/data/project/dplbot/.my.cnf',
                          charset="utf8")
        self.tcursor = toolsdb.cursor(cursorclass=MySQLdb.cursors.DictCursor)
        wikidb = MySQLdb.connect(db='enwiki_p',
                         host='enwiki.analytics.db.svc.wikimedia.cloud',
                         read_default_file='/data/project/dplbot/.my.cnf',
                         charset="utf8")
        self.wcursor = wikidb.cursor(cursorclass=MySQLdb.cursors.DictCursor)

    def log(self, message):
        if self.logging_enabled:
            self.logfile.write(message)

    def unique_dab_count(self, target_id):
        "Return number of non-intentional links to dab pages on target_id"
        num_unique_dabs = 0
        ct_res = self.wcursor.execute(f"""
              SELECT lt_title AS pl_title,
                     lt_title AS t_title,
                     0 AS is_redirect
                FROM pagelinks, linktarget, page
               WHERE lt_namespace = 0
                 AND pl_from = {target_id}
                 AND pl_target_id = lt_id
                 AND lt_title NOT LIKE '%_(disambiguation)'
                 AND lt_title NOT LIKE '%_(number)'
                 AND lt_title = page_title
                 AND page_namespace = 0
                 AND page_is_redirect = 0
                 AND EXISTS (
                             SELECT *
                               FROM page_props
                              WHERE page_id = pp_page
                                AND pp_propname = 'disambiguation'
                            )
         UNION
              SELECT lt_title AS pl_title,
                     rd_title AS t_title,
                     1 AS is_redirect
                FROM pagelinks, linktarget, page a, redirect
               WHERE lt_namespace = 0
                 AND pl_target_id = lt_id
                 AND pl_from = {target_id}
                 AND lt_title NOT LIKE '%_(disambiguation)'
                 AND lt_title NOT LIKE '%_(number)'
                 AND lt_title = a.page_title
                 AND a.page_namespace = 0
                 AND a.page_is_redirect = 1
                 AND a.page_id = rd_from
                 AND rd_namespace = 0
                 AND EXISTS
                       (
                        SELECT *
                          FROM page_props, page b
                         WHERE rd_title = b.page_title
                           AND b.page_namespace = 0
                           AND b.page_is_redirect = 0
                           AND b.page_id = pp_page
                           AND pp_propname = 'disambiguation'
                       )
        """.encode("utf-8"))
        if ct_res:
            count_array = set()  # only count unique titles
            for ct_row in self.wcursor.fetchall():
                count_array.add(ct_row['t_title'])
            num_unique_dabs = len(count_array)
            self.log(f"  {num_unique_dabs = }\n")
        else:
            self.log("  no dab links\n")
        return num_unique_dabs

    def run(self):
        try:
            s = pywikibot.Site()
            # get pages in to_dab_link_count that have at least ADD_THRESHOLD links to dab pages
            self.tcursor.execute(f"""
                    SELECT lc_id
                      FROM to_dab_link_count
                     WHERE lc_amnt >= {ADD_THRESHOLD}
            """.encode("utf-8"))
            idlist = ""
            for row in self.tcursor.fetchall():
                if idlist != "":
                    idlist += ","
                idlist += str(row['lc_id'])
            # but do not transclude Template:Dablinks
            self.wcursor.execute(f"""
                    SELECT page_id, page_title
                      FROM page
                     WHERE page_id IN ({idlist})
                       AND page_namespace = 0
                       AND NOT EXISTS (
                                       SELECT *
                                         FROM templatelinks, linktarget
                                        WHERE tl_target_id = lt_id
                                          AND lt_namespace = 10
                                          AND lt_title = 'Dablinks'
                                          AND tl_from = page_id
                                      )
            """.encode("utf-8"))
            for row in self.wcursor.fetchall():
                # now we have the title of an article that contains many dablinks and needs to be tagged
                title = row['page_title'].decode("utf8").replace('_', ' ')
                try:
                    p = pywikibot.Page(s, title)
                except:
                    print(f"Title error: {title}({type(title)!s})")
                    continue
                self.wcursor.execute(b"SELECT lag FROM heartbeat_p.heartbeat WHERE shard='s1'")
                lag = self.wcursor.fetchone()['lag']
                started = datetime.datetime.now() - datetime.timedelta(seconds=float(lag))
                self.log(f"Add template: {title} id: {row['page_id']}\n")
                if self.unique_dab_count(row['page_id']) < ADD_THRESHOLD:
                    self.log("  -- aborted\n")
                    continue
                try:
                    last_touched = p.latest_revision.timestamp
                    if last_touched > started:
                        self.log(f"  Page {title} has been edited recently, and replag doesn't allow us to see the changes:\n")
                        self.log(f"   (last page edit {last_touched!s}; replica current to {started!s}\n")
                        continue
                except pywikibot.exceptions.NoPageError:
                    self.log(f"  Page {title} does not exist\n")
                    continue
                if not p.botMayEdit():
                    self.log(f"  Blocked by {{{{nobots}}}} template on {title}\n")
                    continue
                content = "{{dablinks|date=" + datetime.date.today().strftime("%B %Y") + "}}\n" + p.text
                self.log(f"  Submitting content for {title}\n")
                p.text = content
                p.save(summary=f"Robot: adding dablinks template; {ADD_THRESHOLD}" +
                            "or more disambig links (see the [[Template:Dablinks/FAQ|FAQ]])",
                       minor=True)

            # locate all articles that contain the {{Dablinks}} template, and see whether to remove items
            self.wcursor.execute(b"""
                    SELECT tl_from, page_title
                      FROM templatelinks, linktarget, page
                     WHERE tl_target_id = lt_id
                       AND lt_namespace = 10
                       AND lt_title = 'Dablinks'
                       AND tl_from = page_id
                       AND page_namespace = 0
            """)
            pages = {}
            for row in self.wcursor.fetchall():
                pages[row['tl_from']] = row['page_title']
            for page_id in pages:
                result = self.tcursor.execute(f"""
                        SELECT lc_amnt
                          FROM to_dab_link_count
                         WHERE lc_id = {page_id}
                """.encode("utf-8"))
                # remove tag if (1) page has < RMV_THRESHOLD dablinks, or (2) has no dablinks at all
                if result > 0:
                    amnt = self.tcursor.fetchone()['lc_amnt']
                if result == 0 or amnt < RMV_THRESHOLD:
                    self.wcursor.execute(b"SELECT lag FROM heartbeat_p.heartbeat WHERE shard='s1'")
                    lag = self.wcursor.fetchone()['lag']
                    started = datetime.datetime.now() - datetime.timedelta(seconds=float(lag))
                    page_title = pages[page_id].decode("utf8").replace('_', ' ')
                    p = pywikibot.Page(s, page_title)
                    self.log(f"Remove template: {page_title} id: {page_id}\n")
                    if self.unique_dab_count(page_id) >= RMV_THRESHOLD:
                        self.log("  -- aborted\n")
                        continue
                    try:
                        last_touched = p.latest_revision.timestamp
                    except pywikibot.exceptions.NoPageError:
                        self.log(f"  Page {page_title} does not exist\n")
                        continue
                    if last_touched > started:
                        self.log(f"  Page {page_title} has been edited recently, and replag doesn't allow us to see the changes:\n")
                        self.log(f"   (last page edit {last_touched!s}; replica current to {started!s}\n")
                        continue
                    if not p.botMayEdit():
                        self.log(f"  Blocked by {{{{nobots}}}} template on {page_title}\n")
                        continue
                    pattern = re.compile(r"\{\{\s*[Dd]ablinks\s*(\|.*?)?\}\}\s*")
                    content = pattern.sub('', p.text)
                    self.log(f"  Submitting content for {page_title}\n")
                    p.text = content
                    p.save(summary="Robot: removing dablinks template; fewer than " +
                             f"{RMV_THRESHOLD} disambig links (see the [[Template:Dablinks/FAQ|FAQ]])",
                           minor=True)
        finally:
            self.wcursor.close()
            self.tcursor.close()
            if self.logging_enabled:
                self.logfile.close()
