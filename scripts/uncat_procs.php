<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$is_error = false;

$fp = fopen("$HOME_DIR/status/uc_results.php", "w");

fputs($fp, "<?PHP\n\n");

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
if (! $mysql) {
  log_error(date("F j G:i", time()), "uncat_procs.php", "toolsdb connect", mysqli_connect_error());
  fputs($fp, "\n$"."dab_error = \"".mysqli_connect_error()."\";\n\n?>\n");
  fclose($fp);
  $is_error = true;
}

$enwiki = get_db_con("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

if (! $enwiki) {
  log_error(date("F j G:i", time()), "uncat_procs.php", "enwiki_p connect", mysqli_connect_error());
  fputs($fp, "\n$"."dab_error = \"".mysqli_connect_error()."\";\n\n?>\n");
  fclose($fp);
  $is_error = true;
}

if (! $is_error) {

  $time_begin = time();
  $begin_run_wiki = date("YmdHis", $time_begin);
  $begin_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_begin));

  fputs($fp, "$"."uc_begin_run = $time_begin;\n" );
  fputs($fp, "$"."uc_begin_run_wiki = \"$begin_run_wiki\";\n" );
  fputs($fp, "$"."uc_begin_run_str = \"$begin_run_str\";\n\n" );

  run_query($mysql, "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
  run_query($mysql, "SET @@max_heap_table_size=2147483648");
  run_query($mysql, "SET @@tmp_table_size=2147483648");
  uc_main();
  
  if (!$is_error) {
    uc_cleanup();
    $time_finish = time();

    $finish_run_wiki = date("YmdHis", $time_finish);
    $finish_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_finish));
    $total_time_str = convert_time($time_finish - $time_begin);

    fputs($fp, "\n$"."uc_finish_run = $time_finish;\n" );
    fputs($fp, "$"."uc_finish_run_wiki = \"$finish_run_wiki\";\n" );
    fputs($fp, "$"."uc_finish_run_str = \"$finish_run_str\";\n\n" );

    fputs($fp, "$"."uc_total_time_str = \"$total_time_str\";\n\n" );

    mysqli_close($mysql);

    fputs($fp, "\n?>\n");
    fclose($fp);

    if (file_exists("$HOME_DIR/status/uc_last_good_run.php"))
      unlink("$HOME_DIR/status/uc_last_good_run.php");

    rename("$HOME_DIR/status/uc_results.php", "$HOME_DIR/status/uc_last_good_run.php");
  }
}
else {
  log_error(date("F j G:i", time()), "uc_procs.php", "mysql connect", mysqli_connect_error());
  fputs($fp, "\n$"."uc_error = \"".mysqli_connect_error()."\";\n\n?>\n");
  fclose($fp);
}


function uc_main ( ) {
  global $mysql, $enwiki, $fp, $is_error;

  $begin = time();

  submit_query($mysql, "DROP TABLE IF EXISTS u_temp_uncategorized_articles");

  submit_query($mysql, "
    CREATE TABLE u_temp_uncategorized_articles (
      uc_id int unsigned NOT NULL default '0',
      uc_title varchar(255) binary NOT NULL default '',
      is_tagged tinyint unsigned NOT NULL default '0',
      PRIMARY KEY pk_uc (uc_id)
    ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC
  ");

  submit_query($mysql, "DROP TABLE IF EXISTS u_temp_categorized_articles");

  submit_query($mysql, "
    CREATE TABLE u_temp_categorized_articles (
      c_id int unsigned NOT NULL default '0',
      c_title varchar(255) binary NOT NULL default '',
      PRIMARY KEY pk_c (c_id)
    ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC
  ");

  submit_query($mysql, "DROP TABLE IF EXISTS temp_deletion_targets");

  submit_query($mysql, "
    CREATE TABLE temp_deletion_targets (
      del_id int unsigned NOT NULL default '0',
      PRIMARY KEY pk_del (del_id)
    ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC
  ");
  
  $u_categories = [];  // c_id => c_title : 
                       // all category description pages, 
                       // except stubs and (most) hiddencats
  
  $res = submit_query($enwiki, "
    SELECT page_id, page_title
      FROM page
     WHERE page_namespace = 14
       AND page_is_redirect = 0
       AND ( NOT EXISTS (
                SELECT * from page_props
                 WHERE pp_page = page_id
                   AND pp_propname = 'hiddencat'
             ) OR (
                page_title IN (
                  'All_disambiguation_pages',
                  'Wikipedia_soft_redirects', 
                  'Redirects_to_Wikisource',
                  'Redirects_to_Wikiquote',
                  'Redirects_to_Wiktionary'
                )
             )
           )
       AND NOT EXISTS (
               SELECT * FROM categorylinks
                WHERE cl_from = page_id
                  AND cl_to = 'Stub_categories'
           )
  ");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($res)) {
    $u_categories[(int) $row['page_id']] = $row['page_title'];
  }
  mysqli_free_result($res);

  # get all non-redirect articles  
  $u_psub = [];
  $res = submit_query($enwiki, "
    SELECT page_id, page_title
      FROM page
     WHERE page_namespace = 0
       AND page_is_redirect = 0
  ");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($res)) {
    $u_psub[(int) $row['page_id']] = $row['page_title'];
  }
  mysqli_free_result($res);

  fputs($fp, "$"."uc_proc_results['uc_get_categories'] = \"".convert_time(time() - $begin)."\";\n");

  # get all links from articles to categories, then look for those in $u_categories
  $begin = time();

  $cat_index = array_flip($u_categories);  
  $u_cat_links = [];
  $res = mysqli_query($enwiki, "SELECT cl_from, cl_to FROM categorylinks", MYSQLI_USE_RESULT);
  if (! $res) {
    $is_error = true;
    return;
  }
  while ($row = mysqli_fetch_assoc($res)) {
    $cl_from = (int) $row['cl_from'];
    if ( isset($u_psub[$cl_from]) && isset($cat_index[$row['cl_to']]) ) {
      $u_cat_links[$cl_from] = true; // article contains a category, we don't care which one
    }
  }
  mysqli_free_result($res);

  fputs($fp, "$"."uc_proc_results['uc_create_cl'] = \"".convert_time(time() - $begin)."\";\n");

  $begin = time();

  $u_already_tagged = [];
  # get articles already tagged as uncategorized
  $res = submit_query($enwiki, "
         SELECT cl_from 
           FROM categorylinks
          WHERE cl_to = 'All_uncategorized_pages'
  ");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($res)) {
    $u_already_tagged[(int) $row['cl_from']] = true;
  }
  mysqli_free_result($res);

  # or containing a related template
  $res = submit_query($enwiki, "
         SELECT tl_from 
           FROM templatelinks, linktarget
          WHERE lt_id = tl_target_id
            AND lt_namespace = 10
            AND (
                 lt_title = 'Uncategorized_stub' OR
                 lt_title = 'Improve_categories'
                )  
  ");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($res)) {
    $u_already_tagged[(int) $row['tl_from']] = true;
  }
  mysqli_free_result($res);
  
  # now save all article ids that are uncategorized

  $uncategorized = [];
  $batches = [];
  $batch = "";
  $count = 0;
  foreach ($u_psub as $ps_id => $ps_title) {
    if (! isset($u_cat_links[$ps_id])) {
      $ptitle = mysqli_real_escape_string($mysql, $ps_title);
      $tag = (isset($u_already_tagged[$ps_id]) ? 1 : 0);
      if ($count > 0)
        $batch .= ",";
      $batch .= "($ps_id, '$ptitle', $tag)";
      $count += 1;
      if ($count >= 128) {
        $batches[] = $batch;
        $batch = "";
        $count = 0;
      }
      $uncategorized[$ps_id] = $ps_title;
    }
  }
  if ($count > 0)
    $batches[] = $batch;

  mysqli_close($mysql);
  $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
  
  foreach ($batches as $bat) {
    submit_query($mysql, "
      INSERT INTO u_temp_uncategorized_articles
                  (uc_id, uc_title, is_tagged)
           VALUES $bat
    ");
    if ($is_error) return;
  }
  mysqli_commit($mysql);
  
  # redo already_tagged without the {{Improve categories}} template

  $u_already_tagged = [];
  # get articles already tagged as uncategorized
  $res = submit_query($enwiki, "
         SELECT cl_from 
           FROM categorylinks
          WHERE cl_to = 'All_uncategorized_pages'
  ");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($res)) {
    $u_already_tagged[(int) $row['cl_from']] = true;
  }
  mysqli_free_result($res);

  # or containing a related template
  $res = submit_query($enwiki, "
         SELECT tl_from 
           FROM templatelinks, linktarget
          WHERE tl_target_id = lt_id
            AND lt_namespace = 10
            AND lt_title = 'Uncategorized_stub'
  ");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($res)) {
    $u_already_tagged[(int) $row['tl_from']] = true;
  }
  mysqli_free_result($res);

  # save the list of articles in tagged categories that were not
  # in the uncategorized list (i.e, incorrectly tagged as uncategorized)
  
  $batches = [];
  $batch = "";
  $count = 0;
  foreach ($u_psub as $ps_id => $ps_title) {
    if ( isset($u_already_tagged[$ps_id]) && ! isset($uncategorized[$ps_id]) ) {
      $ptitle = mysqli_real_escape_string($mysql, $ps_title);
      if ($count > 0)
        $batch .= ",";
      $batch .= "($ps_id, '$ptitle')";
      $count += 1;
      if ($count >= 128) {
        $batches[] = $batch;
        $batch = "";
        $count = 0;
      }
    }
  }
  if ($count > 0)
    $batches[] = $batch;
  
  foreach ($batches as $bat) {
    submit_query($mysql,
      "INSERT IGNORE INTO u_temp_categorized_articles
                     (c_id, c_title)
              VALUES $bat"
    );
    if ($is_error) return;
  }
  mysqli_commit($mysql);
  
  # Create a list of deletion targets so orphan pages can
  # filter these out of the lists.  There's no need to bother
  # working on these articles if they may be deleted soon; if
  # they pass we can work on them then.

  $res = submit_query($enwiki, "
           SELECT cl_from
           FROM categorylinks
          WHERE cl_to = 'All_articles_proposed_for_deletion'
             OR cl_to = 'Articles_for_deletion'
             OR cl_to = 'Redirects_for_deletion'
             OR cl_to = 'Candidates_for_speedy_deletion'
             OR cl_to = 'Articles_tagged_for_copyright_problems'
  ");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($res)) {
    $del_id = (int) $row['cl_from'];
    submit_query($mysql, "
        INSERT IGNORE INTO temp_deletion_targets (del_id)
               VALUES ($del_id)
    ");
  }
  mysqli_commit($mysql);
  mysqli_free_result($res);
  
  fputs($fp, "$"."uc_proc_results['uc_save_results'] = \"".convert_time(time() - $begin)."\";\n");

  $begin = time();
  # rename tables

  submit_query($mysql, "DROP TABLE IF EXISTS uncategorized_articles");
  submit_query($mysql, "RENAME TABLE u_temp_uncategorized_articles TO uncategorized_articles");

  submit_query($mysql, "DROP TABLE IF EXISTS categorized_articles");
  submit_query($mysql, "RENAME TABLE u_temp_categorized_articles TO categorized_articles");

  submit_query($mysql, "DROP TABLE IF EXISTS deletion_targets");
  submit_query($mysql, "RENAME TABLE temp_deletion_targets TO deletion_targets");

  if ($is_error) return;
  fputs($fp, "$"."uc_proc_results['uc_rename_tables'] = \"".convert_time(time() - $begin)."\";\n");

}


function uc_cleanup ( ) {
  
  global $mysql;
  
  mysqli_query($mysql, "DROP TABLE IF EXISTS temp_deletion_targets");
  mysqli_query($mysql, "DROP TABLE IF EXISTS u_temp_categorized_articles");
  mysqli_query($mysql, "DROP TABLE IF EXISTS u_temp_uncategorized_articles");

}


function submit_query ($server, $sql) {
  global $fp, $is_error;

  $begin = time();
  $result = mysqli_query($server, $sql);

  if ($result == false) {
    log_error(date("F j G:i", $begin), "uc_procs.php", $sql, mysqli_error($server));
    fputs($fp, "\n$"."uc_error = \"$sql: ".mysqli_error($server)."\";\n");
    fclose($fp);
	  $mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
    uc_cleanup();
    $is_error = true;
  }
  return $result;
}


function run_query ($server, $sql, $nick = "") {
  global $mysql, $is_error, $fp;

  if ($nick == "")
    $nick = $sql;

  if (!$is_error) {
    $begin = time();

    $result = submit_query($server, $sql);

    if ($result == false || $is_error) {
      $is_error = true;
    }
    else {
      fputs($fp, "$"."uc_proc_results['$nick'] = \"".convert_time(time() - $begin)."\";\n");
    }
  }
  return $result;
}

?>