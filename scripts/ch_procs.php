<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$config="$HOME_DIR/.dplcredentials.cfg";
require ("$HOME_DIR/scripts/SxWiki.php");

$sx=new SxWiki;
$sx->configFile=$config;
$sx->autoLogin = false;
$sx->autoDie = false;

/*if (!$sx->login()) {
  print "Login failed.\n";
  exit;
}*/


$logging_enabled = true;

$is_error = false;
$is_store_error = false;

$fp = fopen("$HOME_DIR/status/ch_results.php", "w");

if ($logging_enabled) $lp = fopen("$HOME_DIR/logs/ch_results.log", "w");
else $lp = "";

fputs($fp, "<?PHP\n\n");

$time_begin = time();
$begin_run_wiki = date("YmdHis", $time_begin);
$begin_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_begin));

fputs($fp, "$"."ch_begin_run = $time_begin;\n" );
fputs($fp, "$"."ch_begin_run_wiki = \"$begin_run_wiki\";\n" );
fputs($fp, "$"."ch_begin_run_str = \"$begin_run_str\";\n\n" );

$tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
submit_and_log_query($tooldb,
  "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
submit_and_log_query($tooldb, "SET @@max_heap_table_size=2147483648");
submit_and_log_query($tooldb, "SET @@tmp_table_size=2147483648");
mysqli_close($tooldb);

ch_main();
create_results_list($sx, $fp, $lp);
ch_finalize_results();

$begin = time();
ch_cleanup();

if (! $is_error) {
  $duration = time() - $begin;
  fputs($fp,
    "$"."ch_proc_results['ch_cleanup'] = \"".convert_time($duration)."\";\n");

  $time_finish = time();

  $finish_run_wiki = date("YmdHis", $time_finish);
  $finish_run_str = str_replace( '_', ' ', date("F j, Y, G:i e", $time_finish));
  $total_time_str = convert_time($time_finish - $time_begin);

  fputs($fp, "\n$"."ch_finish_run = $time_finish;\n" );
  fputs($fp, "$"."ch_finish_run_wiki = \"$finish_run_wiki\";\n" );
  fputs($fp, "$"."ch_finish_run_str = \"$finish_run_str\";\n\n" );
  fputs($fp, "$"."ch_total_time_str = \"$total_time_str\";\n\n" );
  fputs($fp, "\n?>\n");

  fclose($fp);

  if (file_exists("$HOME_DIR/status/ch_last_good_run.php"))
    unlink("$HOME_DIR/status/ch_last_good_run.php");

  rename("$HOME_DIR/status/ch_results.php", "$HOME_DIR/status/ch_last_good_run.php");
}


function ch_main ( ) {

  global $wikidb, $fp, $lp, $is_error, $logging_enabled;

  $begin = time();
  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  submit_query($tooldb,
    "CREATE TABLE IF NOT EXISTS ch_no_fixer (
       nf_article_id int unsigned NOT NULL default 0,
       nf_template_id int unsigned NOT NULL default 0,
       nf_dab_id int unsigned NOT NULL default 0,
       UNIQUE INDEX (nf_article_id, nf_dab_id)
     ) ENGINE=InnoDB"
  );

  # retrieve list of contest dabs

  $ch_articles = [];  // id => title
  $redirects = [];  // id => target id

  $sql = "SELECT mo_id, mo_title FROM mo_articles";
  $result = submit_query($tooldb, $sql);
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($result)) {
    $id = (int) $row['mo_id'];
    $ch_articles[$id] = $row['mo_title'];
    $redirects[$id] = $id;  // non-redirects map to themselves
  }
  mysqli_free_result($result);
  if ($logging_enabled)
    fputs($lp, count($ch_articles)." dab pages in monthly contest.\n");
  mysqli_close($tooldb);

  # get all redirects to contest dabs
  $redir_titles = []; // id => title

  $batches = [];
  $batch = "";
  $count = 0;
  $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

  foreach ($ch_articles as $art) {
    if ($count > 0)
      $batch .= ",";
    $atitle = mysqli_real_escape_string($wikidb, $art);
    $batch .= "'$atitle'";
    $count += 1;
    if ($count >= 128) {
      $batches[] = "($batch)";
      $batch = "";
      $count = 0;
    }
  }

  $dabindex = array_flip($ch_articles);

  foreach ($batches as $bat) {
    $sql = "SELECT rd_from, page_title, rd_title
              FROM redirect, page
             WHERE rd_namespace = 0
               AND rd_from != 0
               AND rd_title IN $bat
               AND rd_from = page_id
               AND page_namespace = 0";
    $result = submit_query($wikidb, $sql);
    if ($is_error) return;
    while ($row = mysqli_fetch_assoc($result)) {
      if (endswith($row['page_title'], "_(disambiguation)")
          || endswith($row['page_title'], "_(number)"))
        continue;
      $id = (int) $row['rd_from'];
      $redirects[$id] = $dabindex[$row['rd_title']];
      $redir_titles[$id] = $row['page_title'];
    }
    mysqli_free_result($result);
  }
  if ($logging_enabled)
    fputs($lp, count($redir_titles)." non-intentional redirects to contest dabs.\n");

  $duration = time() - $begin;
  fputs($fp,
    "$"."ch_proc_results['ch_prepare'] = \"".convert_time($duration)."\";\n");

  # create ch_pl

  $begin = time();

  # retrieve all links from articles to contest pages
  foreach ($ch_articles as $c_id => $c_title)
    $redir_titles[$c_id] = $c_title;
  $dabindex = array_flip($redir_titles);
  $ch_pl = [];

  $batches = [];
  $batch = "";
  $count = 0;
  foreach ($redir_titles as $dab_title) {
    if ($count > 0)
      $batch .= ",";
    $batch .= "'".mysqli_real_escape_string($wikidb, $dab_title)."'";
    $count += 1;
    if ($count >= 128) {
      $batches[] = $batch;
      $batch = "";
      $count = 0;
    }
  }
  if ($count > 0)
    $batches[] = $batch;

  $linktitles = [];
  foreach ($batches as $bat) {
    $sql = "SELECT pl_from, lt_title AS pl_title, page_title
              FROM pagelinks, linktarget, page
             WHERE pl_from_namespace = 0
               AND pl_target_id = lt_id
               AND lt_namespace = 0
               AND lt_title IN ($bat)
               AND page_id = pl_from
               AND page_namespace = 0
               AND page_is_redirect = 0";

    $result = submit_query($wikidb, $sql);
    if ($is_error) return;
    while ($row = mysqli_fetch_assoc($result)) {
      $target_id = $dabindex[$row['pl_title']];
      $pl_from = (int) $row['pl_from'];
      $pl_to = $redirects[$target_id];
      # remove self-referential links
      if ($pl_from == $pl_to || $pl_from == $target_id)
        continue;
      # make sure source and target are unique
      $ch_pl[$pl_from.",".$pl_to] = array(
        'pl_from' => $pl_from,
        'pl_to' => $pl_to,
        'pl_r' => $target_id
      );
      $linktitles[$pl_from] = $row['page_title'];
    }
    mysqli_free_result($result);
  }
  if ($logging_enabled)
    fputs($lp, count($ch_pl)." links from articles to contest dabs.\n");

  $duration = time() - $begin;
  fputs($fp,
    "$"."ch_proc_results['ch_pl'] = \"".convert_time($duration)."\";\n");

  # validate monthly list
  $begin = time();
  foreach ($ch_articles as $id => $title) {
    $dtitle = mysqli_real_escape_string($wikidb, $title);
    $sql = "SELECT COUNT(*) AS c
              FROM page
             WHERE page_id = $id
               AND page_title = '$dtitle'
               AND page_is_redirect = 0
               AND page_namespace = 0
               AND EXISTS (SELECT *
                             FROM page_props
                            WHERE pp_page = page_id
                              AND pp_propname = 'disambiguation'
                          )";
    $result = submit_query($wikidb, $sql);
    if ($is_error) return;
    $row = mysqli_fetch_assoc($result);
    if ($row['c'] == 0) {
      unset($ch_articles[$id]);
    }
    mysqli_free_result($result);
  }
  mysqli_close($wikidb);
  fputs($lp, "Validated ".count($ch_articles)." dab pages on monthly list.\n");

  $monthly_ids = [];
  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  $result = submit_query($tooldb, "SELECT mo_id, mo_title FROM mo_articles");
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($result))
    $monthly_ids[(int) $row['mo_id']] = $row['mo_title'];
  mysqli_free_result($result);

  # save monthly_list_part table

  submit_query($tooldb, "DROP TABLE IF EXISTS monthly_list_part");

  submit_query($tooldb,
    "CREATE TABLE monthly_list_part (
       article_id int unsigned NOT NULL default '0',
       article_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       redirect_id int unsigned NOT NULL default '0',
       redirect_title varchar(255) binary NOT NULL default ''
     ) ENGINE=InnoDB"
  );

  $batches = [];
  $batch = "";
  $count = 0;
  fputs($lp, "ch_pl contains ".count($ch_pl)." links.\n");

  foreach ($ch_pl as $link) {
    if (! isset($ch_articles[$link['pl_to']])) // target no longer eligible for contest
      continue;
    if (! isset($monthly_ids[$link['pl_to']])) // target not on monthly list
      continue;
    $ftitle = mysqli_real_escape_string($tooldb, $linktitles[$link['pl_from']]);
    $ttitle = mysqli_real_escape_string($tooldb, $ch_articles[$link['pl_to']]);
    $rtitle = mysqli_real_escape_string($tooldb, $redir_titles[$link['pl_r']]);
    if ($count > 0)
      $batch .= ",";
    $batch .= "({$link['pl_from']}, '$ftitle', {$link['pl_to']}, '$ttitle', {$link['pl_r']}, '$rtitle')";
    $count += 1;
    if ($count >= 128) {
      $batches[] = $batch;
      $batch = "";
      $count = 0;
    }
  }
  if ($count > 0)
    $batches[] = $batch;

  foreach ($batches as $bat) {
    $sql = "INSERT INTO monthly_list_part
                        (article_id, article_title, dab_id, dab_title, redirect_id, redirect_title)
                 VALUES $bat";
    submit_query($tooldb, $sql);
    if ($is_error) return;
  }
  $mlt_result = mysqli_query($tooldb, "SELECT COUNT(*) FROM monthly_list_part");
  $mlt_row = mysqli_fetch_row($mlt_result);
  fputs($lp, "monthly_list_part contains ".$mlt_row[0]." rows.\n");

  submit_query($tooldb, "DROP TABLE IF EXISTS t_monthly_list_full");

  submit_query($tooldb,
    "CREATE TABLE t_monthly_list_full (
       article_id int unsigned NOT NULL default '0',
       article_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default '',
       redirect_id int unsigned NOT NULL default '0',
       redirect_title varchar(255) binary NOT NULL default '',
       template_id int unsigned NULL,
       template_title varchar(255) binary NULL,
       INDEX i_cmlf (dab_id)
     ) ENGINE=InnoDB AS
         SELECT a.article_id AS article_id,
                a.article_title AS article_title,
                a.dab_id AS dab_id,
                a.dab_title AS dab_title,
                a.redirect_id AS redirect_id,
                a.redirect_title AS redirect_title,
                b.template_id AS template_id,
                b.template_title AS template_title
           FROM monthly_list_part a LEFT OUTER JOIN mo_template_only_articles b
             ON a.article_id = b.article_id
            AND a.dab_id = b.dab_id"
  );

  submit_query($tooldb,
    "UPDATE t_monthly_list_full
        SET template_id = 0
      WHERE template_id IS NULL"
  );

  submit_query($tooldb,
    "UPDATE t_monthly_list_full
        SET template_title = ''
      WHERE template_title IS NULL"
  );

  $mlf_result = mysqli_query($tooldb, "SELECT COUNT(*) FROM t_monthly_list_full");
  $mlf_row = mysqli_fetch_row($mlf_result);
  fputs($lp, "t_monthly_list_full contains ".$mlf_row[0]." rows.\n");

  submit_query($tooldb, "DROP TABLE IF EXISTS monthly_list_part");

  submit_query($tooldb,
    "DELETE FROM t_monthly_list_full
           WHERE NOT EXISTS
                 (
                  SELECT 1
                    FROM mo_pl
                   WHERE article_id = pl_from
                     AND dab_id = pl_to
                 )
             AND NOT EXISTS
                 (
                  SELECT 1
                    FROM mo_template_only_articles o
                   WHERE t_monthly_list_full.article_id = o.article_id
                     AND t_monthly_list_full.dab_id = o.dab_id
                 )"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS monthly_list_full_unique");

  submit_query($tooldb,
    "CREATE TABLE monthly_list_full_unique (
       article_id int unsigned NOT NULL default '0',
       dab_id int unsigned NOT NULL default '0',
       dab_title varchar(255) binary NOT NULL default ''
     ) ENGINE=InnoDB AS
         SELECT article_id,
                dab_id,
                dab_title
           FROM t_monthly_list_full
       GROUP BY article_id, dab_id"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS t_monthly_list_count");

  submit_query($tooldb,
    "CREATE TABLE t_monthly_list_count (
       lc_id int unsigned NOT NULL default '0',
       lc_title varchar(255) binary NOT NULL default '',
       lc_amnt int unsigned NOT NULL default '0'
     ) ENGINE=InnoDB AS
         SELECT dab_id as lc_id,
                dab_title as lc_title,
                count(*) as lc_amnt
           FROM monthly_list_full_unique
       GROUP BY lc_id
       ORDER BY lc_amnt DESC, lc_title"
   );  /* removed overlong key       UNIQUE INDEX u_cmlc (lc_title) */

  submit_query($tooldb, "DROP TABLE IF EXISTS monthly_list_full_unique");

  if ($is_error) return;
  $duration = time() - $begin;
  fputs($fp,
    "$"."ch_proc_results['update_monthly_list'] = \"".convert_time($duration)."\";\n");

  $begin = time();

  # create the ch_fixed_links table

  submit_query($tooldb, "DROP TABLE IF EXISTS ch_fixed_links");

  submit_query($tooldb,
    "CREATE TABLE ch_fixed_links (
       article_id int unsigned NOT NULL default '0',
       article_rev_id int unsigned NOT NULL default '0',
       article_title varchar(255) binary NOT NULL default '',
       dab_id int unsigned NOT NULL default '0',
       UNIQUE INDEX (article_id, dab_id),
       INDEX (dab_id)
     ) ENGINE=InnoDB"
  );

  # load the mo_pl table

  $mo_pl = [];
  $ch_fixed_links = [];
  $sql = "SELECT pl_from, rev_id, pl_to FROM mo_pl";
  $result = submit_query($tooldb, $sql);
  if ($is_error) return;
  while ($row = mysqli_fetch_assoc($result))
    $mo_pl[] = $row;
  mysqli_free_result($result);
  mysqli_close($tooldb);

  # ch_fixed_links = all links in mo_pl that are _not_ in ch_pl where target is in ch_articles

  $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");
  foreach ($mo_pl as $mlink) {
    $key = $mlink['pl_from'].",".$mlink['pl_to'];
    if (isset($ch_pl[$key]))
      continue;
    if (! isset($ch_articles[$mlink['pl_to']]))
      continue;
    $tresult = submit_query($wikidb,
               "SELECT page_title FROM page WHERE page_id = {$mlink['pl_from']}");
    if ($is_error) return;
    if (mysqli_num_rows($tresult) != 1) {
      fputs($lp, "Failed to retrieve unique title for page_id = {$mlink['pl_from']}.\n");
      continue;
    }
    $trow = mysqli_fetch_assoc($tresult);
    $mlink['article_title'] = $trow['page_title'];
    $ch_fixed_links[$key] = $mlink;
  }

  mysqli_close($wikidb);
  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
  $batches = [];
  $batch = "";
  $count = 0;

  foreach ($ch_fixed_links as $flink) {
    if ($count > 0)
      $batch .= ",";
    $atitle = mysqli_real_escape_string($tooldb, $flink['article_title']);
    $batch .= "({$flink['pl_from']}, {$flink['rev_id']}, '$atitle', {$flink['pl_to']})";
    $count += 1;
    if ($count >= 128) {
      $batches[] = $batch;
      $batch = "";
      $count = 0;
    }
  }

  foreach ($batches as $bat) {
    $sql = "INSERT INTO ch_fixed_links
                        (article_id, article_rev_id, article_title, dab_id)
                 VALUES $bat";
    submit_query($tooldb, $sql);
    if ($is_error) return;
  }

  submit_query($tooldb, "DROP TABLE IF EXISTS ch_t_results");

  submit_query($tooldb,
    "CREATE TABLE ch_t_results (
       rev_id int unsigned NOT NULL default 0,
       user varchar(255) binary NOT NULL default '',
       article varchar(255) binary NOT NULL default '',
       dab varchar(255) binary NOT NULL default '',
       type varchar(35) binary NOT NULL default '',
       edited_article varchar(255) binary NOT NULL default '',
       INDEX (article),
       INDEX (dab),
       INDEX (type),
       INDEX (edited_article),
       UNIQUE INDEX (rev_id, dab) 
     ) ENGINE=InnoDB ROW_FORMAT=DYNAMIC"
  );

  submit_query($tooldb,
    "INSERT IGNORE INTO ch_t_results
            SELECT rev_id AS rev_id,
                   user AS user,
                   article AS article,
                   dab AS dab,
                   type AS type,
                   edited_article AS edited_article
              FROM ch_fixed_template_links, ch_results, mo_articles
             WHERE dab_id = mo_id
               AND article = article_title
               AND type = 'template'
               AND edited_article = CONCAT('Template:', template_title)
               AND dab = mo_title"
  );

  submit_query($tooldb, "DROP TABLE IF EXISTS ch_t_no_fixer");

  submit_query($tooldb,
    "CREATE TABLE ch_t_no_fixer (
       nf_article_id int unsigned NOT NULL default 0,
       nf_template_id int unsigned NOT NULL default 0,
       nf_dab_id int unsigned NOT NULL default 0,
       UNIQUE INDEX (nf_article_id, nf_dab_id)
     ) ENGINE=InnoDB"
  );

  submit_query($tooldb,
    "INSERT IGNORE INTO ch_t_no_fixer
            SELECT nf_article_id AS nf_article_id,
                   nf_template_id AS nf_template_id,
                   nf_dab_id AS nf_dab_id
              FROM ch_fixed_template_links, ch_no_fixer
             WHERE article_id = nf_article_id
               AND template_id = nf_template_id
               AND dab_id = nf_dab_id"
  );

  submit_query($tooldb,
    "DELETE FROM ch_fixed_template_links
           WHERE EXISTS
                 (
                  SELECT 1
                    FROM ch_t_results, mo_articles
                   WHERE dab_id = mo_id
                     AND article = article_title
                     AND type = 'template'
                     AND edited_article = CONCAT('Template:', template_title)
                     AND dab = mo_title
                 )"
  );

  submit_query($tooldb,
    "DELETE FROM ch_fixed_template_links
           WHERE EXISTS
                (
                 SELECT 1
                   FROM ch_t_no_fixer
                  WHERE article_id = nf_article_id
                    AND template_id = nf_template_id
                    AND dab_id = nf_dab_id
                )"
  );

  submit_query($tooldb,
    "INSERT IGNORE INTO ch_t_results
            SELECT rev_id AS rev_id,
                   user AS user,
                   article AS article,
                   dab AS dab,
                   type AS type,
                   edited_article AS edited_article
              FROM ch_fixed_links, ch_results, mo_articles
             WHERE dab_id = mo_id
               AND article = article_title
               AND type <> 'template'
               AND dab = mo_title"
  );

  submit_query($tooldb,
    "INSERT IGNORE INTO ch_t_no_fixer
            SELECT nf_article_id AS nf_article_id,
                   nf_template_id AS nf_template_id,
                   nf_dab_id AS nf_dab_id
              FROM ch_fixed_links, ch_no_fixer
             WHERE article_id = nf_article_id
               AND nf_template_id = 0
               AND dab_id = nf_dab_id"
  );

  submit_query($tooldb,
    "DELETE FROM ch_fixed_links
           WHERE EXISTS
                (
                 SELECT 1
                   FROM ch_t_results, mo_articles
                  WHERE dab_id = mo_id
                    AND article = article_title
                    AND type <> 'template'
                    AND dab = mo_title
                )"
  );

  submit_query($tooldb,
    "DELETE FROM ch_fixed_links
           WHERE EXISTS
                (
                 SELECT 1
                   FROM ch_t_no_fixer
                  WHERE article_id = nf_article_id
                    AND nf_template_id = 0
                    AND dab_id = nf_dab_id
                )"
  );
  mysqli_close($tooldb);
  if ($is_error) return;

  $duration = time() - $begin;
  fputs($fp,
    "$"."ch_proc_results['prep_for_fetch'] = \"".convert_time($duration)."\";\n");
}


function create_results_list ($sx, $fp, $lp) {
  global $logging_enabled, $is_error;

  if ($is_error)
    return;

  $begin = time();
  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
  $wikidb = get_db_con_local("enwiki_p", "enwiki.analytics.db.svc.wikimedia.cloud");

  # Start with the newly fixed dabs in ch_fixed_template_links
  $sql = "SELECT mo_id,
                 mo_title
            FROM ch_fixed_template_links, mo_articles
           WHERE dab_id = mo_id
           GROUP BY mo_id, mo_title
           ORDER BY mo_title";

  $result = submit_query($tooldb, $sql);

  while ($row = mysqli_fetch_assoc($result)) {

    $dab_id = $row['mo_id'];
    $dab_title = $row['mo_title'];
    $all_titles = "";

    if ($logging_enabled)
      fputs($lp, "\n\nBEGIN LOOP FOR: $dab_title ".udate()."\n");

    #
    # CREATE REDIRECT ARRAY
    #

    #
    # all_titles contains the title of the dab and its redirects.
    # This is the set of titles we will be looking for using
    # preg_match(). The escape_chars() method in common.php
    # adds upper and lowercase starting characters, and
    # and all possible <space> vs. <underscore> combinations
    # to the matching pattern.
    #
    $all_titles[] = $dab_title;

    $sql = "SELECT mo_r_title
              FROM mo_redirect_list
             WHERE mo_r_target = $dab_id";

    $res_redirs = mysqli_query($tooldb, $sql);

    while ($redir_row = mysqli_fetch_assoc($res_redirs))
      $all_titles[] = $redir_row['mo_r_title'];

      if ($logging_enabled)
        fputs($lp, "  Finished creating redirect array: ".udate()."\n");

    #if ($logging_enabled)
    #  fputs($lp, print_r($all_titles, true));

    #
    # AWARD POINTS FOR FIXED TEMPLATE-ONLY LINKS
    #

    #
    # Deal with fixed template links first. These are only
    # pages that have disambig links through a template and
    # nothing else, verified in mo_procs.
    #
    $sql = "SELECT article_id,
                   article_rev_id,
                   article_title,
                   template_id,
                   template_rev_id,
                   template_title
              FROM ch_fixed_template_links
             WHERE dab_id = $dab_id";

    $res_t_fixed = mysqli_query($tooldb, $sql);

    if ($logging_enabled)
      fputs($lp, "  Finished select from ch_fixed_template_links: ".udate()."\n");

    if ($res_t_fixed) {

      while ($fixed_t_row = mysqli_fetch_assoc($res_t_fixed)) {

        #
        # $a_id = the id of the article that linked to the template
        # $a_rev_id = the rev_id of the article when the monthly scan was run
        # $a_title = the title of the article
        # $t_id = the id of the template
        # $t_rev_id = the rev_id of the template when the monthly scan was run
        # $t_title = the title of the template
        #
        $a_id = $fixed_t_row['article_id'];
        $a_rev_id = $fixed_t_row['article_rev_id'];
        $a_title = $fixed_t_row['article_title'];
        $t_id = $fixed_t_row['template_id'];
        $t_rev_id = $fixed_t_row['template_rev_id'];
        $t_title = $fixed_t_row['template_title'];
        $is_connected = true;
        $all_templates = "";

        if ($logging_enabled)
          fputs($lp, "\n  START article: $a_title\n");

        # See if it's still connected
        $ttitle = mysqli_real_escape_string($wikidb, $t_title);
        $sql = "SELECT 1
                  FROM templatelinks, linktarget
                 WHERE tl_from = $a_id
                   AND tl_target_id = lt_id 
                   AND lt_namespace=10
		  AND lt_title = '$ttitle'
               ";

        $res_is_conn = submit_query($wikidb, $sql);

        if ($logging_enabled)
          fputs($lp, "  Finished templatelinks check: ".udate()."\n");

        if ($res_is_conn) {
          if(mysqli_num_rows($res_is_conn) == 0)
            $is_connected = false;
        }
        mysqli_free_result($res_is_conn);

        # Award a point to whomever fixed the template
        if ($is_connected) {

          #
          # Look for cached value in ch_t_results. We do this because a single template fix
          # can be responsible for many article fixes. Instead of putting this method through
          # find_fixer for each and every article, see if we've already found a fixer and save
          # ourselves the trouble of repeating the work.
          #
          $sql = "SELECT rev_id,
                         user,
                         edited_article
                    FROM ch_t_results
                   WHERE edited_article = 'Template:"
                                          .mysqli_real_escape_string($tooldb, $t_title)."'
                     AND dab = '".mysqli_real_escape_string($tooldb, $dab_title)."'
                   LIMIT 1";

          $res_t_cache = submit_query($tooldb, $sql);

          if ($logging_enabled)
            fputs($lp, "  Finished looking for cached template edit: ".udate()."\n");

          if($res_t_cache && mysqli_num_rows($res_t_cache) > 0) {

            $t_cache_row = mysqli_fetch_assoc($res_t_cache);

            $fix_rev_id = $t_cache_row['rev_id'];
            $esc_user = mysqli_real_escape_string($tooldb, $t_cache_row['user']);
            $esc_article = mysqli_real_escape_string($tooldb, $a_title);
            $esc_dab_title = mysqli_real_escape_string($tooldb, $dab_title);
            $esc_extra = mysqli_real_escape_string($tooldb, $t_cache_row['edited_article']);

            if ($logging_enabled)
              fputs($lp, "  TEMPLATE CACHED: $esc_article $esc_dab_title $esc_extra $esc_user $fix_rev_id\n");

            # Even though we found a result in ch_t_results, that was for a different article that was fixed by the
            # template edit. Therefore we store this as a new row in ch_t_results - same template fix, different article.
            store_results($tooldb, $lp, $fix_rev_id, $esc_user, $esc_article, $esc_dab_title, "template", $esc_extra);
          }
          else
            find_fixer($tooldb, $sx, $lp, $dab_title, $a_title, $all_titles, $t_rev_id, "template", "Template:".$t_title, true);
        }

        # Award a point to whomever removed the template.
        else {
          if ($logging_enabled)
            fputs($lp, "NOT CONNECTED: $a_title $t_title $dab_title\n\n");

          $all_templates[] = $t_title;

          $sql = "SELECT page_title
                    FROM redirect, page
                   WHERE rd_namespace = 10
                     AND rd_title = '".mysqli_real_escape_string($wikidb, $t_title)."'
                     AND rd_from = page_id";

          $res_t_redirs = submit_query($wikidb, $sql);

          if ($res_t_redirs) {
            while ($t_redir_row = mysqli_fetch_assoc($res_t_redirs)) {
              $all_templates[] = $t_redir_row['page_title'];
            }
          }

          if ($logging_enabled)
            fputs($lp, "  Finished creating all_templates array: ".udate()."\n");

          if ($logging_enabled)
            fputs($lp, print_r($all_templates, true));

          find_fixer($tooldb, $sx, $lp, $dab_title, $a_title, $all_templates, $a_rev_id, "template", $a_title, false, "\{\{", "(\||\}\})");
        }
      }
      mysqli_free_result($res_t_fixed);
    }
  } // Finish ch_fixed_template_links dab list loop

  # Now do the newly fixed dabs in ch_fixed_links
  $sql = "SELECT mo_id,
                 mo_title
            FROM ch_fixed_links, mo_articles
           WHERE dab_id = mo_id
           GROUP BY mo_id, mo_title
           ORDER BY mo_title";

  $result = mysqli_query($tooldb, $sql);

  while ($row = mysqli_fetch_assoc($result)) {

    $dab_id = $row['mo_id'];
    $dab_title = $row['mo_title'];
    $all_titles = "";

    if ($logging_enabled)
      fputs($lp, "\n\nBEGIN LOOP FOR: $dab_title ".udate()."\n");

    #
    # CREATE REDIRECT ARRAY
    #

    #
    # all_titles contains the title of the dab and its redirects.
    # This is the set of titles we will be looking for using
    # preg_match(). The escape_chars() method in common.php
    # adds upper and lowercase starting characters, and
    # and all possible <space> vs. <underscore> combinations
    # to the matching pattern.
    #
    $all_titles = array($dab_title);

    $sql = "SELECT mo_r_title
              FROM mo_redirect_list
             WHERE mo_r_target = $dab_id";

    $res_redirs = mysqli_query($tooldb, $sql);

    while ($redir_row = mysqli_fetch_assoc($res_redirs))
      array_push($all_titles, $redir_row['mo_r_title']);

      if ($logging_enabled)
        fputs($lp, "  Finished creating redirect array: ".udate()."\n");

    #if ($logging_enabled)
    #  fputs($lp, print_r($all_titles, true));

    #
    # AWARD POINTS FOR FIXED ARTICLE-TO-DAB LINKS
    #
    $sql = "SELECT article_id,
                   article_rev_id,
                   article_title
              FROM ch_fixed_links
             WHERE dab_id = $dab_id";

    $res_fixed = mysqli_query($tooldb, $sql);

    if ($logging_enabled)
      fputs($lp, "  Finished select from ch_fixed_links: ".udate()."\n");

    while ($fixed_row = mysqli_fetch_assoc($res_fixed)) {

      #
      # $id = the id of the article that linked to the dab
      # $rv_start_id = the rev_id of the article when the monthly scan was run
      # $title = the title of the article that linked to the dab
      #
      $id = $fixed_row['article_id'];
      $rv_start_id = $fixed_row['article_rev_id'];
      $title = $fixed_row['article_title'];
      $content = "";

      if ($logging_enabled)
        fputs($lp, "\n  START article: $title\n");

      $content = find_fixer($tooldb, $sx, $lp, $dab_title, $title, $all_titles, $rv_start_id, "article", $title, true);

      #
      # ARTICLE STILL CONTAINS A DAB TITLE, BUT IS MARKED AS FIXED
      # AWARD POINTS IF A REDIRECT HAS BEEN CHANGED
      #
      if ($content != "") {

        if ($logging_enabled)
          fputs($lp, "  Begin redirect check: ".udate()."\n");

        #
        # The last revision still contained a "link" to the dab. Perhaps
        # a redirect has been changed to point to a different article.
        #
        $redir = "";

        # Search for redirect as normal wikilink
        foreach ($all_titles as $t) {
          $safe_title = escape_chars($t);
          $pattern = "/\[\[\s*{$safe_title}\s*[\||\]\]|#]/";
          if (preg_match($pattern, $content))
            $redir = $t;
        }

        # If that failed, search for redirect within template
        if (!$redir == "") {
          $start = "(\{\{[^\[]*\|)";
          $end = "(\}\}|\|[^\]]*\}\}|#[^\]]*\}\}|\{\{!\}\}[^\]]*\}\})";
          foreach ($all_titles as $t) {
            $safe_title = escape_chars($t);
            $pattern = "/{$start}\s*{$safe_title}\s*{$end}/";
            if (preg_match($pattern, $content))
              $redir = $t;
          }
        }

        # If we found something and we're reasonably sure it is a redirect (by not being the dab title itself capitalized in some strange way)
        if ($redir != "" && $redir != $dab_title && $redir != str_replace("_", " ", $dab_title)
                        && $redir != strtolower(substr($dab_title,0,1)).substr($dab_title,1)
                        && $redir != strtolower(substr(str_replace("_", " ", $dab_title),0,1)).substr(str_replace("_", " ", $dab_title),1) ) {

          $redir = ucfirst($redir);

          # See if it's still a redirect
          $sql = "SELECT page_is_redirect
                    FROM page
                   WHERE page_namespace = 0
                     AND page_title = '".str_replace(" ", "_", mysqli_real_escape_string($tooldb, $redir))."'";

          $is_redirect = 1;
          $res_is_redir = mysqli_query($wikidb, $sql);

          if($res_is_redir) {
            $is_redir_row = mysqli_fetch_assoc($res_is_redir);
            $is_redirect = $is_redir_row['page_is_redirect'];
          }

          $sql = "SELECT rev_id
                    FROM mo_redirect_list
                   WHERE mo_r_title = '".str_replace(" ", "_", mysqli_real_escape_string($tooldb, $redir))."'";

          $res_revid = mysqli_query($tooldb, $sql);

          if($res_revid) {

            #
            # $redir_rev_id = the rev_id of the redirect when the monthly scan was performed
            #
            $revid_row = mysqli_fetch_assoc($res_revid);
            $redir_rev_id = $revid_row['rev_id'];
            $esc_redir = str_replace(" ", "_", $redir);

            # If no longer a redirect, see who changed it into an article
            if ($is_redirect == 0) {
              $redir_commands[] = "#REDIRECT";
              $redir_commands[] = "#redirect";
              $redir_commands[] = "#Redirect";
              $redir_commands[] = "# REDIRECT";
              $redir_commands[] = "# redirect";
              $redir_commands[] = "# Redirect";
              find_fixer($tooldb, $sx, $lp, $dab_title, $title, $redir_commands, $redir_rev_id, "redirect", $esc_redir, false);
            }
            # If still a redirect, see who changed its target
            else
              find_fixer($tooldb, $sx, $lp, $dab_title, $title, $all_titles, $redir_rev_id, "redirect", $esc_redir, false);
          }
        }
      }
    }
  } // Finish ch_fixed_links dab list loop
  mysqli_close($tooldb);
  mysqli_close($wikidb);
  fputs($fp, "$"."ch_proc_results['create_results_list'] = \"".convert_time(time() - $begin)."\";\n");
}


function find_fixer($tooldb, $sx, $lp, $dab_title, $article, $all_titles, $start_rev_id, $type, $extra, $do_second_search, $start = "(\[\[)", $end = "(\||\]\]|#)") {
  global $logging_enabled;

  if ($logging_enabled)
    fputs($lp, "    Entered find_fixer method: ".udate()."\n");

  $fix_rev_id = $fix_user = $content = "";
  $query_continue = true;
  $rvcontinue = false;
  $counter = 0;
  $initial_match = true;

  if ($logging_enabled)
    fputs($lp, "    Find_fixer: working on ".$extra." dab title: ".$dab_title."\n");

  while ($query_continue) {

    $request_vars = array(
            'action' => 'query',
            'prop' => 'revisions',
            'titles' => $extra,
            'rvprop' => 'ids|user|content',
            'rvdir' => 'newer',
            'rvstartid' => $start_rev_id,
            'format' => 'php',
            'rawcontinue' => ''
    );
    if ($rvcontinue)
      $request_vars['rvcontinue'] = $rvcontinue;

    if ($logging_enabled)
      fputs($lp, print_r($request_vars, true));

    if ($logging_enabled)
      fputs($lp, "    Before SxWiki submit: ".udate()."\n");

    if(!$array = $sx->callAPI($request_vars)) {
/*
 * Following is a clumsy hack to get around the fact that prop=revisions now
 * requires some valid rev_id in the rvstartid field. A better solution
 * would be to rewrite all the database queries that store revids to store
 * timestamps instead, and then rewrite this function to use the timestamp.
 */
        $start_rev_id = $start_rev_id - 1;
        continue;
/*      die("SxWiki error.");   */
    }

    if ($logging_enabled)
      fputs($lp, "    After SxWiki submit: ".udate()."\n");

    if(isset($array['query-continue'])) {
      $query_continue = true;
      $rvcontinue = $array['query-continue']['revisions']['rvcontinue'];

      if ($logging_enabled)
        fputs($lp, "    Query-continue: $extra  $dab_title  rvcontinue = $start_rev_id\n");
    }
    else {
      $query_continue = false;

      if ($logging_enabled)
        fputs($lp, "    Query-continue finished: $extra  $dab_title\n");
    }

    $page_id = "";

    if(isset($array['query']['pages']))
      $page_id = key($array['query']['pages']);

    if(isset($array['query']['pages'][$page_id]['revisions'])) {

      $revisions = $array['query']['pages'][$page_id]['revisions'];

      foreach($revisions as $rev) {

       if (isset($rev['*'])) {
        $content = $rev['*'];
        $is_match = false;

        foreach($all_titles as $t) {
          $safe_title = escape_chars($t);
          $pattern = "/{$start}\s*{$safe_title}\s*{$end}/";
          if(preg_match($pattern, $content))
            $is_match = true;
        }

        if (!$is_match && $do_second_search) {
          $s_start = "(\{\{[^\[]*\|)";
          $s_end = "(\}\}|\|[^\]]*\}\}|#[^\]]*\}\}|\{\{!\}\}[^\]]*\}\})";
          foreach($all_titles as $t) {
            $safe_title = escape_chars($t);
            $pattern = "/{$s_start}\s*{$safe_title}\s*{$s_end}/";
            if(preg_match($pattern, $content))
              $is_match = true;
          }
        }

        if ($is_match) {
          $fix_user = "";
          $fix_rev_id = "";
        }
        else {
          if($counter == 0)
            $initial_match = false;
          if ($fix_user == "") {
            $fix_user = $rev['user'];
            $fix_rev_id = $rev['revid'];
          }
        }
        $counter++;
      }
     }
    }
  }

  if ($fix_user != "" && $initial_match) {
    $esc_user = mysqli_real_escape_string($tooldb, $fix_user);
    $esc_article = mysqli_real_escape_string($tooldb, $article);
    $esc_dab_title = mysqli_real_escape_string($tooldb, $dab_title);
    $esc_extra = mysqli_real_escape_string($tooldb, $extra);
    store_results($tooldb, $lp, $fix_rev_id, $esc_user, $esc_article, $esc_dab_title, $type, $esc_extra);
    return "";
  }
  else if (!$initial_match)
    return "";
  else
    return $content;
}


function store_results($tooldb, $lp, $revid, $user, $article, $dab, $type, $edited) {

  global $is_store_error, $logging_enabled;

  if ($logging_enabled)
    fputs($lp, "  STORE RESULTS: User:$user  Article:$article  Dab:$dab  Type:$type  Edited:$edited\n");

  $sql = "INSERT IGNORE INTO ch_t_results (rev_id, user, article, dab, type, edited_article)
               VALUES ($revid, '$user', '$article', '$dab', '$type', '$edited')";

  $result = mysqli_query($tooldb, $sql);

  if ($result == false && !$is_store_error) {
    log_error(date("F j G:i", time()), "ch_procs.php", $sql, mysqli_error($tooldb));
    $is_store_error = true;
  }
}


function ch_finalize_results ( ) {
  global $fp, $is_error;
  $begin = time();
  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  submit_query($tooldb,
    "INSERT IGNORE INTO ch_t_no_fixer
            SELECT article_id AS nf_article_id,
                   template_id AS nf_template_id,
                   dab_id AS nf_dab_id
              FROM ch_fixed_template_links, mo_articles
             WHERE dab_id = mo_id
               AND NOT EXISTS
                   (
                   SELECT 1
                     FROM ch_t_results
                    WHERE article = article_title
                      AND type = 'template'
                      AND edited_article = CONCAT('Template:', template_title)
                      AND dab = mo_title
                   )"
  );

  submit_query($tooldb,
    "INSERT IGNORE INTO ch_t_no_fixer
            SELECT article_id AS nf_article_id,
                   0 AS nf_template_id,
                   dab_id AS nf_dab_id
              FROM ch_fixed_links, mo_articles
             WHERE dab_id = mo_id
               AND NOT EXISTS
                   (
                   SELECT 1
                     FROM ch_t_results
                    WHERE article = article_title
                      AND type <> 'template'
                      AND dab = mo_title
                   )"
  );

  if ($is_error) return;

  submit_query($tooldb, "DROP TABLE IF EXISTS monthly_list_full");
  submit_query($tooldb, "RENAME TABLE t_monthly_list_full TO monthly_list_full");

  submit_query($tooldb, "DROP TABLE IF EXISTS monthly_list_count");
  submit_query($tooldb, "RENAME TABLE t_monthly_list_count TO monthly_list_count");

  submit_query($tooldb, "DROP TABLE IF EXISTS ch_results");
  submit_query($tooldb, "RENAME TABLE ch_t_results TO ch_results");

  submit_query($tooldb, "DROP TABLE IF EXISTS ch_no_fixer");
  submit_query($tooldb, "RENAME TABLE ch_t_no_fixer TO ch_no_fixer");

  mysqli_close($tooldb);
  if ($is_error) return;
  fputs($fp, "$"."ch_proc_results['finalize_results'] = \"".convert_time(time() - $begin)."\";\n");
}


function ch_cleanup ( ) {
  $tooldb = get_db_con_local("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

  mysqli_query($tooldb, "DROP TABLE IF EXISTS ch_t_no_fixer");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS ch_t_results");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS ch_pl_unique");

  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_monthly_list_count");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS monthly_list_full_unique");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS t_monthly_list_full");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS monthly_list_part");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS mo_articles_update");

  mysqli_query($tooldb, "DROP TABLE IF EXISTS ch_pl");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS ch_link_candidates");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS ch_redirects");
  mysqli_query($tooldb, "DROP TABLE IF EXISTS ch_articles");

  mysqli_close($tooldb);
}

function udate() {

  $utimestamp = microtime(true);
  $timestamp = floor($utimestamp);
  $milliseconds = round(($utimestamp - $timestamp) * 1000);

  return date(preg_replace('`(?<!\\\\)u`', $milliseconds, "H:i:s.u"), $timestamp);
}


function submit_query ($server, $sql) {

  global $fp, $is_error;

  if ($is_error) {
    return;
  }

  $result = mysqli_query($server, $sql);

  if ($result == false) {
    log_error(date("F j G:i", time()), "ch_procs.php", $sql, mysqli_error($server));
    fputs($fp, "\n$"."ch_error = \"$sql: ".mysqli_error($server)."\";\n");
    fclose($fp);
    $tooldb = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");
    ch_cleanup();
    $is_error = true;
  }

  return $result;

}


function submit_and_log_query ($server, $sql, $nick = "") {

  global $fp, $is_error;

  if ($is_error) {
    return;
  }

  if ($nick == "")
    $nick = $sql;

  $begin = time();

  $result = submit_query($server, $sql);

  if (! $is_error) {
    $duration = time() - $begin;
    fputs($fp,
      "$"."ch_proc_results['$nick'] = \"".convert_time($duration)."\";\n");
  }

  return $result;

}


function get_db_con_local($schema, $server) {
  global $fp;

  $conn = get_db_con($schema, $server);
  if (!$conn) {
    log_error(date("F j G:i", time()), "ch_procs.php", "$server connect", mysqli_connect_error());
    fputs($fp, "\n$"."ch_error = \"".mysqli_connect_error()."\";\n\n?>\n");
    fclose($fp);
    die(1);
  }
  return $conn;
}

?>