<?php

putenv('TZ=UTC');

include_once ("/data/project/dplbot"."/dplbot/scripts/common.php");

$page_name = "short_leaderboard.php";

$now = time();

$fp = fopen("$HOME_DIR/data/sl.txt", "w");

$mysql = get_db_con("s51290__dpl_p", "tools.db.svc.wikimedia.cloud");

fputs($fp, "<p>This list was last updated ".str_replace( '_', ' ', date('F j, G:i e', $now)).".</p>\n\n");

$sql = "
             SELECT user,
                    count(*) AS count
               FROM ch_results
              GROUP BY user
             HAVING count >= 10
              ORDER BY count DESC, user
              LIMIT 20
        ";

if ($mysql) {

  $res = mysqli_query($mysql, $sql);

  if ($res) {

    $num = mysqli_num_rows($res);

    if ($num > 0) {
      printResultsLocal($res, $fp);
    }
    else fputs($fp, "There are no results in this query.\n\n");

  }
  else {
    log_error(date("F j G:i", time()), $page_name, $sql, mysqli_error($mysql));
    fputs($fp, "Database error: ".mysqli_error($mysql)."</p>\n\n");
  }

  mysqli_close($mysql);
}
else {
  log_error(date("F j G:i", time()), $page_name, "mysql connect", mysqli_connect_error());
  fputs($fp, "Database connection error: ".mysqli_connect_error()."\n\n");
}

fclose($fp);


function printResultsLocal($res, $fp) {

  fputs($fp, "{| border=\"0\" style=\"background:transparent;\"\n");
  $position = 1;
  while ($row = mysqli_fetch_assoc($res)) {
    $user = $row['user'];
    $count = $row['count'];
    fputs($fp, "|-\n|".($position < 10 ? "&nbsp;" : "")."$position. [[User:$user|$user]]\n|&nbsp;&nbsp;&nbsp;\n|align=\"right\"|[https://dplbot.toolforge.org/ch/user_results.php?user=".encodeTitle($user)." $count fixed]\n");
    $position++;
  }
  fputs($fp, "|}\n");
}

?>